<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SparePartsPictures extends Model
{
    protected $table = 'spare_parts_pictures';
    protected $fillable = [
        'spare_parts_id', 'path',
    ];
}
