<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    protected $guarded = [];

    public function workshop()
    {
        return $this->belongsToMany(Workshop::class, 'service_workshops');
    }

    public function pictures()
    {
        return $this->hasMany(ServicesPictures::class);
    }

    public function prices()
    {
        return $this->hasMany(ServicesLabours::class);
    }
}
