<?php

namespace App\Http\Middleware;

use App\Providers\RouteServiceProvider;
use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        $panels = [
            "admin" => "admin panel",
            "workshop-user" => "workshop panel"
        ];
        if (!$request->expectsJson()) {
            if (Auth::guard('admin')->check() || Auth::guard('workshop-user')->check()) {
                if (!Auth::guard('workshop-user')->check()) {
                    if(Auth::guard('admin')->check()){
                        return redirect()->back();
                    }
                    $request->session()->flash('message', "Please log out of this account before trying to log in to the {$panels[$guard]}.");
                    $request->session()->flash('alert-class', 'alert alert-danger');
                }
                if (!Auth::guard('admin')->check()) {
                    if(Auth::guard('workshop-user')->check()){
                        return redirect()->back();
                    }
                    $request->session()->flash('message', "Please log out of this account before trying to log in to the {$panels[$guard]}.");
                    $request->session()->flash('alert-class', 'alert alert-danger');
                }
                if (Auth::guard('admin')->check()) {
                    return redirect(RouteServiceProvider::DASHBOARD);
                } elseif (Auth::guard('workshop-user')->check()) {
                    return redirect(RouteServiceProvider::WORKSHOP_DASHBOARD);
                } else {
                    return redirect("/");
                }
            }
        }
        // if (!$request->expectsJson()) {
        //     if ($guard == 'admin' && Auth::guard($guard)->check()) {
        //         return redirect(RouteServiceProvider::ADMIN_DASHBOARD);
        //     }
        // }
        return $next($request);
    }
}
