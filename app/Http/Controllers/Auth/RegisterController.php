<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use App\Workshop;
use App\WorkshopsPictures;
use Illuminate\Auth\Events\Registered;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
     */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $validator = Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'username' => ['required', 'string', 'max:255', 'unique:users'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'phone' => ['required', 'string', 'max:255'],
        ]);
        return $validator;
    }

    public function register(Request $request)
    {
        $validator = $this->validator($request->all());
        if ($validator->fails()) {
            return redirect()->route('login', ['#tab-2'])->withErrors(['error' => $validator->errors()->all()]);
        }
        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));

        $this->guard()->login($user);

        if ($response = $this->registered($request, $user)) {
            return $response;
        }

        return $request->wantsJson()
        ? new JsonResponse([], 201)
        : redirect($this->redirectPath());
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'username' => $data['username'],
            'phone' => $data['phone'],
        ]);
    }

    public function workshopRegister(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:workshops'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'phone' => ['required', 'string', 'max:255'],
            'location' => ['required', 'string'],
            'address' => ['required', 'string'],
            'no_of_mechanics' => ['required', 'integer'],
            'no_of_electrician' => ['required', 'integer'],
            'no_of_denter' => ['required', 'integer'],
            'no_of_painter' => ['required', 'integer'],
            'no_of_bays' => ['required', 'integer'],
            'major_tools_and_equipments' => ['required', 'string'],
            'image' => ['required', 'array'],
            // 'service_id' => ['required', 'array'],
            // 'service_id.*' => ['integer']
        ]);
        if ($validator->fails()) {
            return redirect()->route('login', ['#tab-3'])->withErrors(['error' => $validator->errors()->all()]);
        }

        $workshop = Workshop::create([
            'name' => $request->name,
            'slug' => Str::slug($request->name, '-'),
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'phone' => $request->phone,
            'location' => $request->phone,
            'address' => $request->address,
            'description' => $request->description,
            'no_of_mechanics' => $request->no_of_mechanics,
            'no_of_electrician' => $request->no_of_electrician,
            'no_of_denter' => $request->no_of_denter,
            'no_of_painter' => $request->no_of_painter,
            'no_of_bays' => $request->no_of_bays,
            'major_tools_and_equipments' => $request->major_tools_and_equipments,

        ]);
        $id = $workshop->id;
        $images = $request->image;

        foreach ($images as $image) {
            $filename = Str::random(15) . '.' . $image->extension();
            Storage::putFileAs("public", $image, $filename);
            WorkshopsPictures::create(['workshop_id' => $id, 'path' => $filename]);
        }

        $request->session()->flash('message', 'Workshop approval send to admin');
        $request->session()->flash('alert-class', 'alert alert-success');
        return redirect()->back();
    }
}
