<?php

namespace App\Http\Controllers\Mechanic;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;

class BackendController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function index()
    {
        return view('mechanic.pages.home');
    }

    public function user()
    {
        $users = User::latest()->paginate(10);;
        return view('mechanic.users.index', compact('users'));
    }
}
