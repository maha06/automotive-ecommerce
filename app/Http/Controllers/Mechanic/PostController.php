<?php

namespace App\Http\Controllers\Mechanic;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Post;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $posts = Post::where('mechanic_id', Auth::user()->id)->latest()->paginate(10);
        return view('mechanic.posts.index')->with(compact(['posts']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('mechanic.posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'name' => Rule::unique('posts', 'name')->where(function ($query) {
                return $query->where('mechanic_id', request("mechanic_id"));
            }),
            'name' => ['required', 'string', 'max:255'],
            'mechanic_id' => ['required', 'integer'],
            'company' => ['required', 'string', 'max:255'],
            'description' => ['required', 'string'],
            'price' => ['required', 'integer'],
            'image.*' => ['required'],
        ]);

        $imagesData = [];
        foreach ($request->image as $key => $image) {
            $filename = Str::random(15) . '.' . $image->extension();
            Storage::putFileAs("public", $image, $filename);
            $imagesData[] = $filename;
        }

        $data['image'] = implode(',', $imagesData);
        $data['slug'] = Str::slug($data['name'], '-');
        Post::Create($data);
        return redirect()->route('posts.index')->with('success', 'Post created successfully.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Post $post)
    {
        return view('mechanic.posts.edit')->with(compact(['post']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, Post $post)
    {
        $data = $request->validate([
            'name' => Rule::unique('posts', 'name')->where(function ($query) {
                return $query->where('mechanic_id', request("mechanic_id"));
            }),
            'name' => ['required', 'string', 'max:255'],
            'mechanic_id' => ['required', 'integer'],
            'company' => ['required', 'string', 'max:255'],
            'description' => ['required', 'string'],
            'price' => ['required', 'integer'],
            'image.*' => ['required'],
        ]);
        $images = [];
        if (is_array($request->image)) {
            if ($post->image) {
                $images[] = $post->image;
            }
            foreach ($request->image as $image) {
                $filename = Str::random(15) . '.' . $image->getClientOriginalExtension();
                Storage::putFileAs('public', $image, $filename);
                $images[] = $filename;
            }
            $data['image'] = implode(',', $images);
        }
        $data['slug'] = Str::slug($data['name'], '-');
        $post->Update($data);
        return redirect()->route('posts.index')->with('success', 'Post updated successfully.');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        foreach ($post->image as $key => $image) {
            Storage::delete('public/' . $image);
        }
        $post->delete();
        return redirect()->back();
    }

    public function delete(Request $request, Post $post, $image)
    {
        $images = [];
        $remainingImages = [];
        $images = explode(',', $post->image);

        foreach ($images as $key => $postImage) {
            if ($postImage == $image) {
                Storage::delete('public/' . $image);
            } else {
                $remainingImages[] = $postImage;
            }
        }
        $remainingImages = implode(',', $remainingImages);
        $post->update([
            'image' => $remainingImages
        ]);

        $request->session()->flash('message', 'Delete Image successfully');
        $request->session()->flash('alert-class', 'alert alert-success');
        return redirect()->back();
    }
}
