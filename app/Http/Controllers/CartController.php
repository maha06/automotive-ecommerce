<?php

namespace App\Http\Controllers;

use App\Cart;
use App\Checkout;
use App\CheckoutDetails;
use App\Http\Traits\SmsTrait;
use App\Oil;
use App\SpareParts;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CartController extends Controller
{
    use SmsTrait;

    public function index()
    {
        $carts = Cart::all();
        return view('pages.carts', compact('carts'));
    }
    public function addSparePart(Request $request)
    {
        $check = Cart::where('post_id', $request->post_id)->first();
        if (!$check) {
            $cart = Cart::create([
                'post_id' => $request->post_id,
                'price' => $request->price,
                'user_id' => Auth::user()->id,
            ]);
            return response()->json($cart);
        }
        $cart = 0;
        return response()->json($cart);
    }

    public function changeUpCartPrice(Cart $cart)
    {
        $quantity = $cart->quantity + 1;
        if ($cart->post_id) {
            $part = SpareParts::find($cart->post_id);
            if ($part->quantity >= $quantity) {
                $cart->update([
                    'quantity' => $quantity,
                    'price' => $cart->post->price * $quantity,
                ]);
            } else {
                return response()->json(false);
            }

        } else {
            $oil = Oil::find($cart->oil_id);
            if ($oil->quantity >= $quantity) {
                $cart->update([
                    'quantity' => $quantity,
                    'price' => $cart->oil->price * $quantity,
                ]);
            } else {
                return response()->json(false);
            }
        }
        $carts = Auth::user()->carts;
        return response()->json(['cart' => $cart, 'carts' => $carts]);
    }

    public function changeDownCartPrice(Cart $cart)
    {
        $quantity = $cart->quantity - 1;

        if ($cart->post_id) {
            $cart->update([
                'quantity' => $quantity,
                'price' => $cart->post->price * $quantity,
            ]);
        } else {
            $cart->update([
                'quantity' => $quantity,
                'price' => $cart->oil->price * $quantity,
            ]);
        }
        $carts = Auth::user()->carts;
        return response()->json(['cart' => $cart, 'carts' => $carts]);
    }

    public function removeCart(Cart $cart)
    {
        $cart->delete();
        return response()->json(true);
    }

    public function cartCheckout()
    {
        $carts = Auth::user()->carts;
        $user = Auth::user();
        return view('pages.checkout', compact('carts', 'user'));
    }

    public function placeOrder(Request $request)
    {

        $carts = Auth::user()->carts;

        $data = $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'max:255'],
            'address' => ['required', 'string'],
            'apartment' => ['nullable', 'string', 'max:255'],
            'phone' => ['required', 'string', 'max:255'],
            'order_notes' => ['nullable', 'string'],
            'city' => ['string', 'required', 'max:255'],
        ]);

        $checkout = Checkout::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'address' => $data['address'],
            'apartment' => $data['apartment'],
            'phone' => $data['phone'],
            'user_id' => $request->user_id,
            'order_notes' => $data['order_notes'],
            'city' => $data['city'],
            'price' => $carts->sum('price'),

        ]);

        foreach ($carts as $cart) {
            if ($cart->post_id) {
                CheckoutDetails::create([
                    'checkout_id' => $checkout->id,
                    'part_id' => $cart->post->id,
                    'oil_id' => 0,
                    'quantity' => $cart->quantity,

                ]);
                $part = SpareParts::find($cart->post->id);
                $quantity = (int) $part->quantity - (int) $cart->quantity;
                $part->quantity = $quantity;
                $part->save();

            } else {
                CheckoutDetails::create([
                    'checkout_id' => $checkout->id,
                    'oil_id' => $cart->oil->id,
                    'quantity' => $cart->quantity,
                    'part_id' => 0,
                ]);
                $oil = Oil::find($cart->oil->id);
                $quantity = (int) $oil->quantity - (int) $cart->quantity;
                $oil->quantity = $quantity;
                $oil->save();
            }

            $cart->delete();
        }
        $this->sendOrderSMS($data['phone']);
        $request->session()->flash('message', 'Booking successfully');
        $request->session()->flash('alert-class', 'alert alert-success');
        return redirect()->route('pages.home');
    }

    public function getCart()
    {
        $carts = Cart::where('user_id', Auth::user()->id)->count();
        return response()->json($carts);
    }
}
