<?php

namespace App\Http\Controllers\Workshop;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Review;
use Illuminate\Support\Facades\Auth;

class ReviewController extends Controller
{
  /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $reviews = Review::where('workshop_id', Auth::user()->id)->latest()->paginate(12);
        return view('workshop.reviews.index', compact('reviews'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Review $review)
    {
        $review->delete();
        $request->session()->flash('message', 'Delete Review successfully');
        $request->session()->flash('alert-class', 'alert alert-success');
        return redirect()->route('review.index');
    }
}
