<?php

namespace App\Http\Controllers\Workshop;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Service;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;

class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $services = Service::where('workshop_id', Auth::user()->id)->latest()->paginate(10);
        return view('workshop.services.index')->with(compact(['services']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('workshop.services.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'name' => Rule::unique('services', 'name')->where(function ($query) {
                return $query->where('workshop_id', request("workshop_id"));
            }),
            'name' => ['required', 'string', 'max:255'],
            'workshop_id' => ['required', 'integer'],
            'price' => ['required', 'integer'],
            'image' => ['required', 'image'],
        ]);

        $image = $request->image;
        $filename = Str::random(15) . '.' . $image->extension();
        Storage::putFileAs("public", $image, $filename);
        $data['image'] = $filename;

        $data['slug'] = Str::slug($data['name'], '-');
        Service::Create($data);
        return redirect()->route('services.index')->with('success', 'Service created successfully.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Service $service)
    {
        return view('workshop.services.edit')->with(compact(['service']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, Service $service)
    {
        $data = $request->validate([
            'name' => Rule::unique('services', 'name')->where(function ($query) {
                return $query->where('workshop_id', request("workshop_id"));
            }),
            'name' => ['required', 'string', 'max:255'],
            'workshop_id' => ['required', 'integer'],
            'price' => ['required', 'integer'],
            'image' => ['required', 'image'],
        ]);

        if ($request->image) {
            $image = $request->image;
            $filename = Str::random(15) . '.' . $image->extension();
            Storage::putFileAs("public", $image, $filename);
            $data['image'] = $filename;
        }

        $data['slug'] = Str::slug($data['name'], '-');
        $service->Update($data);
        return redirect()->route('services.index')->with('success', 'Service updated successfully.');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Service $service)
    {

        Storage::delete('public/' . $service->image);
        $service->delete();
        return redirect()->back();
    }
}
