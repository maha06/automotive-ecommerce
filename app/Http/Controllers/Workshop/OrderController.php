<?php

namespace App\Http\Controllers\Workshop;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Checkout;
use App\Workshop;
use Illuminate\Support\Facades\Auth;
use App\Mail\OrderMail;
use App\Mail\PartMail;
use Illuminate\Support\Facades\Mail;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $orders = Checkout::where('workshop_id', Auth::user()->id)->orderBy('status', 'ASC')->latest()->paginate(10);
        return view('workshop.orders.index')->with(compact(['orders']));
    }


    public function show($id)
    {
        $order = Checkout::find($id);
        return view('workshop.orders.show', compact('order'));
    }

    public function setStatus($id, $status)
    {
        $order = Checkout::find($id);
        $order->update([
            'status' => $status
        ]);

        if($status == 1){
            $status = "Accepted";
            Mail::to($order->email)->send(new OrderMail($status, $order));
        }elseif($status == 2){
            $status = "Completed";
            Mail::to($order->email)->send(new PartMail($order));
        }elseif($status == 3){
            $status = "Cancelled";
            Mail::to($order->email)->send(new OrderMail($status, $order));

        }
        return response()->json($order);
    }
}
