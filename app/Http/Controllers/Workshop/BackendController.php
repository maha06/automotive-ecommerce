<?php

namespace App\Http\Controllers\Workshop;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use App\Car;
use App\Complaint;
use App\Workshop;
use App\Service;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
class BackendController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function index()
    {
        return view('workshop.pages.home');
    }

    public function user()
    {
        $users = User::latest()->paginate(10);;
        return view('workshop.users.index', compact('users'));
    }

    public function getAll($id){
        $car = Car::find($id);
        $models = $car->models;
        return response()->json($models);
    }

    public function complaints(){
        $complaints = Complaint::all();
        return view('workshop.complaints.index', compact('complaints'));
    }

    public function profileEdit(Workshop $workshop){
        $services = Service::all();
        return view('workshop.pages.profile', compact('workshop', 'services'));
    }

    public function profileUpdate(Request $request, Workshop $workshop){
        $validator =  Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
            'phone' => ['required', 'string', 'max:255'],
            'location' => ['required', 'string'],
            'address' => ['required', 'string'],
            'no_of_mechanics' => ['required', 'integer'],
            'no_of_electrician' => ['required', 'integer'],
            'no_of_denter' => ['required', 'integer'],
            'no_of_painter' => ['required', 'integer'],
            'no_of_bays' => ['required', 'integer'],
            'major_tools_and_equipments' => ['required', 'string'],
            'image' => ['nullable', 'image'],
            'service_id' => ['required', 'array'],
            'service_id.*' => ['integer']
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors(['error' => $validator->errors()->all()]);
        }

        if($request->image){
            $image = $request->image;
            $filename = Str::random(15) . '.' . $image->extension();
            Storage::putFileAs("public", $image, $filename);
        }else{
            $filename = $workshop->image;
        }

        $workshop->update([
            'name' => $request->name,
            'slug' => Str::slug($request->name, '-'),
            'phone' => $request->phone,
            'location' => $request->phone,
            'address' => $request->address,
            'no_of_mechanics' => $request->no_of_mechanics,
            'no_of_electrician' => $request->no_of_electrician,
            'no_of_denter' => $request->no_of_denter,
            'no_of_painter' => $request->no_of_painter,
            'no_of_bays' => $request->no_of_bays,
            'major_tools_and_equipments' => $request->major_tools_and_equipments,
            'image' => $filename,
        ]);
        if ($request->service_id) {
            $workshop->services()->sync($request->service_id);
        }
        $request->session()->flash('message', 'Profile updated successfully');
        $request->session()->flash('alert-class', 'alert alert-success');
        return redirect()->back();
    }
}
