<?php

namespace App\Http\Controllers\Workshop;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Booking;
use App\Mail\ApprovalMail;
use App\Mail\SummaryMail;
use Illuminate\Support\Facades\Auth;
use App\Service;
use App\Post;
use Illuminate\Support\Facades\Mail;
class BookingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $bookings = Booking::where('workshop_id', Auth::user()->id)->orderBy('status', 'ASC')->latest()->paginate(12);
        return view('workshop.bookings.index')->with(compact(['bookings']));
    }


    public function show(Booking $booking)
    {
        return view('workshop.bookings.show', compact('booking'));
    }

    public function editProfile(Booking $booking)
    {
        $services = Service::all();
        $posts = Post::all();
        return view('workshop.bookings.edit', compact('booking', 'services', 'posts'));
    }

    public function updateProfile(Request $request, Booking $booking)
    {

        if ($request->service_id) {
            $booking->services()->sync($request->service_id);
        }

        if ($request->post_id) {
            $booking->posts()->sync($request->post_id);
        }

        $request->session()->flash('message', 'Booking updated successfully');
        $request->session()->flash('alert-class', 'alert alert-success');
        return redirect()->back();
    }

    public function setStatus(Booking $booking, $status)
    {
        $booking->update([
            'status' => $status
        ]);

        if($status == 1){
            $status = "Accepted";
            Mail::to($booking->customer_email)->send(new ApprovalMail($status, $booking));
        }elseif($status == 2){
            $status = "Completed";
            Mail::to($booking->customer_email)->send(new SummaryMail($booking));

        }elseif($status == 3){
            $status = "Cancelled";
            Mail::to($booking->customer_email)->send(new ApprovalMail($status, $booking));

        }
        // Mail::to($booking->customer_email)->send(new ApprovalMail($status, $booking));
        return response()->json($booking);
    }
}
