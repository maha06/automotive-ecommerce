<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Oil;
use App\Car;
use App\CarModel;
use App\Cart;
use Illuminate\Support\Facades\Auth;

class EngineOilController extends Controller
{
    public function index(){
        $cars = Car::all();
        $oils = Oil::all();
        return view('pages.engine-oil.home', compact('cars', 'oils'));
    }

    public function searchOil(Request $request){
        $cars = Car::all();
        if($request->name){
            $oils = Oil::where('name', 'LIKE', '%' . $request->name . '%')->orWhere('slug', 'LIKE', '%' . $request->name . '%')->get();
        }
        return view('pages.engine-oil.home', compact('cars', 'oils'));
    }

    public function addOil(Request $request){
        $check = Cart::where('oil_id', $request->oil_id)->first();
        if(!$check){
            $cart = Cart::create([
                'oil_id' => $request->oil_id,
                'price' => $request->price,
                'user_id' => Auth::user()->id,
            ]);
            return response()->json($cart);
        }
        $cart = 0;
        return response()->json($cart);
    }

    public function validateStepOne(Request $request)
    {
        $oils = [];

        $request->validate([
            'make' => ['nullable', 'string', 'max:255'],
            'model' => ['nullable', 'string', 'max:255'],
            'year' => ['nullable', 'string', 'max:255'],
        ]);

        $request->session()->put(['make' => $request->make, 'model' => $request->model, 'year' => $request->year]);
        if (is_array($request->oils)) {
            foreach ($request->oils as $key => $oil) {
                $oils [] = Oil::where('id', $oil)->first();
            }
            return response()->json(['oils' => $oils]);
        }else{
            $parts = 0;
        }
        return response()->json(false);
    }

}
