<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\CarModel;
use App\Car;
use Illuminate\Support\Str;

class ModelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $models = CarModel::all();
        return view('admin.models.index', compact('models'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cars = Car::all();
        return view('admin.models.create', compact('cars'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'name' => ['required', 'unique:models,name', 'string', 'max:255'],
            'car_id' => ['required', 'integer']
        ]);
        $data['slug'] = Str::slug($data['name'], '-');
        CarModel::create($data);
        $request->session()->flash('message', 'New car model created successfully');
        $request->session()->flash('alert-class', 'alert alert-success');
        return redirect()->route('models.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CarModel  $carModel
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $carModel = CarModel::find($id);
        $cars = Car::all();
        return view('admin.models.edit', compact('carModel', 'cars'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CarModel  $carModel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $carModel = CarModel::find($id);
        $data = $request->validate([
            'name' => ['required', 'string', 'max:255', 'unique:cars,name,' . $carModel->id],
            'car_id' => ['required', 'integer']
        ]);

        $data['slug'] = Str::slug($data['name'], '-');
        $carModel->update($data);
        $request->session()->flash('message', 'Car model updated successfully.');
        $request->session()->flash('alert-class', 'alert alert-success');
        return redirect()->route('models.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ServiceCategory  $serviceCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $carModel = CarModel::find($id);
        CarModel::destroy($carModel->id);
        $request->session()->flash('message', 'Car model deleted successfully');
        $request->session()->flash('alert-class', 'alert alert-success');
        return redirect()->route('models.index');
    }
}
