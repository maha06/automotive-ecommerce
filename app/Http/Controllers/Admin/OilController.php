<?php

namespace App\Http\Controllers\Admin;

use App\Admin;
use App\Http\Controllers\Controller;
use App\Oil;
use App\OilsPictures;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class OilController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $oils = Oil::latest()->paginate(10);
        return view('admin.oils.index', compact('oils'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $dealers = Admin::role('oil_dealer')->paginate(10);

        return view('admin.oils.create', compact('dealers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'name' => ['required', 'string', 'max:255', 'unique:oils'],
            'wholesale_price' => ['required', 'integer'],
            'retail_price' => ['required', 'integer'],
            'oil_rate' => ['required', 'integer'],
            'bulk_orders_retail_price' => ['required', 'integer'],
            'image' => ['required', 'array'],
        ]);

        $slug = Str::slug($data['name'], '-');
        $oil = new Oil;
        $oil->name = $request->name;
        $oil->slug = $slug;
        $oil->brand = $request->brand_name;
        $oil->category = $request->category;
        $oil->wholesale_price = $request->wholesale_price;
        $oil->retail_price = $request->retail_price;
        $oil->oil_rate = $request->oil_rate;
        $oil->bulk_orders_retail_price = $request->bulk_orders_retail_price;
        $oil->discount = $request->discount;
        $oil->quantity = $request->quantity;
        $oil->description = $request->description;
        $oil->suggestions = $request->suggestion;
        $oil->company = '';
        //$oil->user_id = Auth::user()->id;
        $oil->user_id = $request->added_by;
        $oil->added_by = "oil_dealer";
        $oil->save();

        $id = $oil->id;
        $images = $request->image;

        foreach ($images as $image) {
            $filename = Str::random(15) . '.' . $image->extension();
            Storage::putFileAs("public", $image, $filename);

            OilsPictures::create(['oil_id' => $id, 'path' => $filename]);

        }
        return redirect()->route('oils.index')->with('success', 'Oil created successfully.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Oil $oil)
    {
        $pictures = OilsPictures::where('oil_id', $oil->id)->get();
        $dealers = Admin::role('oil_dealer')->paginate(10);
        return view('admin.oils.edit', compact('oil', 'pictures', 'dealers'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, Oil $oil)
    {
        $data = $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'wholesale_price' => ['required', 'integer'],
            'retail_price' => ['required', 'integer'],
            'oil_rate' => ['required', 'integer'],
            'bulk_orders_retail_price' => ['required', 'integer'],
        ]);

        $slug = Str::slug($data['name'], '-');
        $oil = Oil::find($oil->id);
        $oil->name = $request->name;
        $oil->slug = $slug;
        $oil->brand = $request->brand_name;
        $oil->category = $request->category;
        $oil->wholesale_price = $request->wholesale_price;
        $oil->retail_price = $request->retail_price;
        $oil->oil_rate = $request->oil_rate;
        $oil->bulk_orders_retail_price = $request->bulk_orders_retail_price;
        $oil->discount = $request->discount;
        $oil->quantity = $request->quantity;
        $oil->description = $request->description;
        $oil->suggestions = $request->suggestion;
        $oil->user_id = $request->added_by;
        $oil->company = '';
        $oil->save();

        $id = $oil->id;
        $images = $request->image;

        if ($images) {
            foreach ($images as $image) {
                $filename = Str::random(15) . '.' . $image->extension();
                Storage::putFileAs("public", $image, $filename);
                OilsPictures::create(['oil_id' => $id, 'path' => $filename]);
            }
        }

        return redirect()->route('oils.index')->with('success', 'Oil updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Oil $oil)
    {
        OilsPictures::where(['oil_id' => $oil->id])->delete();
        $oil->delete();
        return redirect()->back();
    }

    public function delete(Request $request, $id, $image)
    {
        Storage::delete('public/' . $image);
        OilsPictures::where(['oil_id' => $id, 'path' => $image])->delete();

        $request->session()->flash('message', 'Delete Image successfully');
        $request->session()->flash('alert-class', 'alert alert-success');
        return redirect()->back();
    }

    public function show(Oil $oil)
    {
        $pictures = OilsPictures::where('oil_id', $oil->id)->get();

        return view('admin.oils.show', compact('oil', 'pictures'));
    }
}
