<?php

namespace App\Http\Controllers\Admin;

use App\Admin;
use App\Http\Controllers\Controller;
use App\SpareParts;
use App\SparePartsPictures;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class SparePartsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $parts = SpareParts::latest()->paginate(10);
        return view('admin.parts.index', compact('parts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $dealers = Admin::role('oil_dealer')->paginate(10);

        return view('admin.parts.create', compact('dealers'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'name' => ['required', 'string', 'max:255', 'unique:spare_parts'],
            'wholesale_price' => ['required', 'integer'],
            'retail_price' => ['required', 'integer'],
            'bulk_orders_retail_price' => ['required', 'integer'],
            'image' => ['required', 'array'],
        ]);

        $slug = Str::slug($data['name'], '-');
        $part = new SpareParts;
        $part->name = $request->name;
        $part->slug = $slug;
        $part->brand = $request->brand_name;
        $part->category = $request->category;
        $part->sub_category = $request->sub_category;
        $part->wholesale_price = $request->wholesale_price;
        $part->retail_price = $request->retail_price;
        $part->bulk_orders_retail_price = $request->bulk_orders_retail_price;
        $part->discount = $request->discount;
        $part->quantity = $request->quantity;
        $part->description = $request->description;
        $part->user_id = $request->added_by;
        $part->added_by = "parts_dealer";
        $part->save();

        $id = $part->id;
        $images = $request->image;

        foreach ($images as $image) {
            $filename = Str::random(15) . '.' . $image->extension();
            Storage::putFileAs("public", $image, $filename);

            SparePartsPictures::create(['spare_parts_id' => $id, 'path' => $filename]);

        }
        return redirect()->route('parts.index')->with('success', 'Part created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SpareParts  $parts
     * @return \Illuminate\Http\Response
     */
    public function show(SpareParts $part)
    {
        $pictures = SparePartsPictures::where('spare_parts_id', $part->id)->get();

        return view('admin.parts.show', compact('part', 'pictures'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SpareParts  $parts
     * @return \Illuminate\Http\Response
     */
    public function edit(SpareParts $part)
    {
        $pictures = SparePartsPictures::where('spare_parts_id', $part->id)->get();
        $dealers = Admin::role('oil_dealer')->paginate(10);

        return view('admin.parts.edit', compact('part', 'pictures', 'dealers'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SpareParts  $parts
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SpareParts $part)
    {
        $data = $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'wholesale_price' => ['required', 'integer'],
            'retail_price' => ['required', 'integer'],
            'bulk_orders_retail_price' => ['required', 'integer'],
        ]);

        $slug = Str::slug($data['name'], '-');
        $part = SpareParts::find($part->id);
        $part->name = $request->name;
        $part->slug = $slug;
        $part->brand = $request->brand_name;
        $part->category = $request->category;
        $part->sub_category = $request->sub_category;
        $part->wholesale_price = $request->wholesale_price;
        $part->retail_price = $request->retail_price;
        $part->bulk_orders_retail_price = $request->bulk_orders_retail_price;
        $part->discount = $request->discount;
        $part->description = $request->description;
        $part->quantity = $request->quantity;
        $part->user_id = $request->added_by;
        $part->save();

        $id = $part->id;
        $images = $request->image;

        if ($images) {
            foreach ($images as $image) {
                $filename = Str::random(15) . '.' . $image->extension();
                Storage::putFileAs("public", $image, $filename);
                SparePartsPictures::create(['spare_parts_id' => $id, 'path' => $filename]);
            }
        }

        return redirect()->route('parts.index')->with('success', 'Part updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SpareParts  $parts
     * @return \Illuminate\Http\Response
     */
    public function destroy(SpareParts $part)
    {
        SparePartsPictures::where(['spare_parts_id' => $part->id])->delete();
        $part->delete();
        return redirect()->back();
    }

    public function delete(Request $request, $id, $image)
    {
        Storage::delete('public/' . $image);
        SparePartsPictures::where(['spare_parts_id' => $id, 'path' => $image])->delete();

        $request->session()->flash('message', 'Delete Image successfully');
        $request->session()->flash('alert-class', 'alert alert-success');
        return redirect()->back();
    }

}
