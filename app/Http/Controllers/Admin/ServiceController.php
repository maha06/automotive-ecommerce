<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Labour;
use App\Service;
use App\ServicesLabours;
use App\ServicesPictures;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $services = Service::latest()->paginate(10);
        return view('admin.services.index')->with(compact(['services']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $labours = Labour::all();
        return view('admin.services.create', compact('labours'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'price' => ['required'],
            'image' => ['required', 'array'],
        ]);

        $slug = Str::slug($data['name'], '-');
        $service = new Service;
        $service->name = $request->name;
        $service->slug = $slug;
        $service->description = $request->description;
        $service->discount = $request->discount;
        $service->save();
        $id = $service->id;
        $prices = $request->price;
        $labours = $request->labour;
        foreach ($prices as $index => $price) {
            ServicesLabours::create(['service_id' => $id, 'labour_id' => $labours[$index], 'price' => $price]);

        }
        $images = $request->image;

        foreach ($images as $image) {
            $filename = Str::random(15) . '.' . $image->extension();
            Storage::putFileAs("public", $image, $filename);
            ServicesPictures::create(['service_id' => $id, 'path' => $filename]);
        }

        return redirect()->route('services.index')->with('success', 'Service created successfully.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Service $service)
    {
        $pictures = ServicesPictures::where('service_id', $service->id)->get();
        $prices = ServicesLabours::where('service_id', $service->id)->get();
        $labours = Labour::all();

        return view('admin.services.edit')->with(compact(['service', 'pictures', 'prices', 'labours']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, Service $service)
    {
        $data = $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'price' => ['required'],

        ]);

        $slug = Str::slug($data['name'], '-');
        $service = Service::find($service->id);
        $service->name = $request->name;
        $service->slug = $slug;
        $service->description = $request->description;
        $service->discount = $request->discount;
        $service->save();
        $id = $service->id;
        $prices = $request->price;
        $labours = $request->labour;
        foreach ($prices as $index => $price) {
            ServicesLabours::where(['service_id' => $id, 'labour_id' => $labours[$index]])->update(['price' => $price]);

        }
        $images = $request->image;
        if ($images) {
            foreach ($images as $image) {
                $filename = Str::random(15) . '.' . $image->extension();
                Storage::putFileAs("public", $image, $filename);
                ServicesPictures::create(['service_id' => $id, 'path' => $filename]);
            }
        }
        return redirect()->route('services.index')->with('success', 'Service updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Service $service)
    {

        Storage::delete('public/' . $service->image);
        $service->delete();
        return redirect()->back();
    }

    public function delete(Request $request, $id, $image)
    {
        Storage::delete('public/' . $image);
        ServicesPictures::where(['service_id' => $id, 'path' => $image])->delete();

        $request->session()->flash('message', 'Delete Image successfully');
        $request->session()->flash('alert-class', 'alert alert-success');
        return redirect()->back();
    }

    public function show(Service $service)
    {
        $pictures = ServicesPictures::where('service_id', $service->id)->get();
        $prices = ServicesLabours::where('service_id', $service->id)->get();
        $labours = Labour::all();

        return view('admin.services.show')->with(compact(['service', 'pictures', 'prices', 'labours']));
    }

}
