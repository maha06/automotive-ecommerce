<?php

namespace App\Http\Controllers\Admin;

use App\Booking;
use App\Checkout;
use App\Complaint;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;

class PageController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $orders_pending = Checkout::whereDate('created_at', today())->where(['status' => 0])->count();
        $orders_completed = Checkout::whereDate('created_at', today())->where(['status' => 1])->count();
        $orders_cancelled = Checkout::whereDate('created_at', today())->where(['status' => 2])->count();
        $bookings = Booking::whereDate('created_at', today())->count();
        $money = Checkout::whereDate('created_at', today())->sum('price');

        return view('admin.pages.home', compact('orders_pending', 'orders_completed', 'orders_cancelled', 'bookings', 'money'));
    }

    public function filter(Request $request)
    {

        if ($request->filter == 'today') {
            $orders_pending = Checkout::whereDate('created_at', today())->where(['status' => 0])->count();
            $orders_completed = Checkout::whereDate('created_at', today())->where(['status' => 1])->count();
            $orders_cancelled = Checkout::whereDate('created_at', today())->where(['status' => 2])->count();
            $bookings = Booking::whereDate('created_at', today())->count();
            $money = Checkout::whereDate('created_at', today())->sum('price');
            $filter = 'Today';

        } elseif ($request->filter == 'yesterday') {
            $orders_pending = Checkout::whereDate('created_at', today()->subDays(1))->where(['status' => 0])->count();
            $orders_completed = Checkout::whereDate('created_at', today()->subDays(1))->where(['status' => 1])->count();
            $orders_cancelled = Checkout::whereDate('created_at', today()->subDays(1))->where(['status' => 2])->count();
            $bookings = Booking::whereDate('created_at', today()->subDays(1))->count();
            $money = Checkout::whereDate('created_at', today()->subDays(1))->sum('price');
            $filter = 'Yesterday';

        } elseif ($request->filter == 'this_week') {
            $orders_pending = Checkout::whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->where(['status' => 0])->count();
            $orders_completed = Checkout::whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->where(['status' => 1])->count();
            $orders_cancelled = Checkout::whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->where(['status' => 2])->count();
            $bookings = Booking::whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->count();
            $money = Checkout::whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->sum('price');
            $filter = 'This Week';

        } elseif ($request->filter == 'last_week') {
            $previous_week = strtotime("-1 week +1 day");
            $start_week = strtotime("last sunday midnight", $previous_week);
            $end_week = strtotime("next saturday", $start_week);
            $start_week = date("Y-m-d", $start_week);
            $end_week = date("Y-m-d", $end_week);

            $orders_pending = Checkout::whereBetween('created_at', [$start_week, $end_week])->where(['status' => 0])->count();
            $orders_completed = Checkout::whereBetween('created_at', [$start_week, $end_week])->where(['status' => 1])->count();
            $orders_cancelled = Checkout::whereBetween('created_at', [$start_week, $end_week])->where(['status' => 2])->count();
            $bookings = Booking::whereBetween('created_at', [$start_week, $end_week])->count();
            $money = Checkout::whereBetween('created_at', [$start_week, $end_week])->sum('price');
            $filter = 'Last Week';

        } elseif ($request->filter == 'this_month') {
            $orders_pending = Checkout::whereMonth('created_at', date('m'))
                ->whereYear('created_at', date('Y'))->where(['status' => 0])->count();
            $orders_completed = Checkout::whereMonth('created_at', date('m'))
                ->whereYear('created_at', date('Y'))->where(['status' => 1])->count();
            $orders_cancelled = Checkout::whereMonth('created_at', date('m'))
                ->whereYear('created_at', date('Y'))->where(['status' => 2])->count();
            $bookings = Booking::whereMonth('created_at', date('m'))
                ->whereYear('created_at', date('Y'))->count();
            $money = Checkout::whereMonth('created_at', date('m'))
                ->whereYear('created_at', date('Y'))->sum('price');
            $filter = 'This Month';

        } elseif ($request->filter == 'last_month') {
            $orders_pending = Checkout::whereMonth('created_at', '=', Carbon::now()->subMonth()->month)->where(['status' => 0])->count();
            $orders_completed = Checkout::whereMonth('created_at', '=', Carbon::now()->subMonth()->month)->where(['status' => 1])->count();
            $orders_cancelled = Checkout::whereMonth('created_at', '=', Carbon::now()->subMonth()->month)->where(['status' => 2])->count();
            $bookings = Booking::whereMonth('created_at', '=', Carbon::now()->subMonth()->month)->count();
            $money = Checkout::whereMonth('created_at', '=', Carbon::now()->subMonth()->month)->sum('price');
            $filter = 'Last Month';

        } elseif ($request->filter == 'this_year') {
            $orders_pending = Checkout::whereYear('created_at', date('Y'))->where(['status' => 0])->count();
            $orders_completed = Checkout::whereYear('created_at', date('Y'))->where(['status' => 1])->count();
            $orders_cancelled = Checkout::whereYear('created_at', date('Y'))->where(['status' => 2])->count();
            $bookings = Booking::whereYear('created_at', date('Y'))->count();
            $money = Checkout::whereYear('created_at', date('Y'))->sum('price');
            $filter = 'This Year';
        }

        return view('admin.pages.home', compact('filter', 'orders_pending', 'orders_completed', 'orders_cancelled', 'bookings', 'money'));
    }

    public function complaints()
    {
        $complaints = Complaint::all();
        return view('admin.complaints.index', compact('complaints'));
    }
}
