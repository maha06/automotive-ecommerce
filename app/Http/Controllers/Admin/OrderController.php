<?php

namespace App\Http\Controllers\Admin;

use App\Checkout;
use App\CheckoutDetails;
use App\Http\Controllers\Controller;
use App\Oil;
use App\SpareParts;
use App\User;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $orders = Checkout::orderBy('status', 'ASC')->latest()->paginate(10);
        return view('admin.orders.index')->with(compact(['orders']));
    }

    public function show($id)
    {
        $order = Checkout::find($id);
        $status = 'Pending';
        if ($order->status == 1) {
            $status = 'Completed';
        } else if ($order->status == 2) {
            $status = 'Cancelled';
        }
        $details = CheckoutDetails::where('checkout_id', $id)->get();
        return view('admin.orders.show', compact('order', 'details', 'status'));
    }

    public function create()
    {
        $oils = Oil::all();
        $parts = SpareParts::all();
        $users = User::all();

        return view('admin.orders.create', compact('oils', 'parts', 'users'));
    }

    public function store(Request $request)
    {

        $data = $request->validate([
            'user_id' => ['required', 'string', 'max:255'],
            'address' => ['required', 'string'],
            'apartment' => ['nullable', 'string', 'max:255'],
            'order_notes' => ['nullable', 'string'],
            'city' => ['string', 'required', 'max:255'],
        ]);

        $oils = $request->oils;
        $oils_quantity = $request->oil_quantity;
        $oils_price = 0;

        if ($oils[0] != '') {
            foreach ($oils as $index => $value) {

                $oil = Oil::find($value);
                $oils_price += ($oil->retail_price * $oils_quantity[$index]);
                $quantity = (int) $oil->quantity - (int) $oils_quantity[$index];
                $oil->quantity = $quantity;
                $oil->save();
            }
        }

        $parts = $request->parts;
        $parts_quantity = $request->part_quantity;
        $parts_price = 0;

        if ($parts[0] != '') {
            foreach ($parts as $index => $value) {

                $part = SpareParts::find($value);
                $parts_price += ($part->retail_price * $parts_quantity[$index]);
                $quantity = (int) $part->quantity - (int) $parts_quantity[$index];
                $part->quantity = $quantity;
                $part->save();
            }
        }

        $total = $oils_price + $parts_price;
        $user = User::find($request->user_id);

        $checkout = Checkout::create([
            'name' => $user->name,
            'email' => $user->email,
            'address' => $data['address'],
            'apartment' => $data['apartment'],
            'phone' => $user->phone,
            'user_id' => $request->user_id,
            'order_notes' => $data['order_notes'],
            'city' => $data['city'],
            'price' => $total,

        ]);
        if ($oils[0] != '') {
            foreach ($oils as $index => $value) {

                CheckoutDetails::create([
                    'checkout_id' => $checkout->id,
                    'oil_id' => $value,
                    'quantity' => $oils_quantity[$index],
                    'part_id' => 0,
                ]);
            }
        }

        if ($parts[0] != '') {
            foreach ($parts as $index => $value) {

                CheckoutDetails::create([
                    'checkout_id' => $checkout->id,
                    'part_id' => $value,
                    'oil_id' => 0,
                    'quantity' => $parts_quantity[$index],

                ]);
            }
        }

        $request->session()->flash('message', 'Booking successfully');
        $request->session()->flash('alert-class', 'alert alert-success');
        return redirect()->route('orders.index');
    }

    public function status(Request $request)
    {

        Checkout::where('id', $request->id)->update(['status' => $request->status]);
        return redirect()->route('orders.show', $request->id);
    }

}
