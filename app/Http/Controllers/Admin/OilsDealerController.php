<?php

namespace App\Http\Controllers\Admin;

use App\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Spatie\Permission\Models\Role;

class OilsDealerController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $dealers = Admin::role('oil_dealer')->paginate(10);

        return view('admin.oils-dealer.index', compact('dealers'));
    }

    public function create()
    {
        return view('admin.oils-dealer.create');
    }

    public function store(Request $request)
    {
        $data = $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'unique:admins'],
            'contact' => ['required'],
            'nic' => ['required', 'unique:admins'],
            'address' => ['required', 'string'],
            'city' => ['required', 'string'],

        ]);

        $dealer = new Admin;
        $dealer->username = $request->name;
        $dealer->business_name = $request->business_name;
        $dealer->contact = $request->contact;
        $dealer->email = $request->email;
        $dealer->nic = $request->nic;
        $dealer->address = $request->address;
        $dealer->city = $request->city;
        $password = '12345678';
        $dealer->password = Hash::make($password);

        $image = $request->image;
        if (isset($image)) {
            $filename = Str::random(15) . '.' . $image->extension();
            Storage::putFileAs("public", $image, $filename);
            $dealer->picture = $filename;
        } else {
            $dealer->picture = '';
        }

        $dealer->save();

        $role = Role::where('name', 'oil_dealer')->first();
        $user = Admin::where(['id' => $dealer->id])->first();
        $user->assignRole($role);

        return redirect()->route('oils-dealers.index')->with('success', 'Dealer created successfully.');
    }

    public function edit($id)
    {
        $dealer = Admin::find($id);
        return view('admin.oils-dealer.edit', compact('dealer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $data = $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'contact' => ['required'],
            'nic' => ['required'],
            'address' => ['required', 'string'],
            'city' => ['required', 'string'],

        ]);

        $dealer = Admin::find($id);
        $dealer->username = $request->name;
        $dealer->business_name = $request->business_name;
        $dealer->contact = $request->contact;
        $dealer->email = $request->email;
        $dealer->nic = $request->nic;
        $dealer->address = $request->address;
        $dealer->city = $request->city;
        $dealer->password = Hash::make($request->password);
        if (isset($request->image)) {
            $image = $request->image;

            $filename = Str::random(15) . '.' . $image->extension();
            Storage::putFileAs("public", $image, $filename);
            $dealer->picture = $filename;

        }
        $dealer->save();

        return redirect()->route('oils-dealers.index')->with('success', 'Dealer updated successfully.');
    }

    public function destroy(Admin $dealer)
    {
        $dealer->delete();
        return redirect()->back();
    }

    public function show($id)
    {

        $dealer = Admin::find($id);
        return view('admin.oils-dealer.show', compact('dealer'));
    }

}
