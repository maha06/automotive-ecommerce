<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Mechanic;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class MechanicController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $mechanics = Mechanic::latest()->paginate(10);
        return view('admin.mechanics.index')->with(compact(['mechanics']));
    }

    public function changePassword()
    {
        return view('admin.mechanics.change-password');
    }

    public function updatePassword(Request $request)
    {
        $data = $request->validate([
            'new_password' => ['required', 'string', 'min:8'],
            'confirm_password' => ['required_with:new_password', ' same:new_password', 'string', 'min:8'],
        ]);
        $mechanic = Mechanic::where('id', Auth::user()->id)->first();
        $mechanic->update([
            'password' => Hash::make($data['new_password'])
        ]);
        $request->session()->flash('message', 'Password updated successfully');
        $request->session()->flash('alert-class', 'alert alert-success');
        return redirect()->route('mechanics.index');
    }

    public function setStatus($id, $status)
    {
        $mechanic = Mechanic::where('id', $id)->first();
        $mechanic->status = $status;
        $mechanic->save();
        return response()->json($mechanic);
    }
}
