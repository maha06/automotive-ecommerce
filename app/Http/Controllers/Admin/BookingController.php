<?php

namespace App\Http\Controllers\Admin;

use App\Booking;
use App\Car;
use App\Http\Controllers\Controller;
use App\Labour;
use App\Service;
use App\SpareParts;
use App\User;
use App\Workshop;
use Illuminate\Http\Request;

class BookingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $bookings = Booking::latest()->paginate(10);
        return view('admin.bookings.index')->with(compact(['bookings']));
    }

    public function show(Booking $booking)
    {
        $engine_id = $booking->engine_id;
        $workshop = Workshop::find($booking->workshop_id);
        $status = 'Pending';
        if ($booking->status == 1) {
            $status = 'Completed';
        } else if ($booking->status == 2) {
            $status = 'Cancelled';
        }
        //ServicesLabours::where([ 'labour_id'=> $engine_id, 'service_id' =>  ])->get();
        return view('admin.bookings.show', compact('booking', 'workshop', 'status'));
    }

    public function create()
    {
        $workshops = Workshop::all();
        $services = Service::all();
        $users = User::all();
        $parts = SpareParts::all();
        $labours = Labour::all();
        $makes = Car::all();

        return view('admin.bookings.create', compact('workshops', 'services', 'users', 'parts', 'labours', 'makes'));
    }

    public function store(Request $request)
    {
        $data = $request->validate([
            'user_id' => ['required', 'string', 'max:255'],
            'workshop' => ['required', 'string'],
            'date' => ['required'],
            'time' => ['required'],

            'city' => ['string', 'required', 'max:255'],
        ]);

        $user = User::find($request->user_id);

        if ($request->pickup) {
            $pickup = 'Yes';
        } else {
            $pickup = 'No';
        }

        $booking = Booking::create([
            'workshop_id' => $request->workshop,
            'make' => $request->make,
            'category' => 'maintenance',
            'model' => $request->model,
            'mileage' => $request->mileage,
            'registration_number' => $request->registration_number,
            'mobile' => $user->phone,
            'customer_name' => $user->name,
            'city' => $request->city,
            'customer_email' => $user->email,
            'available_date' => $request->date,
            'available_time' => $request->time,
            'pickup' => $request->pickup,
            'pickup_time' => $request->pickup_time,
            'pickup_address' => $request->pickup_address,
            'pickup_contact' => $request->pickup_contact,
            'engine_id' => $request->engine_id,
            'parts_deliver' => $request->parts_deliver,
        ]);

        if ($request->services) {
            $booking->services()->sync($request->services);
        }
        $parts = $request->parts;
        if ($parts[0] != '') {

            $booking->parts()->sync($request->parts);

        }

        $request->session()->flash('message', 'Booking successfully');
        $request->session()->flash('alert-class', 'alert alert-success');
        return redirect()->route('bookings.index');
    }

    public function status(Request $request)
    {
        Booking::where('id', $request->id)->update(['status' => $request->status]);
        return redirect()->route('bookings.show', $request->id);
    }

}
