<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Workshop;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use App\Mail\WorkshopRegisterMail;
class WorkshopController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $workshops = Workshop::orderBy('status', 'ASC')->latest()->paginate(10);
        return view('admin.workshops.index')->with(compact(['workshops']));
    }

    public function changePassword()
    {
        return view('admin.workshops.change-password');
    }

    public function updatePassword(Request $request)
    {
        $data = $request->validate([
            'new_password' => ['required', 'string', 'min:8'],
            'confirm_password' => ['required_with:new_password', ' same:new_password', 'string', 'min:8'],
        ]);
        $workshop = Workshop::where('id', Auth::user()->id)->first();
        $workshop->update([
            'password' => Hash::make($data['new_password'])
        ]);
        $request->session()->flash('message', 'Password updated successfully');
        $request->session()->flash('alert-class', 'alert alert-success');
        return redirect()->route('workshop.index');
    }

    public function setStatus($id, $status)
    {
        $workshop = Workshop::where('id', $id)->first();
        $workshop->status = $status;
        $workshop->save();
        if($workshop->status == 1){
            $status = "Approved";
            Mail::to($workshop->email)->send(new WorkshopRegisterMail());
        }
        return response()->json($workshop);
    }
}
