<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Car;
use Illuminate\Support\Str;

class CarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cars = Car::all();
        return view('admin.cars.index', compact('cars'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.cars.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'name' => ['required', 'unique:cars,name', 'string', 'max:255'],
            'mileage' => ['required', 'string', 'max:255'],
            'year' => ['required', 'string', 'max:255']
        ]);
        $data['slug'] = Str::slug($data['name'], '-');
        Car::create($data);
        $request->session()->flash('message', 'New car created successfully');
        $request->session()->flash('alert-class', 'alert alert-success');
        return redirect()->route('cars.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Car  $car
     * @return \Illuminate\Http\Response
     */
    public function edit(Car $car)
    {

        return view('admin.cars.edit', compact('car'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Car  $car
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Car $car)
    {
        $data = $request->validate([
            'name' => ['required', 'string', 'max:255', 'unique:cars,name,' . $car->id],
            'mileage' => ['required', 'string', 'max:255'],
            'year' => ['required', 'string', 'max:255']
        ]);
        $data['slug'] = Str::slug($data['name'], '-');
        $car->update($data);
        $request->session()->flash('message', 'Car updated successfully.');
        $request->session()->flash('alert-class', 'alert alert-success');
        return redirect()->route('cars.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ServiceCategory  $serviceCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Car $car)
    {
        Car::destroy($car->id);
        $request->session()->flash('message', 'Car deleted successfully');
        $request->session()->flash('alert-class', 'alert alert-success');
        return redirect()->route('cars.index');
    }
}
