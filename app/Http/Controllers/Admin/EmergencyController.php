<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Emergency;
class EmergencyController extends Controller
{
    public function index(){
        $emergencies = Emergency::latest()->paginate(12);
        return view('admin.emergency-towing.index', compact('emergencies'));
    }
}
