<?php

namespace App\Http\Controllers;

use App\Booking;
use App\Car;
use App\CarModel;
use App\Complaint;
use App\Emergency;
use App\GeneralSetting;
use App\Http\Traits\SmsTrait;
use App\Labour;
use App\Mail\VehicleInformationMail;
use App\Oil;
use App\Post;
use App\Review;
use App\Service;
use App\ServicesLabours;
use App\SpareParts;
use App\Workshop;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class PageController extends Controller
{
    use SmsTrait;
    public function index()
    {
        $cars = Car::all();
        $models = CarModel::all();
        return view('pages.home', compact('cars', 'models'));
    }

    public function customerComplaints()
    {
        return view('pages.customer-complaints');
    }

    public function complaintsSubmit(Request $request)
    {
        $data = $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'phone' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'max:255'],
            'message' => ['required', 'string'],
        ]);
        Complaint::create($data);
        $request->session()->flash('success', 'Thankyou for your feedback!');
        $request->session()->flash('alert-class', 'alert-success');
        return redirect()->back();
    }

    public function emergencyTowing()
    {
        return view('pages.emergency-towing');
    }

    public function emergencyTowingRequest(Request $request)
    {

        Emergency::create([
            'user_id' => Auth::user()->id,
        ]);
        $request->session()->flash('success', 'Thankyou! We will contact you as soon as possible');
        $request->session()->flash('alert-class', 'alert-success');
        return redirect()->back();
    }

    public function getAllCarModels($id)
    {
        $car = Car::find($id);
        $models = $car->models;
        return response()->json($models);
    }

    public function searchSpareParts(Request $request)
    {
        $cars = Car::all();
        $models = CarModel::all();
        if ($request->car_id && $request->model_id && $request->part_name) {
            $posts = Post::where('name', 'LIKE', '%' . $request->part_name . '%')->orWhere('slug', 'LIKE', '%' . $request->part_name . '%')->where('car_id', $request->car_id)->where('model_id', $request->model_id)->latest()->paginate(12);
            return view('pages.frontend.spare-parts', compact('models', 'cars', 'posts'));
        }
        if ($request->car_id && $request->model_id) {
            $posts = Post::where('car_id', $request->car_id)->where('model_id', $request->model_id)->latest()->paginate(12);
            return view('pages.frontend.spare-parts', compact('models', 'cars', 'posts'));
        }
        if ($request->car_id && $request->part_name) {
            $posts = Post::where('car_id', $request->car_id)->where('name', 'LIKE', '%' . $request->part_name . '%')->orWhere('slug', 'LIKE', '%' . $request->part_name . '%')->latest()->paginate(12);
            return view('pages.frontend.spare-parts', compact('models', 'cars', 'posts'));
        }
        if ($request->car_id) {
            $posts = Post::where('car_id', $request->car_id)->latest()->paginate(12);
            return view('pages.frontend.spare-parts', compact('models', 'cars', 'posts'));
        }
        if ($request->part_name) {
            $posts = Post::where('name', 'LIKE', '%' . $request->part_name . '%')->orWhere('slug', 'LIKE', '%' . $request->part_name . '%')->latest()->paginate(12);
            return view('pages.frontend.spare-parts', compact('models', 'cars', 'posts'));
        }
        $posts = Post::all();
        return view('pages.frontend.spare-parts', compact('models', 'cars', 'posts'));

    }

    public function about()
    {
        return view('pages.about');
    }

    public function contactUs()
    {
        return view('pages.contact');
    }

    public function workshops()
    {
        $workshops = Workshop::where('status', 1)->latest()->paginate(12);
        return view('pages.frontend.workshops', compact('workshops'));
    }

    public function workshopDetails($slug)
    {
        $workshop = Workshop::where('slug', $slug)->first();
        $reviews = $workshop->reviews;
        return view('pages.frontend.workshop-details', compact('workshop', 'reviews'));
    }

    public function sparePartDetails($slug)
    {
        $part = SpareParts::where('slug', $slug)->first();
        return view('pages.frontend.spare-parts-details', compact('part'));
    }

    public function oils()
    {
        $cars = Car::all();
        $models = CarModel::all();
        $oils = Oil::latest()->paginate(12);
        return view('pages.frontend.oils', compact('oils', 'cars', 'models'));
    }
    public function oilDetails($slug)
    {
        $oil = Oil::where('slug', $slug)->first();
        return view('pages.frontend.oil-details', compact('oil'));
    }
    public function serviceDetails($slug)
    {
        $service = Service::where('slug', $slug)->first();

        $prices = ServicesLabours::where('service_id', $service->id)->get();
        $labours = Labour::all();
        return view('pages.frontend.service-details', compact('service', 'prices', 'labours'));
    }

    public function searchWorkshops(Request $request)
    {
        if ($request->city && $request->workshop_name) {
            $workshops = Workshop::where('status', 1)->where('location', $request->city)->where('name', 'LIKE', '%' . $request->workshop_name . '%')->orWhere('slug', 'LIKE', '%' . $request->workshop_name . '%')->latest()->paginate(12);
            return view('pages.frontend.workshops', compact('workshops'));
        }
        if ($request->city) {
            $workshops = Workshop::where('status', 1)->where('location', $request->city)->latest()->paginate(12);
            return view('pages.frontend.workshops', compact('workshops'));
        }
        if ($request->workshop_name) {
            $workshops = Workshop::where('status', 1)->where('name', 'LIKE', '%' . $request->workshop_name . '%')->orWhere('slug', 'LIKE', '%' . $request->workshop_name . '%')->latest()->paginate(12);
            return view('pages.frontend.workshops', compact('workshops'));
        }
        $workshops = Workshop::where('status', 1)->latest()->paginate(12);
        return view('pages.frontend.workshops', compact('workshops'));

    }

    public function getAllWorkshopCities(Request $request, $city)
    {
        $cityNames = $request->dataCities;
        $workshops = [];
        foreach ($cityNames as $name) {
            $workshops[] = Workshop::where('status', 1)->where('location', 'LIKE', '%' . $name . '%')->first();
        }
        return response()->json($workshops);
    }

    public function getAllWorkshop()
    {
        $workshops = Workshop::where('status', 1)->get();
        return response()->json($workshops);
    }

    public function getAllServices(Workshop $workshop)
    {
        $services = $workshop->services;
        return response()->json($services);
    }

    public function spareParts()
    {
        $cars = Car::all();
        $models = CarModel::all();
        $posts = SpareParts::latest()->paginate(12);
        return view('pages.frontend.spare-parts', compact('posts', 'cars', 'models'));
    }

    public function services()
    {
        $cars = Car::all();
        $models = CarModel::all();
        $services = Service::latest()->paginate(12);
        return view('pages.frontend.services', compact('services', 'cars', 'models'));
    }

    public function serviceRepairsMaintenance()
    {
        $services = Service::all();
        $posts = Post::all();
        $workshops = Workshop::where('status', 1)->get();
        $cars = Car::all();
        $models = CarModel::all();
        return view('pages.vehicle-information', compact('services', 'posts', 'workshops', 'cars', 'models'));
    }

    public function getAll($id)
    {
        $car = Car::where('slug', $id)->first();
        $models = $car->models;
        return response()->json($models);
    }

    public function getAllParts(Request $request, $id)
    {

        $carIds = $request->dataCars;
        $parts = Post::whereIn('car_id', $carIds)->get();
        return response()->json($parts);
    }

    public function getAllWorkshopParts()
    {
        $parts = Post::all();
        return response()->json($parts);
    }

    public function getAllModelParts(Request $request, $id)
    {

        $modelIds = $request->dataModels;
        $parts = Post::whereIn('model_id', $modelIds)->get();
        return response()->json($parts);
    }

    public function getWorkshopCarModel(Workshop $workshop, Car $car, $id)
    {
        $model = CarModel::find($id);
        return response()->json(['model' => $model, 'car' => $car, 'workshop' => $workshop]);
    }

    public function getAllWorkshopModelCarParts(Request $request, $model, $car)
    {
        $modelIds = $request->dataModels;
        $carIds = $request->dataCars;
        $parts = Post::whereIn('model_id', $modelIds)->whereIn('car_id', $carIds)->get();
        return response()->json($parts);
    }

    public function getAllPriceParts(Request $request)
    {
        if ($request->price == '0') {
            $parts = Post::orderBy('price', 'ASC')->get();
        } elseif ($request->price == '1') {
            $parts = Post::orderBy('price', 'DESC')->get();
        } else {
            $parts = Post::orderBy('price', 'ASC')->get();
        }
        return response()->json($parts);
    }

    public function vehicleInformation(Request $request)
    {
        $generalSetting = GeneralSetting::first();

        $booking = Booking::create([
            'workshop_id' => $request->workshop,
            'make' => $request->make,
            'category' => 'maintenance',
            'model' => $request->model,
            'mileage' => $request->mileage,
            'registration_number' => $request->registration_number,
            'mobile' => $request->mobile,
            'customer_name' => $request->customer_name,
            'city' => $request->city,
            'customer_email' => $request->customer_email,
            'available_date' => $request->available_date,
            'available_time' => $request->available_time,
            'pickup' => $request->pickup,
            'pickup_time' => $request->pickup_time,
            'pickup_address' => $request->pickup_address,
            'pickup_contact' => $request->pickup_contact,
            'engine_id' => $request->engine_id,
            'parts_deliver' => $request->parts_deliver,
        ]);

        if ($request->services) {
            $booking->services()->sync($request->services);
        }

        if ($request->parts) {
            $booking->parts()->sync($request->parts);
        }

        $this->sendBookingSMS($request->mobile, $request->available_date, $request->available_time);

        Mail::to($booking->customer_email)->send(new VehicleInformationMail($booking));
        Mail::to($booking->workshop->email)->send(new VehicleInformationMail($booking));

        if (isset($generalSetting)) {
            if ($generalSetting->admin_email) {
                Mail::to($generalSetting->admin_email)->send(new VehicleInformationMail($booking));
            }
        }
        return response()->json(true);
    }

    public function validateStepFour(Request $request)
    {

        $request->validate([
            'make' => ['required', 'string', 'max:255'],
            'model' => ['required', 'string', 'max:255'],
            'mileage' => ['required', 'string', 'max:255'],
            'registration_number' => ['required', 'string', 'max:255'],
            'mobile' => ['required', 'string', 'max:255'],
            'customer_name' => ['required', 'string', 'max:255'],
            'city' => ['required', 'string', 'max:255'],
            'customer_email' => ['required', 'string', 'max:255'],
        ]);

        $request->session()->put(['customer_email' => $request->customer_email, 'make' => $request->make, 'model' => $request->model, 'mileage' => $request->mileage, 'registration_number' => $request->registration_number, 'mobile' => $request->mobile, 'customer_number' => $request->customer_number, 'city' => $request->city]);
        return response()->json(true);

    }

    public function validateStepOne(Request $request)
    {
        $validator = $request->validate([
            'services' => ['required', 'array'],

        ]);

        $location = $request->session()->get('city');
        $services = $request->services;
        // $workshops = Workshop::where('status', 1)->where('location', 'LIKE', '%' . $location . '%')->get();
        // $workshops = Workshop::where('status', 1)->whereHas('services', function ($query) use ($services) {
        //     $query->where('service_id', $services);
        // })->get();
        $workshops = Workshop::where('status', 1)->get();
        return response()->json($workshops);

    }

    public function validateStepTwo(Request $request)
    {
        $validator = $request->validate([
            'workshop' => ['required'],

        ]);

        Workshop::where('status', 1)->where('id', $request->workshop)->first();
        return response()->json(true);

    }

    public function validateStepThree(Request $request)
    {
        $validator = $request->validate([
            'available_date' => ['required', 'string', 'max:255'],
            'available_time' => ['required', 'string', 'max:255'],
            'pickup_time' => ['nullable', 'string', 'max:255'],
            'pickup_address' => ['nullable', 'string'],
            'pickup_contact' => ['nullable', 'string', 'max:255'],
        ]);

        $request->session()->put(['available_date' => $request->available_date, 'available_time' => $request->available_time, 'pickup_time' => $request->pickup_time, 'pickup_address' => $request->pickup_address, 'pickup_contact' => $request->pickup_contact]);

        // if ($request->workshop) {
        //     $workshop = Workshop::where('status', 1)->where('id', $request->workshop)->first();
        //     $parts = $workshop->posts;
        //     return response()->json($parts);
        // }
        $parts = SpareParts::with('pictures')->get();
        return response()->json($parts);
    }

    public function validateStepFive(Request $request)
    {
        $validator = $request->validate([
            'make' => ['required', 'string', 'max:255'],
            'model' => ['required', 'string', 'max:255'],
            'city' => ['required', 'string', 'max:255'],
            'engine_id' => ['required', 'string'],
            'name' => ['required', 'string', 'max:255'],
            'mobile' => ['required', 'string', 'max:255'],
        ]);
        $parts = [];
        $services = [];
        $prices = [];
        $engine_id = $request->engine_id;

        foreach ($request->services as $key => $service) {
            $services[] = Service::where('id', $service)->first();
            $labour = ServicesLabours::where(['labour_id' => $engine_id, 'service_id' => $service])->first();
            if ($labour->count() > 0) {$prices[] = $labour->price;} else {
                $prices[] = 0;
            }
        }
        if ($request->parts) {
            foreach ($request->parts as $key => $part) {
                $parts[] = SpareParts::where('id', $part)->first();
            }
        }

        $workshop = Workshop::where('status', 1)->where('id', $request->workshop)->first();
        //$part = $workshop->posts()->where('id', $request->part)->first();
        return response()->json(['workshop' => $workshop, 'services' => $services, 'parts' => $parts, 'prices' => $prices]);

    }

    public function reviewsSubmit(Request $request)
    {
        if ($request->ajax()) {
            Review::create([
                'name' => $request->name,
                'email' => $request->email,
                'rating' => $request->rating,
                'message' => $request->message,
                'user_id' => $request->user_id,
                'workshop_id' => $request->workshop_id,
            ]);
            $reviews = Review::where('workshop_id', $request->workshop_id)->get();
            return response()->json($reviews);
        }
    }

    public function reviewsShow(Request $request, Workshop $workshop)
    {
        if (count($workshop->reviews) > 0) {
            $star = round(number_format((float) ($workshop->reviews->sum('rating') / $workshop->reviews->count()), 2, '.', ''));
        } else {
            $star = 0;
        }
        return response()->json($star);
    }
}
