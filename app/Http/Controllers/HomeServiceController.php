<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Service;
use App\Workshop;
use App\Post;
use App\Car;
use App\CarModel;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use App\Booking;
use App\GeneralSetting;
use App\Mail\VehicleInformationMail;
use Illuminate\Support\Facades\Mail;

class HomeServiceController extends Controller
{
    public function index(){
        $services = Service::all();
        $posts = Post::all();
        $workshops = Workshop::where('status', 1)->get();
        $cars = Car::all();
        $models = CarModel::all();
        return view('pages.home-services.home', compact('models', 'cars', 'workshops', 'posts', 'services'));
    }

    public function validateStepOne(Request $request)
    {
        $request->validate([
            'make' => ['required', 'string', 'max:255'],
            'model' => ['required', 'string', 'max:255'],
            'mileage' => ['required', 'string', 'max:255'],
            'registration_number' => ['required', 'string', 'max:255'],
            'mobile' => ['required', 'string', 'max:255'],
            'customer_name' => ['required', 'string', 'max:255'],
            'city' => ['required', 'string', 'max:255'],
            'customer_email' => ['required', 'string', 'max:255'],
        ]);

        // $audio = $request->audioName;
        // $filename = Str::random(15) . '.' . $request->extension;
        // Storage::putFileAs("public", $audio, $filename);
        // $audio = $filename;

        $request->session()->put(['customer_email' => $request->customer_email, 'make' => $request->make, 'model' => $request->model, 'mileage' => $request->mileage, 'registration_number' => $request->registration_number, 'mobile' => $request->mobile, 'customer_number' => $request->customer_number, 'city' => $request->city]);
        if (is_array($request->services)) {
            $city = $request->session()->get('city');
            $services = $request->services;
            $workshops = Workshop::where('status', 1)->where('location', 'LIKE', '%' . $city . '%')->whereHas('services',function ($query) use ($services){
                $query->where('service_id', $services);
             })->get();
            return response()->json($workshops);
        }
        return response()->json(false);
    }

    public function validateStepTwo(Request $request)
    {
        if ($request->workshop) {
            Workshop::where('status', 1)->where('id', $request->workshop)->first();
            return response()->json(true);
        }
        return response()->json(false);
    }

    public function validateStepThree(Request $request)
    {
        $workshop = Workshop::where('status', 1)->where('id', $request->workshop)->first();
        $parts = $workshop->posts;
        return response()->json(['parts' => $parts]);
    }

    public function validateStepFour(Request $request)
    {
        $workshop = Workshop::where('status', 1)->where('id', $request->workshop)->first();
        if($workshop){
            return response()->json(true);
        }else{
            return response()->json(false);
        }
    }

    public function validateStepFive(Request $request)
    {
        $parts = [];
        $services = [];

        $request->validate([
            'available_date' => ['required', 'string', 'max:255'],
            'available_time' => ['required', 'string', 'max:255'],
            'pickup_time' => ['nullable', 'string', 'max:255'],
            'pickup_address' => ['nullable', 'string'],
            'pickup_contact' => ['nullable', 'string', 'max:255'],
        ]);

        $request->session()->put(['available_date' => $request->available_date, 'available_time' => $request->available_time, 'pickup_time' => $request->pickup_time, 'pickup_address' => $request->pickup_address, 'pickup_contact' => $request->pickup_contact]);
        foreach ($request->services as $key => $service) {
            $services[] = Service::where('id', $service)->first();
        }

        if(is_array($request->parts)){
            foreach ($request->parts as $key => $part) {
                $parts[] = Post::where('id', $part)->first();
            }
        }else{
            $parts = 0;
        }

        $workshop = Workshop::where('status', 1)->where('id', $request->workshop)->first();
        $part = $workshop->posts()->where('id', $request->part)->first();
        return response()->json(['workshop' => $workshop, 'services' => $services, 'parts' => $parts]);
    }

    public function HomeRepairingServices(Request $request)
    {
        $generalSetting = GeneralSetting::first();
        // $audio = $request->audio;
        // $filename = Str::random(15) . '.mp3';
        // Storage::putFileAs("public", $audio, $filename);
        // $audio = $filename;

        $booking = Booking::create([
            'workshop_id' => $request->workshop,
            'category' => 'home-services',
            // 'audio' => $audio,
            'make' => $request->make,
            'model' => $request->model,
            'mileage' => $request->mileage,
            'registration_number' => $request->registration_number,
            'mobile' => $request->mobile,
            'customer_name' => $request->customer_name,
            'city' => $request->city,
            'customer_email' => $request->customer_email,
            'available_date' => $request->available_date,
            'available_time' => $request->available_time,
        ]);

        if (is_array($request->services)) {
            $booking->services()->sync($request->services);
        }

        if (is_array($request->parts)) {
            $booking->posts()->sync($request->parts);
        }

        Mail::to($booking->customer_email)->send(new VehicleInformationMail($booking));
        Mail::to($booking->workshop->email)->send(new VehicleInformationMail($booking));
        if($generalSetting->admin_email){
            Mail::to($generalSetting->admin_email)->send(new VehicleInformationMail($booking));
        }
        return response()->json(true);
    }
}
