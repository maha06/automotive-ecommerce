<?php

namespace App\Http\Traits;

use Exception;
use Twilio\Rest\Client;

trait SmsTrait
{
    public function sendOrderSMS($contact)
    {
        $cut = strstr($contact, '3');
        $phone = '+92' . $cut;
        $receiverNumber = $phone;
        $message = "Dear customer,
Your order has been booked successfully.
Your order will be delivered within 2 to 3 working days.
Regards,
Team Autoaxis";

        try {

            $account_sid = getenv("TWILIO_SID");
            $auth_token = getenv("TWILIO_TOKEN");
            $twilio_number = getenv("TWILIO_FROM");

            $client = new Client($account_sid, $auth_token);
            $client->messages->create($receiverNumber, [
                'from' => $twilio_number,
                'body' => $message]);

            return true;

        } catch (Exception $e) {
            echo ("Error: " . $e->getMessage());
            return false;
        }
    }

    public function sendBookingSMS($contact, $date, $time)
    {
        $cut = strstr($contact, '3');
        $phone = '+92' . $cut;
        $receiverNumber = $phone;
        $message = "Dear customer,
Your appointment has been booked successfully on " . $date . " at " . $time . ".
We will respect your arrival on time.
Regards,
Team Autoaxis";
        try {

            $account_sid = getenv("TWILIO_SID");
            $auth_token = getenv("TWILIO_TOKEN");
            $twilio_number = getenv("TWILIO_FROM");

            $client = new Client($account_sid, $auth_token);
            $client->messages->create($receiverNumber, [
                'from' => $twilio_number,
                'body' => $message]);

            return true;

        } catch (Exception $e) {
            echo ("Error: " . $e->getMessage());
            return false;
        }
    }
}
