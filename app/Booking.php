<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    protected $guarded = [];

    public function services()
    {
        return $this->belongsToMany(Service::class, 'booking_services');
    }
    public function parts()
    {
        return $this->belongsToMany(SpareParts::class, 'booking_spare_parts');
    }

    public function posts()
    {
        return $this->belongsToMany(Post::class, 'booking_posts');
    }

    public function workshop()
    {
        return $this->belongsTo(Workshop::class);
    }
}
