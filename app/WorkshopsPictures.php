<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WorkshopsPictures extends Model
{
    protected $table = 'workshops_pictures';
    protected $fillable = [
        'workshop_id', 'path',
    ];
}
