<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SpareParts extends Model
{
    public function pictures()
    {
        return $this->hasMany(SparePartsPictures::class);
    }

    public function checkouts()
    {
        return $this->hasMany(CheckoutDetails::class);
    }

}
