<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OilsPictures extends Model
{
    protected $table = 'oils_pictures';
    protected $fillable = [
        'oil_id', 'path',
    ];

}
