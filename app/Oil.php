<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Oil extends Model
{
    protected $guarded = [];
    protected $table = 'oils';

    public function carts()
    {
        return $this->hasMany(Cart::class);
    }

    public function checkouts()
    {
        return $this->hasMany(CheckoutDetails::class);
    }

    public function pictures()
    {
        return $this->hasMany(OilsPictures::class);
    }

    public function cars()
    {
        return $this->belongsToMany(Car::class, 'car_oils');
    }
}
