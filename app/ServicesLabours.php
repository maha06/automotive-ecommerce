<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServicesLabours extends Model
{
    protected $table = 'service_labours';
    protected $fillable = [
        'service_id', 'labour_id', 'price',
    ];
}
