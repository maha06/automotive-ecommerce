<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    protected $guarded = [];

    public function post()
    {
        return $this->belongsTo(SpareParts::class);
    }

    public function oil()
    {
        return $this->belongsTo(Oil::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
