<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    protected $guarded = [];

    public function workshop(){
        return $this->belongsTo(Workshop::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }
}
