<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Checkout extends Model
{
    protected $guarded = [];

    public function post()
    {
        return $this->belongsTo(SpareParts::class);
    }

    public function oil()
    {
        return $this->belongsTo(Oil::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function workshop()
    {
        return $this->belongsTo(Workshop::class);
    }

    public function details()
    {
        return $this->belongsToMany(CheckoutDetails::class);
    }
}
