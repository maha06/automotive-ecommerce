<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $guarded = [];

    public function workshop()
    {
        return $this->belongsTo(Workshop::class);
    }

    public function car()
    {
        return $this->belongsTo(Car::class);
    }

    public function model()
    {
        return $this->belongsTo(CarModel::class);
    }

    public function carts()
    {
        return $this->hasMany(Cart::class);
    }

    public function checkouts()
    {
        return $this->hasMany(Checkout::class);
    }
}
