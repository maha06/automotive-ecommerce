<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CheckoutDetails extends Model
{
    protected $table = 'checkout_details';
    protected $fillable = ['checkout_id', 'part_id', 'oil_id', 'quantity'];

    public function part()
    {
        return $this->belongsTo(SpareParts::class);
    }

    public function oil()
    {
        return $this->belongsTo(Oil::class);
    }

}
