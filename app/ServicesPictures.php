<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServicesPictures extends Model
{
    protected $table = 'services_pictures';
    protected $fillable = [
        'service_id', 'path',
    ];
}
