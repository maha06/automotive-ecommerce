<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Car extends Model
{
    protected $guarded = [];

    public function models()
    {
        return $this->hasMany(CarModel::class);
    }

    public function posts()
    {
        return $this->hasMany(Post::class);
    }

    public function oils(){
        return $this->belongsToMany(Oil::class, 'car_oils');
    }
}
