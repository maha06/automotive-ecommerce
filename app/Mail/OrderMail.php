<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class OrderMail extends Mailable
{
    use Queueable, SerializesModels;
    public $status;
    public $order;

    /**
     * Create a new message instance.
     *
     * @param $status
     */
    public function __construct($status, $order)
    {
        //
        $this->status = $status;
        $this->order = $order;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.order')
        ->subject('Your Booking Request is '. $this->status)
        ->from('admin@mechon.pk');
    }
}
