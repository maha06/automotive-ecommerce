<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ApprovalMail extends Mailable
{
    use Queueable, SerializesModels;
    public $status;
    public $booking;

    /**
     * Create a new message instance.
     *
     * @param $status
     */
    public function __construct($status, $booking)
    {
        //
        $this->status = $status;
        $this->booking = $booking;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.approval')
        ->subject('Your Maintenance Request is '. $this->status)
        ->from('admin@mechon.pk');
    }
}
