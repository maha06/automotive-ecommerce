<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CarModel extends Model
{
    public $table = "models";
    protected $guarded = [];

    public function car()
    {
        return $this->belongsTo(Car::class);
    }

    public function posts()
    {
        return $this->hasMany(Post::class);
    }
}
