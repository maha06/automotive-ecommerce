<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Workshop extends Authenticatable
{
    use Notifiable;

    protected $guard = 'workshop-user';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'password', 'phone', 'address', 'name', 'slug', 'no_of_mechanics', 'no_of_electrician', 'no_of_denter', 'no_of_painter', 'no_of_bays', 'major_tools_and_equipments', 'location', 'image',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function posts()
    {
        return $this->hasMany(Post::class);
    }

    public function services()
    {
        return $this->belongsToMany(Service::class, 'service_workshops');
    }

    public function reviews()
    {
        return $this->hasMany(Review::class);
    }

    public function orders()
    {
        return $this->hasMany(Checkout::class);
    }

    public function pictures()
    {
        return $this->hasMany(WorkshopsPictures::class);
    }

}
