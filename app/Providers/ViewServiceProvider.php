<?php

namespace App\Providers;

use App\ImageManager;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;
use App\GeneralSetting;
use Illuminate\Support\Facades\Auth;

class ViewServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer(['layouts.*', '*'], function ($view) {
            $settings = GeneralSetting::first();
            $view->with(compact('settings'));
        });
    }
}
