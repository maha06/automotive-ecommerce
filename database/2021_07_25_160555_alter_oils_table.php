<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterOilsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('oils', function (Blueprint $table) {
            
            $table->string('brand')->nullable();
            $table->integer('wholesale_price')->default(0);
            $table->integer('retail_price')->default(0);
            $table->string('oil_rate')->nullable();
            $table->integer('bulk_orders_retail_price')->default(0);
            $table->integer('discount')->default(0);
            $table->string('suggestions')->nullable();
            $table->text('description')->nullable()->change();
            $table->dropColumn('price');
            $table->dropColumn('image');
       

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
