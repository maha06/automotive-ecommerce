<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCarOilsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('car_oils', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('oil_id');
            $table->unsignedBigInteger('car_id');
            $table->foreign('oil_id')->references('id')->on('oils')->onDelete('cascade');
            $table->foreign('car_id')->references('id')->on('cars')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('car_oils');
    }
}
