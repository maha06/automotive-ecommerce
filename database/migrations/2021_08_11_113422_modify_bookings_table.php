<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ModifyBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bookings', function (Blueprint $table) {
            $table->string('make')->nullable()->change();
            $table->string('model')->nullable()->change();
            $table->string('mileage')->nullable()->change();
            $table->string('registration_number')->nullable()->change();
            $table->string('parts_deliver')->nullable()->change();
            $table->string('pickup')->nullable()->change();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
