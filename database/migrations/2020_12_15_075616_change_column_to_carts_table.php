<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeColumnToCartsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('carts', function (Blueprint $table) {
            $table->unsignedBigInteger('post_id')->nullable()->change();
            $table->unsignedBigInteger('oil_id')->nullable();
            $table->foreign('oil_id')->references('id')->on('oils')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('carts', function (Blueprint $table) {
            $table->dropForeign('carts_oil_id_foreign');
            $table->dropColumn('oil_id');
            $table->dropColumn('post_id');
        });
    }
}
