<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeMechanicIdToPostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('posts', function (Blueprint $table) {
            $table->dropForeign('posts_mechanic_id_foreign');
            $table->dropUnique('posts_name_mechanic_id_unique');
            $table->dropColumn('mechanic_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('posts', function (Blueprint $table) {
            $table->unsignedBigInteger('mechanic_id')->change();
            $table->foreign('mechanic_id')->references('id')->on('mechanics')->onDelete('cascade');
            $table->unsignedBigInteger('mechanic_id');
        });
    }
}
