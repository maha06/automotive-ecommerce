<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPhoneFieldToMechanicsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mechanics', function (Blueprint $table) {
            $table->string('phone');
            $table->string('name');
            $table->string('slug');
            $table->string('address')->change();
            $table->integer('status')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mechanics', function (Blueprint $table) {
            $table->dropColumn('phone');
            $table->dropColumn('name');
            $table->dropColumn('slug');
            $table->string('address')->nullable();
            $table->dropColumn('status');
        });
    }
}
