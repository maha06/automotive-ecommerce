<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('workshop_id');
            $table->string('make');
            $table->string('model');
            $table->string('year');
            $table->string('registration_number');
            $table->string('customer_name');
            $table->text('address');
            $table->string('city');
            $table->string('available_date')->nullable();
            $table->string('available_time')->nullable();
            $table->string('pickup_time')->nullable();
            $table->string('pickup_address')->nullable();
            $table->string('pickup_contact')->nullable();
            $table->foreign('workshop_id')->references('id')->on('workshops')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bookings');
    }
}
