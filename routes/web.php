<?php

use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/cache', function () {
    Artisan::call('config:cache');
    Artisan::call('cache:clear');
    Artisan::call('route:clear');
    return "<h1>Cache Clear</h1>";
});

Route::get('/home-repairing-services', 'HomeServiceController@index')->name('home-services.index')->middleware('auth');
Route::post('/home-services/home-repairing-services', 'HomeServiceController@HomeRepairingServices')->middleware('auth');
Route::post('/home-services/validate-step-one', 'HomeServiceController@validateStepOne')->middleware('auth');
Route::post('/home-services/validate-step-two', 'HomeServiceController@validateStepTwo')->middleware('auth');
Route::post('/home-services/validate-step-three', 'HomeServiceController@validateStepThree')->middleware('auth');
Route::post('/home-services/validate-step-four', 'HomeServiceController@validateStepFour')->middleware('auth');
Route::post('/home-services/validate-step-five', 'HomeServiceController@validateStepFive')->middleware('auth');
Route::get('sendSMS', 'TwilioSMSController@index')->name('sms');

Route::get('/oils', 'EngineOilController@index')->name('engine-oil.index');
Route::post('/add-to-cart/oil/{id}', 'EngineOilController@addOil')->middleware('auth');
Route::get('/get-cart/', 'CartController@getCart');

// Route::post('/engine-oil/engine-oil-purchase', 'EngineOilController@engineOilPurchase')->middleware('auth');
// Route::post('/engine-oil/validate-step-one', 'EngineOilController@validateStepOne')->middleware('auth');

Route::post('/workshop/reviews', 'PageController@reviewsSubmit')->middleware('auth');
Route::post('/workshop/reviews/{workshop}', 'PageController@reviewsShow');

Route::post('/add-to-cart/spare-part/{id}', 'CartController@addSparePart')->middleware('auth');
Route::post('/change-up-cart/price/{cart}', 'CartController@changeUpCartPrice')->middleware('auth');
Route::post('/change-down-cart/price/{cart}', 'CartController@changeDownCartPrice')->middleware('auth');
Route::post('/remove/cart/{cart}', 'CartController@removeCart')->middleware('auth');
Route::get('/cart', 'CartController@index')->name('pages.cart')->middleware('auth');
Route::get('/cart/checkout', 'CartController@cartCheckout')->name('pages.cart-checkout')->middleware('auth');
Route::post('/cart/checkout/place-order', 'CartController@placeOrder')->name('pages.place-order')->middleware('auth');

Route::get('/', 'PageController@index')->name('pages.home');
Route::get('/about', 'PageController@about')->name('pages.about');
Route::get('/customer-complaints', 'PageController@customerComplaints')->name('pages.customer-complaints');
Route::post('/customer-complaints/submit', 'PageController@complaintsSubmit')->name('pages.complaints.submit');
Route::get('/emergency-towing-repairing', 'PageController@emergencyTowing')->name('pages.emergency-towing')->middleware('auth');
Route::post('/emergency-towing-repairing/request', 'PageController@emergencyTowingRequest')->name('pages.emergency-towing.request')->middleware('auth');

Route::get('/workshops', 'PageController@workshops')->name('pages.frontend.workshops');
Route::get('/workshop/{slug}/details', 'PageController@workshopDetails')->name('pages.frontend.workshop-details');
Route::post('/search/workshops', 'PageController@searchWorkshops')->name('pages.search.workshops');
Route::get('/spare-parts', 'PageController@spareParts')->name('pages.frontend.spare-parts');
Route::get('/spare-parts/{slug}/details', 'PageController@sparePartDetails')->name('pages.frontend.spare-part-details');
//Route::get('/oils', 'PageController@oils')->name('pages.frontend.oils');
Route::get('/oils/{slug}/details', 'PageController@oilDetails')->name('pages.frontend.oil-details');
Route::get('/services', 'PageController@services')->name('pages.frontend.services');
Route::get('/service/{slug}/details', 'PageController@serviceDetails')->name('pages.frontend.service-details');

Route::post('/search/spare-parts', 'PageController@searchSpareParts')->name('pages.search.spare-parts');
Route::get('/contact-us', 'PageController@contactUs')->name('pages.contact-us');
Route::get('/service-repairs-maintenance', 'PageController@serviceRepairsMaintenance')->name('pages.maintenance');
Route::post('/vehicle-information', 'PageController@vehicleInformation');
Route::post('/validate-step-one', 'PageController@validateStepOne');
Route::post('/validate-step-two', 'PageController@validateStepTwo');
Route::post('/validate-step-three', 'PageController@validateStepThree');
Route::post('/validate-step-four', 'PageController@validateStepFour');
Route::post('/validate-step-five', 'PageController@validateStepFive');
Route::Post('/get-all/models/{id}', 'PageController@getAll');
Route::Post('/get-all/spare-parts/{id}', 'PageController@getAllParts');
Route::Post('/get-all/workshop/spare-parts', 'PageController@getAllWorkshopParts');
Route::Post('/get/workshop/{workshop}/car/{car}/model/{id}', 'PageController@getWorkshopCarModel');
Route::Post('/get-all/workshop/model/spare-parts/{id}', 'PageController@getAllModelParts');
Route::Post('/get-all/workshop/model/spare-parts', 'PageController@getAllWorkshopParts');
Route::Post('/get-all/price/spare-parts', 'PageController@getAllPriceParts');
Route::Post('/get-all/workshop/models/{model}/cars/{car}/spare-parts', 'PageController@getAllWorkshopModelCarParts');

Route::Post('/get-all/workshop/{city}', 'PageController@getAllWorkshopCities');
Route::Post('/get-all/workshop', 'PageController@getAllWorkshop');
Route::Post('/get-all/services/{workshop}', 'PageController@getAllServices');
Route::Post('/get-all/car/models/{id}', 'PageController@getAllCarModels');

Auth::routes();
Route::post('workshop/register', 'Auth\RegisterController@workshopRegister')->name('workshop.register');

Route::group(['prefix' => 'admin', "namespace" => "AdminAuth", "as" => "admin."], function () {
    Route::get('/', function () {
        return redirect(route("admin.login"));
    });
    Route::get('login', 'LoginController@showLoginForm')->name('login');
    Route::post('login', 'LoginController@login')->name('login');
    Route::post('/password/email', 'ForgotPasswordController@sendResetLinkEmail')->name('password.email');
    Route::get('/password/reset', 'ForgotPasswordController@showLinkRequestForm')->name('password.request');
    Route::post('/password/reset', 'ResetPasswordController@reset')->name('password.update');
    Route::get('password/reset/{token}', 'ResetPasswordController@showResetForm')->name('password.reset');
});

Route::group(['prefix' => 'admin', "middleware" => "auth:admin", "namespace" => "AdminAuth", "as" => "admin."], function () {
    Route::get('/change-password', 'ChangePasswordController@changePassword')->name('change-password.edit');
    Route::put('/change-password', 'ChangePasswordController@updatePassword')->name('change-password.update');
    Route::post('logout', 'LoginController@logout')->name('logout');
});

Route::group(['prefix' => 'admin', 'middleware' => 'auth:admin', "namespace" => "Admin"], function () {
    Route::get('/', 'PageController@index')->name('admin.pages.home');
    Route::post('/filter', 'PageController@filter')->name('admin.home.filter');

    Route::get('/complaints', 'PageController@complaints')->name('complaints');
    Route::get('/emergencies-towing', 'EmergencyController@index')->name('emergencies-towing.index');

    Route::resource('workshops', 'WorkshopController');
    Route::get('/workshop/change-password', 'WorkshopController@changePassword')->name('admin.workshops.change-password');
    Route::put('/workshop/update-password', 'WorkshopController@updatePassword')->name('admin.workshops.update-password');
    Route::get('/workshop/{id}/set-status/{status}', 'WorkshopController@setStatus')->name('admin.workshop-status');

    Route::resource('reviews', 'ReviewController');
    Route::resource('oils', 'OilController');
    Route::get('/oil/image-delete/{oil}/{image}', 'OilController@delete')->name('oil.picture.delete');

    Route::get('/service/image-delete/{service}/{image}', 'ServiceController@delete')->name('service.picture.delete');

    Route::resource('services', 'ServiceController');
    Route::resource('bookings', 'BookingController');
    Route::resource('orders', 'OrderController');
    Route::resource('cars', 'CarController');
    Route::resource('models', 'ModelController');
    Route::resource('oils-dealers', 'OilsDealerController');
    Route::resource('parts-dealers', 'PartsDealerController');
    Route::resource('labours', 'LabourController');

    Route::post('/order/status', "OrderController@status")->name("order.status");
    Route::post('/booking/status', "BookingController@status")->name("booking.status");

    Route::resource('parts', 'SparePartsController');
    Route::get('/part/image-delete/{part}/{image}', 'SparePartsController@delete')->name('part.picture.delete');

    Route::resource('users', 'UserController');
    Route::get('/{id}/set-status/{status}', 'UserController@setStatus')->name('admin.user-status');
    Route::get('/user/{user}/change-password', 'UserController@changePassword')->name('admin.users.change-password');
    Route::put('/user/{user}/update-password', 'UserController@updatePassword')->name('admin.users.update-password');
    // Contact Inquiries
    Route::get('/general-settings/create', "GeneralSettingController@create")->name("general-settings.create");
    Route::post('/general-settings', "GeneralSettingController@store")->name("general-settings.store");
    Route::get('/general-settings/edit', "GeneralSettingController@edit")->name("general-settings.edit");
    Route::put('/general-settings', "GeneralSettingController@update")->name("general-settings.update");
    Route::resource('contact-enquiries', 'ContactEnquiryController');

});

Route::group(['prefix' => 'workshop'], function () {
    Route::get('/', function () {
        return redirect(route("workshop.login"));
    });
    Route::get('login', 'WorkshopAuth\LoginController@showLoginForm')->name('workshop.login');
    Route::post('login', 'WorkshopAuth\LoginController@login')->name('workshop.login');
    Route::post('/password/email', 'WorkshopAuth\ForgotPasswordController@sendResetLinkEmail')->name('workshop.password.email');
    Route::get('/password/reset', 'WorkshopAuth\ForgotPasswordController@showLinkRequestForm')->name('workshop.password.request');
    Route::post('/password/reset', 'WorkshopAuth\ResetPasswordController@reset')->name('workshop.password.update');
    Route::get('password/reset/{token}', 'WorkshopAuth\ResetPasswordController@showResetForm')->name('workshop.password.reset');
});

Route::group(['prefix' => 'workshop', 'middleware' => 'auth:workshop-user'], function () {
    Route::get('/overview', 'Workshop\BackendController@index')->name('workshop.index');
    // Route::get('/complaint', 'Workshop\BackendController@complaints')->name('complaint');

    Route::resource('booking', 'Workshop\BookingController');
    Route::get('/booking/{booking}/set-status/{status}', 'Workshop\BookingController@setStatus');
    Route::get('/booking/{booking}/edit/profile', 'Workshop\BookingController@editProfile')->name('workshop.edit.profile');
    Route::post('/booking/{booking}/update/profile', 'Workshop\BookingController@updateProfile')->name('workshop.update.profile');

    Route::resource('order', 'Workshop\OrderController');
    Route::get('/order/{id}/set-status/{status}', 'Workshop\OrderController@setStatus');

    Route::resource('review', 'Workshop\ReviewController');
    Route::resource('posts', 'Workshop\PostController');
    Route::get('/posts/image-delete/{post}/{image}', 'Workshop\PostController@delete')->name('post.delete');
    Route::post('/get-all/models/{id}', 'Workshop\BackendController@getAll');
    // Route::get('/users', 'Workshop\BackendController@user')->name('workshop.users');

    Route::get('/profile/{workshop}', 'Workshop\BackendController@editProfile')->name('workshop.profile.edit');
    Route::put('/profile/{workshop}', 'Workshop\BackendController@updateProfile')->name('workshop.profile.update');

    Route::get('/change-password', 'Workshop\ChangePasswordController@changePassword')->name('workshop.change-password.edit');
    Route::put('/change-password', 'Workshop\ChangePasswordController@updatePassword')->name('workshop.change-password.update');
    Route::post('logout', 'WorkshopAuth\LoginController@logout')->name('workshop.logout');
});

Route::get('/create-symlink', function () {
    $projectFolder = base_path() . '/../';
    // The file that you want to create a symlink of
    $source = $projectFolder . "/FhJfSkEKkshfpr/storage/app/public";
    // The path where you want to create the symlink of the above
    $destination = $projectFolder . "/storage";

    if (file_exists($destination)) {
        if (is_link($destination)) {
            return "<h1>Symlink already exists</h1>";
        }
    } else {
        symlink($source, $destination);
        return "<h1>Symlink created successfully</h1>";
    }
});

Route::get('/remove-symlink', function () {
    $projectFolder = base_path() . '/../';
    $destination = $projectFolder . "/storage";
    if (file_exists($destination)) {
        if (is_link($destination)) {
            unlink($destination);
            return "<h1>Removed symlink</h1>";
        }
    } else {
        return "<h1>Symlink does not exist</h1>";
    }
});
