<!doctype html>
<html>
<head>
  <!-- Google Web Fonts
  ================================================== -->

  <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900%7COverpass:300,400,600,700,800,900" rel="stylesheet">

  <!-- Basic Page Needs
  ================================================== -->

  <!--meta info-->
  <meta charset="utf-8">
  @yield('head')
  <meta name="csrf-token" content="{{ csrf_token() }}" />
  <!-- Mobile Specific Metas
  ================================================== -->
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

  <!-- Vendor CSS
  ============================================ -->

  <link rel="stylesheet" href="{{ asset('frontend/font/demo-files/demo.css') }}">

  <!-- CSS theme files
  ============================================ -->
  <link rel="stylesheet" href="{{ asset('frontend/css/bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ asset('frontend/css/fontello.css') }}">
  <link rel="stylesheet" href="{{ asset('frontend/css/owl.carousel.css') }}">
  <link rel="stylesheet" href="{{ asset('frontend/css/style.css') }}">
  <link rel="stylesheet" href="{{ asset('frontend/css/responsive.css') }}">
  <style>
    .lang-button .item-inner > a:after {
        font-family: none !important;
        content: none !important;
    }
    .select-area.tab-col-2 > * {
        width: 100%;
        margin-bottom: 10px;
    }
    .select-area.tab-col-3 > * {
        width: 100%;
    }
    .media-holder.type-2 {
        padding: 90px 0;
        min-height: 0px;
    }
    #header.header-fixed.sticky .logo-wrap .logo > img:last-child ,#header.header-fixed .logo-wrap .logo > img:last-child{
        width:189px;
        height:30px;
    }
    .forgot{
        margin-bottom:10px;
        text-align:right;
    }
    .forgot-password, .forgot-password:hover{
        color:#d51920;
    }
    .model-info > a:hover, .model-info > a {
        background: #ed1c24;
        color: white;
        text-decoration: none;
    }
    .button_cart, .button_cart:hover{
        background: #ed1c24;
        color: white;
        text-decoration: none;
        padding:10px;
    }
    .go-to-top {
        bottom: 4%;
    }
    .box-data{
        background: #d51920;
        color: white;
        padding: 10px;
        margin-bottom:15px !important;
        margin-right:50px;
    }
    .box-data h4{
        color:white;
    }
    .box-data h5{
        color:white;
    }
    .form-container{
        border: 2px solid #d51920;
        padding: 20px;
    }
    #progressbar > li.app-active {
        background: #d51920;
        color: #fff;
        border-color: #d51920;
    }
    #progressbar > li.app-active:before, #progressbar > li.app-active:after {
        border-left-color: #d51920;
    }
    .product {
        box-shadow: 0 0 17px 0 rgba(0, 0, 0, 0.1);
    }
    .table-type-1 tr:first-child > th, .table-type-2 tr > th {
        background: #d51920;
        color: white;
    }
    [class*="table-type"] table tr:last-child > td {
        border-bottom: 2px solid #d51920;
    }
    [class*="table-type"] table tr > td:first-child {
        border-left: 2px solid #d51920;
    }
    [class*="table-type"] table tr > td {
        border-top: 2px solid #d51920;
    }
    [class*="table-type"] table tr > td:last-child {
        border-right: 2px solid #d51920;
    }
    .button_cart, .button_cart:hover {
        background: #d51920;
        color: white;
        text-decoration: none;
        padding: 10px;
    }
    .content-element4:not(:last-child) {
        margin-bottom: 90px;
    }
    #progressbar > li {
        border: 2px solid #d51920;
    }
    #progressbar > li {
        color: black;
    }
    #progressbar > li span:before {
        background-color: #e2e5e5;
        color: black;
    }
    #progressbar > li:after , #progressbar > li:before {
        top: 0px;
        border-bottom-width: 0px;
        border-top-width: 0px;
        border-left: none;
        z-index: 0;
    }
    #progressbar > li:after, #progressbar > li:before {
        left: 0px;
        top: 0px;
        border: none;
        content: " ";
        height: 0;
        width: 0;
        position: absolute;
        pointer-events: none;
        z-index: 2;
    }

    #progressbar > li:before {
        border-bottom-width: 28px;
        border-top-width: 28px;
        border-left: 8px solid #fff;
        z-index: 3;
    }
    #progressbar > li.app-active:before, #progressbar > li.app-active:after {
        border-left-color: #d51920;
    }
    [class*="table-type"] table tr > th, [class*="table-type"] table tr.bg-cell > td {
        color: white;
        font-size: 16px;
        font-weight: normal;
    }
    .content-element3:not(:last-child) {
        margin-bottom: 30px;
    }
    [class*="table-type-1"] table tr  > td{
        color:black;
    }
    .products-holder.view-list .product .pricing-area:not(:last-child), .products-holder.view-grid .product .pricing-area:not(:last-child) {
        margin-bottom: 10px;
    }
    [class*="table-type-2"] table tr > td {
        color:black;
    }
    .settings-view-products .view-type .active, .settings-view-products .view-type *:hover {
        background-color: #d51920;
        color:white;
        border-bottom-color: #d51920;
    }
    ul.pagination {
        text-align: center;
        justify-content: center;
        width: 100%;
    }
    .pagination > li.active > a, .pagination > li:hover > a {
        background: #d51920;
        color:white !important;
        border-bottom-color: #d51920;
    }
    .auto-select-custom-select select{
        width: 100%;
        background: #f2f3f3;
        border-radius: 3px;
        height: 44px;
        padding: 10px 20px;
    }

    .auto-custom-select.auto-select .auto-selected-option {
        background: #e2e5e5;
    }
    .auto-custom-select .auto-options-list li:hover {
        background: #d51920;
        color: #fff;
    }
    input[type="checkbox"]:checked + label::before, input[type="radio"]:checked + label::before {
        background: #d51920;
    }
    .breadcrumbs-wrap{
        padding:20px 0;
    }
    .breadcrumbs-wrap{
        padding:10px 0;
    }

    #header:not(.header-2) .top-header {
        border-bottom: 2px solid #e2e5e5;
    }
    .tabs.style-2 .tabs-nav > li.workshop-details {
        border: none;
        padding: 8px 30px 6px;
        border-bottom: 2px solid transparent;
        border-radius: 0!important;
        letter-spacing: 1px;
    }
    li.workshop-details span{
        margin-right:10px;
    }

    .tabs .tabs-nav > li.workshop-details {
        display: block;
        font-family: 'Overpass', sans-serif;
        text-transform: uppercase;
        font-size: 14px;
        padding: 7px 30px 5px;
        color: #2a2c32;
        position: relative;
        border: 2px solid #31353c;
    }
    .bg-sidebar-item.style-2 form input.btn{
        background: #b1151b;
    }


    /* Rating Star Widgets Style */
    .rating-stars ul {
        list-style-type:none;
        padding:0;

        -moz-user-select:none;
        -webkit-user-select:none;
    }
    .rating-stars ul > li.star {
        display:inline-block;
    }

        /* Idle State of the stars */
    .rating-stars ul > li.star > i.licon {
        font-size:2.5em; /* Change the size of the stars */
        color:#ccc; /* Color on idle state */
    }

    .rating-stars .rating > li.star i {
        color: #FFCC36;
    }

    .rating-stars .rating > li.star.hover i {
        color: #b1151b;
    }

    .rating-stars .rating > li.star.selected i {
        color: #b1151b;
    }
    .rating-stars .rating > li.starSelected {
        display: inline-block;
    }
    .rating-stars .rating > li.starSelected.selected i {
        color: #b1151b;
    }
    .rating-stars .rating > li {
        font-size: 40px;
        margin-right:5px;
    }
    .content-element:not(:last-child) {
        margin-bottom: 10px;
    }

    .bg-sidebar-item {
        padding: 30px 0;
    }

    @media only screen and (min-width: 1499px)
    {
        .logo-top {
            padding: 20px 0px;
        }
    }
    .products-holder.view-grid .product .product-name {
        min-height: 20px;
    }
    [class*="page-section"] {
        padding: 60px 0;
    }
    @media only screen and (max-width: 1499px){
        #header:not(.header-2):not(.header-3):not(.header-4) .top-header .logo-wrap {
            text-align: center;
            padding: 20px 40px;
            border-bottom: 2px solid #e2e5e5;
        }
    }

    .media-holder {
        position: relative;
        padding: 110px 0 50px;
        background-repeat: no-repeat;
        background-position: center;
        -webkit-background-size: cover;
        -moz-background-size: cover;
        -o-background-size: cover;
        background-size: cover;
        height: 100%;
        width: 100%;
    }
  </style>
</head>

<body>
      <div id="loader">
        <div class="box loading"></div>
      </div>
      <div id="wrapper" class="wrapper-container">
        <nav id="mobile-advanced" class="mobile-advanced"></nav>
        @include('partials.hero-section')
            @yield('content')
        @include('partials.footer')
      </div>
	<!-- JS Libs & Plugins
  ============================================ -->
  <script src="{{ asset('frontend/js/libs/jquery.modernizr.js') }}"></script>
  <script src="{{ asset('frontend/js/libs/jquery-2.2.4.min.js') }}"></script>
  <script src="{{ asset('frontend/js/libs/jquery-ui.min.js') }}"></script>
  <script src="{{ asset('frontend/js/libs/retina.min.js') }}"></script>
  <script src="{{ asset('frontend/plugins/mad.customselect.js') }}"></script>
  <script src="{{ asset('frontend/plugins/sticky-sidebar.js') }}"></script>
  <script src="https://maps.googleapis.com/maps/api/js?libraries=places&amp;key=AIzaSyBN4XjYeIQbUspEkxCV2dhVPSoScBkIoic"></script>
  <script src="{{ asset('frontend/plugins/owl.carousel.min.js') }}"></script>
  <script src="{{ asset('frontend/plugins/fancybox/jquery.fancybox.min.js') }}"></script>
  <script src="{{ asset('frontend/plugins/instafeed.min.js') }}"></script>
  <script src="{{ asset('frontend/plugins/twitter/jquery.tweet.js') }}"></script>
  <script src="{{ asset('plugins/bootstrap.js') }}"></script>

  <!-- JS theme files
  ============================================ -->
  <script src="{{ asset('frontend/js/plugins.js') }}"></script>
  <script src="{{ asset('frontend/js/script.js') }}"></script>
  @stack('scripts')
</body>

</html>
