<!doctype html>
<html>
<head>
  <!-- Google Web Fonts
  ================================================== -->

  <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900%7COverpass:300,400,600,700,800,900" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css">

  <!-- Basic Page Needs
  ================================================== -->
  <!--meta info-->
  <meta charset="utf-8">
  @yield('head')
  <meta name="csrf-token" content="{{ csrf_token() }}" />
  <!-- Mobile Specific Metas
  =========================
  ========================= -->
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

  <!-- Vendor CSS
  ============================================ -->

  <link rel="stylesheet" href="{{ asset('frontend/font/demo-files/demo.css') }}">

  <!-- CSS theme files
  ============================================ -->
  <link rel="stylesheet" href="{{ asset('frontend/css/bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ asset('frontend/css/fontello.css') }}">
  <link rel="stylesheet" href="{{ asset('frontend/css/owl.carousel.css') }}">
  <link rel="stylesheet" href="{{ asset('frontend/css/style.css') }}">
  <link rel="stylesheet" href="{{ asset('frontend/css/responsive.css') }}">
  <style>
    .lang-button .item-inner > a:after {
        font-family: none !important;
        content: none !important;
    }
    .select-area.tab-col-2 > * {
        width: 100%;
        margin-bottom: 10px;
    }
    .select-area.tab-col-3 > * {
        width: 100%;
    }
    .media-holder.type-2 {
        padding: 90px 0;
        min-height: 0px;
    }
    #header.header-fixed.sticky .logo-wrap .logo > img:last-child ,#header.header-fixed .logo-wrap .logo > img:last-child{
        width:189px;
        height:30px;
    }
    .forgot{
        margin-bottom:10px;
        text-align:right;
    }
    .forgot-password, .forgot-password:hover{
        color:#d51920;
    }
    .go-to-top {
        bottom: 4%;
    }
    .auto-select-custom-select select{
        width: 100%;
        background: white;
        border-radius: 3px;
        height: 44px;
        padding: 10px 20px;
    }

    .auto-custom-select.auto-select .auto-selected-option {
        background: #e2e5e5;
    }
    .auto-custom-select .auto-options-list li:hover {
        background: #d51920;
        color: #fff;
    }
    /* #header.header-fixed #main-navigation > ul > li > a {
        color: black;
    }


     */

    .content-element6:not(:last-child) {
        margin-bottom: 60px;
    }
    @media only screen and (min-width: 768px)
    {
        .media-holder {
            padding: 170px 0 80px;
        }
    }
    @media only screen and (min-width: 481px)
    {
        .media-holder {
            padding: 150px 0 100px !important;
        }
    }
    @media only screen and (max-width: 767px)
    {
        .media-holder {
            padding: 400px 0 100px;
        }
    }
    /* @media only screen and (min-width: 1281px){
        #header.header-fixed.sticky #main-navigation > ul > li > a {
            color: rgb(248, 223, 223);
        }
    } */
    /* @media only screen and (min-width: 1281px){
        #header.header-fixed.sticky .contact-info-menu .item-inner > a, #header.header-fixed.sticky .contact-info-menu > *.lang-button i {
            color: white;
        }
    } */
    #header.header-fixed #main-navigation > ul > li > a {
        color: black;
    }
    @media only screen and (min-width: 1281px){
        #header.header-fixed.sticky #main-navigation > ul > li > a {
            color: black;
        }
        #header.header-fixed.sticky .contact-info-menu .item-inner > a, #header.header-fixed.sticky .contact-info-menu > *.lang-button i {
            color: black;
        }
    }
    #header.header-fixed .contact-info-menu .item-inner > a, #header.header-fixed .contact-info-menu > *.lang-button i {
        color: black;
        font-weight: bold;
        letter-spacing: 0.5px;
    }
    [class*="flex-row"] > * {
        float: left;
    }
    @media only screen and (min-width: 1499px)
    {
        .logo-top {
            padding: 20px 0px;
        }
    }
    @media only screen and (max-width: 480px){
        .nav-item {
            width: 100%;
        }
    }

    @media only screen and (min-width: 767px){
        .menu-holder {
            border-bottom: 2px solid #e2e5e5;
        }
    }
    @media only screen and (max-width: 1499px){
        .menu-holder {
            border-bottom: 2px solid #e2e5e5;
        }
    }
    #header:not(.header-2) .top-header {
        padding: 0;
        border-bottom: none;
    }
    header {
        padding: 0 0 0 0px;
        border-bottom: none;
    }
    .top-header{
        padding: 0 0 0 0px;
    }
    .menu-holder{
        width:100%;
    }
    .logo-top {
        padding: 20px 0px;
    }
    #main-navigation > ul > li > a {
        padding: 30px 20px 25px;
    }
    @media only screen and (max-width: 1499px){
        #header:not(.header-2):not(.header-3):not(.header-4) .top-header .logo-wrap {
            text-align: center;
            padding: 20px 40px;
            border-bottom: 2px solid #e2e5e5;
        }
    }
    @media only screen and (min-width: 480px){
        .logo-wrap.logo-top{
            /* width: 100%; */
        }
    }
  </style>
</head>

<body>
      <div id="loader">
        <div class="box loading"></div>
      </div>
      <div id="wrapper" class="wrapper-container">
        <nav id="mobile-advanced" class="mobile-advanced"></nav>
        @include('partials.navigation')
            @yield('content')
        @include('partials.footer')
      </div>
	<!-- JS Libs & Plugins
  ============================================ -->

  <script src="{{ asset('frontend/js/libs/jquery.modernizr.js') }}"></script>
  <script src="{{ asset('frontend/js/libs/jquery-2.2.4.min.js') }}"></script>
  <script src="{{ asset('frontend/js/libs/jquery-ui.min.js') }}"></script>
  <script src="{{ asset('frontend/js/libs/retina.min.js') }}"></script>
  <script src="{{ asset('frontend/plugins/mad.customselect.js') }}"></script>
  <script src="{{ asset('frontend/plugins/sticky-sidebar.js') }}"></script>
  <script src="https://maps.googleapis.com/maps/api/js?libraries=places&amp;key=AIzaSyBN4XjYeIQbUspEkxCV2dhVPSoScBkIoic"></script>
  <script src="{{ asset('frontend/plugins/owl.carousel.min.js') }}"></script>
  <script src="{{ asset('frontend/plugins/fancybox/jquery.fancybox.min.js') }}"></script>
  <script src="{{ asset('frontend/plugins/instafeed.min.js') }}"></script>
  <script src="{{ asset('frontend/plugins/twitter/jquery.tweet.js') }}"></script>

  <!-- JS theme files
  ============================================ -->
  <script src="{{ asset('frontend/js/plugins.js') }}"></script>
  <script src="{{ asset('frontend/js/script.js') }}"></script>

  @stack('scripts')
</body>

</html>
