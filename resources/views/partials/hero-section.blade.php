<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css">
<header id="header">

    <!-- top-header -->

    <div class="top-header flex-row flex-justify flex-center">



      <!--main menu-->

      <div class="menu-holder" style="width:100%">

        <div class="menu-wrap flex-row">

          <!-- logo -->

          <div class="logo-wrap logo-top" style="border-bottom: 0px !important;">

            <a href="{{ route('pages.home') }}" class="logo"><img style="height:35px;" src="{{ asset('frontend/images/logo-straight-bk.png') }}" alt=""></a>

          </div>

          <div class="nav-item">

            <!-- - - - - - - - - - - - - - Navigation - - - - - - - - - - - - - - - - -->

            <nav id="main-navigation" class="main-navigation" style="text-align: center;">
              <ul id="menu" class="clearfix">
                <li><a href="{{ route('pages.home') }}">Home</a>
                </li>
                <li><a href="{{ route('pages.about') }}">About</a>
                </li>
                <li><a href="{{ route('pages.frontend.workshops') }}">Workshop</a>
                </li>
                <!-- <li><a href="{{ route('pages.frontend.spare-parts') }}">Spare Parts</a>
                </li> -->

                <li><a href="{{ route('pages.contact-us') }}">Contact us</a>
                </li>
                <li>   <a href="{{ route('pages.cart') }}"><i class="licon-cart" style="font-size:30px"></i><span class="badge badge-danger"  id="badge"></span></a>
                </li>

              </ul>
            </nav>

            <!-- - - - - - - - - - - - - end Navigation - - - - - - - - - - - - - - - -->

          </div>

          <div class="contact-info-menu">
            <div class="contact-info-item">
              <div class="flex-row flex-center">
                <i class="licon-telephone"></i>
                <div class="item-inner">
                  <span>Call Us Today</span>
                  <a href="callto:#"><b>0345-5544331</b></a>
                </div>
              </div>
            </div>

            <div class="contact-info-item lang-button">

              <div class="flex-row flex-center">
              <!-- <div class="item-inner" style="margin-right:5px">
                  Cart</a>
              </div> -->

                @auth
                    <i class="licon-user"></i>
                    <div class="item-inner">
                    <a href="#">{{ Auth::user()->name }}</a>
                    <ul class="dropdown-list">
                        <li><a href="#">Profile</a></li>
                        <li><a href="{{ route('pages.cart') }}">Cart</a></li>
                        <li>
                        <a href="{{ route('logout') }}"
                            onclick="event.preventDefault();document.getElementById('logout-form').submit();">Logout
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                        </li>
                    </ul>
                    </div>
                @else
                    <i class="licon-lock"></i>
                    <div class="item-inner">
                    <a href="{{ route('login') }}">Login</a>
                    </div>
                @endauth

              </div>

            </div>

          </div>

        </div>

      </div>

    </div>

  </header>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

  <script>
    $(document).ready(function(){

        var url = window.location.pathname.split("/")[1] === "staging"
                            ? "/staging/get-cart/"
                            : "http://localhost/mechon/public" + "/get-cart/";


        $.ajax({

            method: "POST",
            url: url,

            dataType: "json",
            success: function (data) {
                console.log(data);

                $('#badge').text(data);

            },
            error: function (request, status, error) {
                console.log(request);
            },
        });
    });

   </script>
