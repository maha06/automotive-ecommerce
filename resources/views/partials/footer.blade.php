<footer id="footer" class="footer">

    <!-- Top footer -->
    {{-- <div class="top-footer">

      <div class="container">

        <div class="row">

          <div class="col-md-3 col-sm-6">

            <div class="widget">

              <div class="content-element3">
                <h5 class="widget-title">Contact Us</h5>
                <p>
                  <span class="sub-title">Auto Trader Inc.</span>
                  9863 - 9867 Mill Road, Cambridge, <br> MG09 99HT
                </p>
              </div>
              <div class="content-element3">
                <ul class="contact-list">
                  <li><span>Phone:</span>800 603 6035</li>
                  <li><span>FAX:</span>800 889 9898 </li>
                  <li><span>E-mail:</span><a href="mailto:#" class="link-text">mail@companyname.com</a></li>
                </ul>
              </div>

            </div>

          </div>
          <div class="col-md-3 col-sm-6">

            <div class="widget">
              <div class="content-element3">
                <h5 class="widget-title">Opening Hours</h5>
                <p>
                  <span class="sub-title">Sales:</span>
                  Mon - Sat: 9:00 AM - 6:00 PM <br> Sunday is closed
                </p>
              </div>
              <div class="content-element3">
                <p>
                  <span class="sub-title">Service:</span>
                  Mon - Sat: 9:00 AM - 6:00 PM <br> Sunday is closed
                </p>
              </div>
            </div>

          </div>
          <div class="col-md-3 col-sm-6">

            <div class="widget">
              <h5 class="widget-title">Services</h5>
              <ul class="info-links">

                <li><a href="#">New Cars For Sale</a></li>
                <li><a href="#">Pre-owend Cars</a></li>
                <li><a href="#">Sell My Car</a></li>
                <li><a href="#">Car Valuations</a></li>
                <li><a href="#">Financing & Insurance</a></li>
                <li><a href="#">Service & Repair</a></li>

              </ul>
            </div>

          </div>
          <div class="col-md-3 col-sm-6">

            <div class="widget">
              <h5 class="widget-title">Connect With Us</h5>
              <ul class="social-icons">

                <li><a href="#"><i class="icon-facebook"></i></a></li>
                <li><a href="#"><i class="icon-twitter"></i></a></li>
                <li><a href="#"><i class="icon-youtube-play"></i></a></li>
                <li><a href="#"><i class="icon-gplus-3"></i></a></li>
                <li><a href="#"><i class="icon-instagram-5"></i></a></li>
                <li><a href="#"><i class="icon-linkedin-3"></i></a></li>

              </ul>
            </div>

            <div class="widget">
              <h5 class="widget-title">Newsletter Sign Up</h5>
              <form id="newsletter">
                <button type="submit" class="btn-email btn btn-style-2 f-right" data-type="submit"><i class="licon-envelope"></i></button>
                <div class="wrapper">
                  <input type="email" placeholder="Enter your email address" name="newsletter-email">
                </div>
              </form>
            </div>

          </div>

        </div>

      </div>
    </div> --}}

    <!-- Copyright -->
    <div class="copyright-section">

      <div class="container">

        <div class="flex-row flex-justify flex-center">

          <div class="copyright">Copyrights MechOn © 2020. All Rights Reserved</div>

          <ul class="info-links hr-type">
            <li><a href="#">About Us</a></li>
            <li><a href="#">Careers</a></li>
            <li><a href="#">FAQ</a></li>
            <li><a href="#">News & Tips</a></li>
            <li><a href="#">Site Map</a></li>
            <li><a href="#">Contact</a></li>
          </ul>

        </div>

      </div>

      {{-- <a href="#" class="btn btn-big btn-style-3"><i class="licon-bubbles"></i>Chat With Us</a> --}}

    </div>

  </footer>
