      <header id="header">

        <!-- top-header -->

        <div class="top-header flex-row flex-justify flex-center">



          <!--main menu-->

          <div class="menu-holder" style="width:100%">

            <div class="menu-wrap flex-row">

              <!-- logo -->

              <div class="logo-wrap logo-top" style="border-bottom: 0px !important;">

                <a href="{{ route('pages.home') }}" class="logo"><img style="height:35px;" src="{{ asset('frontend/images/logo-straight-bk.png') }}" alt=""></a>

              </div>

              <div class="nav-item">

                <!-- - - - - - - - - - - - - - Navigation - - - - - - - - - - - - - - - - -->

                <nav id="main-navigation" class="main-navigation" style="text-align: center;">
                  <ul id="menu" class="clearfix">
                    <li><a href="{{ route('pages.home') }}">Home</a>
                    </li>
                    <li><a href="{{ route('pages.about') }}">About</a>
                    </li>
                    <li><a href="{{ route('pages.frontend.workshops') }}">Workshop</a>
                    </li>
                    <li><a href="{{ route('pages.frontend.spare-parts') }}">Spare Parts</a>
                    </li>

                    <li><a href="{{ route('pages.contact-us') }}">Contact us</a>
                    </li>

                  </ul>
                </nav>

                <!-- - - - - - - - - - - - - end Navigation - - - - - - - - - - - - - - - -->

              </div>

              <div class="contact-info-menu">

                <div class="contact-info-item">

                  <div class="flex-row flex-center">

                    <i class="licon-telephone"></i>
                    <div class="item-inner">
                      <span>Call Us Today</span>
                      <a href="callto:#"><b>0345-5544331</b></a>
                    </div>

                  </div>

                </div>
                <div class="contact-info-item lang-button">

                  <div class="flex-row flex-center">

                    @auth
                        <i class="licon-user"></i>
                        <div class="item-inner">
                        <a href="#">Nabeel Amjad</a>
                        <ul class="dropdown-list">
                            <li><a href="#">Profile</a></li>
                            <li><a href="{{ route('pages.cart') }}">Cart</a></li>
                            <li>
                            <a href="{{ route('logout') }}"
                                onclick="event.preventDefault();document.getElementById('logout-form').submit();">Logout
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                            </li>
                        </ul>
                        </div>
                    @else
                        <i class="licon-lock"></i>
                        <div class="item-inner">
                        <a href="{{ route('login') }}">Login</a>
                        </div>
                    @endauth

                  </div>

                </div>

              </div>

            </div>

          </div>

        </div>

      </header>
