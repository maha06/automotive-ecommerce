@extends('workshop.layouts.layout')

@section('heading')
    <h4>
        <a href="{{ route('workshop.index') }}">
            <i class="icon-arrow-left52 mr-2"></i>
        </a>
        <span class="font-weight-semibold">Home - Workshop</span>
    </h4>
@endsection

@section('breadcrumbs')
    <div class="breadcrumb">
        <a href="{{ route('workshop.index') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
        <span class="breadcrumb-item">Workshop: {{ $workshop->name }}</span>
        <span class="breadcrumb-item active">Edit</span>
    </div>
@endsection
@section('content')
    @include('admin.partials.header')
    <div class="content">
        @include('common.partials.flash')
        <div class="card">
            <div class="card-header header-elements-inline">
                <h5 class="card-title">Workshop</h5>
            </div>
            <div class="card-body">
                <form action="{{ route('workshop.profile.update', $workshop->id) }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <div class="form-group">
                        <label class="font-weight-semibold @error('name') text-danger @enderror">Name <span
                                class="text-red">*</span></label>
                        <div class="form-group-feedback form-group-feedback-right">
                            <input type="text" name="name" value="{{ $workshop->name }}"
                                class="form-control @error('name') border-danger @enderror" required>
                            @error('name')
                            <div class="form-control-feedback text-danger">
                                <i class="icon-cancel-circle2"></i>
                            </div>
                            @enderror
                        </div>
                        @error('name')
                        <span class="form-text text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    @if(count($services))
                        <div class="form-group">
                        <label class="@error('service_id') text-danger @enderror">Select Services <span
                            class="text-danger">*</span>:</label>
                        <div class="form-group-feedback form-group-feedback-right">
                            <select name="service_id[]"
                                class="form-control multiselect-select-all-filtering @error('service_id') text-danger @enderror"
                                multiple="multiple">
                                @foreach ($services as $key => $service)
                                <option @foreach ($workshop->services as $service_id)
                                    @if($service->id == $service_id->id)
                                    selected="selected"
                                    @else
                                    ""
                                    @endif
                                    @endforeach

                                    value="{{ $service->id }}">
                                    {{ $service->name }}
                                </option>
                                @endforeach

                            </select>
                        </div>
                        @error('service_id')
                        <span class="form-text text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    @endif
                    <div class="form-group">
                        <label class="font-weight-semibold @error('phone') text-danger @enderror">Phone <span
                                class="text-red">*</span></label>
                        <div class="form-group-feedback form-group-feedback-right">
                            <input type="number" name="phone" value="{{ $workshop->phone }}"
                                class="form-control @error('phone') border-danger @enderror" required>
                            @error('phone')
                            <div class="form-control-feedback text-danger">
                                <i class="icon-cancel-circle2"></i>
                            </div>
                            @enderror
                        </div>
                        @error('phone')
                        <span class="form-text text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label style="display: block;"
                            class="font-weight-semibold @error('image') text-danger @enderror">Image <span
                                class="text-red">*</span></label>
                        <div class="d-inline-block" style="position: relative;margin-bottom: inherit;">
                        @if($workshop->image)
                                <div class="d-inline-block" style="position:relative;margin-bottom: inherit; ">
                                    <img src="{{ asset('/storage/'. $workshop->image) }}" height="200px" width="200px"/>
                                </div>
                        @else
                            <p>No Image Found</p>
                        @endif
                        <input type="file" accept=".jpeg,.png,.bmp,.gif,.svg,.webp,.jpg" name="image">
                        <span class="form-text text-muted">Accepted formats: jpeg, png, bmp, gif, svg, jpg, or webp</span>
                        @error('image')
                        <span class="form-text text-danger">{{ $message }}</span>
                        @enderror
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="font-weight-semibold @error('address') text-danger @enderror">Address <span
                                class="text-red">*</span></label>
                        <div class="form-group-feedback form-group-feedback-right">
                            <textarea name="address" class="form-control @error('address') border-danger @enderror">{{ $workshop->address }}</textarea>
                            @error('address')
                            <div class="form-control-feedback text-danger">
                                <i class="icon-cancel-circle2"></i>
                            </div>
                            @enderror
                        </div>
                        @error('address')
                        <span class="form-text text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label class="font-weight-semibold @error('location') text-danger @enderror">Location <span
                                class="text-red">*</span></label>
                        <div class="form-group-feedback form-group-feedback-right">
                            <input type="text" name="location" value="{{ $workshop->location }}"
                                class="form-control @error('location') border-danger @enderror" required>
                            @error('location')
                            <div class="form-control-feedback text-danger">
                                <i class="icon-cancel-circle2"></i>
                            </div>
                            @enderror
                        </div>
                        @error('location')
                        <span class="form-text text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label class="font-weight-semibold @error('no_of_mechanics') text-danger @enderror">Number of Mechanics <span
                                class="text-red">*</span></label>
                        <div class="form-group-feedback form-group-feedback-right">
                            <input type="number" name="no_of_mechanics" value="{{ $workshop->no_of_mechanics }}"
                                class="form-control @error('no_of_mechanics') border-danger @enderror" required>
                            @error('no_of_mechanics')
                            <div class="form-control-feedback text-danger">
                                <i class="icon-cancel-circle2"></i>
                            </div>
                            @enderror
                        </div>
                        @error('no_of_mechanics')
                        <span class="form-text text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label class="font-weight-semibold @error('no_of_electrician') text-danger @enderror">Number of Electrician <span
                                class="text-red">*</span></label>
                        <div class="form-group-feedback form-group-feedback-right">
                            <input type="number" name="no_of_electrician" value="{{ $workshop->no_of_electrician }}"
                                class="form-control @error('no_of_electrician') border-danger @enderror" required>
                            @error('no_of_electrician')
                            <div class="form-control-feedback text-danger">
                                <i class="icon-cancel-circle2"></i>
                            </div>
                            @enderror
                        </div>
                        @error('no_of_electrician')
                        <span class="form-text text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label class="font-weight-semibold @error('no_of_denter') text-danger @enderror">Number of Denter <span
                                class="text-red">*</span></label>
                        <div class="form-group-feedback form-group-feedback-right">
                            <input type="number" name="no_of_denter" value="{{ $workshop->no_of_denter }}"
                                class="form-control @error('no_of_denter') border-danger @enderror" required>
                            @error('no_of_denter')
                            <div class="form-control-feedback text-danger">
                                <i class="icon-cancel-circle2"></i>
                            </div>
                            @enderror
                        </div>
                        @error('no_of_denter')
                        <span class="form-text text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label class="font-weight-semibold @error('no_of_painter') text-danger @enderror">Number of Painters <span
                                class="text-red">*</span></label>
                        <div class="form-group-feedback form-group-feedback-right">
                            <input type="number" name="no_of_painter" value="{{ $workshop->no_of_painter }}"
                                class="form-control @error('no_of_painter') border-danger @enderror" required>
                            @error('no_of_painter')
                            <div class="form-control-feedback text-danger">
                                <i class="icon-cancel-circle2"></i>
                            </div>
                            @enderror
                        </div>
                        @error('no_of_painter')
                        <span class="form-text text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label class="font-weight-semibold @error('no_of_bays') text-danger @enderror">Number of Bays <span
                                class="text-red">*</span></label>
                        <div class="form-group-feedback form-group-feedback-right">
                            <input type="number" name="no_of_bays" value="{{ $workshop->no_of_bays }}"
                                class="form-control @error('no_of_bays') border-danger @enderror" required>
                            @error('no_of_bays')
                            <div class="form-control-feedback text-danger">
                                <i class="icon-cancel-circle2"></i>
                            </div>
                            @enderror
                        </div>
                        @error('no_of_bays')
                        <span class="form-text text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label class="font-weight-semibold @error('major_tools_and_equipments') text-danger @enderror">Major Tools & Equipments <span
                                class="text-red">*</span></label>
                        <div class="form-group-feedback form-group-feedback-right">
                            <textarea name="major_tools_and_equipments" class="form-control @error('major_tools_and_equipments') border-danger @enderror">{{ $workshop->major_tools_and_equipments }}</textarea>
                            @error('major_tools_and_equipments')
                            <div class="form-control-feedback text-danger">
                                <i class="icon-cancel-circle2"></i>
                            </div>
                            @enderror
                        </div>
                        @error('major_tools_and_equipments')
                        <span class="form-text text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="text-right">
                        <button type="submit" class="btn btn-primary">Submit <i class="icon-paperplane ml-2"></i></button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
