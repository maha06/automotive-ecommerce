@extends('workshop.layouts.layout')

@section('heading')
    <h4>
        <a href="{{ route('workshop.index') }}">
            <i class="icon-arrow-left52 mr-2"></i>
        </a>
        <span class="font-weight-semibold">Home - Booking</span> - Show
    </h4>
@endsection

@section('breadcrumbs')
<div class="breadcrumb">
    <a href="{{ route('workshop.index') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
    <span class="breadcrumb-item"><a href="{{ route('booking.index') }}"> Booking </a></span>
</div>
@endsection

@section('content')
@include('admin.partials.header')
    <div class="content">
        @include('common.partials.flash')
        <div class="card has-table">
            <div class="row">
                <div class="col-md-12">
                <fieldset class="mb-3 mt-3 ml-2">
                    <legend class="text-uppercase font-size-sm font-weight-bold mb-0" style="font-size:18px;">Customer Details</legend>
                </fieldset>
                </div>
                <div class="col-md-6">
                    <div class="card border-teal-400">
                        <div class="card-header bg-teal-400 text-white header-elements-inline">
                            <h6 class="card-title">Customer Name</h6>
                        </div>

                        <div class="card-body">
                            {{ $booking->customer_name }}
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card border-teal-400">
                        <div class="card-header bg-teal-400 text-white header-elements-inline">
                            <h6 class="card-title">Workshop Name</h6>
                        </div>

                        <div class="card-body">
                            {{ $booking->workshop->name }}
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card border-teal-400">
                        <div class="card-header bg-teal-400 text-white header-elements-inline">
                            <h6 class="card-title">Email</h6>
                        </div>

                        <div class="card-body">
                            {{ $booking->customer_email }}
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card border-teal-400">
                        <div class="card-header bg-teal-400 text-white header-elements-inline">
                            <h6 class="card-title">Mobile</h6>
                        </div>

                        <div class="card-body">
                            {{ $booking->mobile }}
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card border-teal-400">
                        <div class="card-header bg-teal-400 text-white header-elements-inline">
                            <h6 class="card-title">City</h6>
                        </div>

                        <div class="card-body">
                            {{ $booking->city }}
                        </div>
                    </div>
                </div>

            </div>
            <div class="row">
                <div class="col-md-12">
                    <fieldset class="mb-3 mt-3 ml-2">
                        <legend class="text-uppercase font-size-sm font-weight-bold mb-0" style="font-size:18px;">Car Details</legend>
                    </fieldset>
                </div>
                <div class="col-md-6">
                    <div class="card border-teal-400">
                        <div class="card-header bg-teal-400 text-white header-elements-inline">
                            <h6 class="card-title">Make</h6>
                        </div>

                        <div class="card-body">
                            {{ $booking->make }}
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card border-teal-400">
                        <div class="card-header bg-teal-400 text-white header-elements-inline">
                            <h6 class="card-title">Model</h6>
                        </div>

                        <div class="card-body">
                            {{ $booking->model }}
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card border-teal-400">
                        <div class="card-header bg-teal-400 text-white header-elements-inline">
                            <h6 class="card-title">Year</h6>
                        </div>

                        <div class="card-body">
                            {{ $booking->year }}
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card border-teal-400">
                        <div class="card-header bg-teal-400 text-white header-elements-inline">
                            <h6 class="card-title">Registration Number</h6>
                        </div>

                        <div class="card-body">
                            {{ $booking->registration_number }}
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <fieldset class="mb-3 mt-3 ml-2">
                        <legend class="text-uppercase font-size-sm font-weight-bold mb-0" style="font-size:18px;">Appointment Details</legend>
                    </fieldset>
                </div>
                <div class="col-md-6">
                    <div class="card border-teal-400">
                        <div class="card-header bg-teal-400 text-white header-elements-inline">
                            <h6 class="card-title">Available Date</h6>
                        </div>

                        <div class="card-body">
                            {{ $booking->available_date }}
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card border-teal-400">
                        <div class="card-header bg-teal-400 text-white header-elements-inline">
                            <h6 class="card-title">Available Time</h6>
                        </div>

                        <div class="card-body">
                            {{ $booking->available_time }}
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card border-teal-400">
                        <div class="card-header bg-teal-400 text-white header-elements-inline">
                            <h6 class="card-title">Pickup Time</h6>
                        </div>

                        <div class="card-body">
                            {{ $booking->pickup_time }}
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card border-teal-400">
                        <div class="card-header bg-teal-400 text-white header-elements-inline">
                            <h6 class="card-title">Pickup Address</h6>
                        </div>

                        <div class="card-body">
                            {{ $booking->pickup_address }}
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card border-teal-400">
                        <div class="card-header bg-teal-400 text-white header-elements-inline">
                            <h6 class="card-title">Pickup Contact</h6>
                        </div>

                        <div class="card-body">
                            {{ $booking->pickup_contact }}
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <fieldset class="mb-3 mt-3 ml-2">
                        <legend class="text-uppercase font-size-sm font-weight-bold mb-0" style="font-size:18px;">Services Details</legend>
                    </fieldset>
                </div>
                <div class="col-md-12">
                    <table class="table datatable-pagination">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Price</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($booking->services as $key=> $service)
                            <tr>
                                <td>{{$key+1}}</td>
                                <td>{{$service->name }}</td>
                                <td>{{$service->price }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <fieldset class="mb-3 mt-3 ml-2">
                        <legend class="text-uppercase font-size-sm font-weight-bold mb-0" style="font-size:18px;">Parts Details</legend>
                    </fieldset>
                </div>
                <div class="col-md-12">
                    <table class="table datatable-pagination">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Description</th>
                                <th>Company</th>
                                <th>Price</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($booking->posts as $key=> $post)
                            <tr>
                                <td>{{$key+1}}</td>
                                <td>{{$post->name }}</td>
                                <td>{{$post->description }}</td>
                                <td>{{$post->company }}</td>
                                <td>{{$post->price }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
