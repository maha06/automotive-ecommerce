@extends('workshop.layouts.layout')

@section('heading')
    <h4>
        <a href="{{ route('workshop.index') }}">
            <i class="icon-arrow-left52 mr-2"></i>
        </a>
        <span class="font-weight-semibold">Home - Workshop</span>
    </h4>
@endsection

@section('breadcrumbs')
    <div class="breadcrumb">
        <a href="{{ route('workshop.index') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
        <span class="breadcrumb-item">Workshop: {{ $booking->name }}</span>
        <span class="breadcrumb-item active">Edit</span>
    </div>
@endsection
@section('content')
    @include('admin.partials.header')
    <div class="content">
        @include('common.partials.flash')
        <div class="card">
            <div class="card-header header-elements-inline">
                <h5 class="card-title">Workshop</h5>
            </div>
            <div class="card-body">
                <form action="{{ route('workshop.update.profile', $booking->id) }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    {{-- @method('PUT') --}}
                    @if(count($services))
                        <div class="form-group">
                        <label class="@error('service_id') text-danger @enderror">Select Services <span
                            class="text-danger">*</span>:</label>
                        <div class="form-group-feedback form-group-feedback-right">
                            <select name="service_id[]"
                                class="form-control multiselect-select-all-filtering @error('service_id') text-danger @enderror"
                                multiple="multiple">
                                @foreach ($services as $key => $service)
                                <option @foreach ($booking->services as $service_id)
                                    @if($service->id == $service_id->id)
                                    selected="selected"
                                    @else
                                    ""
                                    @endif
                                    @endforeach

                                    value="{{ $service->id }}">
                                    {{ $service->name }}
                                </option>
                                @endforeach

                            </select>
                        </div>
                        @error('service_id')
                        <span class="form-text text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    @endif
                    @if(count($booking->posts) > 0)
                        @if(count($posts))
                            <div class="form-group">
                            <label class="@error('post_id') text-danger @enderror">Select Parts <span
                                class="text-danger">*</span>:</label>
                            <div class="form-group-feedback form-group-feedback-right">
                                <select name="post_id[]"
                                    class="form-control multiselect-select-all-filtering @error('post_id') text-danger @enderror"
                                    multiple="multiple">
                                    @foreach ($posts as $key => $post)
                                    <option @foreach ($booking->posts as $post_id)
                                        @if($post->id == $post_id->id)
                                        selected="selected"
                                        @else
                                        ""
                                        @endif
                                        @endforeach

                                        value="{{ $post->id }}">
                                        {{ $post->name }}
                                    </option>
                                    @endforeach

                                </select>
                            </div>
                            @error('post_id')
                            <span class="form-text text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        @endif

                    @endif
                    <div class="text-right">
                        <button type="submit" class="btn btn-primary">Submit <i class="icon-paperplane ml-2"></i></button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
