@extends('workshop.layouts.layout')

@section('heading')
    <h4>
        <a href="{{ route('admin.pages.home') }}">
            <i class="icon-arrow-left52 mr-2"></i>
        </a>
        <span class="font-weight-semibold">Home - Order</span> - Show
    </h4>
@endsection

@section('breadcrumbs')
<div class="breadcrumb">
    <a href="{{ route('admin.pages.home') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
    <span class="breadcrumb-item"><a href="{{ route('orders.index') }}"> Order </a></span>
</div>
@endsection

@section('content')
@include('admin.partials.header')
    <div class="content">
        @include('common.partials.flash')
        <div class="card has-table">

            <div class="row">
                <div class="col-md-12">
                <fieldset class="mb-3 mt-3 ml-2">
                    <legend class="text-uppercase font-size-sm font-weight-bold mb-0" style="font-size:18px;">Billing Details</legend>
                </fieldset>
                </div>
                <div class="col-md-6">
                    <div class="card border-teal-400">
                        <div class="card-header bg-teal-400 text-white header-elements-inline">
                            <h6 class="card-title">Customer Name</h6>
                        </div>

                        <div class="card-body">
                            {{ $order->name }}
                        </div>
                    </div>
                </div>
                @if ($order->post_id)
                    <div class="col-md-6">
                        <div class="card border-teal-400">
                            <div class="card-header bg-teal-400 text-white header-elements-inline">
                                <h6 class="card-title">Workshop Name</h6>
                            </div>

                            <div class="card-body">
                                {{ $order->post->workshop->name }}
                            </div>
                        </div>
                    </div>

                @endif
                <div class="col-md-6">
                    <div class="card border-teal-400">
                        <div class="card-header bg-teal-400 text-white header-elements-inline">
                            <h6 class="card-title">Name</h6>
                        </div>

                        <div class="card-body">
                            @if ($order->post_id)
                                {{ $order->post->name }}
                            @else
                                {{ $order->oil->name }}
                            @endif
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card border-teal-400">
                        <div class="card-header bg-teal-400 text-white header-elements-inline">
                            <h6 class="card-title">Quantity</h6>
                        </div>

                        <div class="card-body">
                            {{ $order->quantity }}
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card border-teal-400">
                        <div class="card-header bg-teal-400 text-white header-elements-inline">
                            <h6 class="card-title">Price</h6>
                        </div>

                        <div class="card-body">
                            {{ $order->price }}
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="card border-teal-400">
                        <div class="card-header bg-teal-400 text-white header-elements-inline">
                            <h6 class="card-title">Email</h6>
                        </div>

                        <div class="card-body">
                            {{ $order->email }}
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card border-teal-400">
                        <div class="card-header bg-teal-400 text-white header-elements-inline">
                            <h6 class="card-title">Mobile</h6>
                        </div>

                        <div class="card-body">
                            {{ $order->phone }}
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card border-teal-400">
                        <div class="card-header bg-teal-400 text-white header-elements-inline">
                            <h6 class="card-title">Address</h6>
                        </div>

                        <div class="card-body">
                            {{ $order->address }}
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card border-teal-400">
                        <div class="card-header bg-teal-400 text-white header-elements-inline">
                            <h6 class="card-title">Apartment</h6>
                        </div>

                        <div class="card-body">
                            {{ $order->apartment }}
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card border-teal-400">
                        <div class="card-header bg-teal-400 text-white header-elements-inline">
                            <h6 class="card-title">City</h6>
                        </div>

                        <div class="card-body">
                            {{ $order->city }}
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card border-teal-400">
                        <div class="card-header bg-teal-400 text-white header-elements-inline">
                            <h6 class="card-title">Order Notes</h6>
                        </div>

                        <div class="card-body">
                            {{ $order->order_notes }}
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                <fieldset class="mb-3 mt-3 ml-2">
                    <legend class="text-uppercase font-size-sm font-weight-bold mb-0" style="font-size:18px;">Customer Details</legend>
                </fieldset>
                </div>
                <div class="col-md-6">
                    <div class="card border-teal-400">
                        <div class="card-header bg-teal-400 text-white header-elements-inline">
                            <h6 class="card-title">Name</h6>
                        </div>

                        <div class="card-body">
                            {{ $order->user->name }}
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card border-teal-400">
                        <div class="card-header bg-teal-400 text-white header-elements-inline">
                            <h6 class="card-title">Email</h6>
                        </div>

                        <div class="card-body">
                            {{ $order->user->email }}
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card border-teal-400">
                        <div class="card-header bg-teal-400 text-white header-elements-inline">
                            <h6 class="card-title">Mobile</h6>
                        </div>

                        <div class="card-body">
                            {{ $order->user->phone }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
