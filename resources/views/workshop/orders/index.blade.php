@extends('workshop.layouts.layout')

@section('heading')
    <h4>
        <a href="{{ route('workshop.index') }}">
            <i class="icon-arrow-left52 mr-2"></i>
        </a>
        <span class="font-weight-semibold">Home - Orders</span> - View All
    </h4>
@endsection

@section('breadcrumbs')
<div class="breadcrumb">
    <a href="{{ route('workshop.index') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
    <span class="breadcrumb-item"><a href="{{ route('order.index') }}"> Orders </a></span>
    <span class="breadcrumb-item active">View All</span>
</div>
@endsection

@section('content')
@include('admin.partials.header')
    <div class="content">
        @include('common.partials.flash')
        <div class="card has-table">
            <div class="card-header header-elements-inline">
                <h5 class="card-title">Orders</h5>
            </div>
            <table class="table datatable-basic">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Customer Name</th>
                        <th> Name</th>
                        <th>Phone</th>
                        <th>Quantity</th>
                        <th>Total Price</th>
                        <th>Status</th>
                        <th>Created At</th>
                        <th class="text-center">Actions</th>
                    </tr>
                </thead>
                <tbody>
                @forelse($orders as $key=> $order)
                    <tr>
                        <td>{{$key+1}}</td>
                        <td>{{$order->name }}</td>
                        @if ($order->post_id)
                            <td>{{$order->post->name }}</td>
                        @else
                            <td>{{ $order->oil->name }}</td>
                        @endif
                        <td>{{$order->phone }}</td>
                        <td>{{$order->quantity }}</td>
                        <td>{{ $order->price }}</td>
                        <td class="data">
                            <select data-id="{{ $order->id }}" class="activeInactive form-control" name="is_active"
                                id="is_active">
                                    <option {{ $order->status == 0 ? 'selected' : '' }} value="0" name="hidden_is_active" class="hidden_is_active" id="hidden_is_active">Pending</option>
                                    <option {{ $order->status == 1 ? 'selected' : '' }} value="1" name="hidden_is_active" class="hidden_is_active" id="hidden_is_active" >Accepted</option>
                                    <option {{ $order->status == 2 ? 'selected' : '' }} value="2" name="hidden_is_active" class="hidden_is_active" id="hidden_is_active">Completed</option>
                                    <option {{ $order->status == 3 ? 'selected' : '' }} value="3" name="hidden_is_active" class="hidden_is_active" id="hidden_is_active">Cancelled</option>
                                </select>
                            <input type="hidden" class="order_id" name="order_id" id="order_id"
                                value="{{ $order->id }}" />
                        </td>
                        <td>{{$order->created_at }}</td>
                        <td class="text-center">
                            <div class="list-icons">
                                <div class="dropdown">
                                    <a href="#" class="list-icons-item" data-toggle="dropdown">
                                        <i class="icon-menu9"></i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        <a href="{{ route('order.show', [$order->id]) }}" class="dropdown-item">
                                            <i class="icon-eye"></i>
                                            Show
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                @empty
                <tr>
                    <td colspan="12">
                        <div class="alert alert-info text-center">
                            No Orders Recieved So Far
                            <br>
                        </div>
                    </td>
                </tr>
                @endforelse
                </tbody>
            </table>
            {!! $orders->links() !!}
        </div>
    </div>
@endsection

@push('scripts')
    <script src="{{asset('backend/js/plugins/tables/datatables/datatables.min.js')}}"></script>
    <script src="{{asset('backend/js/demo_pages/datatables_basic.js')}}"></script>
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
@endpush
@push('scripts')
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <script>
        $('.activeInactive').change(function(is_active){
            var order_id = this.getAttribute('data-id');
            var url = window.location.pathname.split("/")[1] === "staging"
                            ? "/staging/workshop/order/" + order_id + "/set-status/" + is_active.target.value
                            : "" + "/workshop/order/" + order_id + "/set-status/" + is_active.target.value;

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                method: 'get',
                url: url,
                dataType: 'json',
                success:function(data){
                    if(data == 'done')
                    {
                    $('#is_active').bootstrapToggle('on');
                    alert("Data Inserted");
                    }
                }
            });
        });
    </script>
@endpush
