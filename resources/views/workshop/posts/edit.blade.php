@extends('workshop.layouts.layout')

@section('heading')
    <h4>
        <a href="{{ route('workshop.index') }}">
            <i class="icon-arrow-left52 mr-2"></i>
        </a>
        <span class="font-weight-semibold">Home - Post</span>
    </h4>
@endsection

@section('breadcrumbs')
    <div class="breadcrumb">
        <a href="{{ route('posts.index') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
        <span class="breadcrumb-item">Post: {{ $post->name }}</span>
        <span class="breadcrumb-item active">Edit</span>
    </div>
@endsection
@section('content')
    @include('admin.partials.header')
    <div class="content">
        @include('common.partials.flash')
        <div class="card">
            <div class="card-header header-elements-inline">
                <h5 class="card-title">Post</h5>
            </div>
            <div class="card-body">
                <form action="{{ route('posts.update', $post->id) }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <div class="form-group">
                        <label class="font-weight-semibold @error('name') text-danger @enderror">Name <span
                                class="text-red">*</span></label>
                        <div class="form-group-feedback form-group-feedback-right">
                            <input type="text" name="name" value="{{ $post->name }}"
                                class="form-control @error('name') border-danger @enderror" required>
                            @error('name')
                            <div class="form-control-feedback text-danger">
                                <i class="icon-cancel-circle2"></i>
                            </div>
                            @enderror
                        </div>
                        @error('name')
                        <span class="form-text text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label class="font-weight-semibold @error('company') text-danger @enderror">Company <span
                                class="text-red">*</span></label>
                        <div class="form-group-feedback form-group-feedback-right">
                            <input type="text" name="company" value="{{ $post->company }}"
                                class="form-control @error('company') border-danger @enderror" required>
                            @error('company')
                            <div class="form-control-feedback text-danger">
                                <i class="icon-cancel-circle2"></i>
                            </div>
                            @enderror
                        </div>
                        @error('company')
                        <span class="form-text text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label class="font-weight-semibold @error('car_id') text-danger @enderror">Car <span
                            class="text-red">*</span></label>
                        <div class="form-group-feedback form-group-feedback-right">
                            <select name="car_id" class="form-control cars form-control-select2 @error('car_id') border-danger @enderror" required>
                                <option value="">Select Car</option>
                                @forelse ($cars as $car)
                                    <option {{ $car->id == $post->car_id ? 'selected' : '' }} value="{{ $car->id }}">{{ $car->name }}</option>
                                @empty
                                    <option value="">No Car Found</option>
                                @endforelse
                            </select>
                            @error('car_id')
                            <div class="form-control-feedback text-danger">
                                <i class="icon-cancel-circle2"></i>
                            </div>
                            @enderror
                        </div>
                        @error('car_id')
                        <span class="form-text text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <input type="hidden" id="hidden_model_id" value="{{ $post->model_id }}" />
                    <div class="form-group">
                        <label class="font-weight-semibold @error('model_id') text-danger @enderror">Model <span
                            class="text-red">*</span></label>
                        <div class="form-group-feedback form-group-feedback-right">
                            <select id="carmodel" name="model_id" class="form-control form-control-select2 @error('model_id') border-danger @enderror" required>
                                <option value="">Select Model</option>
                                @forelse ($models as $model)
                                    <option {{ $model->id == $post->model_id ? 'selected' : '' }}  value="{{ old("model_id") ??  $model->id}}">{{ $model->name }}</option>
                                @empty
                                    <option value="">No Model Found</option>
                                @endforelse
                            </select>
                            @error('model_id')
                            <div class="form-control-feedback text-danger">
                                <i class="icon-cancel-circle2"></i>
                            </div>
                            @enderror
                        </div>
                        @error('model_id')
                        <span class="form-text text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label class="font-weight-semibold @error('price') text-danger @enderror">Price <span
                                class="text-red">*</span></label>
                        <div class="form-group-feedback form-group-feedback-right">
                            <input type="number" name="price" value="{{ $post->price }}"
                                class="form-control @error('price') border-danger @enderror" required>
                            @error('price')
                            <div class="form-control-feedback text-danger">
                                <i class="icon-cancel-circle2"></i>
                            </div>
                            @enderror
                        </div>
                        @error('price')
                        <span class="form-text text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <input type="hidden" name="workshop_id" value="{{ auth()->user()->id }}"/>
                    <div class="form-group">
                        <label class="font-weight-semibold @error('description') text-danger @enderror">Description <span
                                class="text-red">*</span></label>
                        <div class="form-group-feedback form-group-feedback-right">
                            <textarea name="description" class="form-control tinymce @error('description') border-danger @enderror">{{ $post->description }}</textarea>
                            @error('description')
                            <div class="form-control-feedback text-danger">
                                <i class="icon-cancel-circle2"></i>
                            </div>
                            @enderror
                        </div>
                        @error('description')
                        <span class="form-text text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label style="display: block;"
                            class="font-weight-semibold @error('image') text-danger @enderror">Image <span
                                class="text-red">*</span></label>
                        <div class="d-inline-block" style="position: relative;margin-bottom: inherit;">
                        @if($post->image)
                            {{-- @foreach(explode(',' , $post->image) as $image) --}}
                                <div class="d-inline-block" style="position:relative;margin-bottom: inherit; ">
                                    <img src="{{ asset('/storage/'. $post->image) }}" height="200px" width="200px"/>
                                    {{-- <a href="{{route('post.delete',[$post->id,$image])}}" class="btn btn-danger text-white"
                                        onclick="return confirm('Are you sure you want to delete this image?')"
                                        style="position: absolute; top: 0; right: 0;"> &times; </a> --}}
                                </div>
                            {{-- @endforeach --}}
                        @else
                            <p>No Image Found</p>
                        @endif
                        <input type="file" accept=".jpeg,.png,.bmp,.gif,.svg,.webp,.jpg" name="image">
                        <span class="form-text text-muted">Accepted formats: jpeg, png, bmp, gif, svg, jpg, or webp</span>
                        @error('image')
                        <span class="form-text text-danger">{{ $message }}</span>
                        @enderror
                        </div>
                    </div>
                    <div class="text-right">
                        <button type="submit" class="btn btn-primary">Submit <i class="icon-paperplane ml-2"></i></button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
<script>
$(document).on("change", ".cars", function () {
    var car_id = $(this).val();
    var model_id = $("#hidden_model_id").val();
    console.log(model_id);
    var token = $('meta[name="csrf-token"]').attr("content");
    var url = window.location.pathname.split("/")[1] === "staging"
                            ? "/staging/workshop/get-all/models/" + car_id
                            : "" + "/workshop/get-all/models/" + car_id;

    $.ajax({
        headers: {
            "X-CSRF-TOKEN": token,
        },
        method: "POST",
        url: url,
        data: {
            car_id: car_id,
        },
        dataType: "json",
        success: function (data) {
            $("#carmodel").empty();
            if (data.length === 0) {
                $("#carmodel").append(
                    '<option value="">No Model Found For This Car</option>'
                );
            } else {
                $("#carmodel").append(
                    '<option value="">None selected</option>'
                );
                $.each(data, function (index, data) {
                    $("#carmodel").append(
                        '<option value="' +
                            data.id +
                            '">' +
                            data.name +
                            "</option>"
                    );
                });
            }
            $("#carModel option[value=" + model_id + "]").attr(
                "selected",
                true
            );
        },
        error: function (request, status, error) {
            console.log(request);
        },
    });
});

</script>
@endpush
