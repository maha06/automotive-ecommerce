<div class="content-element">
    <div class="modal-body" >
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="form-example-wrap">
                <div class="cmp-tb-hd" style="text-align: center;margin-bottom:20px;">
                    <a href="javascript:;" onclick="nextPrev(1)" data-value="1" id="parts-required-yes" class="btn btn-style-3 required-parts">Yes</a>
                    <a href="javascript:;" onclick="requiredParts(this)" data-value="0" id="parts-required-no" class="required-parts btn btn-style-3">No</a>
                </div>
                <div class="cmp-tb-hd" style="text-align: center;margin-bottom:20px;">
                    <a href="javascript:;" onclick="requiredParts(this)" data-value="0" id="parts-required-workshop" class="required-parts btn btn-style-3">Will be decided at workshop</a>
                </div>
            </div>
        </div>
    </div>
</div>
