<div class="row">
    <h3 style="text-align: center;margin-top:20px;"><b>Customer Details</b></h3>
    <div class="col-sm-12">
        <div class="table-type-2">
          <table>
            <tbody>
              <tr>
                <th>Name</th>
                <td><span id="confirm_customer_name"></span></td>
              </tr>
              <tr>
                <th>Email</th>
                <td><span id="confirm_customer_email"></span></td>
              </tr>
              <tr>
                <th>City</th>
                <td><span id="confirm_city"></span></td>
              </tr>
              <tr>
                <th>Selected Workshop</th>
                <td><span id="confirm_workshop_name"></span></td>
              </tr>
              <tr>
                <th>Mobile</th>
                <td><b><span id="confirm_mobile"></span></b></td>
              </tr>
            </tbody>
          </table>
        </div>
    </div>
</div>


<div class="row">
    <h3 style="text-align: center;margin-top:20px;"><b>Car Details</b></h3>
    <div class="col-sm-12">
        <div class="table-type-2">
          <table>
            <tbody>
              <tr>
                <th>Make</th>
                <td><span id="confirm_make"></span></td>
              </tr>
              <tr>
                <th>Model</th>
                <td><span id="confirm_model"></span></td>
              </tr>
              <tr>
                <th>Mileage</th>
                <td><span id="confirm_mileage"></span></td>
              </tr>
              <tr>
                <th>Registration Number</th>
                <td><b><span id="confirm_registration_number"></span></b></td>
              </tr>
            </tbody>
          </table>
        </div>
    </div>
</div>


<div class="row">
    <h3 style="text-align: center;margin-top:20px;"><b>Appoinment Details</b></h3>
    <div class="col-sm-12">
        <div class="table-type-2">
          <table>
            <tbody>
              <tr id="available-date">
                <th>Available Date</th>
                <td><span id="confirm_available_date"></span></td>
              </tr>
              <tr id="available-time">
                <th>Available Time</th>
                <td><span id="confirm_available_time"></span></td>
              </tr>
            </tbody>
          </table>
        </div>
    </div>
</div>

<div class="row">
    <h3 style="text-align: center;margin-top:20px;"><b>Selected Services</b></h3>
    <div class="col-sm-12">
        <div class="table-type-1">
          <table id="confirm_services">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Service name</th>
                    <th>Service price</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
    </div>
</div>

<div class="row">
    <h3 style="text-align: center;margin-top:20px;"><b>Selected Parts</b></h3>
    <div class="col-sm-12">
        <div class="table-type-1">
          <table id="confirm_parts">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Part name</th>
                    <th>Part price</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
    </div>
</div>

<div class="row">
    <h3 style="text-align: center;margin-top:20px;"><b>Total Cost</b></h3>
    <div class="col-sm-12">
        <div class="table-type-1">
          <table>
            <tbody>
              <tr>
                <th>Total Service Cost</th>
                <th>Total Parts Cost</th>
                <th>Total Cost</th>
              </tr>
              <tr class="total-cell">
                <td>
                  <div>PKR <span id="total_service_cost"></span></div>
                </td>
                <td>PKR <span id="total_parts_cost"></span></td>
                <td>PKR <span id="total_cost"></span></td>
              </tr>
            </tbody>
          </table>
        </div>
    </div>
</div>
