@extends('layouts.hero-section')
@section('head')
    <meta name="keywords" content="">
    <meta name="description" content="">
    <title>Mechon</title>
@endsection
@section('content')
<div id="content">

    <!-- - - - - - - - - - - - - - Breadcrumbs - - - - - - - - - - - - - - - - -->

    <div class="breadcrumbs-wrap"  style="background:#616161">

      <div class="container">

        <h2 class="page-title">About Us</h2>

        <ul class="breadcrumbs">

          <li><a href="{{ route('pages.home') }}">Home</a></li>
          <li>About Us</li>

        </ul>

      </div>

    </div>

    <!-- - - - - - - - - - - - - end Breadcrumbs - - - - - - - - - - - - - - - -->

    <div class="page-section">

      <div class="container">

        <div class="row">
          <div class="col-sm-6">

            <img src="images/555x355_img1.jpg" alt="">

          </div>
          <div class="col-sm-6">

            <h2><b>Welcome to Auto Trader!</b></h2>
            <p class="text-size-medium"><b>Aliquam erat volutpat. Duis ac turpis. Integer rutrum ante eu lacus. </b></p>
            <p class="text-size-medium">Vestibulum libero nisl, porta vel. Vivamus eget nibh. Etiam cursus leo vel metus. Nulla facilisi. Aenean nec eros vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae.</p>
            <p class="text-size-medium">Suspendisse sollicitudin velit sed leo. Ut pharetra augue nec augue. Nam elit agna,endrerit sit amet, tincidunt ac, viverra sed, nulla. Donec porta diam eu massa. Quisque diam lorem, interdum vitae,dapibus ac, scelerisque vitae, pede. Donec eget tellus non erat lacinia fermentum.</p>

          </div>
        </div>

      </div>

    </div>

    <div class="page-section-bg">

      <div class="container">

        <h2 class="section-title align-center">Company Statistics</h2>

        <div class="row counter-wrap">

          <div class="col-lg-3 col-md-6 col-xs-6">

            <div class="counter">
              <span class="licon-medal-empty"></span>
              <div class="counter-inner">
                <h3 class="timer count-number" data-to="25" data-speed="1500">0</h3>
                <p>years of successful work</p>
              </div>
            </div>

          </div>
          <div class="col-lg-3 col-md-6 col-xs-6">

            <div class="counter">
              <span class="licon-smile"></span>
              <div class="counter-inner">
                <h3 class="timer count-number" data-to="5970" data-speed="1500">0</h3>
                <p>satisfied clients</p>
              </div>
            </div>

          </div>
          <div class="col-lg-3 col-md-6 col-xs-6">

            <div class="counter">
              <span class="licon-earth"></span>
              <div class="counter-inner">
                <h3 class="timer count-number" data-to="17" data-speed="1500">0</h3>
                <p>dealer branches</p>
              </div>
            </div>

          </div>
          <div class="col-lg-3 col-md-6 col-xs-6">

            <div class="counter">
              <span class="licon-car2"></span>
              <div class="counter-inner">
                <h3 class="timer count-number" data-to="44278" data-speed="1500">0</h3>
                <p>vehicles for sale</p>
              </div>
            </div>

          </div>

        </div>

      </div>

    </div>

    <div class="page-section half-bg-col">

      <div class="img-col-right"><div class="col-bg" data-bg="images/975x480_img1.jpg"></div></div>

      <div class="container">
        <div class="row">
          <div class="col-md-6">

            <h2><b>Auto Service and Repair</b></h2>
            <p>Vestibulum iaculis lacinia est. Proin dictum elementum velit. Fusce euismod consequat ante. Lorem ipsum dolor sit amet, consectetuer adipisMauris accumsan nulla vel diam. Sed in lacus ut enim adipiscing aliquet. Nulla venenatis. </p>
            <p class="text-size-medium"><b>Aliquam dapibus tincidunt metus. Praesent justo dolor, lobortis quis, lobortis dignissim, pulvinar ac, lorem. </b></p>
            <p>Vestibulum sed ante. Donec sagittis euismod purus.Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis.</p>
            <a href="#" class="info-btn">Schedule a Service Appointment</a>

          </div>
          <div class="col-md-6"></div>
        </div>
      </div>

    </div>

    <div class="page-section half-bg-col">

      <div class="img-col-left"><div class="col-bg" data-bg="images/975x480_img2.jpg"></div></div>

      <div class="container">
        <div class="row">
          <div class="col-md-6"></div>
          <div class="col-md-6">

            <h2><b>Car Loans and Auto Financing</b></h2>
            <p>Vestibulum iaculis lacinia est. Proin dictum elementum velit. Fusce euismod consequat ante. Lorem ipsum dolor sit amet, consectetuer adipisMauris accumsan nulla vel diam. Sed in lacus ut enim adipiscing aliquet. Nulla venenatis. </p>
            <p class="text-size-medium"><b>Aliquam dapibus tincidunt metus. Praesent justo dolor, lobortis quis, lobortis dignissim, pulvinar ac, lorem. </b></p>
            <p>Vestibulum sed ante. Donec sagittis euismod purus.Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis.</p>
            <a href="#" class="info-btn">Read More</a>

          </div>
        </div>
      </div>

    </div>

    <div class="page-section half-bg-col">

      <div class="img-col-right"><div class="col-bg" data-bg="images/975x480_img3.jpg"></div></div>

      <div class="container">
        <div class="row">
          <div class="col-md-6">

            <h2><b>Auto Insurance</b></h2>
            <p>Vestibulum iaculis lacinia est. Proin dictum elementum velit. Fusce euismod consequat ante. Lorem ipsum dolor sit amet, consectetuer adipisMauris accumsan nulla vel diam. Sed in lacus ut enim adipiscing aliquet. Nulla venenatis. </p>
            <p class="text-size-medium"><b>Aliquam dapibus tincidunt metus. Praesent justo dolor, lobortis quis, lobortis dignissim, pulvinar ac, lorem. </b></p>
            <p>Vestibulum sed ante. Donec sagittis euismod purus.Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis.</p>
            <a href="#" class="info-btn">Read More</a>

          </div>
          <div class="col-md-6"></div>
        </div>
      </div>

    </div>

    <div class="page-section-bg">

      <div class="container">

        <h2 class="section-title align-center">Our Showroom</h2>
        <div class="carousel-type-3 portfolio-holder style-2">

          <div class="owl-carousel" data-max-items="3" data-item-margin="30">

            <!-- Slide -->
            <div class="item-carousel">
              <!-- Carousel Item -->
              <!-- - - - - - - - - - - - - - Project - - - - - - - - - - - - - - - - -->

              <div class="project">

                <!-- - - - - - - - - - - - - - Project Image - - - - - - - - - - - - - - - - -->

                <div class="project-image">

                  <img src="images/360x240_img7.jpg" alt="">
                  <a href="images/360x240_img7.jpg" class="project-link project-action fancybox" data-fancybox="group"></a>

                </div>

                <!-- - - - - - - - - - - - - - End of Project Image - - - - - - - - - - - - - - - - -->

              </div>

              <!-- - - - - - - - - - - - - - End of Project - - - - - - - - - - - - - - - - -->
              <!-- /Carousel Item -->
            </div>
            <!-- /Slide -->

            <!-- Slide -->
            <div class="item-carousel">
              <!-- Carousel Item -->
              <!-- - - - - - - - - - - - - - Project - - - - - - - - - - - - - - - - -->

              <div class="project">

                <!-- - - - - - - - - - - - - - Project Image - - - - - - - - - - - - - - - - -->

                <div class="project-image">

                  <img src="images/360x240_img8.jpg" alt="">
                  <a href="images/360x240_img8.jpg" class="project-link project-action fancybox" data-fancybox="group"></a>

                </div>

                <!-- - - - - - - - - - - - - - End of Project Image - - - - - - - - - - - - - - - - -->

              </div>

              <!-- - - - - - - - - - - - - - End of Project - - - - - - - - - - - - - - - - -->
              <!-- /Carousel Item -->
            </div>
            <!-- /Slide -->

            <!-- Slide -->
            <div class="item-carousel">
              <!-- Carousel Item -->
              <!-- - - - - - - - - - - - - - Project - - - - - - - - - - - - - - - - -->

              <div class="project">

                <!-- - - - - - - - - - - - - - Project Image - - - - - - - - - - - - - - - - -->

                <div class="project-image">

                  <img src="images/360x240_img9.jpg" alt="">
                  <a href="images/360x240_img9.jpg" class="project-link project-action fancybox" data-fancybox="group"></a>

                </div>

                <!-- - - - - - - - - - - - - - End of Project Image - - - - - - - - - - - - - - - - -->

              </div>

              <!-- - - - - - - - - - - - - - End of Project - - - - - - - - - - - - - - - - -->
              <!-- /Carousel Item -->
            </div>
            <!-- /Slide -->

          </div>

        </div>

      </div>

    </div>

    <div class="page-section">

      <div class="container">

        <h2 class="section-title align-center">Customers Testimonials</h2>

        <!-- - - - - - - - - - - - - Owl-Carousel - - - - - - - - - - - - - - - -->

        <div class="carousel-type-2">

          <div class="owl-carousel" data-max-items="2">

            <!-- Slide -->
            <div class="item-carousel">
              <!-- Carousel Item -->

              <!-- - - - - - - - - - - - - - Testimonial - - - - - - - - - - - - - - - - -->
              <div class="testimonial type-2">

                <div class="author-box">

                  <a href="#" class="avatar">
                    <img src="images/120x120_photo1.jpg" alt="">
                  </a>

                </div>

                <div class="testimonial-holder">

                  <h6>Very Fast Delivery</h6>
                  <blockquote>
                    <p>“Nulla facilisi. Aenean nec eros. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Suspendisse sollicitudin.”</p>
                  </blockquote>
                  <div class="author-info">

                    <h6 class="author-name">Inga North</h6>
                    <span class="author-position">Customer</span>

                  </div>

                </div>

              </div>
              <!-- /Carousel Item -->

            </div>
            <!-- /Slide -->

            <!-- Slide -->
            <div class="item-carousel">
              <!-- Carousel Item -->

              <!-- - - - - - - - - - - - - - Testimonial - - - - - - - - - - - - - - - - -->
              <div class="testimonial type-2">

                <div class="author-box">

                  <a href="#" class="avatar">
                    <img src="images/120x120_photo2.jpg" alt="">
                  </a>

                </div>

                <div class="testimonial-holder">

                  <h6>Very Fast Delivery</h6>
                  <blockquote>
                    <p>“Nulla facilisi. Aenean nec eros. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Suspendisse sollicitudin.”</p>
                  </blockquote>
                  <div class="author-info">

                    <h6 class="author-name">Patrick Pool</h6>
                    <span class="author-position">Customer</span>

                  </div>

                </div>

              </div>
              <!-- /Carousel Item -->

            </div>
            <!-- /Slide -->

            <!-- Slide -->
            <div class="item-carousel">
              <!-- Carousel Item -->

              <!-- - - - - - - - - - - - - - Testimonial - - - - - - - - - - - - - - - - -->
              <div class="testimonial type-2">

                <div class="author-box">

                  <a href="#" class="avatar">
                    <img src="images/120x120_photo1.jpg" alt="">
                  </a>

                </div>

                <div class="testimonial-holder">

                  <h6>Very Fast Delivery</h6>
                  <blockquote>
                    <p>“Nulla facilisi. Aenean nec eros. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Suspendisse sollicitudin.”</p>
                  </blockquote>
                  <div class="author-info">

                    <h6 class="author-name">Inga North</h6>
                    <span class="author-position">Customer</span>

                  </div>

                </div>

              </div>
              <!-- /Carousel Item -->

            </div>
            <!-- /Slide -->

            <!-- Slide -->
            <div class="item-carousel">
              <!-- Carousel Item -->

              <!-- - - - - - - - - - - - - - Testimonial - - - - - - - - - - - - - - - - -->
              <div class="testimonial type-2">

                <div class="author-box">

                  <a href="#" class="avatar">
                    <img src="images/120x120_photo2.jpg" alt="">
                  </a>

                </div>

                <div class="testimonial-holder">

                  <h6>Very Fast Delivery</h6>
                  <blockquote>
                    <p>“Nulla facilisi. Aenean nec eros. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Suspendisse sollicitudin.”</p>
                  </blockquote>
                  <div class="author-info">

                    <h6 class="author-name">Patrick Pool</h6>
                    <span class="author-position">Customer</span>

                  </div>

                </div>

              </div>
              <!-- /Carousel Item -->

            </div>
            <!-- /Slide -->

            <!-- Slide -->
            <div class="item-carousel">
              <!-- Carousel Item -->

              <!-- - - - - - - - - - - - - - Testimonial - - - - - - - - - - - - - - - - -->
              <div class="testimonial type-2">

                <div class="author-box">

                  <a href="#" class="avatar">
                    <img src="images/120x120_photo1.jpg" alt="">
                  </a>

                </div>

                <div class="testimonial-holder">

                  <h6>Very Fast Delivery</h6>
                  <blockquote>
                    <p>“Nulla facilisi. Aenean nec eros. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Suspendisse sollicitudin.”</p>
                  </blockquote>
                  <div class="author-info">

                    <h6 class="author-name">Inga North</h6>
                    <span class="author-position">Customer</span>

                  </div>

                </div>

              </div>
              <!-- /Carousel Item -->

            </div>
            <!-- /Slide -->

            <!-- Slide -->
            <div class="item-carousel">
              <!-- Carousel Item -->

              <!-- - - - - - - - - - - - - - Testimonial - - - - - - - - - - - - - - - - -->
              <div class="testimonial type-2">

                <div class="author-box">

                  <a href="#" class="avatar">
                    <img src="images/120x120_photo2.jpg" alt="">
                  </a>

                </div>

                <div class="testimonial-holder">

                  <h6>Very Fast Delivery</h6>
                  <blockquote>
                    <p>“Nulla facilisi. Aenean nec eros. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Suspendisse sollicitudin.”</p>
                  </blockquote>
                  <div class="author-info">

                    <h6 class="author-name">Patrick Pool</h6>
                    <span class="author-position">Customer</span>

                  </div>

                </div>

              </div>
              <!-- /Carousel Item -->

            </div>
            <!-- /Slide -->

          </div>

        </div>

      </div>

    </div>

    <div class="call-out holder-bg half-bg-col">

      <div class="img-col-left"><div class="col-bg" data-bg="images/960x266_bg1.jpg"></div></div>

      <div class="img-col-right"><div class="col-bg" data-bg="images/960x266_bg2.jpg"></div></div>

      <div class="container">

        <div class="row">

          <div class="col-md-6">

            <div class="align-center">
              <h2>Looking to Sell Your Car?</h2>
              <p>Get cash offers to your inbox in hours</p>
              <a href="#" class="btn btn-big">Submit Your Vehicle Now</a>
            </div>

          </div>
          <div class="col-md-6">

            <div class="align-center">
              <h2>Looking For A Car?</h2>
              <p>We have thousands of new and used cars for sale</p>
              <a href="#" class="btn btn-big">View Inventory</a>
            </div>

          </div>
        </div>

      </div>

    </div>

  </div>
@endsection
