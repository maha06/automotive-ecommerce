<div class="content-element">
    <div class="modal-body" >
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="form-example-wrap">
                <!-- <div class="cmp-tb-hd" >
                    <p style="color: black;
                    font-size: 20px;
                    font-weight: bold;">Do you want to take an appointment? (Appointment ensures quality service and no wait time)</p>
                    <div class="form-example-int">
                        <div class="form-group">
                            <input type="checkbox" name="appointment" id="checkbox-2">
                            <label style="color: black;margin-bottom:20px;" for="checkbox-2">Do you want to take an appointment?</label>
                        </div>
                    </div>
                </div> -->
               <div  id="appoint_details">
                   <div class="form-example-int col-md-6">
                       <div class="form-group">
                           <label> Date <span style="color:#b1151b">*</span></label>
                           <div class="nk-int-st">
                               <input type="date" class="form-control input-sm"  id="available_date" name="available_date">
                           </div>
                       </div>
                   </div>
                   <div class="form-example-int col-md-6">
                       <div class="form-group">
                           <label> Time <span style="color:#b1151b">*</span></label>
                           <div class="nk-int-st">
                               <input type="time" class="form-control input-sm"  id="available_time" name="available_time">
                           </div>
                       </div>
                   </div>
                   <div class="form-example-int">
                       <div class="form-group">
                           <input type="checkbox" name="appointment" id="checkbox-1">
                           <label style="color:black;margin-bottom:20px;margin-top:20px;" for="checkbox-1">Do you need pick up for your vehicle?</label>
                       </div>
                   </div>

                   <div id="appointment_yes" style="display: none">
                       <div class="form-example-int col-md-4">
                           <div class="form-group">
                               <label>Time <span style="color:#b1151b">*</span></label>
                               <div class="nk-int-st">
                                   <input type="time" class="form-control input-sm" id="pickup_time" name="pickup_time">
                               </div>
                           </div>
                       </div>
                       <div class="form-example-int col-md-4">
                           <div class="form-group">
                               <label>Address <span style="color:#b1151b">*</span> </label>
                               <div class="nk-int-st">
                                   <input type="text" class="form-control input-sm" id="pickup_address" name="pickup_address">
                               </div>
                           </div>
                       </div>

                   </div>
               </div>
            </div>
        </div>
    </div>
</div>
