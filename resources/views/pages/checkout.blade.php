@extends('layouts.hero-section')
@section('head')
    <meta name="keywords" content="">
    <meta name="description" content="">
    <title>Mechon</title>
@endsection
@section('content')
<div id="content">
    <div class="breadcrumbs-wrap"  style="background:#616161">
      <div class="container">
        <h2 class="page-title">Checkout</h2>
        <ul class="breadcrumbs">
          <li><a href="{{ route('pages.home') }}">Home</a></li>
          <li>Checkout</li>
        </ul>
      </div>
    </div>
</div>
<div id="content" class="page-content-wrap" style="background: #F2F3F3;">
    <div class="container">
        @include('common.partials.flash')
        <form class="contact-form type-2" method="post" action="{{ route('pages.place-order') }}">
            @csrf
        <div class="content-element3">

          <div class="row">
            <div class="col-md-6 col-sm-12">

              <div class="content-element8">

                <h4><b>Billing Details</b></h4>
                  <div class="row" style="margin-bottom:30px;">
                    <div class="col-sm-12">
                      <label class="required">Name</label>
                      <input type="text" name="name" value="{{ $user->name }}" required>
                    </div>
                  </div>
                  <div class="row" style="margin-bottom:30px;">
                    <div class="col-sm-12">
                      <label class="required">Adress</label>
                      <input type="text" placeholder="Street address" name="address" required>
                      <input type="text" placeholder="Apartment, suit, unit etc. (optional)" name="apartment">
                    </div>
                  </div>
                  <div class="row" style="margin-bottom:30px;">
                    <div class="col-sm-12">
                      <label class="required">Town / City</label>
                      <input type="text" name="city" required>
                    </div>
                  </div>
                  <div class="row" style="margin-bottom:30px;">
                    <div class="col-sm-6">
                      <label class="required">Phone</label>
                      <input type="tel" name="phone" value="{{ $user->phone }}" required>
                    </div>
                    <div class="col-sm-6">
                      <label class="required">Mail</label>
                      <input type="email" name="email" value="{{ $user->email }}" required>
                    </div>
                  </div>
              </div>
            </div>
            <input type="hidden" name="user_id" value="{{ $user->id }}" required>
            <div class="col-md-6 col-sm-12">
                <h4><b>Additional Details</b></h4>
                <label>Order Notes</label>
                <textarea rows="5" name="order_notes" placeholder="Notes about your order, e.g. special notes for delivery."></textarea>
            </div>
          </div>

        </div>

            <div class="content-element3">
            <h4><b>Your Order</b></h4>
            <div class="content-element7">
                <div class="shop-cart-form order-type table-type-1">
                <table>
                    <thead>
                    <tr>
                        <th class="product-col">Product</th>
                        <th></th>
                        <th class="total-col">Total</th>
                    </tr>
                    </thead>
                    @foreach ($carts as $cart)
                    <tr>
                        <td colspan="2" data-title="Product">

                            <div class="product-description">
                                @if ($cart->post_id)
                                    <h6 class="product-name"><a href="#">{{ $cart->post->name }}</a></h6>
                                @else
                                    <h6 class="product-name"><a href="#">{{ $cart->oil->name }}</a></h6>
                                @endif

                                <span class="product-size">Quantity: {{ $cart->quantity }}</span>
                            </div><!--/ .product-info -->

                        </td>
                        @if ($cart->post_id)
                            <td class="price">PKR {{ $cart->post->retail_price * $cart->quantity }}</td>
                        @else
                            <td class="price">PKR {{ $cart->oil->retail_price * $cart->quantity }}</td>
                        @endif

                    </tr>
                    @endforeach
                    <tr>
                    <td class="order-total">
                        <div>Cart subtotal:</div>
                    </td>
                    <td></td>
                    <td class="price">PKR {{ $carts->sum('price') }}</td>
                    </tr>
                    <tr>
                    <td class="order-total">
                        <div>Shipping:</div>
                    </td>
                    <td></td>
                    <td>
                        <div class="input-wrapper">

                        <input type="radio" checked name="radio-1" id="radio-1">
                        <label for="radio-1">Free shipping</label>

                        </div>
                    </td>
                    </tr>
                    <tr class="total-cell">
                    <td class="order-total">
                        Order Total:
                    </td>
                    <td></td>
                    <td class="price">PKR {{ $carts->sum('price') }}</td>
                    </tr>
                </table>
                </div>

            </div>

            <div class="content-element7">

                <ul class="custom-list payment-option">

                <li>

                    <div class="input-wrapper style-2">
                    <input type="checkbox" id="checkbox2" class="type-2" name="checkbox2">
                    <label for="checkbox2">I’ve read and accept the <a href="#" class="link-text2 required">Terms & Conditions</a></label>
                    </div>

                </li>

                <li>

                    <button class="btn btn-style-3">Place Order</button>

                </li>

                </ul>

            </div>

            </div>
        </form>
      </div>
</div>
@endsection
