@extends('layouts.hero-section')
@section('head')
    <meta name="keywords" content="">
    <meta name="description" content="">
    <title>Mechon | Customer Complaints</title>
@endsection
@section('content')
<div id="content">

    <!-- - - - - - - - - - - - - - Breadcrumbs - - - - - - - - - - - - - - - - -->

    <div class="breadcrumbs-wrap" style="background:#616161;">

      <div class="container">

        <h2 class="page-title">Customer Complaints</h2>

        <ul class="breadcrumbs">

          <li><a href="{{ route('pages.home') }}">Home</a></li>
          <li>Customer Complaints</li>

        </ul>

      </div>

    </div>

    <!-- - - - - - - - - - - - - end Breadcrumbs - - - - - - - - - - - - - - - -->
    <div class="page-section-bg">

      <div class="container">

        <h2><b>We Welcome Your Feedback and Comments</b></h2>
        @include('common.partials.flash')
        <form class="contact-form" action="{{ route('pages.complaints.submit') }}" method="POST">
        @csrf
          <div class="row">
            <div class="col-sm-6">

              <label class="required">You Name</label>
              <input type="text" name="name" required>

              <label class="required">You email</label>
              <input type="email" name="email" required>

              <label>Phone number</label>
              <input type="tel" name="phone" required>
            </div>
            <div class="col-sm-6">

              <label>Message</label>
              <textarea name="message" rows="10" ></textarea>

            </div>
            <div class="col-sm-12" style="text-align: center">
                <button style="margin-top:30px;" type="submit" class="btn btn-style-3" data-type="submit">Send Message</button>

              </div>
          </div>

        </form>

      </div>

    </div>

</div>
@endsection
