@extends('layouts.hero-section')
@section('head')
    <meta name="keywords" content="">
    <meta name="description" content="">
    <title>Mechon</title>
@endsection
@section('content')
<div id="content">
    <div class="breadcrumbs-wrap"  style="background:#616161">
      <div class="container">
        <h2 class="page-title">Cart</h2>
        <ul class="breadcrumbs">
          <li><a href="{{ route('pages.home') }}">Home</a></li>
          <li>Cart</li>
        </ul>
      </div>
    </div>
</div>

<div id="content" class="page-content-wrap">

    @if (count($carts) > 0)
    <div class="container">
        <div class="content-element6">
            <div id="success-message" style="display: none;" class="alert alert-success">
                Quantity Updated
            </div>
            <div id="error-message" style="display: none;" class="alert alert-warning">
                Quantity cannot be updated, as there is no more stock
            </div>

            <div class="shop-cart-form table-type-1 responsive-table">
            <table>
                <thead>
                <tr>
                    <th class="close-product"></th>
                    <th class="product-col">Product</th>
                    <th class="price-col">Price</th>
                    <th class="qty-col">Quantity</th>
                    <th class="total-col">Total</th>
                </tr>
                </thead>
                @foreach ($carts as $cart)
                    <tr>
                        <td class="close-product shopping-cart-full"><button id="remove-cart" data-id="{{ $cart->id }}" class="item-close"></button></td>
                        <td data-title="Product">

                            <div >
                                @if ($cart->post_id)
                                    <h6>{{ $cart->post->name }}</h6>
                                @else
                                    <h6>{{ $cart->oil->name }}</h6>
                                @endif
                            </div><!--/ .product-info -->


                        </td>
                        <td data-title="Price" class="price">PKR @if ($cart->post_id)
                            {{ $cart->post->retail_price }}
                            @else
                                {{ $cart->oil->retail_price }}
                            @endif
                        </td>
                        <td data-title="Quantity">
                            <div class="quantity">
                                <button id="down-quantity" type="button"  data-id="{{ $cart->id }}" class="qty-minus"><i class="licon-chevron-down"></i></button>
                                <input type="text" value="{{ $cart->quantity }}"  readonly="">
                                <button  id="up-quantity" type="button" data-id="{{ $cart->id }}" class="qty-plus"><i class="licon-chevron-up"></i></button>
                            </div>
                        </td>
                        <td data-title="Total" id="cart-total-price-{{ $cart->id }}" class="price">PKR {{ $cart->price }}</td>
                    </tr>
                @endforeach
            </table>
            </div>
        </div>
        <div class="content-element6">

            <div class="row">
            <div class="col-sm-6">

            </div>
            <div class="col-sm-6">

                <h3 class="section-title">Cart Totals</h3>
                <div class="table-type-2 style-2">
                <table>
                    <tbody>
                    <tr>
                        <th>Subtotal</th>
                        <td class="price" id="grandsubtotal">PKR {{ Auth::user()->carts->sum('price') }}</td>
                    </tr>
                    <tr class="total-cell">
                        <th style="color:white !important;">Total</th>
                        <td class="price" id="grandtotal">PKR {{ Auth::user()->carts->sum('price') }}</td>
                    </tr>
                    <tr>
                        <td colspan="2">
                        <div class="align-center"><a href="{{ route('pages.cart-checkout') }}" class="btn btn-style-3">Proceed to checkout</a></div>
                        </td>
                    </tr>
                    </tbody>
                </table>
                </div>

            </div>
            </div>

        </div>

    </div>
    @else
    <div class="button" style="width:100%;text-align:center;">
        <a href="{{ route('pages.frontend.spare-parts') }}" class="btn btn-style-3">Search More Parts</a>
    </div>
    @endif

  </div>
@endsection
@push('scripts')
<script>

$(document).on("click", "#remove-cart", function (event) {
    event.preventDefault();
    var token = $('meta[name="csrf-token"]').attr("content");
    var cart_id = $(this).data("id");
    var grandTotal = '';
    var url = window.location.pathname.split("/")[1] === "staging"
                            ? "/staging/remove/cart/" + cart_id
                            : "" + "/remove/cart/" + cart_id;
    $.ajax({
        headers: {
            "X-CSRF-TOKEN": token,
        },
        method: "POST",
        url: url,
        data: {
            cart_id:  cart_id,
        },
        dataType: "json",
        success: function (data) {
            window.location.reload();
        },
        error: function (request, status, error) {
            console.log(request);
        },
    });
});


    $(document).on("click", "#down-quantity", function (event) {
        event.preventDefault();
        var token = $('meta[name="csrf-token"]').attr("content");
        var cart_id = $(this).data("id");
        var grandTotal = 0;
        var url = window.location.pathname.split("/")[1] === "staging"
                                ? "/staging/change-down-cart/price/" + cart_id
                                : "http://localhost/mechon/public" + "/change-down-cart/price/" + cart_id;
        $.ajax({
            headers: {
                "X-CSRF-TOKEN": token,
            },
            method: "POST",
            url: url,
            data: {
                cart_id:  cart_id,
            },
            dataType: "json",
            success: function (data) {
                console.log(data);
                $('#cart-total-price-'+data.cart.id).empty();
                $('#grandtotal').empty();
                $('#grandsubtotal').empty();

                $('#cart-total-price-'+data.cart.id).append("PKR "+data.cart.price);

                $.each(data.carts, function (index, element) {
                    grandTotal = grandTotal + element.price;
                });
                $('#grandsubtotal').append("PKR "+grandTotal);
                $('#grandtotal').append("PKR "+grandTotal);
                var count = $('#badge').text();
                $('#badge').text(parseInt(count)-1);
            },
            error: function (request, status, error) {
                console.log(request);
            },
        });
    });

    $(document).on("click", "#up-quantity", function (event) {
        event.preventDefault();
        var token = $('meta[name="csrf-token"]').attr("content");
        var cart_id = $(this).data("id");
        var grandTotal = 0;
        var url = window.location.pathname.split("/")[1] === "staging"
                                ? "/staging/change-up-cart/price/" + cart_id
                                : "http://localhost/mechon/public" + "/change-up-cart/price/" + cart_id;
        $.ajax({
            headers: {
                "X-CSRF-TOKEN": token,
            },
            method: "POST",
            url: url,
            data: {
                cart_id:  cart_id,
            },
            dataType: "json",
            success: function (data) {
                console.log(data);
                if ( ! data) {

                var message = document.getElementById("error-message");
                message.setAttribute("style", "display:block");
                }
                $('#cart-total-price-'+data.cart.id).empty();
                $('#grandtotal').empty();
                $('#grandsubtotal').empty();

                $('#cart-total-price-'+data.cart.id).append("PKR "+data.cart.price);

                $.each(data.carts, function (index, element) {
                    grandTotal = grandTotal + element.price;
                });
                $('#grandsubtotal').append("PKR "+grandTotal);
                $('#grandtotal').append("PKR "+grandTotal);
                var message = document.getElementById("success-message");
                message.setAttribute("style", "display:block");
                var count = $('#badge').text();
                $('#badge').text(parseInt(count)+1);
            },
            error: function (request, status, error) {

                var message = document.getElementById("error-message");
                message.setAttribute("style", "display:block");
            },
        });
    });
</script>
@endpush
