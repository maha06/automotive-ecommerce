@extends('layouts.hero-section')
@section('head')
    <meta name="keywords" content="">
    <meta name="description" content="">
    <title>Mechno</title>
@endsection
@section('content')
<div id="content">

    <!-- - - - - - - - - - - - - - Breadcrumbs - - - - - - - - - - - - - - - - -->

    <div class="breadcrumbs-wrap" style="background:#616161">

      <div class="container">

        <h2 class="page-title">Oil Services</h2>

        <ul class="breadcrumbs">

          <li><a href="{{ route('pages.home') }}">Home</a></li>
          <li>Engine oils for you Vehicle</li>

        </ul>

      </div>

    </div>
    <div class="page-section">

        <div class="container">
            <div class="content-element3">

                <div class="row">
                    <div class="col-sm-12">

                        <form class="contact-form style-2" method="post" action="#">
                            @csrf
                            <div class="content-element3 tab">
                                <div class="row">
                                    <div class="col-md-4 form-group">
                                        <label>Make <span style="color:#b1151b">*</span></label>
                                        <div class="auto-custom-select">
                                            <select  class="cars" data-default-text="Select Car" name="make" id="make" required>
                                                @forelse ($cars as $car)
                                                    <option value="{{ $car->slug }}">{{ $car->name }}</option>
                                                @empty
                                                    <option value="">Cars Not Found</option>
                                                @endforelse
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-4 form-group">
                                        <label> Model <span style="color:#b1151b">*</span></label>
                                        <div class="select-col">
                                            <div class="auto-select-custom-select">
                                                <select name="model" id="model" required>
                                                    <option value="">Select Car First</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4 form-group">
                                        <label> Mileage <span style="color:#b1151b">*</span></label>
                                        <div class="auto-custom-select">
                                            <input type="text" class="form-control" name="mileage" id="mileage" required>
                                        </div>
                                    </div>
                                 </div>
                                <br>
                                <div class="content-element4">
                                    <div class="row">
                                        <h4 style="margin:20px 0;text-align:center;text-decoration:underline;font-weight:bold">Available Engine Oils</h4>
                                    </div>
                                    @include('pages.engine-oil.oils')
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@push('scripts')
<script>
    $(document).on("click", ".add-to-oil-cart", function (event) {
    event.preventDefault();
    var token = $('meta[name="csrf-token"]').attr("content");
    var oil_id = $(this).data("oil");
    var price = $(this).data("price");
    var url = window.location.pathname.split("/")[1] === "staging"
                            ? "/staging/add-to-cart/oil/"+ oil_id
                            : "http://localhost/mechon/public" + "/add-to-cart/oil/"+oil_id;
    $.ajax({
        headers: {
            "X-CSRF-TOKEN": token,
        },
        method: "POST",
        url: url,
        data: {
            oil_id:  oil_id,
            price: price,
        },
        dataType: "json",
        success: function (data) {
            console.log(data);
            if(data != 0){
                var message = document.getElementById("cart-add-success-message");
                message.setAttribute("style", "display:block");
                var count = $('#badge').text();
                $('#badge').text(parseInt(count)+1);
            }else{
                var message = document.getElementById("cart-already-add-success-message");
                message.setAttribute("style", "display:block");
            }
        },
        error: function (request, status, error) {
            console.log(request);
        },
    });
});

$(document).on("change", ".cars", function () {
    var car_id = $(this).val();
    var token = $('meta[name="csrf-token"]').attr("content");
    var url = window.location.pathname.split("/")[1] === "staging"
                            ? "/staging/get-all/models/" + car_id
                            : "" + "/get-all/models/" + car_id;

    $.ajax({
        headers: {
            "X-CSRF-TOKEN": token,
        },
        method: "POST",
        url: url,
        data: {
            car_id: car_id,
        },
        dataType: "json",
        success: function (data) {
            $("#model").empty();
            if (data.length === 0) {
                $("#model").append(
                    '<option value="">No Model Found For This Car</option>'
                );
            } else {
                $("#model").append(
                    '<option value="">None selected</option>'
                );
                $.each(data, function (index, data) {
                    $("#model").append(
                        '<option value="' +
                            data.slug +
                            '">' +
                            data.name +
                            "</option>"
                    );
                });
            }
        },
        error: function (request, status, error) {
            console.log(request);
        },
    });
});

</script>

@endpush
@endsection
