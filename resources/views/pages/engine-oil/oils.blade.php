<div class="products-holder view-grid">
    <div id="cart-add-success-message" style="display: none;" class="alert alert-success">
        Add to Cart
    </div>
    <div id="cart-already-add-success-message" style="display: none;" class="alert alert-success">
        Already Add to Cart
    </div>
    <div class="row flex-row">
        @forelse ($oils as $oil)
            <div class="col-sm-4 col-xs-6">
                <div class="product">
@php
    if ($pic = $oil->pictures->first()) {

@endphp
    <!-- <img src="{{ (config('app.url'). '/mechon/storage/app/public/'. $pic->path) }}" height="280px" /> -->
    <a href="{{ route('pages.frontend.oil-details', $oil->slug) }}" target="_blank">
        <img style="height: 280px;" src="{{ asset('/storage/' . $pic->path) }}" alt="">
    </a>
@php
}
@endphp
                    <div class="product-description">

                    <div class="parts-title" style="display:flex;">
                        <h5 style="width:50%;" class="product-name"><a href="{{ route('pages.frontend.oil-details', $oil->slug) }}" target="_blank">{{ $oil->name }}</a></h5>
                    </div>
                    <div class="pricing-area">
                    @if ($oil->discount > 0)
                        <div class="pricing-area">
                            <div class="product-price" >
                                <span style="text-decoration: line-through;"> PKR {{ $oil->retail_price }} </span><sup style="color:Red"> {{ $oil->discount }}% </sup>
                            </div>
                            @php
                            $price = $oil->retail_price;
                            $discount = ($price * $oil->discount)/100;
                            $total = $price - $discount;
                            @endphp
                            <div class="product-price">
                                PKR {{ $total }}
                            </div>

                        </div>
                        @if ($oil->quantity > 0)
                            @auth
                                <a href="javascript:;" style="width:100%;" data-oil="{{$oil->id}}" data-price="{{$total}}" class="add-to-oil-cart btn btn-small btn-style-3"><i class="licon-cart"></i>Add to Cart</a>
                            @else
                                <a href="{{ route('login') }}" id="userId" value="0" style="width:100%;"  class="btn btn-small btn-style-3"><i class="licon-cart"></i>You Need To First Login</a>
                            @endauth
                        @else
                                Out of Stock
                        @endif
                        @else
                        <div class="pricing-area">
                            <div class="product-price">
                                PKR {{ $oil->retail_price }}
                            </div>
                        </div>
                        @if ($oil->quantity > 0)
                            @auth
                                <a href="javascript:;" style="width:100%;" data-oil="{{$oil->id}}" data-price="{{$oil->retail_price}}" class="add-to-oil-cart btn btn-small btn-style-3"><i class="licon-cart"></i>Add to Cart</a>
                            @else
                                <a href="{{ route('login') }}" id="userId" value="0" style="width:100%;"  class="btn btn-small btn-style-3"><i class="licon-cart"></i>You Need To First Login</a>
                            @endauth
                        @else
                                Out of Stock
                        @endif
                        @endif
                        </div>

                </div>

                </div>
            </div>
        @empty
            <h2 style="width:100%;text-align:center;">Oils Not Available!</h2>
        @endforelse
    </div>

</div>
