<div class="row">
    <h3 style="text-align: center;margin-top:20px;"><b>Car Details</b></h3>
    <div class="col-sm-12">
        <div class="table-type-2">
          <table>
            <tbody>
              <tr>
                <th>Make</th>
                <td><span id="confirm_make"></span></td>
              </tr>
              <tr>
                <th>Model</th>
                <td><span id="confirm_model"></span></td>
              </tr>
              <tr>
                <th>Year</th>
                <td><span id="confirm_year"></span></td>
              </tr>
            </tbody>
          </table>
        </div>
    </div>
</div>
<div class="row">
    <h3 style="text-align: center;margin-top:20px;"><b>Total Order Details</b></h3>
    <div class="col-sm-12">
        <div class="table-type-1">
          <table>
            <tbody>
              <tr>
                <th>Total Oil Cost</th>
                <th>Total Cost</th>
              </tr>
              <tr class="total-cell">
                <td>
                  <div>PKR <span id="total_oil_cost"></span></div>
                </td>
                <td>PKR <span id="total_cost"></span></td>
              </tr>
            </tbody>
          </table>
        </div>
    </div>
</div>
