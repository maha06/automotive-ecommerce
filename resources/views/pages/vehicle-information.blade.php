@extends('layouts.hero-section')
@section('head')
    <meta name="keywords" content="">
    <meta name="description" content="">
    <title>Mechon</title>
@endsection
@section('content')
<div id="content">

    <!-- - - - - - - - - - - - - - Breadcrumbs - - - - - - - - - - - - - - - - -->

    <div class="breadcrumbs-wrap" style="background:#616161">

      <div class="container">

        <h2 class="page-title">Service, Repairs & Maintenance</h2>

        <ul class="breadcrumbs">

          <li><a href="{{ route('pages.home') }}">Home</a></li>
          <li>Service and Repair for you Vehicle</li>

        </ul>

      </div>

    </div>
    <div class="page-section">

        <div class="container form-container">

            <div class="content-element3">

                <ul id="progressbar">
                    <li style="border-left: 2px solid #b1151b;" class="step first app-active"><span></span>Services</li>
                    <li style="border-left: 2px solid #b1151b;" class="step"><span></span>Workshop</li>
                    <li style="border-left: 2px solid #b1151b;" class="step"><span></span>Appointment</li>
                    <li style="border-left: 2px solid #b1151b;" class="step"><span></span>Parts</li>
                    <li style="border-left: 2px solid #b1151b;" class="step"><span></span>Vehicle </li>
                    <li style="border-left: 2px solid #b1151b;" class="step"><span>Confirmation</span></li>
                </ul>

            </div>

            <div class="content-element3">

                <div class="row">
                    <div class="col-sm-12">
                        <div class="alert alert-success" style="display:none;" id="approvel-message">
                            <button type="button" class="close" data-dismiss="alert"></button>
                            Your request has been sent to workshop for approval. We will notify you
                        </div>
                        <div class="alert alert-error" style="display:none;" id="error">
                          </div>
                        <form id="regForm" class="contact-form style-2" onsubmit="this.validateForm()">
                            @csrf

                            <div class="content-element4 tab" style="display: none;">
                                    @include('pages.services')
                            </div>
                            <div class="content-element4 tab" style="display: none;">
                                <div class="row">
                                    @include('pages.workshops')
                                </div>
                            </div>
                            <div class="content-element4 tab" style="display: none;">
                                <div class="row">
                                    @include('pages.appointment')
                                </div>
                            </div>
                            <div class="content-element4 tab" style="display: none;">
                                    @include('pages.spare-parts')
                            </div>
                            <div class="content-element4 tab" style="display: none;">
                            <div class="row">
                                <div class="col-md-4 form-group"></div>
                                <div class="col-md-4 form-group">
                                    <label>Where do you want parts to get delivered? <span style="color:#b1151b">(* if pickup option is selected)</span></label>
                                    <div class="auto-custom-select">
                                        <select data-default-text="Select One" name="parts_deliver" id="parts_deliver" required>
                                            <option value="Workshop">Workshop</option>
                                            <option value="Home">Home</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4 form-group"></div>
                            </div><hr>
                                <div class="row">
                                    <div class="col-md-4 form-group">
                                        <label>Make <span style="color:#b1151b">*</span></label>
                                        <div class="auto-custom-select">
                                            <select  class="cars" data-default-text="Select Car" name="make" id="make" required>
                                                @forelse ($cars as $car)
                                                    <option value="{{ $car->slug }}">{{ $car->name }}</option>
                                                @empty
                                                    <option value="">Cars Not Found</option>
                                                @endforelse
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-4 form-group">
                                        <label> Model <span style="color:#b1151b">*</span></label>
                                        <div class="select-col">
                                            <div class="auto-select-custom-select">
                                                <select name="model" id="model" required>
                                                    <option value="">Select Car First</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4 form-group">
                                        <label> Mileage </label>
                                        <div class="auto-custom-select">
                                            <input type="text" class="form-control" placeholder="Enter Mileage" name="mileage" id="mileage" required>
                                        </div>
                                    </div>
                                 </div>
                                <br>
                                <div class="row">
                                <div class="col-md-4 form-group">
                                        <label> Engine Type  <span style="color:#b1151b">*</span> </label>
                                        <div class="auto-custom-select">
                                        <select data-default-text="Select One" name="engine" id="engine" required>

                                        @foreach(\App\Labour::all() as $data)
                                        <option value="{{$data->id}}"> {{$data->labours}}
                                        </option>
                                        @endforeach
                                    </select> </div>
                                    </div>
                                    <div class="col-md-4 form-group">
                                        <label> Registration Number </label>
                                        <input type="text" class="form-control" name="registration_number" value="" placeholder="Enter Car Registration Number" id="registration_number" required>
                                    </div>
                                    <div class="col-md-4 form-group">
                                         <label> City <span style="color:#b1151b">*</span></label>
                                         <div class="auto-custom-select">
                                            <select data-default-text="Select City" name="city" id="city" required>
                                                <option value="lahore">Lahore</option>
                                                <option value="kasur">Kasur</option>
                                            </select>
                                        </div>
                                     </div>

                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-md-4 form-group">
                                        <label> Customer name <span style="color:#b1151b">*</span></label>
                                        <input type="text" class="form-control" placeholder="Name" name="customer_name" id="customer_name" required>
                                    </div>
                                     <div class="col-md-4 form-group">
                                         <label> Customer Email </label>
                                         <input type="email" name="customer_email" value="" placeholder="Email" id="customer_email" class="form-control" required>
                                     </div>
                                     <div class="col-md-4 form-group">
                                        <label> Mobile <span style="color:#b1151b">*</span></label>
                                        <input type="text" class="form-control" name="mobile" placeholder="Enter Mobile" id="mobile" required>
                                    </div>
                                </div>
                            </div>
                            <div class="content-element4 tab" style="display: none;">
                                    @include('pages.confirmation')
                                <br>
                                <br>
                                <br>
                                <div class="row">
                                    <div class="col-md-4">
                                        <h6>Terms and Conditions</h6>
                                    </div>
                                    <div class="col-md-8 align-right">
                                        <button class="btn btn-style-3 add-to-cart-button" type="button" id="agreed_btn" onclick="agreed_button()">Agreed</button>
                                        <button class="btn btn-style-3 add-to-cart-button" type="button" id="take_print" style="display: none" onclick="window.print()">Take Print</button>
                                    </div>

                                </div>
                                </div>
                            <div class="align-right">
                                <a href="javascript:;" class="btn btn-style-2" id="prevBtn" onclick="nextPrev(-1)">Prev</a>
                                <a href="javascript:;" class="btn btn-style-3" id="nextBtn" onclick="nextPrev(1)">Next</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@push('scripts')
<script>
    function agreed_button(){
        $('#take_print').css('display', 'inline-block');
        $('#agreed_btn').css('display', 'none');
    }

    function onlyUnique(value, index, self) {
        return self.indexOf(value) === index;
    }

    var services = [];
    var service_cost = parseInt(0, 10);
    var parts_cost = parseInt(0, 10);
    var workshop = '';
    var parts = [];
    var appointment = '';
    var workshop_detail = [];
    const hostUrl = "http://localhost/mechon/public";
    var pickup = '';


    $('.added_cart').css('display', 'none');
    $('.remove_service_cart').css('display', 'none');
    function add_to_cart_service(id){
        var service_id = $(id).data('id');
            services.push(service_id);
            // $('#item_added_'+service_id).css('display', 'block');

            $('#add_service_cart_'+service_id).css('display', 'none');
            $('#remove_service_from_cart_'+service_id).css('display', 'block');
    }

    function remove_service_from_cart(id){
        var service_id = $(id).data('id');
        removeElement(services, service_id);
        $('#item_added_'+service_id).css('display', 'none');
        $(id).css('display', 'none');
        $('#remove_service_from_cart_'+service_id).css('display', 'none');
        $('#add_service_cart_'+service_id).css('display', 'block');
    }


    function select_workshop(id){
        workshop = $(id).data('id');
        $(id).css('display', 'none');
        $('.select_workshop_button').css('display', 'none');
        $('.remove_workshop_button').css('display', 'none');
        $('#remove_cart_workshop_'+workshop).css('display', 'block');
    }

    function remove_workshop(id){
        workshop = '';
        $(id).css('display', 'none');

        $('.select_workshop_button').css('display', 'block');
        $('.remove_workshop_button').css('display', 'none');
        $('#add_cart_workshop_'+$(id).data('id')).css('display', 'block');
    }

    function add_to_cart_part(id){
        var part_id = $(id).data('id');
        parts.push(part_id);
        $(id).css('display', 'none');
        $('#remove_part_cart_'+part_id).css('display', 'block')
    }

    function remove_cart_part(id){
        var part_id = $(id).data('id');
        removeElement(parts, part_id);
        $(id).css('display', 'none');
        $('#add_part_cart_'+part_id).css('display', 'block');
    }


    function validateStepFive(){
        var token = $('meta[name="csrf-token"]').attr('content');
        var url = window.location.pathname.split("/")[1] === "staging"
                            ? "/staging/validate-step-five"
                            : hostUrl + "/validate-step-five";

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': token
            },
            url: url,
            method: 'POST',
            data: {
                name: $('#customer_name').val(),
                mobile: $('#mobile').val(),
                make: $('#make').val(),
                model: $('#model').val(),
                city: $('#city').val(),
                engine_id: $('#engine').val(),
                workshop: workshop,
                services: services,
                parts: parts
            },
            dataType: "json",
            success: function(result){
                console.log(result);
                $('#confirm_customer_name').html($('#customer_name').val());
                $('#confirm_make').html($('#make').val());
                $('#confirm_city').html($('#city').val());
                $('#confirm_engine').html(
$("#engine option:selected").text());
                $('#confirm_model').html($('#model').val());
                $('#confirm_mobile').html($('#mobile').val());
                $('#confirm_mileage').html($('#mileage').val());
                $('#confirm_registration_number').html($('#registration_number').val());
                $('#confirm_mobile').html($('#mobile').val());
                $('#confirm_customer_email').html($('#customer_email').val());
                $('#confirm_available_date').html($('#available_date').val());
                $('#confirm_available_time').html($('#available_time').val());
                $('#confirm_workshop_name').html(result.workshop.name);
                $('#confirm_parts-delivery').html($('#parts_deliver').val());

                if(pickup == 'Yes'){
                    $('#confirm_pickup').html('Yes');
                    $('#confirm_pickup_time').html($('#pickup_time').val());
                    $('#confirm_pickup_address').html($('#pickup_address').val());
                    $('#confirm_pickup_contact').html($('#pickup_contact').val());
                } else {
                    $('#pickup').css('display', 'none');
                    $('#pickup-time').css('display', 'none');
                    $('#pickup-address').css('display', 'none');
                    $('#pickup-contact').css('display', 'none');
                }



                $('#confirm_services tbody').remove();
                $.each(result.services, function(index, element){
                    $('#confirm_services').append(`<tr>
                        <td>${index+1}</td>
                        <td>${element.name}</td>
                        <td>${result.prices[index]}</td> </tr>
                    `);
                    service_cost += parseInt(result.prices[index], 10);

                });
                $('#total_service_cost').html(service_cost);
                $('#confirm_parts tbody').remove();
                    $.each(result.parts, function(index, element){
                        $('#confirm_parts').append(
                            `<tr>
                                <td>${index+1}</td>
                                <td>${element.name}</td>
                                <td>${element.retail_price}</td>
                            </tr>`
                        );
                        parts_cost += parseInt(element.retail_price, 10);
                    });
                    $('#total_parts_cost').html(parts_cost);
                    $('#total_cost').html(service_cost + parts_cost);
                    $('#error').css('display', 'none');

            },
            error: function(result){
                $('#error').text("Fields marked with * are necessary to fill in");
                $('#error').css('display', 'block');

                nextPrev(-1);
            }
        });
    }

    function validateForm(){
        var token = $('meta[name="csrf-token"]').attr('content');
        var url = window.location.pathname.split("/")[1] === "staging"
                            ? "/staging/vehicle-information"
                            : hostUrl + "/vehicle-information";
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': token
            },
            url: url,
            method: 'POST',
            data: {
                services: services,
                parts: parts,
                workshop: workshop,
                make: $('#make').val(),
                model: $('#model').val(),
                mileage: $('#mileage').val(),
                available_date: $('#available_date').val(),
                available_time: $('#available_time').val(),
                pickup: pickup,
                pickup_time: $('#pickup_time').val(),
                pickup_address: $('#pickup_address').val(),
                pickup_contact: $('#pickup_contact').val(),
                customer_name: $('#customer_name').val(),
                registration_number: $('#registration_number').val(),
                mobile: $('#mobile').val(),
                city: $('#city').val(),
                customer_email: $('#customer_email').val(),
                engine_id: $('#engine').val(),
                parts_deliver: $('#parts_deliver').val(),
            },
            dataType: "json",
            success: function(result){
                $('#approvel-message').css('display', 'block');
                setTimeout(() => {
                    window.location.reload();
                },2000)
            }
        });
    }

    function removeElement(array, elem) {
        var index = array.indexOf(elem);
        if (index > -1) {
            array.splice(index, 1);
        }
    }


    var currentTab = 0;
    showTab(currentTab);

    function showTab(n){
        var x = document.getElementsByClassName('tab');
        x[n].style.display = "block";

        if(n === 0){
            document.getElementById('prevBtn').style.display = 'none';
        }else{
            document.getElementById('prevBtn').style.display = 'inline-block';
        }

        if(n === (x.length -1 )){
            document.getElementById('nextBtn').innerHTML = 'Submit';
        }else{
            document.getElementById('nextBtn').innerHtml = 'Next';
        }

        //Functions for AJAX Call for form steps

        console.log('tab_number = '+ x.length + n );
        console.log('tab_number = '+ n );

        if(n === 0){
         console.log('Basic Details');
        }else if(n === 1){
            validateStepOne();
            console.log('Service');
        }else if(n === 2){
            validateStepTwo();
            console.log('Workshops')
        }else if(n === 3){
            validateStepThree();
            console.log('Appointment');
        }else if(n === 4){

            console.log('Spare parts');
        }else if(n === 5){
            validateStepFive()
            console.log('Confirmation');
        }

        fixStepIndicator(n);
    }

    function validateStepFour(){
        console.log("get services");
        var token = $('meta[name="csrf-token"]').attr('content');
        var url = window.location.pathname.split("/")[1] === "staging"
                            ? "/staging/validate-step-one"
                            : hostUrl + "/validate-step-one";

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': token
            },
            url: url,
            method: 'POST',
            data: {
                make: $('#make').val(),
                model: $('#model').val(),
                mileage: $('#mileage').val(),
                customer_name: $('#customer_name').val(),
                registration_number: $('#registration_number').val(),
                mobile: $('#mobile').val(),
                city: $('#city').val(),
                customer_email: $('#customer_email').val(),
            },
            dataType: "json",
            success: function(result){
                console.log(result);
            },
            error:function(error){
                nextPrev(-1);
            }
        });
    }

    function validateStepOne(){
        console.log("get workshops");
        var token = $('meta[name="csrf-token"]').attr('content');
        var url = window.location.pathname.split("/")[1] === "staging"
                            ? "/staging/validate-step-one"
                            : hostUrl + "/validate-step-one";

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': token
            },
            url: url,
            method: 'POST',
            data: {
                services: services
            },
            dataType: "json",
            success: function(result){
                console.log('f', result);
                $('#filtered_workshops tbody tr').remove();
                $.each(result, function(index, element){
                    console.log(index + ': '+ element.name);
                    $('#filtered_workshops').append(
                        "<tr style='color: black;'>" +
                        "<td>"+element.name+"</td>" +
                        "<td>"+ element.address + "</td>"+
                        "<td> <img src='/storage/"+element.image +"'/> </td>"+
                        "<td>"+ element.location + "</td>"+
                        "<td><a style='text-decoration:underline' href='/workshop/"+element.slug+"/details'>Profile</a></td>"+
                            "<td style='padding:25px 3px;display:table-cell;'>" + `
                        <a href="javascript:;" data-id="${element.id}" style="text-align:center;margin: 5px" class="button_cart workshop_select select_workshop_button" onclick="select_workshop(this)" type="button" id="add_cart_workshop_${element.id}" data-toggle="modal" data-target="#getAppointment">Select Workshop</a>
                        <a href="javascript:;" data-id="${element.id}" style="text-align:center;margin: 5px" class="button_cart workshop_select remove_workshop_button " onclick="remove_workshop(this)" type="button" id="remove_cart_workshop_${element.id}" >Remove Workshop</a>
                        `
                        +"</td></tr>"
                    )
                });
                $('.remove_workshop_button').css('background-color', 'grey');
                $('.remove_workshop_button').css('display', 'none');
                $('#error').css('display', 'none');

            },
            error:function(error){

                nextPrev(-1);
                $('#error').text("Atleast select one service");
                $('#error').css('display', 'block');

            }
        });
    }

    function validateStepTwo(){
        var token = $('meta[name="csrf-token"]').attr('content');
        var url = window.location.pathname.split("/")[1] === "staging"
                            ? "/staging/validate-step-two"
                            : hostUrl + "/validate-step-two";

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': token
            },
            url: url,
            method: 'POST',
            data: {
                workshop: workshop,
            },
            dataType: "json",
            success: function(result){
               if(!result){
                nextPrev(-1);
                $('#error').text("Atleast select one workshop");
                $('#error').css('display', 'block');

               }
               //$('#error').css('display', 'none');
            },
            error: function(result){
                console.log('here');
                nextPrev(-1);
                $('#error').text("Atleast select one workshop");
                $('#error').css('display', 'block');

            }
        });
    }

    function validateStepThree(){
        var token = $('meta[name="csrf-token"]').attr('content');
        var url = window.location.pathname.split("/")[1] === "staging"
                            ? "/staging/validate-step-three"
                            : hostUrl + "/validate-step-three";

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': token
            },
            url: url,
            method: 'POST',
            data: {
                available_date: $('#available_date').val(),
                available_time: $('#available_time').val(),
                pickup_time: $('#pickup_time').val(),
                pickup_address: $('#pickup_address').val(),
                pickup_contact: $('#pickup_contact').val(),
                workshop: workshop,
            },
            dataType: "json",
            success: function(result){


                    $('#prod_holder').empty();
                    $.each(result, function (index, element) {
                        console.log(element);
                        $('#prod_holder').append(
                            ` <div class="col-sm-4 col-xs-6">

                                <div class="product">
                                <a href='/spare-parts/${element.slug}/details' ">

                                    <img src="/storage/${element.pictures[0].path}"> </a>
                                    <div class="product-description">

                                    <div class="parts-title" style="display:flex;">
                                    <a href='/spare-parts/${element.slug}/details' "><h5 style="width:50%;" class="product-name">${element.name}</h5><a>
                                    </div>

                                    <div class="pricing-area flex-row flex-justify" style="margin-top:40px;">

                                        <div class="product-price">
                                        PKR ${element.retail_price}
                                        </div>


                                    </div>
                                    <a href="javascript:;" class="btn btn-small btn-style-3 add_part_cart button_cart" id="add_part_cart_${element.id}" onclick="add_to_cart_part(this)" data-id="${element.id}"><i class="licon-cart"></i>Add to cart</a>
                                    <a href="javascript:;" class="btn btn-small btn-style-3 remove_part_cart button_cart remove-cart-button remove_spare_part_button" id="remove_part_cart_${element.id}" onclick="remove_cart_part(this)" data-id="${element.id}"><i class="licon-cart"></i>Remove from cart</a>

                                    </div>

                                </div>
                            </div>`
                        );
                    });
                $('.remove_spare_part_button').css('display', 'none');
                $('.remove_spare_part_button').css('background-color', 'grey');
                $('#error').css('display', 'none');

            },
            error:function(error){
                nextPrev(-1);
                $('#error').text("Fields marked with * are necessary to fill in");
                $('#error').css('display', 'block');


            }

        });
    }

    function nextPrev(n) {
        // This function will figure out which tab to display
        var x = document.getElementsByClassName("tab");
        // Exit the function if any field in the current tab is invalid:
        // if (n == 1 && !validateForm()) return false;
        // Hide the current tab:
        x[currentTab].style.display = "none";
        // Increase or decrease the current tab by 1:
        currentTab = currentTab + n;
        // if you have reached the end of the form... :
        if (currentTab >= x.length) {
            //...the form gets submitted:
            document.getElementById("regForm").onsubmit = this.validateForm();
            return false;
        }
        // Otherwise, display the correct tab:
        showTab(currentTab);
        // var t = n + tab_num;
        console.log('tab number: ', currentTab);
    }
    function fixStepIndicator(n) {
        // This function removes the "active" class of all steps...
        var i, x = document.getElementsByClassName("step");
        for (i = 0; i < x.length; i++) {
            x[i].className = x[i].className.replace(" app-active", "");
        }
        //... and adds the "active" class to the current step:
        x[n].className += " app-active";
    }
    $(document).ready(function(){
        $('#checkbox-1').click(function(){
            if($(this).prop("checked") == true){
                $('#appointment_yes').css('display', 'block');
                pickup = 'Yes';

            }
            else if($(this).prop("checked") == false){
                $('#appointment_yes').css('display', 'none');
                pickup = 'No';
            }

        });

        $('#checkbox-2').click(function(){

            if($(this).prop("checked") == true){
                $('#appoint_details').css('display', 'block');
            }else if($(this).prop("checked") == false){
                $('#appoint_details').css('display', 'none');
            }
        });
    });




$(document).on("change", ".cars", function () {
    var car_id = $(this).val();
    var token = $('meta[name="csrf-token"]').attr("content");
    var url = window.location.pathname.split("/")[1] === "staging"
                            ? "/staging/get-all/models/" + car_id
                            : hostUrl + "/get-all/models/" + car_id;

    $.ajax({
        headers: {
            "X-CSRF-TOKEN": token,
        },
        method: "POST",
        url: url,
        data: {
            car_id: car_id,
        },
        dataType: "json",
        success: function (data) {
            $("#model").empty();
            if (data.length === 0) {
                $("#model").append(
                    '<option value="">No Model Found For This Car</option>'
                );
            } else {
                $("#model").append(
                    '<option value="">None selected</option>'
                );
                $.each(data, function (index, data) {
                    $("#model").append(
                        '<option value="' +
                            data.slug +
                            '">' +
                            data.name +
                            "</option>"
                    );
                });
            }
        },
        error: function (request, status, error) {
            console.log(request);
        },
    });
});

</script>

@endpush
@endsection
