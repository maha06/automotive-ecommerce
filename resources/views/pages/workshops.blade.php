<div class="content-element">
    <div class="row">
        <div class="col-sm-12">
            <div class="table-type-1">
                <table id="filtered_workshops">
                    <thead>
                    <tr>
                        <th>Workshop Name</th>
                        <th>Address</th>
                        <th>Image</th>
                        <th>Location</th>
                        <th>Profile</th>
                        <th>Actions</th>
                    </tr>

                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
