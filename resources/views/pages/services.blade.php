<div class="row">
    @forelse ($services as $service)
        <div class="col-md-4" style="margin-bottom:2%">
            <div class="product">
                <!-- - - - - - - - - - - - - - Product Image - - - - - - - - - - - - - - - - -->
                @php
                        if ($pic = $service->pictures->first()) {
                        @endphp
                            <img src="{{ (config('app.url'). '/mechon/storage/app/public/'. $pic->path) }}" />
                            <!-- <img src="{{ asset('/storage/' . $pic->path) }}" alt="{{ $pic->path }}"> -->

                        @php
                        }
                        @endphp
                <!-- - - - - - - - - - - - - - End of Product Image - - - - - - - - - - - - - - - - -->

                <!-- - - - - - - - - - - - - - Product Description - - - - - - - - - - - - - - - - -->
                <div class="product-description">

                  <h5 class="product-name"><a href="#">{{ $service->name }}</a></h5>
<!-- <div class="added_cart" id="item_added_{{$service->id}}"><h5>Added to cart</h5></div> -->
                  <div class="pricing-area flex-row flex-justify">
<!--
                    <div class="product-price">
                      PKR {{ $service->price }}
                    </div> -->
                    <!-- <ul class="rating">
                      <li><i class="licon-star"></i></li>
                      <li><i class="licon-star"></i></li>
                      <li><i class="licon-star"></i></li>
                      <li class="empty-half"><i class="licon-star"></i></li>
                      <li class="empty"><i class="licon-star"></i></li>
                    </ul> -->

                  </div style="line-height:50px">
                  <a href="javascript:;" onclick="add_to_cart_service(this)" data-id="{{$service->id}}" id="add_service_cart_{{$service->id}}" class="btn btn-small btn-style-3"><i class="licon-cart"></i>Add to Cart</a>
                  <a href="javascript:;" onclick="remove_service_from_cart(this)" data-id="{{$service->id}}" id="remove_service_from_cart_{{$service->id}}" class="btn btn-small btn-style-3" style="background-color:grey; display:none"><i class="fa fa-trash"></i>Remove from Cart</a>
                </div>
                <!-- - - - - - - - - - - - - - End of Product Description - - - - - - - - - - - - - - - - -->

              </div>
        </div>
    @empty
        <h1>Service Not Found</h1>
    @endforelse
</div>
