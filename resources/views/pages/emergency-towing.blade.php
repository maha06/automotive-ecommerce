@extends('layouts.hero-section')
@section('head')
    <meta name="keywords" content="">
    <meta name="description" content="">
    <title>Mechon | Emergency Towing &  Repairing</title>
@endsection
@section('content')
<div id="content">

    <!-- - - - - - - - - - - - - - Breadcrumbs - - - - - - - - - - - - - - - - -->

    <div class="breadcrumbs-wrap" style="background:#616161;">

      <div class="container">

        <h2 class="page-title">Emergency Towing &  Repairing</h2>

        <ul class="breadcrumbs">

          <li><a href="{{ route('pages.home') }}">Home</a></li>
          <li>Emergency Towing &  Repairing</li>

        </ul>

      </div>

    </div>

    <!-- - - - - - - - - - - - - end Breadcrumbs - - - - - - - - - - - - - - - -->
    <div class="page-section">
        <form method="post" action="{{ route('pages.emergency-towing.request') }}">
        @csrf
        <div class="container">
            @include('common.partials.flash')
            <div class="content-element3">
                <h4>Emergency Service</h4>
                <p>The following emergency services are available 24/7:
                <ul class="custom-list type-4" style="color: black;">

                    <li>Vehicle Towing (Doesn't include heavy commercial vehicles)</li>
                    <li>Vehicle emergency repairs</li>
                    <li>Emergency Vehicle pick and drop</li>

                </ul>
                </p>

            </div>
            <hr class="item-divider-2 style-3">

            <div class="content-element3">
                <div class="row">
                    <div class="col-md-12">
                        <h6 style="margin-bottom:20px;">Please Call at our Emergency Help-line Numbers below (Available 24/7) </h6>
                    </div>
                    <div class="col-md-3">
                        <h4>Emergency Numbers</h4>
                    </div>
                    <div class="col-md-3">
                        <div class="table-type-2">
                            <table>
                                <tr>
                                    <td>Number 1</td>
                                </tr>
                                <tr>
                                    <td>Number 2</td>
                                </tr>
                                <tr>
                                    <td>Number 3</td>
                                </tr>
                                <tr>
                                    <td>Number 4</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <hr class="item-divider-2 style-3">
            <div class="content-element3">
                <div class="row" style="margin-bottom: 20px;">
                    <div class="col-md-2">
                        <h4>Cost Estimate</h4>
                    </div>
                    <div class="col-md-4">
                        <div class="table-type-2">
                            <table>
                                <thead>
                                <tr>
                                    <td>Visit Fee</td>
                                    <th>500 RS</th>
                                </tr>
                                <tr>
                                    <td>Repairing Charges</td>
                                    <th>As Per Actual</th>
                                </tr>
                                <tr>
                                    <td>Parts Charges</td>
                                    <th>As Per Actual</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6" style="text-align: end;">
                        <input type="Submit" value="Submit" class="btn btn-style-3" />
                    </div>
                </div>
            </div>
        </div>
    </form>
    </div>
</div>
@endsection
