@extends('layouts.hero-section')
@section('head')
    <meta name="keywords" content="">
    <meta name="description" content="">
    <title>Mechon</title>
@endsection
@section('content')
<div class="content">
    <div class="media-holder">

        <div class="container">
            @include('common.partials.flash')
            <div class="content-element6 align-center">
                <h2 style="color:black;font-size:55px;">Find The Best Auto Workshops In Your Area!</h2>
                <h4 style="color:black">Select Your Car & Book Your Appoinment Today!</h4>
            </div>

          <div class="tabs type-2 tabs-section clearfix">
            <!--tabs navigation-->
            <ul class="tabs-nav clearfix">
                <li>
                    <a href="#tab-1">Find Workshops</a>
                </li>
                <li>
                    <a href="#tab-2">Find Spare Parts</a>
                </li>
            </ul>
            <!--tabs content-->
            <div class="tabs-content">
                <div id="tab-2">
                    <p>Find parts and accessories for your vehicle.</p>
                    <form action="{{ route('pages.search.spare-parts') }}" method="post">
                        <div class="select-area tab-col-auto">
                            @csrf
                            <div class="select-col">
                                <div class="auto-custom-select">
                                    <select class="car-makes" name="car_id">
                                        <option value="">Select Car Makes</option>
                                        @forelse ($cars as $car)
                                            <option value="{{ $car->id }}">{{ $car->name }}</option>
                                        @empty
                                            <option value="">No Car Found</option>
                                        @endforelse
                                    </select>
                                </div>
                            </div>
                            <div class="select-col">
                                <div class="auto-custom-select">
                                    <div class="select-col">
                                        <div class="auto-select-custom-select">
                                            <select name="model_id" id="car-models">
                                                <option value="">Select Car First</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="select-col col-extra2">
                                <input type="text" name="part_name" placeholder="a product, brand or keyword">
                            </div>
                            <div class="select-col">
                                <input type="submit" class="btn btn-style-3" style="color:#b1151b;
                                background:white;
                                border-color: white;" value="Search"/>
                            </div>
                        </div>
                    </form>
                    <a href="{{ route('pages.frontend.spare-parts') }}" class="info-btn">More search options</a>
                </div>
                <div id="tab-1">
                    <p>Find workshops around you.</p>
                    <form action="{{ route('pages.search.workshops') }}" method="post">
                        @csrf
                        <div class="select-area tab-col-auto">
                            <div class="select-col">
                                <div class="auto-custom-select">
                                    <select name="city">
                                        <option value="">Select City</option>
                                        <option value="kasur">Kasur</option>
                                        <option value="lahore">Lahore</option>
                                        <option value="karachi">Karachi</option>
                                    </select>
                                </div>
                            </div>
                            <div class="select-col col-extra2">
                                <div class="auto-custom-select">
                                    <input type="text" name="workshop_name" placeholder="Workshop Name">
                                </div>
                            </div>

                            <div class="select-col">
                                <input type="submit" class="btn btn-style-3" style="color:#b1151b;
                                background:white;
                                border-color: white;" value="Search"/>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
          </div>
        </div>
      </div>
        <div class="page-section-bg">
            <div class="container">
              <h2 class="section-title">Main Services</h2>
              <div class="icons-box">
                <div class="row flex-row">
                  <div class="col-md-4 col-xs-6">
                    <div class="icons-wrap">
                      <a href="{{ route('pages.maintenance') }}" class="icons-item type-2">
                        <img src="{{ asset('frontend/images/maintaince.jpg') }}" alt="">
                        <div class="item-box">
                            <i class="licon-oil-pressure"></i>
                            <h4 class="icons-box-title">Service, Repairs & Maintenance</h4>
                        </div>
                      </a>
                    </div>
                  </div>
                  <div class="col-md-4 col-xs-6">
                    <div class="icons-wrap">
                      <a href="{{ route('pages.frontend.spare-parts') }}" class="icons-item type-2">
                        <img src="{{ asset('frontend/images/maintaince.jpg') }}" alt="">
                        <div class="item-box">
                            <i class="licon-road"></i>
                            <h4 class="icons-box-title">Spare Parts Purchase</h4></h4>
                        </div>
                      </a>
                    </div>
                  </div>
                  <div class="col-md-4 col-xs-6">
                    <div class="icons-wrap">
                      <a href="{{ route('engine-oil.index') }}" class="icons-item type-2">
                        <img src="{{ asset('frontend/images/maintaince.jpg') }}" alt="">
                        <div class="item-box">
                            <i class="licon-car-battery"></i>
                            <h4 class="icons-box-title">Engine Oil Purchasing</h4>
                        </div>
                      </a>
                    </div>
                  </div>
                  <div class="col-md-4 col-xs-6">
                    <div class="icons-wrap">
                      <a href="{{ route('pages.emergency-towing') }}" class="icons-item type-2">
                        <img src="{{ asset('frontend/images/maintaince.jpg') }}" alt="">
                        <div class="item-box">
                            <i class="licon-transmission"></i>
                            <h4 class="icons-box-title">Emergency Towing & Repairing</h4>
                        </div>
                      </a>
                    </div>
                  </div>
                  <div class="col-md-4 col-xs-6">
                    <div class="icons-wrap">
                      <a href="{{ route('pages.customer-complaints') }}" class="icons-item type-2">
                        <img src="{{ asset('frontend/images/maintaince.jpg') }}" alt="">
                        <div class="item-box">
                            <i class="licon-check"></i>
                            <h4 class="icons-box-title">Customer Complaints</h4>
                        </div>
                      </a>
                    </div>
                  </div>
                  <div class="col-md-4 col-xs-6">
                    <div class="icons-wrap">
                      <a href="{{ route('home-services.index') }}" class="icons-item type-2">
                        <img src="{{ asset('frontend/images/maintaince.jpg') }}" alt="">
                        <div class="item-box">
                            <i class="licon-car2"></i>
                            <h4 class="icons-box-title">Home Repairing Services</h4>
                        </div>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
</div>
@endsection

@push('scripts')
<script>
$(document).on("change", ".car-makes", function () {
    var car_id = $(this).val();
    var token = $('meta[name="csrf-token"]').attr("content");
    var url = window.location.pathname.split("/")[1] === "staging"
                            ? "/staging/get-all/car/models/" + car_id
                            : "" + "/get-all/car/models/" + car_id;

    $.ajax({
        headers: {
            "X-CSRF-TOKEN": token,
        },
        method: "POST",
        url: url,
        data: {
            car_id: car_id,
        },
        dataType: "json",
        success: function (data) {
            $("#car-models").empty();
            if (data.length === 0) {
                $("#car-models").append(
                    '<option value="">No Model Found For This Car</option>'
                );
            } else {
                $("#car-models").append(
                    '<option value="">None selected</option>'
                );
                $.each(data, function (index, data) {
                    $("#car-models").append(
                        '<option value="' +
                            data.id +
                            '">' +
                            data.name +
                            "</option>"
                    );
                });
            }
        },
        error: function (request, status, error) {
            console.log(request);
        },
    });
});

</script>
@endpush
