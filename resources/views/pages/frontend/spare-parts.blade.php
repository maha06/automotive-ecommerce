@extends('layouts.hero-section')
@section('head')
    <meta name="keywords" content="">
    <meta name="description" content="">
    <title>Mechno</title>
@endsection
@section('content')
<div id="content">

    <!-- - - - - - - - - - - - - - Breadcrumbs - - - - - - - - - - - - - - - - -->

    <div class="breadcrumbs-wrap" style="background:#616161">

        <div class="container">

            <h2 class="page-title">Spare Parts</h1>

            <ul class="breadcrumbs">

            <li><a href="{{ route('pages.home') }}">Home</a></li>
            <li>Spare Parts</li>

            </ul>

        </div>
    </div>
</div>

<div id="content" class="page-content-wrap">

    <div class="container">

        <div class="row">

            <aside id="sidebar" class="sbl style-2 col-md-3 col-sm-12">

            <div class="filter-section">

                <div class="accordion toggle no-of">
                </div>

{{--
                <div class="accordion toggle">

                    <!--accordion item-->
                    <div class="accordion-item">
                        <h6 class="a-title active">Year</h6>
                        <div class="a-content">

                        <div class="input-wrapper">

                            <input type="checkbox" name="checkbox-4" id="checkbox-4" checked="">
                            <label for="checkbox-4">Any</label>

                        </div>

                        <div class="input-wrapper">

                            <input type="checkbox" name="checkbox-5" id="checkbox-5">
                            <label for="checkbox-5">2018</label>

                        </div>

                        <div class="input-wrapper">

                            <input type="checkbox" name="checkbox-6" id="checkbox-6">
                            <label for="checkbox-6">2017</label>

                        </div>

                        <div class="input-wrapper">

                            <input type="checkbox" name="checkbox-7" id="checkbox-7">
                            <label for="checkbox-7">2016</label>

                        </div>

                        <div class="input-wrapper">

                            <input type="checkbox" name="checkbox-8" id="checkbox-8">
                            <label for="checkbox-8">2015</label>

                        </div>

                        <div class="input-wrapper">

                            <input type="checkbox" name="checkbox-9" id="checkbox-9">
                            <label for="checkbox-9">2014</label>

                        </div>

                        <div class="input-wrapper">

                            <input type="checkbox" name="checkbox-10" id="checkbox-10">
                            <label for="checkbox-10">2013</label>

                        </div>

                        <div class="input-wrapper">

                            <input type="checkbox" name="checkbox-11" id="checkbox-11">
                            <label for="checkbox-11">2012</label>

                        </div>

                        <div class="input-wrapper">

                            <input type="checkbox" name="checkbox-12" id="checkbox-12">
                            <label for="checkbox-12">2011</label>

                        </div>

                        <div class="input-wrapper">

                            <input type="checkbox" name="checkbox-13" id="checkbox-13">
                            <label for="checkbox-13">2010</label>

                        </div>

                        </div>
                    </div>

                </div> --}}

                <div class="accordion toggle">

                    <!--accordion item-->
                    <div class="accordion-item">
                        <h6 class="a-title active">Make</h6>
                        <div class="a-content">

                        <div class="input-wrapper">

                            <input type="checkbox" name="checkbox-14" id="checkbox-14" checked>
                            <label for="checkbox-14">Any</label>

                        </div>

                        @forelse ($cars as $key => $car)
                            <div class="input-wrapper">

                                <input type="checkbox" class="checkbox-cars" name="car_ids[]" id="checkbox-car-{{ $car->id }}" data-car={{ $car->id }}>
                                <label for="checkbox-car-{{ $car->id }}">{{ $car->name }}</label>

                            </div>
                        @empty
                            <h3>No Car Found</h3>
                        @endforelse

                        </div>
                    </div>

                </div>

                <div class="accordion toggle">

                    <!--accordion item-->
                    <div class="accordion-item">
                        <h6 class="a-title active">Model</h6>
                        <div class="a-content">

                        <div class="input-wrapper">

                            <input type="checkbox" name="checkbox-25" id="checkbox-25" checked="">
                            <label for="checkbox-25">Any</label>

                        </div>

                        @forelse ($models as $key => $model)
                            <div class="input-wrapper">

                                <input type="checkbox" class="checkbox-models" name="model_ids[]" id="checkbox-model-{{ $model->id }}" data-model={{ $model->id }}>
                                <label for="checkbox-model-{{ $model->id }}">{{ $model->name }}</label>

                            </div>
                        @empty
                            <h3>No Car Found</h3>
                        @endforelse

                        </div>
                    </div>

                </div>

                {{-- <div class="accordion toggle no-of">
                    <!--accordion item-->
                    <div class="accordion-item">
                        <h6 class="a-title active">Price</h6>
                        <div class="a-content"> --}}

                        <!--price-->
                        {{-- <fieldset class="price-scale">
                            <div class="clearfix range-values">
                            <input class="f-left first-limit price-range" readonly type="text" min="1000" value="PKR 1,000">
                            <input class="f-right last-limit price-range" readonly type="text" min="100000" value="PKR 1,00,000">
                            </div>
                            <div id="price"></div>
                        </fieldset> --}}

                        {{-- </div>
                    </div>
                </div> --}}
            </div>

            </aside>
            <main id="main" class="col-md-9 col-sm-12">

            <div class="dealers-section product-type">

                <h3 class="section-title">{{ $posts->count() }} Spare Parts in Workshop Pakistan</h3>

                <!-- dealer sorting -->

                <div class="flex-row flex-justify">

                <div class="sort-dealer">


                    <div class="sort-col">
                        <div class="auto-custom-select auto-select">
                            <select name="price-filter"  id="price_filter" data-default-text="Apply Filter">
                                <option value="1">High to Low</option>
                                <option value="0">Low to High</option>
                            </select>
                        </div>
                    </div>

                </div>

                <div class="settings-view-products">

                    <div class="sort-criteria">

                    <div class="view-type">

                        <a href="#" title="grid View" data-view="grid" data-target="#product-holder" class="active"><i class="licon-icons"></i></a>

                        <a href="#" title="list View" data-view="list" data-target="#product-holder"><i class="licon-menu2"></i></a>

                    </div>

                    </div><!--/ .sort-criteria -->

                </div>

                </div>

            </div>

            <div id="product-holder" class="products-holder view-grid">
                @include('common.partials.flash')
                <div id="cart-add-success-message" style="display: none;" class="alert alert-success">
                    <button type="button" class="close" data-dismiss="alert"></button>
                    Add to Cart
                </div>
                <div id="cart-already-add-success-message" style="display: none;" class="alert alert-success">
                    <button type="button" class="close" data-dismiss="alert"></button>
                    Already Add to Cart
                </div>
                <div class="row flex-row" id="spare-parts-data">
                    @forelse ($posts as $post)

                        <div class="col-sm-6 col-xs-6">

                            <!-- - - - - - - - - - - - - - Product - - - - - - - - - - - - - - - - -->
                            <div class="product">

                            <!-- - - - - - - - - - - - - - Product Image - - - - - - - - - - - - - - - - -->
                            <figure class="product-image">
                            <a href="{{ route('pages.frontend.spare-part-details', $post->slug) }}">
                            @php

if ($pic = $post->pictures->first()) {

@endphp
    <img src="{{ (config('app.url'). '/mechon/storage/app/public/'. $pic->path) }}" height="280px" />
    <!-- <img style="height: 280px;" src="{{ asset('/storage/' . $pic->path) }}" alt=""> -->

@php
}
@endphp
                              </a>
                            </figure>
                            <!-- - - - - - - - - - - - - - End of Product Image - - - - - - - - - - - - - - - - -->

                            <!-- - - - - - - - - - - - - - Product Description - - - - - - - - - - - - - - - - -->
                            <div class="product-description">

                                <div class="row">
                                    <div class="col-xs-12">

                                        <div style="display: flex;">
                                            <h5 class="product-name"><a href="{{ route('pages.frontend.spare-part-details', $post->slug) }}">{{ $post->name }}</a></h5>

                                        </div>

                                        @if ($post->discount > 0)
                                        <div class="pricing-area">
                                            <div class="product-price" >
                                              <span style="text-decoration: line-through;"> PKR {{ $post->retail_price }} </span><sup style="color:Red"> {{ $post->discount }}% </sup>
                                            </div>
                                            @php
                                            $price = $post->retail_price;
                                            $discount = ($price * $post->discount)/100;
                                            $total = $price - $discount;
                                            @endphp
                                            <div class="product-price" >
                                              PKR {{ $total }}
                                            </div>

                                        </div>
                                            @if ($post->quantity > 0)
                                                @auth
                                                    <a href="javascript:;" style="width:100%;" data-part="{{$post->id}}" data-price="{{$total}}"class="add-to-spare-part-cart btn btn-small btn-style-3"><i class="licon-cart"></i>Add to Cart</a>
                                                @else
                                                    <a href="{{ route('login') }}" id="userId" value="0" style="width:100%;"  class="btn btn-small btn-style-3"><i class="licon-cart"></i>You Need To First Login</a>
                                                @endauth
                                            @else
                                                    Out of Stock
                                            @endif
                                        @else
                                        <div class="pricing-area">
                                            <div class="product-price">
                                              PKR {{ $post->retail_price }}
                                            </div>
                                        </div>
                                            @if ($post->quantity > 0)
                                                @auth
                                                    <a href="javascript:;" style="width:100%;" data-part="{{$post->id}}" data-price="{{$post->retail_price}}"class="add-to-spare-part-cart btn btn-small btn-style-3"><i class="licon-cart"></i>Add to Cart</a>
                                                @else
                                                    <a href="{{ route('login') }}" id="userId" value="0" style="width:100%;"  class="btn btn-small btn-style-3"><i class="licon-cart"></i>You Need To First Login</a>
                                                @endauth
                                            @else
                                                Out of Stock
                                            @endif
                                        @endif




                                    </div>
                                </div>

                            </div>
                            <!-- - - - - - - - - - - - - - End of Product Description - - - - - - - - - - - - - - - - -->

                            </div>
                            <!-- - - - - - - - - - - - - - / Product - - - - - - - - - - - - - - - - -->

                        </div>
                    @empty
                        <h3 style="width:100%;text-align:center">Workshops Not Found</h3>
                    @endforelse

                </div>

            </div>
            {!! $posts->links() !!}
            {{-- <ul class="pagination">
                <li><a href="#" class="prev-page"></a></li>
                <li><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#" class="next-page"></a></li>
            </ul> --}}

            </main>

        </div>
    </div>
</div>
@endsection
@push('scripts')
<script>
$(document).on("click", ".add-to-spare-part-cart", function (event) {
    event.preventDefault();
    var token = $('meta[name="csrf-token"]').attr("content");
    var part_id = $(this).data("part");
    var price = $(this).data("price");
    var url = window.location.pathname.split("/")[1] === "staging"
                            ? "/staging/add-to-cart/spare-part/"+ part_id
                            : "http://localhost/mechon/public" + "/add-to-cart/spare-part/"+part_id;
    $.ajax({
        headers: {
            "X-CSRF-TOKEN": token,
        },
        method: "POST",
        url: url,
        data: {
            post_id:  part_id,
            price: price,
        },
        dataType: "json",
        success: function (data) {
            console.log(data);
            if(data != 0){
                var message = document.getElementById("cart-add-success-message");
                message.setAttribute("style", "display:block");
                var count = $('#badge').text();
                $('#badge').text(parseInt(count)+1);
            }else{
                var message = document.getElementById("cart-already-add-success-message");
                message.setAttribute("style", "display:block");
            }
        },
        error: function (request, status, error) {
            console.log(request);
        },
    });
});
        $(document).on('change', '#price_filter', function(){
            var price = $(this).val();
            var userId = $('#userId').val();
            var url = window.location.pathname.split("/")[1] === "staging"
            ? "/staging/get-all/price/spare-parts"
            : "" + "/get-all/price/spare-parts";

            var token = $('meta[name="csrf-token"]').attr("content");
            $.ajax({
                headers: {
                    "X-CSRF-TOKEN": token,
                },
                method: "POST",
                url: url,
                data: {
                    price: price
                },
                dataType: "json",
                success: function (data) {
                    if(data.length > 0){
                        $('#spare-parts-data').empty();
                        $.each(data, function (index, element) {
                            var workshop_id = element.workshop_id;
                            var car_id = element.car_id;
                            var model_id = element.model_id;
                            var token = $('meta[name="csrf-token"]').attr("content");

                            var url = window.location.pathname.split("/")[1] === "staging"
                            ? "/staging/get/workshop/" + workshop_id + "/car/" + car_id + "/model/" + model_id
                            : "" + "/get/workshop/" + workshop_id + "/car/" + car_id + "/model/" + model_id;

                            $.ajax({
                                headers: {
                                    "X-CSRF-TOKEN": token,
                                },
                                method: "POST",
                                url: url,
                                data: {
                                    workshop_id: workshop_id,
                                    car_id: car_id,
                                    model_id: model_id,
                                },
                                dataType: "json",
                                success: function (result) {
                                    workshopName = result.workshop.name;
                                    carName = result.car.name;
                                    modelName = result.model.name;
                                    $('#spare-parts-data').append(`
                                        <div class="col-sm-6 col-xs-6">
                                            <div class="product">
                                            <figure class="product-image">
                                                <img style="height: 280px;" src="/storage/${element.image}" alt="${element.name}">
                                            </figure>
                                            <div class="product-description">
                                                <div class="row">
                                                    <div class="col-xs-12">
                                                        <div style="display: flex;">
                                                            <h5 class="product-name"><a href="#">${element.name}</a></h5>
                                                            <div class="rating-area" style="width: 50%;text-align: right;">
                                                                <h5 class="product-name">${element.company}</h5>
                                                            </div>
                                                        </div>
                                                        <div class="pricing-area">
                                                            <div class="product-price">
                                                            PKR ${element.price}
                                                            </div>
                                                        </div>
                                                        <div class="pricing-area">
                                                            <div class="contact-item">
                                                                <div class="contact-title"><span class="contact-desc"><h5>${element.description}</h5></span></div> &nbsp;
                                                            </div>
                                                        </div>
                                                        <div class="pricing-area">
                                                            <div class="mpg">
                                                            <span>Workshop</span>${workshopName}
                                                            </div>
                                                            <div class="mpg">
                                                                <span>Car</span>${carName} (${modelName})
                                                            </div>
                                                        </div>
                                                        ${userId == 0 ? `<a href="/login" id="userId" value="0" style="width:100%;"  class="btn btn-small btn-style-3"><i class="licon-cart"></i>You Need To First Login</a>
` : `<a href="javascript:;" style="width:100%;" data-part="${element.id}" data-price="${element.price}"class="add-to-spare-part-cart btn btn-small btn-style-3"><i class="licon-cart"></i>Add to Cart</a>`}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    `);
                                },
                                error: function (request, status, error) {
                                    console.log("andar wala error a gya ha");
                                    console.log(request);
                                },
                            });
                        });
                    }
                },
                error: function (request, status, error) {
                    console.log("error a gya ha");
                    console.log(request);
                },
            });
        });

        var dataCars = [];
        var dataModels = [];
         $(document).on('click' , '.checkbox-cars', function () {
            var check = $(this).val();
            var userId = $('#userId').val();
            var carValue = $(this).data("car");
            if ($(this).is(':checked')) {
                dataCars.push($(this).data("car"));
            }
            if (!$(this).is(':checked')) {
                dataCars.pop($(this).val());
            }

            if(dataCars.length <= 0){
                var url = window.location.pathname.split("/")[1] === "staging"
                            ? "/staging/get-all/workshop/spare-parts"
                            : "" + "/get-all/workshop/spare-parts";

            }else{
                if(dataModels.length > 0){
                    var url = window.location.pathname.split("/")[1] === "staging"
                            ? "/staging/get-all/workshop/models/" + dataModels + "/cars/" + dataCars + "/spare-parts/"
                            : "" + "/get-all/workshop/models/" + dataModels + "/cars/" + dataCars + "/spare-parts/";

                }else{
                    var url = window.location.pathname.split("/")[1] === "staging"
                            ? "/staging/get-all/spare-parts/" + dataCars
                            : "" + "/get-all/spare-parts/" + dataCars;

                }

            }
            var workshopName = '';
            var carName = '';
            var modelName = '';
            var token = $('meta[name="csrf-token"]').attr("content");
            $.ajax({
                headers: {
                    "X-CSRF-TOKEN": token,
                },
                method: "POST",
                url: url,
                data: {
                    dataModels: dataModels,
                    dataCars: dataCars,
                },
                dataType: "json",
                success: function (data) {
                    if(data.length > 0){
                        $('#spare-parts-data').empty();
                        $.each(data, function (index, element) {
                            var workshop_id = element.workshop_id;
                            var car_id = element.car_id;
                            var model_id = element.model_id;
                            var token = $('meta[name="csrf-token"]').attr("content");
                            var url = window.location.pathname.split("/")[1] === "staging"
                            ? "/staging/get/workshop/" + workshop_id + "/car/" + car_id + "/model/" + model_id
                            : "" + "/get/workshop/" + workshop_id + "/car/" + car_id + "/model/" + model_id;

                            $.ajax({
                                headers: {
                                    "X-CSRF-TOKEN": token,
                                },
                                method: "POST",
                                url: url,
                                data: {
                                    workshop_id: workshop_id,
                                    car_id: car_id,
                                    model_id: model_id,
                                },
                                dataType: "json",
                                success: function (result) {
                                    workshopName = result.workshop.name;
                                    carName = result.car.name;
                                    modelName = result.model.name;

                                    $('#spare-parts-data').append(`
                                        <div class="col-sm-6 col-xs-6">
                                            <div class="product">
                                            <figure class="product-image">
                                                <img style="height: 280px;" src="/storage/${element.image}" alt="${element.name}">
                                            </figure>
                                            <div class="product-description">
                                                <div class="row">
                                                    <div class="col-xs-12">
                                                        <div style="display: flex;">
                                                            <h5 class="product-name"><a href="#">${element.name}</a></h5>
                                                            <div class="rating-area" style="width: 50%;text-align: right;">
                                                                <h5 class="product-name">${element.company}</h5>
                                                            </div>
                                                        </div>
                                                        <div class="pricing-area">
                                                            <div class="product-price">
                                                            PKR ${element.price}
                                                            </div>
                                                        </div>
                                                        <div class="pricing-area">
                                                            <div class="contact-item">
                                                                <div class="contact-title"><span class="contact-desc"><h5>${element.description}</h5></span></div> &nbsp;
                                                            </div>
                                                        </div>
                                                        <div class="pricing-area">
                                                            <div class="mpg">
                                                            <span>Workshop</span>${workshopName}
                                                            </div>
                                                            <div class="mpg">
                                                                <span>Car</span>${carName} (${modelName})
                                                            </div>
                                                        </div>
                                                        ${userId == 0 ? `<a href="/login" id="userId" value="0" style="width:100%;"  class="btn btn-small btn-style-3"><i class="licon-cart"></i>You Need To First Login</a>
` : `<a href="javascript:;" style="width:100%;" data-part="${element.id}" data-price="${element.price}"class="add-to-spare-part-cart btn btn-small btn-style-3"><i class="licon-cart"></i>Add to Cart</a>`}                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    `);
                                },
                                error: function (request, status, error) {
                                    console.log(request);
                                },
                            });
                        });
                    }
                },
                error: function (request, status, error) {
                    console.log(request);
                },
            });
        });


        $(document).on('click' , '.checkbox-models', function () {
            var check = $(this).val();
            var userId = $('#userId').val();
            var modelValue = $(this).data("model");
            if ($(this).is(':checked')) {
                dataModels.push($(this).data("model"));
            }
            if (!$(this).is(':checked')) {
                dataModels.pop($(this).val());
            }

            if(dataModels.length <= 0){
                var url = window.location.pathname.split("/")[1] === "staging"
                            ? "/staging/get-all/workshop/model/spare-parts"
                            : "" + "/get-all/workshop/model/spare-parts";

            }else{
                if(dataCars.length > 0){
                    var url = window.location.pathname.split("/")[1] === "staging"
                            ? "/staging/get-all/workshop/models/" + dataModels + "/cars/" + dataCars + "/spare-parts/"
                            : "" + "/get-all/workshop/models/" + dataModels + "/cars/" + dataCars + "/spare-parts/";

                }else{
                    var url = window.location.pathname.split("/")[1] === "staging"
                            ? "/staging/get-all/workshop/model/spare-parts/" + dataModels
                            : "" + "/get-all/workshop/model/spare-parts/" + dataModels;

                }
            }
            var workshopName = '';
            var carName = '';
            var modelName = '';
            var token = $('meta[name="csrf-token"]').attr("content");
            $.ajax({
                headers: {
                    "X-CSRF-TOKEN": token,
                },
                method: "POST",
                url: url,
                data: {
                    dataModels: dataModels,
                    dataCars: dataCars,
                },
                dataType: "json",
                success: function (data) {
                    if(data.length > 0){
                        $('#spare-parts-data').empty();
                        $.each(data, function (index, element) {
                            var workshop_id = element.workshop_id;
                            var car_id = element.car_id;
                            var model_id = element.model_id;
                            var token = $('meta[name="csrf-token"]').attr("content");
                            var url = window.location.pathname.split("/")[1] === "staging"
                            ? "/staging/get/workshop/" + workshop_id + "/car/" + car_id + "/model/" + model_id
                            : "" + "/get/workshop/" + workshop_id + "/car/" + car_id + "/model/" + model_id;

                            $.ajax({
                                headers: {
                                    "X-CSRF-TOKEN": token,
                                },
                                method: "POST",
                                url: url,
                                data: {
                                    workshop_id: workshop_id,
                                    car_id: car_id,
                                    model_id: model_id,
                                },
                                dataType: "json",
                                success: function (result) {
                                    workshopName = result.workshop.name;
                                    carName = result.car.name;
                                    modelName = result.model.name;

                                    $('#spare-parts-data').append(`
                                        <div class="col-sm-6 col-xs-6">
                                            <div class="product">
                                            <figure class="product-image">
                                                <img src="/storage/${element.image}" alt="${element.name}">
                                            </figure>
                                            <div class="product-description">
                                                <div class="row">
                                                    <div class="col-xs-12">
                                                        <div style="display: flex;">
                                                            <h5 class="product-name"><a href="#">${element.name}</a></h5>
                                                            <div class="rating-area" style="width: 50%;text-align: right;">
                                                                <h5 class="product-name">${element.company}</h5>
                                                            </div>
                                                        </div>
                                                        <div class="pricing-area">
                                                            <div class="product-price">
                                                            PKR ${element.price}
                                                            </div>
                                                        </div>
                                                        <div class="pricing-area">
                                                            <div class="contact-item">
                                                                <div class="contact-title"><span class="contact-desc"><h5>${element.description}</h5></span></div> &nbsp;
                                                            </div>
                                                        </div>
                                                        <div class="pricing-area">
                                                            <div class="mpg">
                                                            <span>Workshop</span>${workshopName}
                                                            </div>
                                                            <div class="mpg">
                                                                <span>Car</span>${carName} (${modelName})
                                                            </div>
                                                        </div>
                                                        ${userId == 0 ? `<a href="/login" id="userId" value="0" style="width:100%;"  class="btn btn-small btn-style-3"><i class="licon-cart"></i>You Need To First Login</a>
` : `<a href="javascript:;" style="width:100%;" data-part="${element.id}" data-price="${element.price}"class="add-to-spare-part-cart btn btn-small btn-style-3"><i class="licon-cart"></i>Add to Cart</a>`}                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    `);
                                },
                                error: function (request, status, error) {
                                    console.log(request);
                                },
                            });
                        });
                    }
                },
                error: function (request, status, error) {
                    console.log(request);
                },
            });
        });
    </script>
@endpush
