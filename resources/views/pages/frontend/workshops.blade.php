@extends('layouts.hero-section')
@section('head')
    <meta name="keywords" content="">
    <meta name="description" content="">
    <title>Mechno</title>
@endsection
@section('content')
<div id="content">

    <!-- - - - - - - - - - - - - - Breadcrumbs - - - - - - - - - - - - - - - - -->

    <div class="breadcrumbs-wrap" style="background:#616161">

        <div class="container">

            <h2 class="page-title">Workshops</h2>

            <ul class="breadcrumbs">

            <li><a href="{{ route('pages.home') }}">Home</a></li>
            <li>Workshops</li>

            </ul>

        </div>
    </div>
</div>

<div id="content" class="page-content-wrap">

    <div class="container">

        <div class="row">

            <aside id="sidebar" class="sbl style-2 col-md-3 col-sm-12">

            <div class="filter-section">

                <div class="accordion toggle no-of">

                </div>


                <div class="accordion toggle">

                    <!--accordion item-->
                    <div class="accordion-item">
                        <h6 class="a-title active">City</h6>
                        <div class="a-content">

                        <div class="input-wrapper">

                            <input type="checkbox" name="checkbox-4" id="checkbox-4" checked="">
                            <label for="checkbox-4">Any</label>

                        </div>

                        <div class="input-wrapper">

                            <input type="checkbox" data-city="kasur" class="checkbox-cities" name="checkbox-citties[]" id="checkbox-5">
                            <label for="checkbox-5">Kasur</label>

                        </div>
                        <div class="input-wrapper">

                            <input type="checkbox" data-city="lahore" class="checkbox-cities" name="checkbox-citties[]" id="checkbox-6">
                            <label for="checkbox-6">Lahore</label>

                        </div>
                        <div class="input-wrapper">

                            <input type="checkbox" data-city="karachi" class="checkbox-cities" name="checkbox-citties[]" id="checkbox-7">
                            <label for="checkbox-7">Karachi</label>

                        </div>


                        </div>
                    </div>

                </div>
            </div>

            </aside>
            <main id="main" class="col-md-9 col-sm-12">

            <div class="dealers-section product-type">

                <h3 class="section-title">Workshops in Pakistan</h3>

                <!-- dealer sorting -->

                <div class="flex-row flex-justify">

                <div class="sort-dealer">


                    <div class="sort-col">

                    </div>

                </div>

                <div class="settings-view-products">

                    <div class="sort-criteria">

                    <div class="view-type">

                        <a href="#" title="grid View" data-view="grid" data-target="#product-holder" class="active"><i class="licon-icons"></i></a>

                        <a href="#" title="list View" data-view="list" data-target="#product-holder"><i class="licon-menu2"></i></a>

                    </div>

                    </div><!--/ .sort-criteria -->

                </div>

                </div>

            </div>

            <div id="product-holder" class="products-holder view-grid">

                <div class="row flex-row" id="workshop-data">
                    @forelse ($workshops as $workshop)

                        <div class="col-sm-4 col-xs-4">

                            <!-- - - - - - - - - - - - - - Product - - - - - - - - - - - - - - - - -->
                            <div class="product">
                            <!-- - - - - - - - - - - - - - Product Image - - - - - - - - - - - - - - - - -->
                            <figure class="product-image">
                            <a href="{{ route('pages.frontend.workshop-details', $workshop->slug) }}">
@php
if ($pic = $workshop->pictures->first()) {

@endphp
    <img src="{{ (config('app.url'). '/mechon/storage/app/public/'. $pic->path) }}" height="280px" />
    <!-- <img style="height: 280px;" src="{{ asset('/storage/' . $pic->path) }}" alt=""> -->
@php
}
@endphp
</a>

                            </figure>
                            <!-- - - - - - - - - - - - - - End of Product Image - - - - - - - - - - - - - - - - -->

                            <!-- - - - - - - - - - - - - - Product Description - - - - - - - - - - - - - - - - -->
                            <div class="product-description">

                                <div class="row">
                                    <div class="col-xs-12">
                                    <div style="display: flex;">
                                    <h4 class="product-name" ><a href="{{ route('pages.frontend.workshop-details', $workshop->slug) }}">{{ $workshop->name }}</a></h4>
   </div>

                                        <!-- <div class="pricing-area">
                                            <div class="contact-item">
                                                <div class="contact-title"><i class="licon-telephone"></i><span class="contact-desc"><h5>{{ $workshop->phone }}</h5></span></div>
                                            </div>
                                        </div> -->
                                        <div class="pricing-area">

                                            <div class="mpg">
                                                <h6 class="contact-title">
                                                    <span><i class="licon-map-marker" style="margin-right: 5px;color: #b1151b;font-size: 16px;vertical-align: -2px;"></i></span>{{ $workshop->address }}
                                                </h6>
                                            </div>
                                            <!-- <div class="mpg">
                                                <h6 class="contact-title">
                                                    <span><i class="licon-city" style="margin-right: 5px;color: #b1151b;font-size: 16px;vertical-align: -2px;"></i></span>{{ $workshop->location }}
                                                </h6>
                                            </div> -->
                                        </div>
                                        @if (count($workshop->reviews) > 0)
                                        <div class="rating-area" style="width: 50%;text-align: right;">
                                            <ul class="rating">
                                                @php
                                                    $star = round(number_format((int) ($workshop->reviews->sum('rating') / $workshop->reviews->count()), 2, '.', ''));
                                                @endphp
                                                <p style="display: none" id="review_show"></p>
                                                @for ($i = 0; $i < $star; $i++)
                                                    <li><i class="licon-star"></i></li>
                                                @endfor
                                                @if($star == 4)
                                                    <li class="empty"><i class="licon-star"></i></li>
                                                @elseif($star == 3)
                                                    <li class="empty"><i class="licon-star"></i></li>
                                                    <li class="empty"><i class="licon-star"></i></li>
                                                @elseif($star == 2)
                                                    <li class="empty"><i class="licon-star"></i></li>
                                                    <li class="empty"><i class="licon-star"></i></li>
                                                    <li class="empty"><i class="licon-star"></i></li>
                                                @elseif($star == 1)
                                                    <li class="empty"><i class="licon-star"></i></li>
                                                    <li class="empty"><i class="licon-star"></i></li>
                                                    <li class="empty"><i class="licon-star"></i></li>
                                                    <li class="empty"><i class="licon-star"></i></li>
                                                @endif
                                            </ul> ({{ $workshop->reviews->count() }})
                                        </div>
                                    @else
                                        <div class="rating-area" style="width: 50%;text-align: right;">
                                            No Rating yet
                                        </div>
                                    @endif

                                    </div>
                                </div>

                            </div>
                            <!-- - - - - - - - - - - - - - End of Product Description - - - - - - - - - - - - - - - - -->

                            </div>
                            <!-- - - - - - - - - - - - - - / Product - - - - - - - - - - - - - - - - -->

                        </div>
                    @empty
                        <h3 style="width:100%;text-align:center">Workshops Not Found</h3>
                    @endforelse

                </div>

            </div>

            {!! $workshops->links() !!}

            </main>

        </div>
    </div>


</div>
@endsection
@push('scripts')
    <script>
           var dataCities = [];
         $(document).on('click' , '.checkbox-cities', function () {
            var carValue = $(this).data("city");
            if ($(this).is(':checked')) {
                dataCities.push($(this).data("city"));
            }
            if (!$(this).is(':checked')) {
                dataCities.pop($(this).val());
            }

            if(dataCities.length <= 0){
                var url = window.location.pathname.split("/")[1] === "staging"
                            ? "/staging/get-all/workshop"
                            : "" + "/get-all/workshop";

            }else{
                var url = window.location.pathname.split("/")[1] === "staging"
                            ? "/staging/get-all/workshop/"+ dataCities
                            : "" + "/get-all/workshop/"+ dataCities;

            }
            var token = $('meta[name="csrf-token"]').attr("content");
            $.ajax({
                headers: {
                    "X-CSRF-TOKEN": token,
                },
                method: "POST",
                url: url,
                data: {
                    dataCities: dataCities,
                },
                dataType: "json",
                success: function (data) {
                    if(data.length > 0){
                        $('#workshop-data').empty();
                        var rating = '';
                        $.each(data, function (index, element) {

                            var linkUrl = window.location.pathname.split("/")[1] === "staging"
                            ? "/staging/workshops/" + element.slug + "/details"
                            : "" + "/workshops/" + element.slug + "/details";


                            var token = $('meta[name="csrf-token"]').attr("content");
                            var url = window.location.pathname.split("/")[1] === "staging"
                            ? "/staging/workshop/reviews/" + element.id
                            : "" + "/workshop/reviews/" + element.id;
                            $.ajax({
                                headers: {
                                    "X-CSRF-TOKEN": token,
                                },
                                method: "POST",
                                url: url,
                                data: {
                                    workshop_id:  element.id,
                                },
                                dataType: "json",
                                success: function (data) {

                                    var token = $('meta[name="csrf-token"]').attr("content");
                                    var servicesUrl = window.location.pathname.split("/")[1] === "staging"
                                                            ? "/staging/get-all/services/"+ element.id
                                                            : "" + "/get-all/services/"+element.id;
                                    $.ajax({
                                        headers: {
                                            "X-CSRF-TOKEN": token,
                                        },
                                        method: "POST",
                                        url: servicesUrl,
                                        data: {
                                            workshop_id:  element.id,
                                        },
                                        dataType: "json",
                                        success: function (services) {
                                            $('#workshop-data').append(`
                                                <div class="col-sm-6 col-xs-6">
                                                    <div class="product">
                                                        <div style="display: flex;padding: 10px;">
                                                            <h5 class="product-name" style="width:50%;"><a href="${linkUrl}">${element.name}</a></h5>
                                                            ${data.rating != 0 ? `<div class="rating-area" style="width: 50%;text-align: right;">
                                                            <ul class="rating">
                                                                    ${data == 5 ? `<li><i class="licon-star"></i></li>
                                                                        <li><i class="licon-star"></i></li>
                                                                        <li><i class="licon-star"></i></li>
                                                                        <li><i class="licon-star"></i></li>
                                                                        <li><i class="licon-star"></i></li>` : ``}

                                                                    ${data == 4 ? `
                                                                        <li><i class="licon-star"></i></li>
                                                                        <li><i class="licon-star"></i></li>
                                                                        <li><i class="licon-star"></i></li>
                                                                        <li><i class="licon-star"></i></li>
                                                                        <li class="empty"><i class="licon-star"></i></li>` : ``}
                                                                    ${data == 3 ? `
                                                                        <li><i class="licon-star"></i></li>
                                                                        <li><i class="licon-star"></i></li>
                                                                        <li><i class="licon-star"></i></li>
                                                                        <li class="empty"><i class="licon-star"></i></li>
                                                                        <li class="empty"><i class="licon-star"></i></li>` : ``}
                                                                    ${data == 2 ? `
                                                                        <li><i class="licon-star"></i></li>
                                                                        <li><i class="licon-star"></i></li>
                                                                        <li class="empty"><i class="licon-star"></i></li>
                                                                        <li class="empty"><i class="licon-star"></i></li>
                                                                        <li class="empty"><i class="licon-star"></i></li>` : ``}
                                                                    ${data == 1 ? `
                                                                        <li><i class="licon-star"></i></li>
                                                                        <li class="empty"><i class="licon-star"></i></li>
                                                                        <li class="empty"><i class="licon-star"></i></li>
                                                                        <li class="empty"><i class="licon-star"></i></li>
                                                                        <li class="empty"><i class="licon-star"></i></li>` : ``}
                                                                </ul> (${data})
                                                            </div>` : `<p>Not Rating Yet</p>`}

                                                        </div>
                                                        <figure class="product-image">
                                                            <a href="${linkUrl}"><img src="/storage/${element.image}" alt="${element.name}"></a>
                                                        </figure>
                                                        <div class="product-description">
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <div class="pricing-area">
                                                                        <ul class="contact-item">
                                                                            ${services.map(service => `<li style="color: black;
                                                                                list-style: inside;">${service.name}</li>`).join("")}
                                                                        </ul>
                                                                    </div>
                                                                    <div class="pricing-area">
                                                                        <div class="contact-item">
                                                                            <div class="contact-title"><i class="licon-telephone"></i><span class="contact-desc"><h5>${element.phone}</h5></span></div> &nbsp;
                                                                        </div>
                                                                    </div>
                                                                    <div class="pricing-area">
                                                                        <div class="mpg">
                                                                            <h6 class="contact-title">
                                                                                <span><i class="licon-map-marker" style="margin-right: 5px;color: #b1151b;font-size: 16px;vertical-align: -2px;"></i></span>${element.address}
                                                                            </h6>
                                                                        </div>
                                                                        <div class="mpg">
                                                                            <h6 class="contact-title">
                                                                                <span><i class="licon-city" style="margin-right: 5px;color: #b1151b;font-size: 16px;vertical-align: -2px;"></i></span>${element.location}
                                                                            </h6>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                `);
                                        },
                                        error: function (request, status, error) {
                                            console.log(request);
                                        },
                                    });
                                },
                                error: function (request, status, error) {
                                    console.log(request);
                                },
                            });
                        });
                    }else{
                        $('#workshop-data').empty();
                        $('#workshop-data').append(`<h1 style="text-align:center;width:100%;">Workshops not found in your city</h1>`);
                    }
                },
                error: function (request, status, error) {
                    console.log(request);
                },
            });
        });

    </script>
@endpush
