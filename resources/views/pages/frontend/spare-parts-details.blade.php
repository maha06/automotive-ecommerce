@extends('layouts.hero-section')
@section('head')
    <meta name="keywords" content="">
    <meta name="description" content="">
    <title>Mechno</title>
@endsection
<style>
.mySlides {display: none}
img {vertical-align: middle;}

/* Slideshow container */
.slideshow-container {
  max-width: 1000px;
  position: relative;
  margin: auto;
}

/* Next & previous buttons */
.prev, .next {
  cursor: pointer;
  position: absolute;
  top: 50%;
  width: auto;
  padding: 16px;
  margin-top: -22px;
  color: white;
  font-weight: bold;
  font-size: 18px;
  transition: 0.6s ease;
  border-radius: 0 3px 3px 0;
  user-select: none;
}

/* Position the "next button" to the right */
.next {
  right: 0;
  border-radius: 3px 0 0 3px;
}

/* On hover, add a black background color with a little bit see-through */
.prev:hover, .next:hover {
  background-color: rgba(0,0,0,0.8);
}

/* Caption text */
.text {
  color: #f2f2f2;
  font-size: 15px;
  padding: 8px 12px;
  position: absolute;
  bottom: 8px;
  width: 100%;
  text-align: center;
}

/* Number text (1/3 etc) */
.numbertext {
  color: #f2f2f2;
  font-size: 12px;
  padding: 8px 12px;
  position: absolute;
  top: 0;
}

/* The dots/bullets/indicators */
.dot {
  cursor: pointer;
  height: 15px;
  width: 15px;
  margin: 0 2px;
  background-color: #bbb;
  border-radius: 50%;
  display: inline-block;
  transition: background-color 0.6s ease;
}

.active, .dot:hover {
  background-color: #717171;
}

/* Fading animation */
.fade {
  -webkit-animation-name: fade;
  -webkit-animation-duration: 1.5s;
  animation-name: fade;
  animation-duration: 1.5s;
}

@-webkit-keyframes fade {
  from {opacity: .4}
  to {opacity: 1}
}

@keyframes fade {
  from {opacity: .4}
  to {opacity: 1}
}

/* On smaller screens, decrease text size */
@media only screen and (max-width: 300px) {
  .prev, .next,.text {font-size: 11px}
}
</style>

@section('content')
<div id="content">

    <!-- - - - - - - - - - - - - - Breadcrumbs - - - - - - - - - - - - - - - - -->

    <div class="breadcrumbs-wrap" style="background:#616161">

        <div class="container">

            <h2 class="page-title">Part Details</h2>

            <ul class="breadcrumbs">

            <li><a href="{{ route('pages.home') }}">Home</a></li>
            <li><a href="{{ route('pages.frontend.spare-parts') }}">Parts</a></li>
            <li>{{ $part->name }}</li>

            </ul>

        </div>
    </div>
</div>

<div id="content">

    <div class="page-section">

        <div class="container">

          <div class="row">

            <main id="main" class="col-md-12 col-sm-12">

              <div class="single-post">

                <div class="review-section">

                  <article class="entry review-item">
                  <h1 style="color:#b1151b; font-weight:bolder">{{ $part->name }}</h1>

                    <!-- - - - - - - - - - - - - - Attachment - - - - - - - - - - - - - - - - -->

                    <div class="entry-attachment">

                      <!-- <figure class="thumbnail-attachment"><img src="{{ asset('/storage/' . $part->image) }}" alt=""></figure> -->
                 @if( $part->pictures->count() > 0)
                      <div class="slideshow-container">

                      @foreach($part->pictures as $pic)

<div class="mySlides fade">
  <img src="{{ (config('app.url'). '/mechon/storage/app/public/'. $pic->path) }}" width="700" height="500">
</div>

@endforeach
<a class="prev" onclick="plusSlides(-1)">&#10094;</a>
<a class="next" onclick="plusSlides(1)">&#10095;</a>
</div>
<br>
<div style="text-align:center">
@foreach($part->pictures as $i => $pic)
  <span class="dot" onclick="currentSlide({{$i+1}})"></span>
@endforeach
</div>
@else
<h3 >No image to display</h3>

@endif
                    </div>
                    <div style="margin-top:5%; margin-bottom:5%">
                        {!! $part->description !!}

                        <ul style="margin-top:1%;">
                            <li><strong style="color:#b1151b">Category: </strong> {{$part->category}}</li>
                            <li><strong style="color:#b1151b">Sub category: </strong>{{$part->sub_category}}</li>
                            <li><strong style="color:#b1151b">Brand: </strong>{{$part->brand}}</li>

                        </ul>

                        <div style="margin-top:5%;">

                        @if ($part->discount > 0)

                        <div class="product-price"><span style="text-decoration: line-through;"> PKR {{ $part->retail_price }} </span><sup style="color:Red"> {{ $part->discount }}% </sup></div>

                        @php
                        $price = $part->retail_price;
                        $discount = ($price * $part->discount)/100;
                        $total = $price - $discount;
                        @endphp
                        <h2 style="color:#b1151b">
                          PKR {{ $total }}
                        </h2>

                        @else
                        <h2 style="color:#b1151b"> Price
                        <span>{{$part->retail_price}}<span></h2>
                        @endif

                        </div>
                    </div>
                   </article>

                </div>
              </div>
            </main>
          </div>
        </div>
      </div>
</div>
@endsection
@push('scripts')
<script>

        var slideIndex = 1;
        showSlides(slideIndex);

        function plusSlides(n) {
        showSlides(slideIndex += n);
        }

        function currentSlide(n) {
        showSlides(slideIndex = n);
        }

        function showSlides(n) {
        var i;
        var slides = document.getElementsByClassName("mySlides");
        var dots = document.getElementsByClassName("dot");
        if (n > slides.length) {slideIndex = 1}
        if (n < 1) {slideIndex = slides.length}
        for (i = 0; i < slides.length; i++) {
            slides[i].style.display = "none";
        }
        for (i = 0; i < dots.length; i++) {
            dots[i].className = dots[i].className.replace(" active", "");
        }
        slides[slideIndex-1].style.display = "block";
        dots[slideIndex-1].className += " active";
        }

</script>
@endpush
