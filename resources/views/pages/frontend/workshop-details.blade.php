@extends('layouts.hero-section')
@section('head')
    <meta name="keywords" content="">
    <meta name="description" content="">
    <title>Mechno</title>
@endsection
<style>
.mySlides {display: none; }
img {vertical-align: middle;}

/* Slideshow container */
.slideshow-container {
  max-width: 1000px;
  position: relative;
  margin: auto;
}

/* Next & previous buttons */
.prev, .next {
  cursor: pointer;
  position: absolute;
  top: 50%;
  width: auto;
  padding: 16px;
  margin-top: -22px;
  color: white;
  font-weight: bold;
  font-size: 18px;
  transition: 0.6s ease;
  border-radius: 0 3px 3px 0;
  user-select: none;
}

/* Position the "next button" to the right */
.next {
  right: 0;
  border-radius: 3px 0 0 3px;
}

/* On hover, add a black background color with a little bit see-through */
.prev:hover, .next:hover {
  background-color: rgba(0,0,0,0.8);
}

/* Caption text */
.text {
  color: #f2f2f2;
  font-size: 15px;
  padding: 8px 12px;
  position: absolute;
  bottom: 8px;
  width: 100%;
  text-align: center;
}

/* Number text (1/3 etc) */
.numbertext {
  color: #f2f2f2;
  font-size: 12px;
  padding: 8px 12px;
  position: absolute;
  top: 0;
}

/* The dots/bullets/indicators */
.dot {
  cursor: pointer;
  height: 15px;
  width: 15px;
  margin: 0 2px;
  background-color: #bbb;
  border-radius: 50%;
  display: inline-block;
  transition: background-color 0.6s ease;
}

.active, .dot:hover {
  background-color: #717171;
}

/* Fading animation */
.fade {
  -webkit-animation-name: fade;
  -webkit-animation-duration: 1.5s;
  animation-name: fade;
  animation-duration: 1.5s;
}

@-webkit-keyframes fade {
  from {opacity: .4}
  to {opacity: 1}
}

@keyframes fade {
  from {opacity: .4}
  to {opacity: 1}
}

/* On smaller screens, decrease text size */
@media only screen and (max-width: 300px) {
  .prev, .next,.text {font-size: 11px}
}
</style>


@section('content')
<div id="content">

    <!-- - - - - - - - - - - - - - Breadcrumbs - - - - - - - - - - - - - - - - -->

    <div class="breadcrumbs-wrap" style="background:#616161">

        <div class="container">

            <h2 class="page-title">Workshop Details</h2>

            <ul class="breadcrumbs">

            <li><a href="{{ route('pages.home') }}">Home</a></li>
            <li><a href="{{ route('pages.frontend.workshops') }}">Workshops</a></li>
            <li>{{ $workshop->name }}</li>

            </ul>

        </div>
    </div>
</div>

<div id="content">

    <div class="page-section">

        <div class="container">

          <div class="row">

            <main id="main" class="col-md-12 col-sm-12">

              <div class="single-post">

                <div class="review-section">

                  <article class="entry review-item">

                    <!-- - - - - - - - - - - - - - Attachment - - - - - - - - - - - - - - - - -->
<h1 style="color:#b1151b; font-weight:bolder">{{ $workshop->name }}</h1>
                    <div class="entry-attachment">

                    @if( $workshop->pictures->count() > 0)
                      <div class="slideshow-container" >

                      @foreach($workshop->pictures as $pic)
                        <div class="mySlides fade" >
                        <img src="{{ (config('app.url'). '/mechon/storage/app/public/'. $pic->path) }}" width="700" height="500">
                        </div>
                       @endforeach
                        <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
                        <a class="next" onclick="plusSlides(1)">&#10095;</a>
                        </div>
                        <br>
                        <div style="text-align:center">
                        @foreach($workshop->pictures as $i => $pic)
                        <span class="dot" onclick="currentSlide({{$i+1}})"></span>
                        @endforeach
                        </div>
                        @else
                        <h3 >No image to display</h3>

                        @endif

                    </div>
                    <div style="margin-top:5%; margin-bottom:5%">
                        {!! $workshop->description !!}

                        <ul style="margin-top:1%;">
                            <li><strong style="color:#b1151b">No of Mechanics: </strong> {{$workshop->no_of_mechanics}}</li>
                            <li><strong style="color:#b1151b">No of Electricians: </strong>{{$workshop->no_of_electrician}}</li>
                            <li><strong style="color:#b1151b">No of Denter: </strong>{{$workshop->no_of_denter}}</li>
                            <li><strong style="color:#b1151b">No of Painter: </strong>{{$workshop->no_of_painter}}</li>
                            <li><strong style="color:#b1151b">No of Bays: </strong>{{$workshop->no_of_bays}}</li>

                        </ul>

                        <div style="margin-top:1%;">
                        <label> <strong style="color:#b1151b"> Major Tools and Equipments </strong> </label>
                        <span>{{$workshop->major_tools_and_equipments}}<span>
                        </div>
                    </div>
                    <!-- - - - - - - - - - - - - - End of Attachment - - - - - - - - - - - - - - - - -->
                    <div class="tabs tabs-section style-2 clearfix ui-tabs ui-widget ui-widget-content ui-corner-all">
                        <!--tabs navigation-->
                        <ul class="tabs-nav clearfix ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all" role="tablist">
                            <li data-tab="tab1" class="workshop-details ui-state-default ui-corner-top" role="tab" tabindex="-1" aria-controls="tab-1" aria-labelledby="ui-id-1" aria-selected="true">
                                <span class="licon-city"></span>{{ $workshop->location }}
                            </li>
                            <li data-tab="tab2" class="workshop-details ui-state-default ui-corner-top" role="tab" tabindex="0" aria-controls="tab-2" aria-labelledby="ui-id-2" aria-selected="false">
                                <span class="licon-phone"></span>{{ $workshop->phone }}
                            </li>
                            <li data-tab="tab3" class="workshop-details ui-state-default ui-corner-top" role="tab" tabindex="-1" aria-controls="tab-3" aria-labelledby="ui-id-3" aria-selected="false">
                                <span class="licon-map-marker"></span>{{ $workshop->address }}
                            </li>
                            <li class="ui-state-default ui-corner-top ui-tabs-active ui-state-active" role="tab" tabindex="0" aria-controls="tab-4" aria-labelledby="ui-id-4" aria-selected="false">
                              <a href="#tab-4" class="ui-tabs-anchor" role="presentation" tabindex="-1" id="ui-id-4">Reviews</a>
                            </li>
                        </ul>
                        <div class="tabs-content">
                            <div id="tab-4" aria-labelledby="ui-id-4" class="ui-tabs-panel ui-widget-content ui-corner-bottom" role="tabpanel" aria-expanded="true" aria-hidden="false" style="display: block;">

                              <div class="content-element">
                                <div id="review-success-message" style="display: none;" class="alert alert-success">
                                    <button type="button" class="close" data-dismiss="alert"></button>
                                    Thanks For Your Feedback!
                                </div>
                                {{-- <h5 class="section-title">{{ $reviews->count() }} Reviews</h5> --}}
                                {{-- <div class="gravatar">

                                    <a href="#"><img src="{{ asset('frontend/images/100x100_photo4.jpg') }}" alt=""></a>

                                </div> --}}
                                <ol class="comments-list" id="reviews-list" >
                                    @forelse ($reviews as $review)
                                        <li class="comment">
                                            <article>
                                                <div class="comment-body">

                                                    <header class="comment-meta">

                                                    <h6 class="comment-author"><a href="#">{{ $review->name }}</a></h6>
                                                    <time datetime="2017-10-17 02:41" class="comment-date single">{{ $review->created_at }}</time>
                                                    <ul class="rating f-right">
                                                        @if ($review->rating == 5)
                                                            <li><i class="licon-star"></i></li>
                                                            <li><i class="licon-star"></i></li>
                                                            <li><i class="licon-star"></i></li>
                                                            <li><i class="licon-star"></i></li>
                                                            <li><i class="licon-star"></i></li>
                                                        @elseif($review->rating == 4)
                                                            <li><i class="licon-star"></i></li>
                                                            <li><i class="licon-star"></i></li>
                                                            <li><i class="licon-star"></i></li>
                                                            <li><i class="licon-star"></i></li>
                                                            <li class="empty"><i class="licon-star"></i></li>
                                                        @elseif($review->rating == 3)
                                                            <li><i class="licon-star"></i></li>
                                                            <li><i class="licon-star"></i></li>
                                                            <li><i class="licon-star"></i></li>
                                                            <li class="empty"><i class="licon-star"></i></li>
                                                            <li class="empty"><i class="licon-star"></i></li>
                                                        @elseif($review->rating == 2)
                                                            <li><i class="licon-star"></i></li>
                                                            <li><i class="licon-star"></i></li>
                                                            <li class="empty"><i class="licon-star"></i></li>
                                                            <li class="empty"><i class="licon-star"></i></li>
                                                            <li class="empty"><i class="licon-star"></i></li>
                                                        @elseif($review->rating == 1)
                                                            <li><i class="licon-star"></i></li>
                                                            <li class="empty"><i class="licon-star"></i></li>
                                                            <li class="empty"><i class="licon-star"></i></li>
                                                            <li class="empty"><i class="licon-star"></i></li>
                                                            <li class="empty"><i class="licon-star"></i></li>
                                                        @endif
                                                    </ul>

                                                    </header>

                                                    <p>{{ $review->message }}</p>

                                                </div>
                                            </article>
                                        </li>

                                    @empty
                                        <h5 class="section-title">No Reviews</h5>
                                    @endforelse
                                </ol>
                              </div>

                              <div class="content-element">

                                <div class="bg-sidebar-item style-2">

                                  <h5 class="section-title">Add a Review</h5>
                                  <p class="required">Your email address will not be published. Required fields are marked</p>

                                  <form id="reviewForm" method="post" class="contact-form">
                                    @csrf
                                    <div class="content-element7">

                                      <div class="row">
                                        <div class="col-sm-6">

                                          <label class="required">Name</label>
                                          <input type="text" required name="name" id="name" @auth value={{ Auth::user()->name }} @endauth>

                                        </div>
                                        <div class="col-sm-6">

                                          <label class="required">Email</label>
                                          <input type="text"required  name="email" id="email" @auth value={{ Auth::user()->email }} @endauth>

                                        </div>
                                      </div>
                                      @auth
                                          <input type="hidden" required value="{{ Auth::user()->id }}" id="user_id" name="user_id" />
                                      @endauth
                                      <input type="hidden" required value="{{ $workshop->id }}" id="workshop_id" name="workshop_id" />
                                    </div>
                                    <div class="content-element7">
                                      <label>Your Rating</label>
                                        <div class='rating-stars'>
                                            <ul id='stars' class="rating" required>
                                                <li class='star' data-value='1' title='Poor'><i class="licon-star"></i></li>
                                                <li class='star' data-value='2' title='Fair'><i class="licon-star"></i></li>
                                                <li class='star' data-value='3' title='Good'><i class="licon-star"></i></li>
                                                <li class='star' data-value='4' title='Excellent'><i class="licon-star"></i></li>
                                                <li class='star' data-value='5' title='WOW!!!'><i class="licon-star"></i></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <input type="hidden" id="rating" name="rating" value="" />
                                    <div class="content-element7">
                                      <label class="required">Comment</label>
                                      <textarea required rows="6" id="message" name="message"></textarea>
                                        @auth
                                            <input type="submit" class="btn btn-style-3" value="Submit Review" />
                                        @else
                                            <a href="{{ route('login') }}" class="btn btn-style-3">You Need To First Login</a>
                                        @endauth
                                    </div>

                                  </form>

                                </div>

                              </div>

                            </div>
                          </div>
                    </div>
                    <!-- - - - - - - - - - - - - - End of Entry body - - - - - - - - - - - - - - - - -->
                  </article>

                </div>
              </div>
            </main>
          </div>
        </div>
      </div>
</div>
@endsection
@push('scripts')
<script>
    $(document).ready(function () {
    /* 1. Visualizing things on Hover - See next part for action on click */
    $("#stars li")
        .on("mouseover", function () {
            var onStar = parseInt($(this).data("value"), 10); // The star currently mouse on

            // Now highlight all the stars that's not after the current hovered star
            $(this)
                .parent()
                .children("li.star")
                .each(function (e) {
                    if (e < onStar) {
                        $(this).addClass("hover");
                    } else {
                        $(this).removeClass("hover");
                    }
                });
        })
        .on("mouseout", function () {
            $(this)
                .parent()
                .children("li.star")
                .each(function (e) {
                    $(this).removeClass("hover");
                });
        });

        /* 2. Action to perform on click */
    $("#stars li").on("click", function () {
        var onStar = parseInt($(this).data("value"), 10); // The star currently selected
        var stars = $(this).parent().children("li.star");

        for (i = 0; i < stars.length; i++) {
            $(stars[i]).removeClass("selected");
        }

        for (i = 0; i < onStar; i++) {
            $(stars[i]).addClass("selected");
        }

        // JUST RESPONSE (Not needed)
        var ratingValue = parseInt(
            $("#stars li.selected").last().data("value"),
            10
        );

        document.getElementById("rating").value = onStar;
        });
    });


    $(document).on("submit", "#reviewForm", function (event) {
    event.preventDefault();
    var token = $('meta[name="csrf-token"]').attr("content");
    var url = window.location.pathname.split("/")[1] === "staging"
                            ? "/staging/workshop/reviews"
                            : "" + "/workshop/reviews";

    $.ajax({
        headers: {
            "X-CSRF-TOKEN": token,
        },
        method: "POST",
        url: url,
        data: {
            name:  $('#name').val(),
            email:  $('#email').val(),
            rating:  $('#rating').val(),
            message:  $('#message').val(),
            user_id:  $('#user_id').val(),
            workshop_id:  $('#workshop_id').val(),
        },
        dataType: "json",
        success: function (data) {
            var message = document.getElementById("review-success-message");
            message.setAttribute("style", "display:block");
            $('#reviews-list').empty();
            $.each(data, function(index, element){
                $('#reviews-list').append(`
                <li class="comment">
                    <article>
                        <div class="comment-body">

                            <header class="comment-meta">

                            <h6 class="comment-author"><a href="#">${element.name}</a></h6>
                            <p class="comment-date single">${element.created_at}</p>
                            <ul class="rating f-right">
                                ${element.rating == 5 ? `<li><i class="licon-star"></i></li>
                                    <li><i class="licon-star"></i></li>
                                    <li><i class="licon-star"></i></li>
                                    <li><i class="licon-star"></i></li>
                                    <li><i class="licon-star"></i></li>` : ``}

                                ${element.rating == 4 ? `
                                    <li><i class="licon-star"></i></li>
                                    <li><i class="licon-star"></i></li>
                                    <li><i class="licon-star"></i></li>
                                    <li><i class="licon-star"></i></li>
                                    <li class="empty"><i class="licon-star"></i></li>` : ``}
                                ${element.rating == 3 ? `
                                    <li><i class="licon-star"></i></li>
                                    <li><i class="licon-star"></i></li>
                                    <li><i class="licon-star"></i></li>
                                    <li class="empty"><i class="licon-star"></i></li>
                                    <li class="empty"><i class="licon-star"></i></li>` : ``}
                                ${element.rating == 2 ? `
                                    <li><i class="licon-star"></i></li>
                                    <li><i class="licon-star"></i></li>
                                    <li class="empty"><i class="licon-star"></i></li>
                                    <li class="empty"><i class="licon-star"></i></li>
                                    <li class="empty"><i class="licon-star"></i></li>` : ``}
                                ${element.rating == 1 ? `
                                    <li><i class="licon-star"></i></li>
                                    <li class="empty"><i class="licon-star"></i></li>
                                    <li class="empty"><i class="licon-star"></i></li>
                                    <li class="empty"><i class="licon-star"></i></li>
                                    <li class="empty"><i class="licon-star"></i></li>` : ``}
                                </ul>

                            </header>

                            <p>${element.message}</p>

                        </div>
                    </article>
                </li>
                `);
            });
        },
        error: function (request, status, error) {
            console.log(request);
        },
    });
});
</script>
<script>

        var slideIndex = 1;
        showSlides(slideIndex);

        function plusSlides(n) {
        showSlides(slideIndex += n);
        }

        function currentSlide(n) {
        showSlides(slideIndex = n);
        }

        function showSlides(n) {
        var i;
        var slides = document.getElementsByClassName("mySlides");
        var dots = document.getElementsByClassName("dot");
        if (n > slides.length) {slideIndex = 1}
        if (n < 1) {slideIndex = slides.length}
        for (i = 0; i < slides.length; i++) {
            slides[i].style.display = "none";
        }
        for (i = 0; i < dots.length; i++) {
            dots[i].className = dots[i].className.replace(" active", "");
        }
        slides[slideIndex-1].style.display = "block";
        dots[slideIndex-1].className += " active";
        }

</script>
@endpush
