@extends('layouts.hero-section')
@section('head')
    <meta name="keywords" content="">
    <meta name="description" content="">
    <title>Mechon</title>
@endsection
@section('content')
<div id="content">

    <!-- - - - - - - - - - - - - - Breadcrumbs - - - - - - - - - - - - - - - - -->

    <div class="breadcrumbs-wrap" style="background:#616161;">

      <div class="container">

        <h2 class="page-title">Contact Us</h2>

        <ul class="breadcrumbs">

          <li><a href="{{ route('pages.home') }}">Home</a></li>
          <li>Contact Us</li>

        </ul>

      </div>

    </div>

    <!-- - - - - - - - - - - - - end Breadcrumbs - - - - - - - - - - - - - - - -->

    <div class="page-section">

      <div class="container">

        <h2 class="section-title">Contact Information</h2>

        <div class="row">

          <div class="col-md-8 col-sm-12">

            <!-- Google map -->
            <div class="map-section">
              <div id="googleMap" class="map-container type-2"></div>
            </div>

          </div>
          <div class="col-md-4 col-sm-12">

            <div class="content-element7">

              <div class="contact-item">
                <h6 class="contact-title"><i class="licon-map-marker"></i>Address</h6>
                <span class="contact-desc">9863 - 9867 Mill Road, Cambridge, MG09 99HT </span>
                <a href="http://maps.google.com?q=41.9791948,-87.9461776" class="info-btn" target="_blank">Get Directions</a>
              </div>

            </div>

            <div class="content-element7">

              <div class="tabs tabs-section clearfix">
                <!--tabs navigation-->
                <ul class="tabs-nav clearfix">
                  <li class="">
                    <a href="#tab-1">Sale</a>
                  </li>
                  <li class="">
                    <a href="#tab-2">Service</a>
                  </li>
                  <li class="">
                    <a href="#tab-3">Parts</a>
                  </li>
                </ul>
                <!--tabs content-->
                <div class="tabs-content">
                  <div id="tab-1">

                    <div class="contact-section">

                      <div class="contact-item">
                        <h6 class="contact-title"><i class="licon-telephone"></i>Phone</h6>
                        <span class="contact-desc">800-987-65-44</span>
                      </div>

                      <div class="contact-item">
                        <h6 class="contact-title"><i class="licon-at-sign"></i>email</h6>
                        <a href="#" class="contact-desc">sale@companyname.com</a>
                      </div>

                      <div class="contact-item">
                        <h6 class="contact-title"><i class="licon-clock3"></i>Opening Hours</h6>
                        <div class="flex-row flex-justify">
                          <span class="contact-desc">Monday</span>
                          <span class="contact-desc">9:00 AM - 5:00 PM</span>
                        </div>
                        <div class="flex-row flex-justify">
                          <span class="contact-desc">Tuesday</span>
                          <span class="contact-desc">9:00 AM - 5:00 PM</span>
                        </div>
                        <div class="flex-row flex-justify">
                          <span class="contact-desc">Wednesday</span>
                          <span class="contact-desc">9:00 AM - 5:00 PM</span>
                        </div>
                        <div class="flex-row flex-justify">
                          <span class="contact-desc">Thursday</span>
                          <span class="contact-desc">9:00 AM - 5:00 PM</span>
                        </div>
                        <div class="flex-row flex-justify">
                          <span class="contact-desc">Friday</span>
                          <span class="contact-desc">9:00 AM - 5:00 PM</span>
                        </div>
                        <div class="flex-row flex-justify">
                          <span class="contact-desc">Saturday</span>
                          <span class="contact-desc">9:00 AM - 2:00 PM</span>
                        </div>
                        <div class="flex-row flex-justify">
                          <span class="contact-desc">Sunday</span>
                          <span class="contact-desc">Closed</span>
                        </div>
                      </div>

                    </div>

                  </div>
                  <div id="tab-2">

                    <div class="contact-section">

                      <div class="contact-item">
                        <h6 class="contact-title"><i class="licon-telephone"></i>Phone</h6>
                        <span class="contact-desc">800-987-65-44</span>
                      </div>

                      <div class="contact-item">
                        <h6 class="contact-title"><i class="licon-at-sign"></i>email</h6>
                        <a href="#" class="contact-desc">sale@companyname.com</a>
                      </div>

                      <div class="contact-item">
                        <h6 class="contact-title"><i class="licon-clock3"></i>Opening Hours</h6>
                        <div class="flex-row flex-justify">
                          <span class="contact-desc">Monday</span>
                          <span class="contact-desc">9:00 AM - 5:00 PM</span>
                        </div>
                        <div class="flex-row flex-justify">
                          <span class="contact-desc">Tuesday</span>
                          <span class="contact-desc">9:00 AM - 5:00 PM</span>
                        </div>
                        <div class="flex-row flex-justify">
                          <span class="contact-desc">Wednesday</span>
                          <span class="contact-desc">9:00 AM - 5:00 PM</span>
                        </div>
                        <div class="flex-row flex-justify">
                          <span class="contact-desc">Thursday</span>
                          <span class="contact-desc">9:00 AM - 5:00 PM</span>
                        </div>
                        <div class="flex-row flex-justify">
                          <span class="contact-desc">Friday</span>
                          <span class="contact-desc">9:00 AM - 5:00 PM</span>
                        </div>
                        <div class="flex-row flex-justify">
                          <span class="contact-desc">Saturday</span>
                          <span class="contact-desc">9:00 AM - 2:00 PM</span>
                        </div>
                        <div class="flex-row flex-justify">
                          <span class="contact-desc">Sunday</span>
                          <span class="contact-desc">Closed</span>
                        </div>
                      </div>

                    </div>

                  </div>
                  <div id="tab-3">

                    <div class="contact-section">

                      <div class="contact-item">
                        <h6 class="contact-title"><i class="licon-telephone"></i>Phone</h6>
                        <span class="contact-desc">800-987-65-44</span>
                      </div>

                      <div class="contact-item">
                        <h6 class="contact-title"><i class="licon-at-sign"></i>email</h6>
                        <a href="#" class="contact-desc">sale@companyname.com</a>
                      </div>

                      <div class="contact-item">
                        <h6 class="contact-title"><i class="licon-clock3"></i>Opening Hours</h6>
                        <div class="flex-row flex-justify">
                          <span class="contact-desc">Monday</span>
                          <span class="contact-desc">9:00 AM - 5:00 PM</span>
                        </div>
                        <div class="flex-row flex-justify">
                          <span class="contact-desc">Tuesday</span>
                          <span class="contact-desc">9:00 AM - 5:00 PM</span>
                        </div>
                        <div class="flex-row flex-justify">
                          <span class="contact-desc">Wednesday</span>
                          <span class="contact-desc">9:00 AM - 5:00 PM</span>
                        </div>
                        <div class="flex-row flex-justify">
                          <span class="contact-desc">Thursday</span>
                          <span class="contact-desc">9:00 AM - 5:00 PM</span>
                        </div>
                        <div class="flex-row flex-justify">
                          <span class="contact-desc">Friday</span>
                          <span class="contact-desc">9:00 AM - 5:00 PM</span>
                        </div>
                        <div class="flex-row flex-justify">
                          <span class="contact-desc">Saturday</span>
                          <span class="contact-desc">9:00 AM - 2:00 PM</span>
                        </div>
                        <div class="flex-row flex-justify">
                          <span class="contact-desc">Sunday</span>
                          <span class="contact-desc">Closed</span>
                        </div>
                      </div>

                    </div>

                  </div>
                </div>
              </div>

            </div>

            <div class="content-element7">

              <div class="contact-item">
                <h6 class="contact-title"><i class="licon-share2"></i>Social networks</h6>
                <ul class="social-icons style-2">

                  <li class="fb-icon"><a href="#"><i class="icon-facebook"></i></a></li>
                  <li class="tweet-icon"><a href="#"><i class="icon-twitter"></i></a></li>
                  <li class="youtube-icon"><a href="#"><i class="icon-youtube"></i></a></li>
                  <li class="google-icon"><a href="#"><i class="icon-gplus-3"></i></a></li>
                  <li class="insta-icon"><a href="#"><i class="icon-instagram-5"></i></a></li>
                  <li class="in-icon"><a href="#"><i class="icon-linkedin"></i></a></li>

                </ul>
              </div>

            </div>

          </div>

        </div>

      </div>

    </div>

    <div class="page-section-bg">

      <div class="container">

        <h2><b>We Welcome Your Feedback and Comments</b></h2>
        <form id="contact-form" class="contact-form">

          <div class="row">
            <div class="col-sm-4">

              <label class="required">You Name</label>
              <input type="text" name="cf-name">

              <label class="required">You email</label>
              <input type="text" name="cf-email">

            </div>
            <div class="col-sm-4">

              <label>Phone number</label>
              <input type="text" name="phone">

              <label>Department</label>
              <div class="custom-select">
                <div class="select-title">Service</div>
                <ul id="menu-type" class="select-list"></ul>
                <select class="hide">
                  <option value="menu">Service 1</option>
                  <option value="menu">Service 2</option>
                  <option value="menu">Service 3</option>
                </select>
              </div>

            </div>
            <div class="col-sm-4">

              <label>Message</label>
              <textarea name="cf-message" rows="5"></textarea>

              <button type="submit" class="btn btn-style-3" data-type="submit">Send Message</button>

            </div>
          </div>

        </form>

      </div>

    </div>

</div>
@endsection
