
@if (Session('success'))
    <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert"></button>
        {!!Session('success')!!}
    </div>
@endif

@if (Session('warning'))
    <div class="alert alert-warning">
        <button type="button" class="close" data-dismiss="alert"></button>
        {!!Session('warning')!!}
    </div>
@endif

@if (Session('danger'))
    <div class="alert alert-danger" style="background: #b1151b;
    color: white;">
        <button style="color:white" type="button" class="close" data-dismiss="alert"></button>
        {!!Session('danger')!!}
    </div>
@endif

@if ($errors->any())
    {!! implode('', $errors->all('<div style="background: #b1151b;
    color: white;" class="alert alert-danger">
    <button style="color:white" type="button" class="close" data-dismiss="alert"></button>:message</div>')) !!}
@endif

@if (session('message'))
    <div class="alert {{ session('alert-class', 'alert-success') }}">
        <button type="button" class="close" data-dismiss="alert"></button>
        {!!Session('message')!!}
    </div>
@endif
