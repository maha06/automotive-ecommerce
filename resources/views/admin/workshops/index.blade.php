@extends('admin.layouts.layout')

@section('heading')
    <h4><a href="{{ route('workshops.index') }}"><i class="icon-arrow-left52 mr-2"></i></a> <span
            class="font-weight-semibold">Home - Workshops</span> - View All</h4>
@endsection

@section('breadcrumbs')
    <div class="breadcrumb">
        <a href="{{ route('workshops.index') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
        <span class="breadcrumb-item">Workshops</span>
        <span class="breadcrumb-item active">View All</span>
    </div>
@endsection

@section('content')
   @include('admin.partials.header')
    <div class="content">
        @include('common.partials.flash')
        <div class="card has-table">
            <div class="card-header header-elements-inline">
                <h5 class="card-title">All Workshops</h5>
            </div>

            <table class="table datatable-basic">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>Address</th>
                    <th>Status</th>
                    <th>Created At</th>
                    <th class="text-center">Actions</th>
                </tr>
                </thead>
                <tbody>
                @forelse ($workshops as $workshop)
                    <tr>
                        <td>{{ $workshop->name }}</td>
                        <td>{{ $workshop->email }}</td>
                        <td>{{ $workshop->phone }}</td>
                        <td>{{ $workshop->address }}</td>
                        <td class="data">
                            <select data-status-id="{{ $workshop->id }}" class="activeInactive form-control" name="status"
                                    id="status">
                                <option {{ $workshop->status == 1 ? 'selected' : '' }} value="1" name="hidden_status" class="hidden_status" id="hidden_status" >Active</option>
                                <option {{ $workshop->status == 0 ? 'selected' : '' }} value="0" name="hidden_status" class="hidden_status" id="hidden_status">Inactive</option>
                            </select>
                        </td>
                        <td>{{ $workshop->created_at }}</td>
                        <td class="text-center">
                            <div class="list-icons">
                                <div class="dropdown">
                                    <a href="#" class="list-icons-item" data-toggle="dropdown">
                                        <i class="icon-menu9"></i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        <a href="{{ route('admin.workshops.change-password', $workshop->id) }}"
                                           class="dropdown-item"><i class="icon-pencil5"></i> Change Password</a>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="7">
                            <div class="alert alert-info text-center">
                                No Workshops Added So Far
                            </div>
                        </td>
                    </tr>
                @endforelse
                </tbody>
            </table>
            {!! $workshops->links() !!}
        </div>
    </div>
@endsection
@push('scripts')
    <script>

        $('.activeInactive').change(function(status){
            var workshop_id = this.getAttribute('data-status-id');
            var url = window.location.pathname.split("/")[1] === "staging"
                            ? "/staging/admin/workshop/"+ workshop_id +"/set-status/"+status.target.value
                            : "" + "/admin/workshop/"+ workshop_id +"/set-status/"+status.target.value;

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                method: 'get',
                url: url,
                dataType: 'json',
                success:function(data){
                    if(data == 'done')
                    {
                        $('#status').bootstrapToggle('on');
                    }
                }
            });
        });
    </script>
@endpush
