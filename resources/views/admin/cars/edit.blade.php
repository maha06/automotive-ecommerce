@extends('admin.layouts.layout')

@section('heading')
    <h4><a href="{{ route('admin.pages.home') }}"><i class="icon-arrow-left52 mr-2"></i></a> <span class="font-weight-semibold">Home - {{ $car->name }}</span> - Edit</h4>
@endsection

@section('breadcrumbs')
    <div class="breadcrumb">
        <a href="{{ route('admin.pages.home') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
        <span class="breadcrumb-item">{{ $car->name }}</span>
        <span class="breadcrumb-item active">Edit</span>
    </div>
@endsection

@section('content')
    @include('admin.partials.header')
    <div class="content">
        @include('common.partials.flash')
        <div class="card">
            <div class="card-header header-elements-inline">
                <h5 class="card-title">Edit Make Name</h5>
            </div>
            <div class="card-body">
                <form action="{{ route('cars.update',[$car->id])}}" method="POST">
                    @csrf
                    @method('PUT')
                    <fieldset class="mb-3">
                        <legend class="text-uppercase font-size-sm font-weight-bold">Fill the form below to edit car </legend>
                    </fieldset>
                    <div class="form-group">
                        <label class="font-weight-semibold @error('name') text-danger @enderror">Name</label>
                        <div class="form-group-feedback form-group-feedback-right">
                            <input type="text" name="name" value="{{ old('name') ?? $car->name }}" class="form-control @error('name') border-danger @enderror">
                            @error('name')
                            <div class="form-control-feedback text-danger">
                                <i class="icon-cancel-circle2"></i>
                            </div>
                            @enderror
                        </div>
                        @error('name')
                        <span class="form-text text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label class="font-weight-semibold @error('mileage') text-danger @enderror">Mileage</label>
                        <div class="form-group-feedback form-group-feedback-right">
                            <input type="text" name="mileage" value="{{ old('mileage') ?? $car->mileage }}" class="form-control @error('mileage') border-danger @enderror" required>
                            @error('mileage')
                            <div class="form-control-feedback text-danger">
                                <i class="icon-cancel-circle2"></i>
                            </div>
                            @enderror
                        </div>
                        @error('mileage')
                        <span class="form-text text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label class="font-weight-semibold @error('year') text-danger @enderror">Year</label>
                        <div class="form-group-feedback form-group-feedback-right">
                            <input type="text" name="year" value="{{ old('year') ?? $car->year }}" class="form-control @error('year') border-danger @enderror">
                            @error('year')
                            <div class="form-control-feedback text-danger">
                                <i class="icon-cancel-circle2"></i>
                            </div>
                            @enderror
                        </div>
                        @error('year')
                        <span class="form-text text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="text-right">
                        <button type="submit" class="btn btn-primary">Update <i class="icon-paperplane ml-2"></i></button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
