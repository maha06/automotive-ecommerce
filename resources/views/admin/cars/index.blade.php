@extends('admin.layouts.layout')

@section('heading')
    <h4><a href="{{ route('admin.pages.home') }}"><i class="icon-arrow-left52 mr-2"></i></a> <span class="font-weight-semibold">Home - Cars</span> - View All</h4>
@endsection

@section('breadcrumbs')
    <div class="breadcrumb">
        <a href="{{ route('admin.pages.home') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
        <span class="breadcrumb-item">Available Makes</span>
        <span class="breadcrumb-item active">View All</span>
    </div>
@endsection

@section('content')
    @include('admin.partials.header')
    <div class="content">
        @include('common.partials.flash')
        <div class="card has-table">
            <div class="card-header header-elements-inline">
                <h5 class="card-title">All Makes</h5>
                <div class="header-elements">
                    <a href="{{ route('cars.create') }}" class="mt-2 btn btn-primary">
                        Create New Make
                    </a>
                </div>
            </div>

            <table class="table datatable-basic">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Created At</th>
                        <th>Last Modified</th>
                        <th class="text-center">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($cars as $car)
                    <tr>
                        <td>{{ $car->name }}</td>
                        <td>{{ $car->created_at }}</td>
                        <td>{{ $car->updated_at }}</td>
                        <td class="text-center">
                            <div class="list-icons">
                                <div class="dropdown">
                                    <a href="#" class="list-icons-item" data-toggle="dropdown">
                                        <i class="icon-menu9"></i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        <a href="{{ route('cars.edit', [$car->id]) }}" class="dropdown-item"><i class="icon-pencil5"></i> Edit</a>
                                        <a href="#" class="dropdown-item"  onclick="event.preventDefault(); if(confirm('Are you sure you want to perform this action? ')){document.getElementById('delete-car-{{ $car->id }}-form').submit();}">
                                            <i class="icon-trash"></i>
                                            Delete
                                        </a>
                                        <form action="{{ route('cars.destroy',[$car->id]) }}" id="delete-car-{{ $car->id }}-form" method="POST" style="display: none;">
                                            @csrf
                                            @method('DELETE')
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                    @empty
                    <tr>
                        <td colspan="5">
                            <div class="alert alert-info text-center">
                                No Make Added So Far
                                <br>
                                <a href="{{ route('cars.create') }}" class="mt-2 btn btn-primary">
                                    Create New Make
                                </a>
                            </div>
                        </td>
                    </tr>
                    @endforelse
                </tbody>
            </table>
        </div>
    </div>
@endsection
