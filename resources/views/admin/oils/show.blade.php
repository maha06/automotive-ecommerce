@extends('admin.layouts.layout')

@section('heading')
    <h4>
        <a href="{{ route('admin.pages.home') }}">
            <i class="icon-arrow-left52 mr-2"></i>
        </a>
        <span class="font-weight-semibold">Home - Oils</span> - Show
    </h4>
@endsection

@section('breadcrumbs')
<div class="breadcrumb">
    <a href="{{ route('admin.pages.home') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
    <span class="breadcrumb-item"><a href="{{ route('oils.index') }}"> Oils </a></span>
</div>
@endsection

@section('content')
@include('admin.partials.header')
    <div class="content">
        @include('common.partials.flash')
        <div class="card has-table">
                <fieldset class="mb-3 mt-3 ml-2">
                    <legend class="text-uppercase font-size-sm font-weight-bold mb-0" style="font-size:18px;">Oil Details</legend>
                </fieldset>

            <div class="row" style="padding:2%">

                <div class="col-md-3">
                    <div class="card border-teal-400">
                        <div class="card-header bg-teal-400 text-white header-elements-inline">
                            <h6 class="card-title">Oil Name</h6>
                        </div>

                        <div class="card-body">
                            {{ $oil->name }}
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card border-teal-400">
                        <div class="card-header bg-teal-400 text-white header-elements-inline">
                            <h6 class="card-title">Brand Name</h6>
                        </div>

                        <div class="card-body">
                            {{ $oil->brand }}
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card border-teal-400">
                        <div class="card-header bg-teal-400 text-white header-elements-inline">
                            <h6 class="card-title">Category</h6>
                        </div>

                        <div class="card-body">
                            {{ $oil->category }}
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card border-teal-400">
                        <div class="card-header bg-teal-400 text-white header-elements-inline">
                            <h6 class="card-title">Oil Price</h6>
                        </div>

                        <div class="card-body">
                            {{ $oil->oil_rate }}
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card border-teal-400">
                        <div class="card-header bg-teal-400 text-white header-elements-inline">
                            <h6 class="card-title">Retail Price</h6>
                        </div>

                        <div class="card-body">
                            {{ $oil->retail_price }}
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card border-teal-400">
                        <div class="card-header bg-teal-400 text-white header-elements-inline">
                            <h6 class="card-title">Wholesale Price</h6>
                        </div>

                        <div class="card-body">
                            {{ $oil->wholesale_price }}
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card border-teal-400">
                        <div class="card-header bg-teal-400 text-white header-elements-inline">
                            <h6 class="card-title">Bulk Order Retail Price</h6>
                        </div>

                        <div class="card-body">
                            {{ $oil->bulk_orders_retail_price }}
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card border-teal-400">
                        <div class="card-header bg-teal-400 text-white header-elements-inline">
                            <h6 class="card-title">Discount</h6>
                        </div>

                        <div class="card-body">
                            {{ $oil->discount }}
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card border-teal-400">
                        <div class="card-header bg-teal-400 text-white header-elements-inline">
                            <h6 class="card-title">Description</h6>
                        </div>
                        <div class="card-body">
                            {!! $oil->description !!}
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card border-teal-400">
                        <div class="card-header bg-teal-400 text-white header-elements-inline">
                            <h6 class="card-title">Suggestion</h6>
                        </div>

                        <div class="card-body">
                            {!! $oil->suggestions !!}
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card border-teal-400">
                        <div class="card-header bg-teal-400 text-white header-elements-inline">
                            <h6 class="card-title">Quantity</h6>
                        </div>

                        <div class="card-body">
                            {{ $oil->quantity }}
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card border-teal-400">
                        <div class="card-header bg-teal-400 text-white header-elements-inline">
                            <h6 class="card-title">Dealer</h6>
                        </div>
                        @php
                            $admin = \App\Admin::where('id', $oil->user_id)->first();
                        @endphp
                        <div class="card-body">
                            {{ $admin->business_name }}
                        </div>
                    </div>
                </div>


                <div class="col-md-12">
                        <label style="display: block;"
                            class="font-weight-semibold @error('image') text-danger @enderror">Images </label>
                        <div class="d-inline-block" style="position: relative;margin-bottom: inherit;">
                        @if($pictures)
                            @foreach($pictures as $image)

                                <div class="d-inline-block" style="position:relative;margin-bottom: inherit; ">
                                    <img src="{{ (config('app.url'). '/mechon/storage/app/public/'. $image->path) }}" height="200px" width="200px"/>

                                </div>
                            @endforeach
                        @else
                            <p>No Image Found</p>
                        @endif

                        </div>
                </div>

            </div>

        </div>
    </div>
@endsection
