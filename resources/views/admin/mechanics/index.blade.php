@extends('admin.layouts.layout')

@section('heading')
    <h4><a href="{{ route('mechanics.index') }}"><i class="icon-arrow-left52 mr-2"></i></a> <span
            class="font-weight-semibold">Home - Mechanics</span> - View All</h4>
@endsection

@section('breadcrumbs')
    <div class="breadcrumb">
        <a href="{{ route('mechanics.index') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
        <span class="breadcrumb-item">Mechanics</span>
        <span class="breadcrumb-item active">View All</span>
    </div>
@endsection

@section('content')
   @include('admin.partials.header')
    <div class="content">
        @include('common.partials.flash')
        <div class="card has-table">
            <div class="card-header header-elements-inline">
                <h5 class="card-title">All Mechanics</h5>
            </div>

            <table class="table datatable-basic">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>Address</th>
                    <th>Status</th>
                    <th class="text-center">Actions</th>
                </tr>
                </thead>
                <tbody>
                @forelse ($mechanics as $mechanic)
                    <tr>
                        <td>{{ $mechanic->name }}</td>
                        <td>{{ $mechanic->email }}</td>
                        <td>{{ $mechanic->phone }}</td>
                        <td>{{ $mechanic->address }}</td>
                        <td class="data">
                            <select data-status-id="{{ $mechanic->id }}" class="activeInactive form-control" name="status"
                                    id="status">
                                <option {{ $mechanic->status == 1 ? 'selected' : '' }} value="1" name="hidden_status" class="hidden_status" id="hidden_status" >Active</option>
                                <option {{ $mechanic->status == 0 ? 'selected' : '' }} value="0" name="hidden_status" class="hidden_status" id="hidden_status">Inactive</option>
                            </select>
                        </td>
                        <td class="text-center">
                            <div class="list-icons">
                                <div class="dropdown">
                                    <a href="#" class="list-icons-item" data-toggle="dropdown">
                                        <i class="icon-menu9"></i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        <a href="{{ route('admin.mechanics.change-password', $mechanic->id) }}"
                                           class="dropdown-item"><i class="icon-pencil5"></i> Change Password</a>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="7">
                            <div class="alert alert-info text-center">
                                No Users Added So Far
                            </div>
                        </td>
                    </tr>
                @endforelse
                </tbody>
            </table>
            {!! $mechanics->links() !!}
        </div>
    </div>
@endsection
@push('scripts')
    <script>

        $('.activeInactive').change(function(status){
            var mechanic_id = this.getAttribute('data-status-id');
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                method: 'get',
                url: "/admin/mechanic/"+ mechanic_id +"/set-status/"+status.target.value,
                dataType: 'json',
                success:function(data){
                    if(data == 'done')
                    {
                        $('#status').bootstrapToggle('on');
                    }
                }
            });
        });
    </script>
@endpush
