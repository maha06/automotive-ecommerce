@extends('admin.layouts.layout')

@section('heading')
    <h4><a href="{{ route('users.index') }}"><i class="icon-arrow-left52 mr-2"></i></a> <span
            class="font-weight-semibold">Home - Client: {{ $user->name }}</span> - Edit</h4>
@endsection

@section('breadcrumbs')
    <div class="breadcrumb">
        <a href="{{ route('users.index') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
        <span class="breadcrumb-item">Client: {{ $user->name }}</span>
        <span class="breadcrumb-item active">Edit</span>
    </div>
@endsection

@section('content')
    @include('admin.partials.header')
    <div class="content">
        @include('common.partials.flash')

        <div class="card">
            <div class="card-header header-elements-inline">
                <h5 class="card-title">Edit Client</h5>
            </div>

            <div class="card-body">
                <form action="{{ route('users.update', $user->id) }}" method="POST">
                    @csrf
                    @method('PATCH')
                    <fieldset class="mb-3">
                        <legend class="text-uppercase font-size-sm font-weight-bold">Fill the form below to edit client
                        </legend>
                    </fieldset>
                    <div class="form-group">
                        <label class="font-weight-semibold @error('name') text-danger @enderror">Name <span
                                class="text-danger">*</span>: </label>
                        <div class="form-group-feedback form-group-feedback-right">
                            <input type="text" name="name" value="{{ old('name') ?? $user->name }}"
                                   class="form-control @error('name') border-danger @enderror" required>
                            @error('name')
                            <div class="form-control-feedback text-danger">
                                <i class="icon-cancel-circle2"></i>
                            </div>
                            @enderror
                        </div>
                        @error('name')
                        <span class="form-text text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label class="font-weight-semibold @error('username') text-danger @enderror">Username<span
                                class="text-danger">*</span>: </label>
                        <div class="form-group-feedback form-group-feedback-right">
                            <input type="text" name="username" value="{{ old('username') ?? $user->username }}"
                                   class="form-control @error('username') border-danger @enderror" required>
                            @error('username')
                            <div class="form-control-feedback text-danger">
                                <i class="icon-cancel-circle2"></i>
                            </div>
                            @enderror
                        </div>
                        @error('username')
                        <span class="form-text text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label class="font-weight-semibold @error('email') text-danger @enderror">Email<span
                                class="text-danger">*</span>: </label>
                        <div class="form-group-feedback form-group-feedback-right">
                            <input type="email" name="email" value="{{ old('email') ?? $user->email }}"
                                   class="form-control @error('email') border-danger @enderror" required>
                            @error('email')
                            <div class="form-control-feedback text-danger">
                                <i class="icon-cancel-circle2"></i>
                            </div>
                            @enderror
                        </div>
                        @error('email')
                        <span class="form-text text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label class="font-weight-semibold @error('phone') text-danger @enderror">Phone<span
                                class="text-danger">*</span>: </label>
                        <div class="form-group-feedback form-group-feedback-right">
                            <input type="text" pattern="^([0-9\s\-\+\(\)]*)$" name="phone" value="{{ old('phone') ?? $user->phone }}"
                                   class="form-control @error('phone') border-danger @enderror"
                                   oninvalid="this.setCustomValidity('Please enter numbers and special characters only')"
                                   oninput="setCustomValidity('')" required>
                            @error('phone')
                            <div class="form-control-feedback text-danger">
                                <i class="icon-cancel-circle2"></i>
                            </div>
                            @enderror
                        </div>
                        @error('phone')
                        <span class="form-text text-danger">{{ $message }}</span>
                        @enderror
                    </div>
            <div class="text-right">
                <button type="submit" class="btn btn-primary">Update <i class="icon-paperplane ml-2"></i></button>
            </div>
            </form>
        </div>
    </div>
    </div>
@endsection
