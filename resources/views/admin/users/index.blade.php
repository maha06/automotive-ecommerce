@extends('admin.layouts.layout')

@section('heading')
    <h4><a href="{{ route('users.index') }}"><i class="icon-arrow-left52 mr-2"></i></a> <span
            class="font-weight-semibold">Home - Clients</span> - View All</h4>
@endsection

@section('breadcrumbs')
    <div class="breadcrumb">
        <a href="{{ route('users.index') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
        <span class="breadcrumb-item">Clients</span>
        <span class="breadcrumb-item active">View All</span>
    </div>
@endsection

@section('content')
   @include('admin.partials.header')
    <div class="content">
        @include('common.partials.flash')
        <div class="card has-table">
            <div class="card-header header-elements-inline">
                <h5 class="card-title">All Clients - {{ $users->count() }}</h5>
                <div class="header-elements">
                    <a href="{{ route('users.create') }}" class="mt-2 btn btn-primary">
                        Create New Client
                    </a>
                </div>
            </div>

            <table class="table datatable-basic">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>User Name</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>Status</th>
                    <th class="text-center">Actions</th>
                </tr>
                </thead>
                <tbody>
                @forelse ($users as $user)
                    <tr>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->username }}</td>
                        <td>{{ $user->email }}</td>
                        <td>{{ $user->phone }}</td>
                        <td class="data">
                            <select data-status-id="{{ $user->id }}" class="activeInactive form-control" name="status"
                                    id="status">
                                <option {{ $user->status == 1 ? 'selected' : '' }} value="1" name="hidden_status" class="hidden_status" id="hidden_status" >Active</option>
                                <option {{ $user->status == 0 ? 'selected' : '' }} value="0" name="hidden_status" class="hidden_status" id="hidden_status">Inactive</option>
                            </select>
                        </td>
                        <td class="text-center">
                            <div class="list-icons">
                                <div class="dropdown">
                                    <a href="#" class="list-icons-item" data-toggle="dropdown">
                                        <i class="icon-menu9"></i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        <a href="{{ route('admin.users.change-password', $user->id) }}"
                                            class="dropdown-item"><i class="icon-pencil5"></i> Change Password</a>
                                        <a href="{{ route('users.edit', $user->id) }}"
                                           class="dropdown-item"><i class="icon-pencil5"></i> Edit</a>
                                        @if($user->master != 1)
                                        <a href="#" class="dropdown-item"
                                           onclick="event.preventDefault(); if(confirm('Are you sure you want to perform this action?')){document.getElementById('delete-user-{{ $user->id }}-form').submit();}">
                                            <i class="icon-trash"></i>
                                            Delete
                                        </a>
                                        <form action="{{ route('users.destroy', $user->id) }}"
                                              id="delete-user-{{ $user->id }}-form" method="POST"
                                              style="display: none;">
                                            @csrf
                                            @method('DELETE')
                                        </form>
                                            @endif
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="7">
                            <div class="alert alert-info text-center">
                                No Clients Added So Far
                                <br>
                                <a href="{{ route('users.create') }}" class="mt-2 btn btn-primary">
                                    Create New Client
                                </a>
                            </div>
                        </td>
                    </tr>
                @endforelse
                </tbody>
            </table>
            {!! $users->links() !!}
        </div>
    </div>
@endsection
@push('scripts')
    <script>

        $('.activeInactive').change(function(status){
            var user_id = this.getAttribute('data-status-id');
            var url = window.location.pathname.split("/")[1] === "staging"
                            ? "/staging/admin/"+ user_id +"/set-status/"+status.target.value
                            : "" + "/admin/"+ user_id +"/set-status/"+status.target.value;

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                method: 'get',
                url: url,
                dataType: 'json',
                success:function(data){
                    if(data == 'done')
                    {
                        $('#status').bootstrapToggle('on');
                    }
                }
            });
        });
    </script>
@endpush
