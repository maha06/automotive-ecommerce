@extends('admin.layouts.layout')

@section('heading')
    <h4><a href="{{ route('admin.pages.home') }}"><i class="icon-arrow-left52 mr-2"></i></a> <span
            class="font-weight-semibold">Home - Labour Categories</span> - View All</h4>
@endsection

@section('breadcrumbs')
    <div class="breadcrumb">
        <a href="{{ route('labours.index') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
        <span class="breadcrumb-item">Labours</span>
        <span class="breadcrumb-item active">View All</span>
    </div>
@endsection

@section('content')
   @include('admin.partials.header')
    <div class="content">
        @include('common.partials.flash')
        <div class="card has-table">
            <div class="card-header header-elements-inline">
                <h5 class="card-title">All Categories</h5>
                <div class="header-elements">
                    <a href="{{ route('labours.create') }}" class="mt-2 btn btn-primary">
                        Create New
                    </a>
                </div>
            </div>
            <table class="table datatable-basic">
                <thead>
                <tr>

                    <th>Name</th>
                    <th>Created At</th>
                    <th>Modified At</th>
                    <th class="text-center">Actions</th>
                </tr>
                </thead>
                <tbody>
                @forelse ($labours as $labour)
                    <tr>

                        <td>{{ $labour->labours }}</td>
                        <td>{{ $labour->created_at }}</td>
                        <td>{{ $labour->updated_at }}</td>
                        <td class="text-center">
                            <div class="list-icons">
                                <div class="dropdown">
                                    <a href="#" class="list-icons-item" data-toggle="dropdown">
                                        <i class="icon-menu9"></i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        <a href="{{ route('labours.edit', $labour->id) }}"
                                           class="dropdown-item"><i class="icon-pencil5"></i> Edit
                                        </a>
                                        <a href="#" class="dropdown-item"
                                           onclick="event.preventDefault(); if(confirm('Are you sure you want to perform this action?')){document.getElementById('delete-labour-{{ $labour->id }}-form').submit();}">
                                            <i class="icon-trash"></i>
                                            Delete
                                        </a>
                                        <form action="{{ route('labours.destroy', $labour->id) }}"
                                              id="delete-labour-{{ $labour->id }}-form" method="POST"
                                              style="display: none;">
                                            @csrf
                                            @method('DELETE')
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="7">
                            <div class="alert alert-info text-center">
                                No Labour Category Added So Far
                                <br>
                                <a href="{{ route('labours.create') }}" class="mt-2 btn btn-primary">
                                    Create New
                                </a>
                            </div>
                        </td>
                    </tr>
                @endforelse
                </tbody>
            </table>
            {!! $labours->links() !!}
        </div>
    </div>
@endsection
