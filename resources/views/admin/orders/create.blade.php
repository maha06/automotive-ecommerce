@extends('admin.layouts.layout')

@section('heading')
    <h4>
        <a href="{{ route('admin.pages.home') }}">
            <i class="icon-arrow-left52 mr-2"></i>
        </a>
        <span class="font-weight-semibold">Home - Orders</span>
    </h4>
@endsection

@section('breadcrumbs')
    <div class="breadcrumb">
        <a href="{{ route('orders.index') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
        <span class="breadcrumb-item">Order</span>
        <span class="breadcrumb-item active">Create</span>
    </div>
@endsection

@section('content')
    @include('admin.partials.header')
    <div class="content">
        @include('common.partials.flash')

        <div class="card">
            <div class="card-header header-elements-inline">
                <h5 class="card-title">Orders</h5>
            </div>

            <div class="card-body">
                <form action="{{ route('orders.store') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label class="font-weight-semibold @error('added_by') text-danger @enderror">Customers
                            <span class="text-red">*</span>
                            </label>
                            <div class="form-group-feedback form-group-feedback-right">
                            <select name="user_id"
                                    class="form-control multiselect-select-all-filtering @error('added_by') text-danger @enderror"
                                    >

                                @foreach($users as $user)
                                    <option value="{{ $user->id}}">{{  $user->name  }}</option>

                                @endforeach
                            </select>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="font-weight-semibold @error('city') text-danger @enderror">City <span
                                    class="text-red">*</span></label>
                            <div class="form-group-feedback form-group-feedback-right">
                                <input type="text" name="city" value="{{ old('city') }}"
                                    class="form-control @error('city') border-danger @enderror" required>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="font-weight-semibold @error('apartment') text-danger @enderror">Appartment</label>
                            <div class="form-group-feedback form-group-feedback-right">
                                <input type="text" name="apartment" value="{{ old('apartment') }}"
                                    class="form-control @error('apartment') border-danger @enderror" >
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="font-weight-semibold @error('address') text-danger @enderror">Address<span
                                    class="text-red">*</span></label>
                            <div class="form-group-feedback form-group-feedback-right">
                                <input type="text" name="address" value="{{ old('address') }}"
                                    class="form-control @error('address') border-danger @enderror" required>
                            </div>
                        </div>
                    </div>

                    <div id="oilform">
                    <label class="font-weight-semibold" >Add Oils </label>
                    <input type="button" value="+" id="add" style="margin:1%" class="btn btn-xs btn-info"/>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                <span class="input-group-text">Item</span>
                                </div>

                                <select class="form-control" id="language" name="oils[]" >
                                <option value=''>--select--</option>
                                @foreach($oils as $l)
                                    <option value="{{$l->id}}" >{{$l->name}} [{{$l->quantity}} items]</option>
                                @endforeach
                                </select>
                                </div>
                            </div>
                            <div class="col-sm-4">
                            <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                <span class="input-group-text">Quantity</span>
                                </div>
                                <input class="form-control" type="number" name="oil_quantity[]" value="1" min="1">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="partform">
                    <label class="font-weight-semibold" >Add Parts </label>
                    <input type="button" value="+" id="part-add" style="margin:1%" class="btn btn-xs btn-info"/>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                <span class="input-group-text">Item</span>
                                </div>

                                <select class="form-control" name="parts[]" >
                                <option value="">--select--</option>
                                @foreach($parts as $l)
                                    <option value="{{$l->id}}" >{{$l->name}} [{{$l->quantity}} items]</option>
                                @endforeach
                                </select>
                                </div>
                            </div>
                            <div class="col-sm-4">
                            <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                <span class="input-group-text">Quantity</span>
                                </div>
                                <input class="form-control" type="number" name="part_quantity[]" value="1" min="1">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="font-weight-semibold @error('order_notes') text-danger @enderror"> Order Notes </label>
                        <div class="form-group-feedback form-group-feedback-right">
                        <textarea class="ckeditor form-control" name="order_notes"></textarea>
                        </div>
                    </div>

                    <div class="text-right">
                        <button type="submit" class="btn btn-primary">Submit <i class="icon-paperplane ml-2"></i></button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script src="//cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>

<script type="text/javascript">
$(document).ready(function () {

    $("#add").click(function() {

        HTML = [];

        HTML = "<div class=\"row\"><div class=\"col-sm-6\"><div class=\"input-group mb-2\"> <div class=\"input-group-prepend\"><span class=\"input-group-text\">Item</span></div><select class=\"form-control\" id=\"language\" name='oils[]' >";

        <?php foreach ($oils as $l) {?>
            HTML += "<option value=<?php echo $l->id; ?> > <?php echo $l->name; ?> [<?php echo $l->quantity; ?> items] </option>";
        <?php }?>
        HTML += "</select></div></div><div class=\"col-sm-4\"><div class=\"input-group mb-2\"><div class=\"input-group-prepend\"><span class=\"input-group-text\">Quantity</span></div><input type=\"number\" min='1' value='1' name=\"oil_quantity[]\" class=\"form-control\"> <input type=\"button\" value=\"-\" id=\"remove\" style=\"margin:2%\" class=\"btn btn-xs btn-danger\" /></div></div>";
        $('#oilform').append(HTML);

    });

    $("#part-add").click(function() {

        HTML = [];

        HTML = "<div class=\"row\"><div class=\"col-sm-6\"><div class=\"input-group mb-2\"> <div class=\"input-group-prepend\"><span class=\"input-group-text\">Item</span></div><select class=\"form-control\" name='parts[]' >";

        <?php foreach ($parts as $l) {?>
            HTML += "<option value=<?php echo $l->id; ?> > <?php echo $l->name; ?> [<?php echo $l->quantity; ?> items] </option>";
        <?php }?>
        HTML += "</select></div></div><div class=\"col-sm-4\"><div class=\"input-group mb-2\"><div class=\"input-group-prepend\"><span class=\"input-group-text\">Quantity</span></div><input type=\"number\" min='1' value='1' name=\"part_quantity[]\" class=\"form-control\"><input type=\"button\" value=\"-\" id=\"remove\" style=\"margin:2%\" class=\"btn btn-xs btn-danger\" /></div></div>";
        $('#partform').append(HTML);

    });

    $(document).on('click', '#remove', function(e){
        $(this).parent().parent().remove();
    });
});

</script>
@endsection
