@extends('admin.layouts.layout')

@section('heading')
    <h4>
        <a href="{{ route('admin.pages.home') }}">
            <i class="icon-arrow-left52 mr-2"></i>
        </a>
        <span class="font-weight-semibold">Home - Order</span> - Show
    </h4>
@endsection

@section('breadcrumbs')
<div class="breadcrumb">
    <a href="{{ route('admin.pages.home') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
    <span class="breadcrumb-item"><a href="{{ route('orders.index') }}"> Order </a></span>
</div>
@endsection

@section('content')
@include('admin.partials.header')
    <div class="content">
        @include('common.partials.flash')
        <div class="card has-table" style="padding:2%">
            <form action="{{ route('order.status') }}" method="POST">
                @csrf
                <div class="row">
                    <div class="col-md-6"> </div>
                    <div class="col-md-4">
                        <select class="form-control" name="status">
                            <option value='{{$order->status}}'>{{$status}}</option>
                            <option style="font-size: 1pt; background-color: #000000;" disabled>&nbsp;</option>
                            <option value='0'>Pending</option>
                            <option value='1'>Completed</option>
                            <option value='2'>Cancelled</option>
                        </select>
                    </div>
                    <input type="hidden" name='id' value='{{$order->id}}'>
                    <div class="col-md-2">
                        <button type="submit" class="btn btn-info">Update</button>
                    </div>
                </div>
            </form>
                <div class="row">
                <div class="col-md-12">
                    <fieldset class="mb-3 mt-3 ml-2">
                        <legend class="text-uppercase font-size-sm font-weight-bold mb-0" style="font-size:18px;">TOTAL BILL : {{ $order->price }}</legend>
                    </fieldset>
                </div>
                <div class="col-md-12">
                    <div class="card border-teal-400">
                        <div class="card-header bg-teal-400 text-white header-elements-inline">
                            <h6 class="card-title">Customer Details</h6>
                        </div>

                        <div class="card-body">
                                <li>
                                    <span style="font-weight:bold"> Name:  </span>{{ $order->name }}
                                </li>
                                <li>
                                    <span style="font-weight:bold"> Email:  </span>{{ $order->email }}
                                </li><li>
                                    <span style="font-weight:bold"> Contact:  </span>{{ $order->phone }}
                                </li><li>
                                    <span style="font-weight:bold"> Address:  </span>{{ $order->address }}
                                </li><li>
                                    <span style="font-weight:bold"> Apartment:  </span>{{ $order->appartment }}
                                </li><li>
                                    <span style="font-weight:bold"> City:  </span>{{ $order->city }}
                                </li><li>
                                    <span style="font-weight:bold"> Order Notes:  </span>{{ $order->order_notes }}
                                </li>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">

                <div class="col-md-12">
                    <div class="card border-teal-400">
                        <div class="card-header bg-teal-400 text-white header-elements-inline">
                            <h6 class="card-title">Order Details</h6>
                        </div>

                        <div class="card-body">

                        <table class="table datatable-basic">
                <thead>
                    <tr>
                        <th>#</th>
                        <!-- <th>Order Number</th> -->
                        <th>Product </th>
                        <th>Single item Price</th>
                        <th>Quantity</th>
                    </tr>
                </thead>
                <tbody>
                @forelse($details as $key=> $order)
                    <tr>
                        <td>{{$key+1}}</td>
                        @if ($order->part_id > 0)
                            <td>{{$order->part->name }}</td>
                        @else
                            <td>{{$order->oil->name }}</td>
                        @endif
                        @if ($order->part_id > 0)
                            <td>{{$order->part->retail_price }}</td>
                        @else
                            <td>{{$order->oil->retail_price }}</td>
                        @endif
                        <td>{{$order->quantity }}</td>

                    </tr>
                @empty
                <tr>
                    <td colspan="12">
                        <div class="alert alert-info text-center">
                            No Orders Recieved So Far
                            <br>
                        </div>
                    </td>
                </tr>
                @endforelse
                </tbody>
            </table>


                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection
