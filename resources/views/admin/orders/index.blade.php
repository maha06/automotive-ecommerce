@extends('admin.layouts.layout')

@section('heading')
    <h4>
        <a href="{{ route('admin.pages.home') }}">
            <i class="icon-arrow-left52 mr-2"></i>
        </a>
        <span class="font-weight-semibold">Home - Orders</span> - View All
    </h4>
@endsection

@section('breadcrumbs')
<div class="breadcrumb">
    <a href="{{ route('admin.pages.home') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
    <span class="breadcrumb-item"><a href="{{ route('orders.index') }}"> Orders </a></span>
    <span class="breadcrumb-item active">View All</span>
</div>
@endsection

@section('content')
@include('admin.partials.header')
    <div class="content">
        @include('common.partials.flash')
        <div class="card has-table">
            <div class="card-header header-elements-inline">
                <h5 class="card-title">Orders</h5>
            </div>
            <table class="table datatable-basic">
                <thead>
                    <tr>
                        <th>#</th>
                        <!-- <th>Order Number</th> -->
                        <th>Customer Name</th>
                        <th>Phone</th>
                        <th>Total Price</th>
                        <th>Status</th>
                        <th>Created At</th>
                        <th class="text-center">Actions</th>
                    </tr>
                </thead>
                <tbody>
                @forelse($orders as $key=> $order)
                    <tr>
                        <td>{{$key+1}}</td>
                        <!-- <td>{{$order->order_number }}</td> -->
                        <td>{{$order->name }}</td>
                        <td>{{$order->phone }}</td>
                        <td>{{ $order->price }}</td>
                        <td>
                            @if ($order->status == 0)
                            <span class="badge badge-primary">Pending</span>
                            @elseif ($order->status == 1)
                            <span class="badge badge-success"> Completed </span>
                            @elseif ($order->status == 2)
                            <span class="badge badge-danger"> Cancelled </span>
                            @endif
                        </td>
                        <td>{{$order->created_at }}</td>
                        <td class="text-center">
                            <div class="list-icons">
                                <div class="dropdown">
                                    <a href="#" class="list-icons-item" data-toggle="dropdown">
                                        <i class="icon-menu9"></i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        <a href="{{ route('orders.show', [$order->id]) }}" class="dropdown-item">
                                            <i class="icon-eye"></i>
                                            Show
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                @empty
                <tr>
                    <td colspan="12">
                        <div class="alert alert-info text-center">
                            No Orders Recieved So Far
                            <br>
                        </div>
                    </td>
                </tr>
                @endforelse
                </tbody>
            </table>
            {!! $orders->links() !!}
        </div>
    </div>
@endsection

@push('scripts')
    <script src="{{asset('backend/js/plugins/tables/datatables/datatables.min.js')}}"></script>
    <script src="{{asset('backend/js/demo_pages/datatables_basic.js')}}"></script>
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
@endpush
