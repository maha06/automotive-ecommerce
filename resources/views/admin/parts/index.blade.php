@extends('admin.layouts.layout')

@section('heading')
    <h4><a href="{{ route('parts.index') }}"><i class="icon-arrow-left52 mr-2"></i></a> <span
            class="font-weight-semibold">Home - Spare Parts</span> - View All</h4>
@endsection

@section('breadcrumbs')
    <div class="breadcrumb">
        <a href="{{ route('parts.index') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
        <span class="breadcrumb-item">Spare Parts</span>
        <span class="breadcrumb-item active">View All</span>
    </div>
@endsection

@section('content')
   @include('admin.partials.header')
    <div class="content">
        @include('common.partials.flash')
        <div class="card has-table">
            <div class="card-header header-elements-inline">
                <h5 class="card-title">All Spare Parts</h5>
                <div class="header-elements">
                    <a href="{{ route('parts.create') }}" class="mt-2 btn btn-primary">
                        Create New Oil
                    </a>
                </div>
            </div>
            <table class="table datatable-basic">
                <thead>
                <tr>
                    <th>Image</th>
                    <th>Name</th>
                    <th>Brand</th>
                    <th>Retail Price</th>
                    <th>Dealer</th>
                    <th class="text-center">Actions</th>
                </tr>
                </thead>
                <tbody>
                @forelse ($parts as $part)
                    <tr> <td>
                        @php

                        if ($pic = $part->pictures->first()) {

                        @endphp
                            <img src="{{ (config('app.url'). '/mechon/storage/app/public/'. $pic->path) }}" height="120px" width="120px"/>
                        @php
                    }
                        @endphp
                        </td>
                        <td>{{ $part->name }}</td>
                        <td>{{ $part->brand }}</td>
                        <td>{{ $part->retail_price }}</td>
                        @php

$admin = \App\Admin::where('id', $part->user_id)->first();
    @endphp
<td>{{  $admin->business_name}} </td>
                        <td class="text-center">
                            <div class="list-icons">
                                <div class="dropdown">
                                    <a href="#" class="list-icons-item" data-toggle="dropdown">
                                        <i class="icon-menu9"></i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        <a href="{{ route('parts.edit', $part->id) }}"
                                           class="dropdown-item"><i class="icon-pencil5"></i> Edit
                                        </a>
                                        <a href="{{ route('parts.show', [$part->id]) }}" class="dropdown-item">
                                            <i class="icon-eye"></i>
                                            Show
                                        </a>
                                        <a href="#" class="dropdown-item"
                                           onclick="event.preventDefault(); if(confirm('Are you sure you want to perform this action?')){document.getElementById('delete-post-{{ $part->id }}-form').submit();}">
                                            <i class="icon-trash"></i>
                                            Delete
                                        </a>
                                        <form action="{{ route('parts.destroy', $part->id) }}"
                                              id="delete-post-{{ $part->id }}-form" method="POST"
                                              style="display: none;">
                                            @csrf
                                            @method('DELETE')
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="7">
                            <div class="alert alert-info text-center">
                                No Parts Added So Far
                                <br>
                                <a href="{{ route('parts.create') }}" class="mt-2 btn btn-primary">
                                    Create New Part
                                </a>
                            </div>
                        </td>
                    </tr>
                @endforelse
                </tbody>
            </table>
            {!! $parts->links() !!}
        </div>
    </div>
@endsection
