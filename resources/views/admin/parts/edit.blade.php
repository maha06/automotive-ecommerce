@extends('admin.layouts.layout')

@section('heading')
    <h4>
        <a href="{{ route('admin.pages.home') }}">
            <i class="icon-arrow-left52 mr-2"></i>
        </a>
        <span class="font-weight-semibold">Home - Spare Parts</span>
    </h4>
@endsection

@section('breadcrumbs')
    <div class="breadcrumb">
        <a href="{{ route('parts.index') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
        <span class="breadcrumb-item">Part: {{ $part->name }}</span>
        <span class="breadcrumb-item active">Edit</span>
    </div>
@endsection
@section('content')
    @include('admin.partials.header')
    <div class="content">
        @include('common.partials.flash')
        <div class="card">
            <div class="card-header header-elements-inline">
                <h5 class="card-title">Spare Part</h5>
            </div>
            <div class="card-body">
                <form action="{{ route('parts.update', $part->id) }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label class="font-weight-semibold @error('name') text-danger @enderror">Name <span
                                    class="text-red">*</span></label>
                            <div class="form-group-feedback form-group-feedback-right">
                                <input type="text" name="name" value="{{ $part->name }}"
                                    class="form-control @error('name') border-danger @enderror" required>
                                @error('name')
                                <div class="form-control-feedback text-danger">
                                    <i class="icon-cancel-circle2"></i>
                                </div>
                                @enderror
                            </div>
                            @error('name')
                            <span class="form-text text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group col-md-6">
                            <label class="font-weight-semibold @error('brand_name') text-danger @enderror">Brand Name </label>
                            <div class="form-group-feedback form-group-feedback-right">
                                <input type="text" name="brand_name" value="{{ $part->brand }}"
                                    class="form-control @error('brand_name') border-danger @enderror" >
                                @error('brand_name')
                                <div class="form-control-feedback text-danger">
                                    <i class="icon-cancel-circle2"></i>
                                </div>
                                @enderror
                            </div>
                            @error('name')
                            <span class="form-text text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label class="font-weight-semibold @error('wholesale_price') text-danger @enderror">Wholesale Price <span
                                    class="text-red">*</span></label>
                            <div class="form-group-feedback form-group-feedback-right">
                                <input type="text" name="wholesale_price" value="{{ $part->wholesale_price }}"
                                    class="form-control @error('wholesale_price') border-danger @enderror" required>
                                @error('wholesale_price')
                                <div class="form-control-feedback text-danger">
                                    <i class="icon-cancel-circle2"></i>
                                </div>
                                @enderror
                            </div>
                            @error('wholesale_price')
                            <span class="form-text text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group col-md-6">
                            <label class="font-weight-semibold @error('retail_price') text-danger @enderror">Retail Price
                            <span class="text-red">*</span> </label>
                            <div class="form-group-feedback form-group-feedback-right">
                                <input type="text" name="retail_price" value="{{ $part->retail_price }}"
                                    class="form-control @error('retail_price') border-danger @enderror" >
                                @error('retail_price')
                                <div class="form-control-feedback text-danger">
                                    <i class="icon-cancel-circle2"></i>
                                </div>
                                @enderror
                            </div>
                            @error('retail_price')
                            <span class="form-text text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="row">

                        <div class="form-group col-md-6">
                            <label class="font-weight-semibold @error('bulk_orders_retail_price') text-danger @enderror">Bulk Order Retail Price
                            <span class="text-red">*</span> </label>
                            <div class="form-group-feedback form-group-feedback-right">
                                <input type="text" name="bulk_orders_retail_price" value="{{ $part->bulk_orders_retail_price }}"
                                    class="form-control @error('bulk_orders_retail_price') border-danger @enderror" >
                                @error('bulk_orders_retail_price')
                                <div class="form-control-feedback text-danger">
                                    <i class="icon-cancel-circle2"></i>
                                </div>
                                @enderror
                            </div>
                            @error('bulk_orders_retail_price')
                            <span class="form-text text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group col-md-6">
                            <label class="font-weight-semibold @error('discount') text-danger @enderror">Discount (If any) </label>
                            <div class="form-group-feedback form-group-feedback-right">
                                <input type="text" name="discount" value="{{ $part->discount }} "
                                    class="form-control @error('discount') border-danger @enderror" >
                                @error('discount')
                                <div class="form-control-feedback text-danger">
                                    <i class="icon-cancel-circle2"></i>
                                </div>
                                @enderror
                            </div>
                            @error('discount')
                            <span class="form-text text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="row">

                        <div class="form-group col-md-6">
                            <label class="font-weight-semibold @error('category') text-danger @enderror">Category </label>
                            <div class="form-group-feedback form-group-feedback-right">
                                <input type="text" name="category" value="{{ $part->category }}"
                                    class="form-control @error('category') border-danger @enderror" >
                                @error('category')
                                <div class="form-control-feedback text-danger">
                                    <i class="icon-cancel-circle2"></i>
                                </div>
                                @enderror
                            </div>
                            @error('category')
                            <span class="form-text text-danger">{{ $message }}</span>
                            @enderror
                        </div>

                        <div class="form-group col-md-6">
                            <label class="font-weight-semibold @error('sub_category') text-danger @enderror">Sub Category </label>
                            <div class="form-group-feedback form-group-feedback-right">
                                <input type="text" name="sub_category" value="{{ $part->sub_category }}"
                                    class="form-control @error('sub_category') border-danger @enderror" >
                                @error('sub_category')
                                <div class="form-control-feedback text-danger">
                                    <i class="icon-cancel-circle2"></i>
                                </div>
                                @enderror
                            </div>
                            @error('sub_category')
                            <span class="form-text text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="font-weight-semibold @error('description') text-danger @enderror">Description </label>
                        <div class="form-group-feedback form-group-feedback-right">
                            <textarea name="description" class="ckeditor form-control @error('description') border-danger @enderror" rows="6">
                                {{$part->description}}
                            </textarea>
                            @error('description')
                            <div class="form-control-feedback text-danger">
                                <i class="icon-cancel-circle2"></i>
                            </div>
                            @enderror
                        </div>
                        @error('description')
                        <span class="form-text text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label class="font-weight-semibold @error('quantity') text-danger @enderror">Quantity </label>
                            <div class="form-group-feedback form-group-feedback-right">
                                <input type="text" name="quantity" value="{{$part->quantity}} "
                                    class="form-control @error('quantity') border-danger @enderror" >
                                @error('quantity')
                                <div class="form-control-feedback text-danger">
                                    <i class="icon-cancel-circle2"></i>
                                </div>
                                @enderror
                            </div>
                            @error('quantity')
                            <span class="form-text text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group col-md-6">
                            <label class="font-weight-semibold @error('added_by') text-danger @enderror">Creator </label>
                            <div class="form-group-feedback form-group-feedback-right">
                            <select name="added_by"
                                    class="form-control multiselect-select-all-filtering @error('added_by') text-danger @enderror"
                                    >
                                    @php

                                $admin = \App\Admin::where('id', $part->user_id)->first();
                                    @endphp
                                    <option value="{{$part->user_id}}">{{ $admin->business_name }}</option>
                                @foreach($dealers as $dealer)
                                    <option value="{{ $dealer->id}}">{{  $dealer->business_name  }}</option>
                                @endforeach
                            </select>
                        </div>
                        @error('added_by')
                        <span class="form-text text-danger">{{ $message }}</span>
                        @enderror
                        </div>
                    </div>

                    <div class="form-group">
                        <label style="display: block;"
                            class="font-weight-semibold @error('image') text-danger @enderror">Image <span
                                class="text-red">*</span></label>
                        <div class="d-inline-block" style="position: relative;margin-bottom: inherit;">
                        @if($pictures)
                            @foreach($pictures as $image)

                                <div class="d-inline-block" style="position:relative;margin-bottom: inherit; ">
                                    <img src="{{ (config('app.url'). '/mechon/storage/app/public/'. $image->path) }}" height="200px" width="200px"/>
                                     <a href="{{route('part.picture.delete',[$part->id,$image->path])}}" class="btn btn-danger text-white"
                                        onclick="return confirm('Are you sure you want to delete this image?')"
                                        style="position: absolute; top: 0; right: 0;"> &times; </a>
                                </div>
                            @endforeach
                        @else
                            <p>No Image Found</p>
                        @endif
                        <input type="file" accept=".jpeg,.png,.bmp,.gif,.svg,.webp,.jpg" name="image[]" multiple>

                        <span class="form-text text-muted">Accepted formats: jpeg, png, bmp, gif, svg, jpg, or webp</span>
                        @error('image')
                        <span class="form-text text-danger">{{ $message }}</span>
                        @enderror
                        </div>
                    </div>
                    <div class="text-right">
                        <button type="submit" class="btn btn-primary">Submit <i class="icon-paperplane ml-2"></i></button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script src="//cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('.ckeditor').ckeditor();
    });
</script>
@endsection
