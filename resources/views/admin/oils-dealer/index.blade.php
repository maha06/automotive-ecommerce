@extends('admin.layouts.layout')

@section('heading')
    <h4><a href="{{ route('oils-dealers.index') }}"><i class="icon-arrow-left52 mr-2"></i></a> <span
            class="font-weight-semibold">Home - Oil Dealers</span> - View All</h4>
@endsection

@section('breadcrumbs')
    <div class="breadcrumb">
        <a href="{{ route('oils-dealers.index') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
        <span class="breadcrumb-item">Oil Dealers</span>
        <span class="breadcrumb-item active">View All</span>
    </div>
@endsection

@section('content')
   @include('admin.partials.header')
    <div class="content">
        @include('common.partials.flash')
        <div class="card has-table">
            <div class="card-header header-elements-inline">
                <h5 class="card-title">All Dealers</h5>
                <div class="header-elements">
                    <a href="{{ route('oils-dealers.create') }}" class="mt-2 btn btn-primary">
                        Add New Dealer
                    </a>
                </div>
            </div>
            <table class="table datatable-basic">
                <thead>
                <tr>
                    <th>Image</th>
                    <th>Business Name</th>
                    <th>Contact</th>
                    <th>Address</th>
                    <th>Registered at</th>
                    <th class="text-center">Actions</th>
                </tr>
                </thead>
                <tbody>
                @forelse ($dealers as $dealer)
                    <tr> <td>
                        @if($dealer->picture)
                        <img src="{{ (config('app.url'). '/mechon/storage/app/public/'. $dealer->picture) }}" height="120px" width="120px"/>
                        @else
                        <p> No image uploaded </p>
                        @endif
                        </td>
                        <td>{{ $dealer->name }}</td>

                        <td>{{ $dealer->business_name }}</td>
                        <td>{{ $dealer->contact }}</td>
                        <td>{{ $dealer->address }}</td>
                        <td>{{ $dealer->created_at }} </td>
                        <td class="text-center">
                            <div class="list-icons">
                                <div class="dropdown">
                                    <a href="#" class="list-icons-item" data-toggle="dropdown">
                                        <i class="icon-menu9"></i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        <a href="{{ route('oils-dealers.edit', $dealer->id) }}"
                                           class="dropdown-item"><i class="icon-pencil5"></i> Edit
                                        </a>
                                        <a href="{{ route('oils-dealers.show', [$dealer->id]) }}" class="dropdown-item">
                                            <i class="icon-eye"></i>
                                            Show
                                        </a>
                                        <a href="#" class="dropdown-item"
                                           onclick="event.preventDefault(); if(confirm('Are you sure you want to perform this action?')){document.getElementById('delete-post-{{ $dealer->id }}-form').submit();}">
                                            <i class="icon-trash"></i>
                                            Delete
                                        </a>
                                        <form action="{{ route('oils-dealers.destroy', $dealer->id) }}"
                                              id="delete-post-{{ $dealer->id }}-form" method="POST"
                                              style="display: none;">
                                            @csrf
                                            @method('DELETE')
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="7">
                            <div class="alert alert-info text-center">
                                No Dealer Added So Far
                                <br>
                                <a href="{{ route('oils-dealers.create') }}" class="mt-2 btn btn-primary">
                                    Create New Dealers
                                </a>
                            </div>
                        </td>
                    </tr>
                @endforelse
                </tbody>
            </table>
            {!! $dealers->links() !!}
        </div>
    </div>
@endsection
