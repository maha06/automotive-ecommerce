@extends('admin.layouts.layout')

@section('heading')
    <h4>
        <a href="{{ route('admin.pages.home') }}">
            <i class="icon-arrow-left52 mr-2"></i>
        </a>
        <span class="font-weight-semibold">Home - Oil Dealer</span> - Show
    </h4>
@endsection

@section('breadcrumbs')
<div class="breadcrumb">
    <a href="{{ route('admin.pages.home') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
    <span class="breadcrumb-item"><a href="{{ route('oils-dealers.index') }}"> Oil Dealer </a></span>
</div>
@endsection

@section('content')
@include('admin.partials.header')
    <div class="content">
        @include('common.partials.flash')
        <div class="card has-table">
                <fieldset class="mb-3 mt-3 ml-2">
                    <legend class="text-uppercase font-size-sm font-weight-bold mb-0" style="font-size:18px;">Dealer Details</legend>
                </fieldset>

            <div class="row" style="padding:2%">

                <div class="col-md-6">
                    <div class="card border-teal-400">
                        <div class="card-header bg-teal-400 text-white header-elements-inline">
                            <h6 class="card-title">Username</h6>
                        </div>

                        <div class="card-body">
                            {{ $dealer->username }}
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card border-teal-400">
                        <div class="card-header bg-teal-400 text-white header-elements-inline">
                            <h6 class="card-title">Business Name</h6>
                        </div>

                        <div class="card-body">
                            {{ $dealer->business_name }}
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card border-teal-400">
                        <div class="card-header bg-teal-400 text-white header-elements-inline">
                            <h6 class="card-title">Contact</h6>
                        </div>

                        <div class="card-body">
                            {{ $dealer->contact }}
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card border-teal-400">
                        <div class="card-header bg-teal-400 text-white header-elements-inline">
                            <h6 class="card-title">NIC</h6>
                        </div>

                        <div class="card-body">
                            {{ $dealer->nic }}
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card border-teal-400">
                        <div class="card-header bg-teal-400 text-white header-elements-inline">
                            <h6 class="card-title">Email</h6>
                        </div>

                        <div class="card-body">
                            {{ $dealer->email }}
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card border-teal-400">
                        <div class="card-header bg-teal-400 text-white header-elements-inline">
                            <h6 class="card-title">Address</h6>
                        </div>

                        <div class="card-body">
                            {{ $dealer->address }}
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card border-teal-400">
                        <div class="card-header bg-teal-400 text-white header-elements-inline">
                            <h6 class="card-title">City</h6>
                        </div>

                        <div class="card-body">
                            {{ $dealer->city }}
                        </div>
                    </div>
                </div>

                <div class="col-md-12">
                        <label style="display: block;"
                            class="font-weight-semibold @error('image') text-danger @enderror">Images </label>
                        <div class="d-inline-block" style="position: relative;margin-bottom: inherit;">

                        @if($dealer->picture)
                        <img src="{{ (config('app.url'). '/mechon/storage/app/public/'. $dealer->picture) }}" height="120px" width="120px"/>
                        @else
                        <p> No image to show </p>
                        @endif

                        </div>
                </div>

            </div>

        </div>
    </div>
@endsection
