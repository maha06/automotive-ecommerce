@extends('admin.layouts.layout')

@section('heading')
    <h4>
        <a href="{{ route('admin.pages.home') }}">
            <i class="icon-arrow-left52 mr-2"></i>
        </a>
        <span class="font-weight-semibold">Home - Oil Dealers</span>
    </h4>
@endsection

@section('breadcrumbs')
    <div class="breadcrumb">
        <a href="{{ route('oils-dealers.index') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
        <span class="breadcrumb-item">Oil Dealer</span>
        <span class="breadcrumb-item active">Create</span>
    </div>
@endsection

@section('content')
    @include('admin.partials.header')
    <div class="content">
        @include('common.partials.flash')

        <div class="card">
            <div class="card-header header-elements-inline">
                <h5 class="card-title">Oil Dealer</h5>
            </div>

            <div class="card-body">
                <form action="{{ route('oils-dealers.store') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label class="font-weight-semibold @error('name') text-danger @enderror">User Name <span
                                    class="text-red">*</span></label>
                            <div class="form-group-feedback form-group-feedback-right">
                                <input type="text" name="name" value="{{ old('name') }}"
                                    class="form-control @error('name') border-danger @enderror" required>
                                @error('name')
                                <div class="form-control-feedback text-danger">
                                    <i class="icon-cancel-circle2"></i>
                                </div>
                                @enderror
                            </div>
                            @error('name')
                            <span class="form-text text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group col-md-6">
                            <label class="font-weight-semibold @error('business_name') text-danger @enderror">Business Name </label>
                            <div class="form-group-feedback form-group-feedback-right">
                                <input type="text" name="business_name" value="{{ old('business_name') }}"
                                    class="form-control @error('business_name') border-danger @enderror" >
                                @error('business_name')
                                <div class="form-control-feedback text-danger">
                                    <i class="icon-cancel-circle2"></i>
                                </div>
                                @enderror
                            </div>
                            @error('business_name')
                            <span class="form-text text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-4">
                            <label class="font-weight-semibold @error('contact') text-danger @enderror">Contact <span
                                    class="text-red">*</span></label>
                            <div class="form-group-feedback form-group-feedback-right">
                                <input type="text" name="contact" value="{{ old('contact') }}"
                                    class="form-control @error('contact') border-danger @enderror" required>
                                @error('contact')
                                <div class="form-control-feedback text-danger">
                                    <i class="icon-cancel-circle2"></i>
                                </div>
                                @enderror
                            </div>
                            @error('contact')
                            <span class="form-text text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group col-md-4">
                            <label class="font-weight-semibold @error('nic') text-danger @enderror">NIC
                            <span class="text-red">*</span> </label>
                            <div class="form-group-feedback form-group-feedback-right">
                                <input type="text" name="nic" value="{{ old('nic') }}"
                                    class="form-control @error('nic') border-danger @enderror" >
                                @error('nic')
                                <div class="form-control-feedback text-danger">
                                    <i class="icon-cancel-circle2"></i>
                                </div>
                                @enderror
                            </div>
                            @error('nic')
                            <span class="form-text text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group col-md-4">
                            <label class="font-weight-semibold @error('email') text-danger @enderror">Email
                            <span class="text-red">*</span> </label>
                            <div class="form-group-feedback form-group-feedback-right">
                                <input type="text" name="email" value="{{ old('email') }}"
                                    class="form-control @error('email') border-danger @enderror" >
                                @error('email')
                                <div class="form-control-feedback text-danger">
                                    <i class="icon-cancel-circle2"></i>
                                </div>
                                @enderror
                            </div>
                            @error('email')
                            <span class="form-text text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label class="font-weight-semibold @error('address') text-danger @enderror">Address <span
                                    class="text-red">*</span></label>
                            <div class="form-group-feedback form-group-feedback-right">
                                <input type="text" name="address" value="{{ old('address') }}"
                                    class="form-control @error('address') border-danger @enderror" required>
                                @error('address')
                                <div class="form-control-feedback text-danger">
                                    <i class="icon-cancel-circle2"></i>
                                </div>
                                @enderror
                            </div>
                            @error('address')
                            <span class="form-text text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group col-md-6">
                            <label class="font-weight-semibold @error('city') text-danger @enderror">City
                            <span class="text-red">*</span> </label>
                            <div class="form-group-feedback form-group-feedback-right">
                                <input type="text" name="city" value="{{ old('city') }}"
                                    class="form-control @error('city') border-danger @enderror" >
                                @error('city')
                                <div class="form-control-feedback text-danger">
                                    <i class="icon-cancel-circle2"></i>
                                </div>
                                @enderror
                            </div>
                            @error('city')
                            <span class="form-text text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="font-weight-semibold @error('image') text-danger @enderror">Picture
                         </label>
                        <div class="form-group-feedback form-group-feedback-right">
                            <input type="file" accept=".jpeg,.png,.bmp,.gif,.svg,.webp,.jpg" name="image" >
                            <span class="form-text text-muted">Accepted formats: jpeg, png, bmp, gif, svg, jpg, or webp</span>
                            @error('image')
                            <div class="form-control-feedback text-danger">
                                <i class="feature_image-cancel-circle2"></i>
                            </div>
                            @enderror
                        </div>
                        @error('image')
                        <span class="form-text text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="text-right">
                        <span class="text-muted" style="margin-right:3%">Default Password is 12345678 </span>
                        <button type="submit" class="btn btn-primary">Submit <i class="icon-paperplane ml-2"></i></button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
