@extends('admin.layouts.layout')

@section('heading')
    <h4>
        <a href="{{ route('admin.pages.home') }}">
            <i class="icon-arrow-left52 mr-2"></i>
        </a>
        <span class="font-weight-semibold">Home - Services</span>
    </h4>
@endsection

@section('breadcrumbs')
    <div class="breadcrumb">
        <a href="{{ route('services.index') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
        <span class="breadcrumb-item">Service</span>
        <span class="breadcrumb-item active">Create</span>
    </div>
@endsection

@section('content')
    @include('admin.partials.header')
    <div class="content">
        @include('common.partials.flash')

        <div class="card">
            <div class="card-header header-elements-inline">
                <h5 class="card-title">Services</h5>
            </div>

            <div class="card-body">
                <form action="{{ route('services.store') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label class="font-weight-semibold @error('name') text-danger @enderror">Name <span
                                class="text-red">*</span></label>
                        <div class="form-group-feedback form-group-feedback-right">
                            <input type="text" name="name" value="{{ old('name') }}"
                                class="form-control @error('name') border-danger @enderror" required>
                            @error('name')
                            <div class="form-control-feedback text-danger">
                                <i class="icon-cancel-circle2"></i>
                            </div>
                            @enderror
                        </div>
                        @error('name')
                        <span class="form-text text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label class="font-weight-semibold @error('description') text-danger @enderror">Description </label>
                        <div class="form-group-feedback form-group-feedback-right">
                            <textarea name="description" class="form-control @error('description') border-danger @enderror" rows="6"></textarea>
                            @error('description')
                            <div class="form-control-feedback text-danger">
                                <i class="icon-cancel-circle2"></i>
                            </div>
                            @enderror
                        </div>
                        @error('description')
                        <span class="form-text text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label class="font-weight-semibold @error('price') text-danger @enderror">Price <span
                                class="text-red">*</span></label>
                        <div class="form-group-feedback form-group-feedback-right">
                            @foreach($labours as $labour)
                            <div class="row">
                                <div class="col-md-4"> <input type="hidden" name="labour[]" value="{{ $labour->id }}">{{ $labour->labours }}</div>
                                <div class="col-md-3">
                                <input type="text" name="price[]"
                                class="form-control @error('price') border-danger @enderror" value="0">
                                </div>
                            </div>
                            @endforeach
                            @error('price')
                            <div class="form-control-feedback text-danger">
                                <i class="icon-cancel-circle2"></i>
                            </div>
                            @enderror
                        </div>
                        @error('price')
                        <span class="form-text text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group col-md-3">
                        <label class="font-weight-semibold @error('discount') text-danger @enderror">Discount </label>
                        <div class="form-group-feedback form-group-feedback-right">
                                <input type="text" name="discount"
                                class="form-control @error('discount') border-danger @enderror" value="0">

                            @error('discount')
                            <div class="form-control-feedback text-danger">
                                <i class="icon-cancel-circle2"></i>
                            </div>
                            @enderror
                        </div>
                        @error('discount')
                        <span class="form-text text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label class="font-weight-semibold @error('image') text-danger @enderror">Image
                            <span class="text-red">*</span></label>
                        <div class="form-group-feedback form-group-feedback-right">
                            <input type="file" accept=".jpeg,.png,.bmp,.gif,.svg,.webp,.jpg" name="image[]" multiple>
                            <span class="form-text text-muted">Accepted formats: jpeg, png, bmp, gif, svg, jpg, or webp</span>
                            @error('image')
                            <div class="form-control-feedback text-danger">
                                <i class="feature_image-cancel-circle2"></i>
                            </div>
                            @enderror
                        </div>
                        @error('image')
                        <span class="form-text text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="text-right">
                        <button type="submit" class="btn btn-primary">Submit <i class="icon-paperplane ml-2"></i></button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
