@extends('admin.layouts.layout')

@section('heading')
    <h4>
        <span class="font-weight-semibold">Home</span> - Dashboard
    </h4>
@endsection

@section('breadcrumbs')
    <div class="breadcrumb">
        <a href="{{ route('admin.pages.home') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
        <span class="breadcrumb-item active">Dashboard</span>
    </div>
@endsection

@section('content')
    @include('admin.partials.header')
    <!-- Content area -->
    <div class="content">
        @include('common.partials.flash')
        <!-- Dashboard content -->
        <div class="row">
            @if(Auth::guard('admin')->check())
            <div class="col-xl-12">
                <!-- Stats boxes -->
                <div class="form-group">
                <form action="{{ route('admin.home.filter') }}" method="POST">
                @csrf
        <div class="row">
            <div class="col-md-9"></div>
            <div class="col-md-2">
                <select class="form-control" name="filter">
                <optgroup>
                  <option value=""> {!! $filter ?? 'Today' !!} </option>
                </optgroup>
                <optgroup label="__________________________________">
                    <option value="today">Today</option>
                    <option value="yesterday">Yesterday</option>
                    <option value="this_week">This Week</option>
                    <option value="last_week">Last Week</option>
                    <option value="this_month">This Month</option>
                    <option value="last_month">Last Month</option>
                    <option value="this_year">This Year</option>
                </optgroup>
                </select>
            </div>
            <div class="col-md-1">
                <button  class="btn btn-default" id="go" >Go</button>
            </div>
        </div>
    </form>
    </div>
<div class="row" >
    <div class="card text-white bg-grey col-md-4" style="margin-right: 5%; margin-left: 12%">
        <div class="card-body">
        <div class="text-value">{!! $money !!}</div>
        <div>Earnings </div>
        <div class="progress progress-white progress-xs my-2">
            <div class="progress-bar" role="progressbar" style="width: {{ $money }}%" aria-valuenow="{{ $money }}" aria-valuemin="0" aria-valuemax="1000000"></div>
        </div>
        <small class="text-white">Keep making more.</small>
        </div>
    </div>
    <div class="card text-white bg-grey col-md-4">
        <div class="card-body">
        <div class="text-value">{!! $bookings !!}</div>
        <div>Workshop Visits</div>
        <div class="progress progress-white progress-xs my-2">
            <div class="progress-bar" role="progressbar" style="width: {{ $bookings }}%" aria-valuenow="{{ $bookings }}" aria-valuemin="0" aria-valuemax="100"></div>
        </div>
        <small class="text-white">Keep spreading the word.</small>
        </div>
    </div>
</div>
<div class="row" >
    <div class="col-md-1"></div>
    <div class="card text-white bg-orange col-md-3">
        <div class="card-body">
        <div class="text-value">{!! $orders_pending !!}</div>
        <div>Pending Orders </div>
        <div class="progress progress-white progress-xs my-2">
            <div class="progress-bar" role="progressbar" style="width: {{ $orders_pending }}%" aria-valuenow="{{ $orders_pending }}" aria-valuemin="0" aria-valuemax="100"></div>
        </div>
        <small class="text-white">Deliver on time.</small>
        </div>
    </div>

    <div class="card text-white bg-success col-md-3" style="margin-right:2%; margin-left:2%;">
        <div class="card-body">
        <div class="text-value">{!! $orders_completed !!}</div>
        <div>Completed Orders</div>
        <div class="progress progress-white progress-xs my-2">
            <div class="progress-bar" role="progressbar" style="width: {{ $orders_completed }}%" aria-valuenow="{{ $orders_completed }}" aria-valuemin="0" aria-valuemax="100"></div>
        </div>
        <small class="text-white">Keep it up.</small>
        </div>
    </div>
    <div class="card text-white bg-danger col-md-3">
        <div class="card-body">
        <div class="text-value">{!! $orders_cancelled !!}</div>
        <div>Cancelled Orders</div>
        <div class="progress progress-white progress-xs my-2">
            <div class="progress-bar" role="progressbar" style="width: {{ $orders_cancelled }}%" aria-valuenow="{{ $orders_cancelled }}" aria-valuemin="0" aria-valuemax="100"></div>
        </div>
        <small class="text-white">Better luck next time.</small>
        </div>
    </div>
</div>

                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
                        <div class="card bg-teal-400">
                            <a href="{{ route('users.index') }}" class="text-white">
                                <div class="text-center card-body">
                                    <div>
                                        <h1 class="font-weight-semibold mb-0"><i class="icon-3x mr-1 icon-users"></i>
                                        </h1>
                                    </div>
                                    <div class="pt-2">
                                        <h1 class="font-weight-semibold mb-0">Clients</h1>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
                        <div class="card bg-teal-400">
                            <a href="{{ route('oils.index') }}" class="text-white">
                                <div class="text-center card-body">
                                    <div>
                                        <h1 class="font-weight-semibold mb-0"><i class="icon-3x mr-1 icon-car"></i>
                                        </h1>
                                    </div>
                                    <div class="pt-2">
                                        <h1 class="font-weight-semibold mb-0">Oils</h1>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
                        <div class="card bg-teal-400">
                            <a href="{{ route('workshops.index') }}" class="text-white">
                                <div class="text-center card-body">
                                    <div>
                                        <h1 class="font-weight-semibold mb-0"><i class="icon-3x mr-1 icon-users"></i>
                                        </h1>
                                    </div>
                                    <div class="pt-2">
                                        <h1 class="font-weight-semibold mb-0">Workshop</h1>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
                        <div class="card bg-teal-400">
                            <a href="{{ route('orders.index') }}" class="text-white">
                                <div class="text-center card-body">
                                    <div>
                                        <h1 class="font-weight-semibold mb-0"><i class="icon-3x mr-1 icon-cart"></i>
                                        </h1>
                                    </div>
                                    <div class="pt-2">
                                        <h1 class="font-weight-semibold mb-0">Orders</h1>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
                        <div class="card bg-teal-400">
                            <a href="{{ route('services.index') }}" class="text-white">
                                <div class="text-center card-body">
                                    <div>
                                        <h1 class="font-weight-semibold mb-0"><i class="icon-3x mr-1 icon-copy"></i>
                                        </h1>
                                    </div>
                                    <div class="pt-2">
                                        <h1 class="font-weight-semibold mb-0">Services</h1>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
                        <div class="card bg-teal-400">
                            <a href="{{ route('cars.index') }}" class="text-white">
                                <div class="text-center card-body">
                                    <div>
                                        <h1 class="font-weight-semibold mb-0"><i class="icon-3x mr-1 icon-car"></i>
                                        </h1>
                                    </div>
                                    <div class="pt-2">
                                        <h1 class="font-weight-semibold mb-0">Makes</h1>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
                        <div class="card bg-teal-400">
                            <a href="{{ route('models.index') }}" class="text-white">
                                <div class="text-center card-body">
                                    <div>
                                        <h1 class="font-weight-semibold mb-0"><i class="icon-3x mr-1 icon-car"></i>
                                        </h1>
                                    </div>
                                    <div class="pt-2">
                                        <h1 class="font-weight-semibold mb-0">Models</h1>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
                        <div class="card bg-teal-400">
                            <a href="{{ route('bookings.index') }}" class="text-white">
                                <div class="text-center card-body">
                                    <div>
                                        <h1 class="font-weight-semibold mb-0"><i class="icon-3x mr-1 icon-cart"></i>
                                        </h1>
                                    </div>
                                    <div class="pt-2">
                                        <h1 class="font-weight-semibold mb-0">Bookings</h1>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
                        <div class="card bg-teal-400">
                            <a href="{{ route('emergencies-towing.index') }}" class="text-white">
                                <div class="text-center card-body">
                                    <div>
                                        <h1 class="font-weight-semibold mb-0"><i class="icon-3x mr-1 icon-phone"></i>
                                        </h1>
                                    </div>
                                    <div class="pt-2">
                                        <h1 class="font-weight-semibold mb-0">Emergency Towing</h1>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
                        <div class="card bg-teal-400">
                            <a href="{{ route('reviews.index') }}" class="text-white">
                                <div class="text-center card-body">
                                    <div>
                                        <h1 class="font-weight-semibold mb-0"><i class="icon-3x mr-1 icon-stars"></i>
                                        </h1>
                                    </div>
                                    <div class="pt-2">
                                        <h1 class="font-weight-semibold mb-0">Reviews</h1>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
                        <div class="card bg-teal-400">
                            <a href="{{ route('complaints') }}" class="text-white">
                                <div class="text-center card-body">
                                    <div>
                                        <h1 class="font-weight-semibold mb-0"><i class="icon-3x mr-1 icon-phone"></i>
                                        </h1>
                                    </div>
                                    <div class="pt-2">
                                        <h1 class="font-weight-semibold mb-0">Complaints</h1>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
                        <div class="card bg-teal-400">
                            <a href="{{ route('general-settings.edit',[1]) }}" class="text-white">
                                <div class="text-center card-body">
                                    <div>
                                        <h1 class="font-weight-semibold mb-0"><i class="icon-3x mr-1 icon-gear"></i>
                                        </h1>
                                    </div>
                                    <div class="pt-2">
                                        <h1 class="font-weight-semibold mb-0">General Settings</h1>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <!-- /Stats boxes -->
                </div>
            </div>
            @endif
            <!-- /dashboard content -->
        </div>
        <!-- /content area -->
    </div>
@endsection
