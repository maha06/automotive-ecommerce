@extends('admin.layouts.layout')

@section('heading')
    <h4>
        <a href="{{ route('admin.pages.home') }}">
            <i class="icon-arrow-left52 mr-2"></i>
        </a>
        <span class="font-weight-semibold">Home - Bookings</span> - View All
    </h4>
@endsection

@section('breadcrumbs')
<div class="breadcrumb">
    <a href="{{ route('admin.pages.home') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
    <span class="breadcrumb-item"><a href="{{ route('bookings.index') }}"> Bookings </a></span>
    <span class="breadcrumb-item active">View All</span>
</div>
@endsection

@section('content')
@include('admin.partials.header')
    <div class="content">
        @include('common.partials.flash')
        <div class="card has-table">
            <div class="card-header header-elements-inline">
                <h5 class="card-title">Bookings</h5>
            </div>
            <table class="table datatable-basic">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Customer Name</th>
                        <th>Workshop Name</th>
                        <th>Status</th>
                        <th>Created At</th>
                        <th class="text-center">Actions</th>
                    </tr>
                </thead>
                <tbody>
                @forelse($bookings as $key=> $booking)
                    <tr>
                        <td>{{$key+1}}</td>
                        <td>{{$booking->customer_name }}</td>
                        <td>{{$booking->workshop->name }}</td>
                        <td>
                        @if ($booking->status == 0)
                            <span class="badge badge-primary">Pending</span>
                            @elseif ($booking->status == 1)
                            <span class="badge badge-success"> Completed </span>
                            @elseif ($booking->status == 2)
                            <span class="badge badge-danger"> Cancelled </span>
                            @endif
                        </td>
                        <td>{{$booking->created_at }}</td>
                        <td class="text-center">
                            <div class="list-icons">
                                <div class="dropdown">
                                    <a href="#" class="list-icons-item" data-toggle="dropdown">
                                        <i class="icon-menu9"></i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        <a href="{{ route('bookings.show', [$booking->id]) }}" class="dropdown-item">
                                            <i class="icon-eye"></i>
                                            Show
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                @empty
                <tr>
                    <td colspan="12">
                        <div class="alert alert-info text-center">
                            No Bookings Recieved So Far
                            <br>
                        </div>
                    </td>
                </tr>
                @endforelse
                </tbody>
            </table>
            {!! $bookings->links() !!}
        </div>
    </div>
@endsection

@push('scripts')
    <script src="{{asset('backend/js/plugins/tables/datatables/datatables.min.js')}}"></script>
    <script src="{{asset('backend/js/demo_pages/datatables_basic.js')}}"></script>
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
@endpush
@push('scripts')
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <script>
        $('.activeInactive').change(function(is_active){
            var booking_id = this.getAttribute('data-id');
            var url = window.location.pathname.split("/")[1] === "staging"
                            ? "/staging/workshop/booking/" + booking_id + "/set-status/" + is_active.target.value
                            : "" + "/workshop/booking/" + booking_id + "/set-status/" + is_active.target.value;

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                method: 'get',
                url: url,
                dataType: 'json',
                success:function(data){
                    if(data == 'done')
                    {
                    $('#is_active').bootstrapToggle('on');
                    alert("Data Inserted");
                    }
                }
            });
        });
    </script>
@endpush
