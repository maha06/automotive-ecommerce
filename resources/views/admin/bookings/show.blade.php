@extends('admin.layouts.layout')

@section('heading')
    <h4>
        <a href="{{ route('admin.pages.home') }}">
            <i class="icon-arrow-left52 mr-2"></i>
        </a>
        <span class="font-weight-semibold">Home - Booking</span> - Show
    </h4>
@endsection

@section('breadcrumbs')
<div class="breadcrumb">
    <a href="{{ route('admin.pages.home') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
    <span class="breadcrumb-item"><a href="{{ route('bookings.index') }}"> Booking </a></span>
</div>
@endsection

@section('content')
@include('admin.partials.header')
    <div class="content">
        @include('common.partials.flash')
        <div class="card has-table" style="padding:2%">
            <form action="{{ route('booking.status') }}" method="POST">
                @csrf
                <div class="row">
                    <div class="col-md-6"> </div>
                    <div class="col-md-4">
                        <select class="form-control" name="status">
                            <option value='{{$booking->status}}'>{{$status}}</option>
                            <option style="font-size: 1pt; background-color: #000000;" disabled>&nbsp;</option>
                            <option value='0'>Pending</option>
                            <option value='1'>Completed</option>
                            <option value='2'>Cancelled</option>
                        </select>
                    </div>
                    <input type="hidden" name='id' value='{{$booking->id}}'>
                    <div class="col-md-2">
                        <button type="submit" class="btn btn-info">Update</button>
                    </div>
                </div>
            </form>
        <div class="row" style="margin-top:2%">
                <div class="col-md-12">
                    <div class="card border-teal-400">
                        <div class="card-header bg-teal-400 text-white header-elements-inline">
                            <h6 class="card-title">Customer Details</h6>
                        </div>

                        <div class="card-body">
                                <li>
                                    <span style="font-weight:bold"> Name:  </span>{{ $booking->customer_name }}
                                </li><li>
                                    <span style="font-weight:bold"> Email:  </span>{{ $booking->customer_email }}
                                </li><li>
                                    <span style="font-weight:bold"> Contact:  </span>{{ $booking->mobile }}
                                </li><li>
                                    <span style="font-weight:bold"> City:  </span>{{ $booking->city }}
                                </li>
@if( $booking->make)
                                <li>
                                    <span style="font-weight:bold"> Car:  </span>{{  $booking->make }}
                                </li>
@endif
@if( $booking->model)
                                <li>
                                    <span style="font-weight:bold"> Model:  </span>{{  $booking->model }}
                                </li>
@endif
@if( $booking->mileage)
                                <li>
                                    <span style="font-weight:bold"> Mileage:  </span>{{  $booking->mileage }}
                                </li>
@endif
@if( $booking->registration_number)
                                <li>
                                    <span style="font-weight:bold"> Registration Number:  </span>{{  $booking->registration_number }}
                                </li>
@endif

                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                <fieldset class="mb-3 mt-3 ml-2">
                    <legend class="text-uppercase font-size-sm font-weight-bold mb-0" style="font-size:18px;"></legend>
                </fieldset>
                </div>
                <div class="col-md-12">
                    <div class="card border-teal-400">
                        <div class="card-header bg-teal-400 text-white header-elements-inline">
                            <h6 class="card-title">Appoitment Details</h6>
                        </div>

                        <div class="card-body">
                                <li>
                                    <span style="font-weight:bold"> Workshop:  </span>{{ $workshop->name }}
                                </li>
                                <li>
                                    <span style="font-weight:bold"> Appoitment Date:  </span>{{ $booking->available_date }}
                                </li><li>
                                    <span style="font-weight:bold"> Appoitment Time:  </span>{{ $booking->available_time }}
                                </li>
@if( $booking->pickup)
                                <li>
                                    <span style="font-weight:bold"> Pickup:  </span>{{  $booking->pickup }}
                                </li>
@endif
@if( $booking->pickup_time)
                                <li>
                                    <span style="font-weight:bold"> Pickup Time:  </span>{{  $booking->pickup_time }}
                                </li>
@endif
@if( $booking->pickup_address)
                                <li>
                                    <span style="font-weight:bold"> Pickup Address:  </span>{{  $booking->pickup_address }}
                                </li>
@endif
@if( $booking->pickup_contact)
                                <li>
                                    <span style="font-weight:bold"> Pickup Contact:  </span>{{  $booking->pickup_contact }}
                                </li>
@endif

                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <fieldset class="mb-3 mt-3 ml-2">
                        <legend class="text-uppercase font-size-sm font-weight-bold mb-0" style="font-size:18px;">Services Details</legend>
                    </fieldset>
                </div>
                <div class="col-md-12">
                    <table class="table datatable-pagination">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Price</th>
                            </tr>
                        </thead>
                        {{$booking->engine_id}}
                        <tbody>
                        @foreach($booking->services as $key=> $service)
                            <tr>
                                <td>{{$key+1}}</td>
                                <td>{{$service->name }}</td>
                                @php
                                    $labour = \App\ServicesLabours::where([ 'labour_id'=> $booking->engine_id, 'service_id' => $service->id ])->get();
                                @endphp
                                @if( ! $labour->isEmpty() )
                                <td>{{$labour[0]->price }}</td>
                                @else <td>0</td>
                                @endif
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <fieldset class="mb-3 mt-3 ml-2">
                        <legend class="text-uppercase font-size-sm font-weight-bold mb-0" style="font-size:18px;">Parts Details</legend>
                    </fieldset>
                </div>
                <div class="col-md-12">
                    <table class="table datatable-pagination">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Description</th>
                                <th>Company</th>
                                <th>Price</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($booking->posts as $key=> $post)
                            <tr>
                                <td>{{$key+1}}</td>
                                <td>{{$post->name }}</td>
                                <td>{{$post->description }}</td>
                                <td>{{$post->company }}</td>
                                <td>{{$post->retail_price }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
