@extends('admin.layouts.layout')

@section('heading')
    <h4>
        <a href="{{ route('admin.pages.home') }}">
            <i class="icon-arrow-left52 mr-2"></i>
        </a>
        <span class="font-weight-semibold">Home - Bookings</span>
    </h4>
@endsection

@section('breadcrumbs')
    <div class="breadcrumb">
        <a href="{{ route('bookings.index') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
        <span class="breadcrumb-item">Booking</span>
        <span class="breadcrumb-item active">Create</span>
    </div>
@endsection

@section('content')
    @include('admin.partials.header')
    <div class="content">
        @include('common.partials.flash')

        <div class="card">
            <div class="card-header header-elements-inline">
                <h5 class="card-title">Bookings</h5>
            </div>

            <div class="card-body">
                <form action="{{ route('bookings.store') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label class="font-weight-semibold @error('added_by') text-danger @enderror">Customers
                            <span class="text-red">*</span>
                            </label>
                            <div class="form-group-feedback form-group-feedback-right">
                            <select name="user_id"
                                    class="form-control multiselect-select-all-filtering @error('added_by') text-danger @enderror"
                                    >

                                @foreach($users as $user)
                                    <option value="{{ $user->id}}">{{  $user->name  }}</option>

                                @endforeach
                            </select>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="font-weight-semibold @error('city') text-danger @enderror">City <span
                                    class="text-red">*</span></label>
                            <div class="form-group-feedback form-group-feedback-right">
                                <input type="text" name="city" value="{{ old('city') }}"
                                    class="form-control @error('city') border-danger @enderror" required>
                            </div>
                        </div>
                        <div class="form-group col-md-3">
                            <label class="font-weight-semibold @error('make') text-danger @enderror">Car</label>
                            <div class="form-group-feedback form-group-feedback-right">
                                <select class="form-control" name="make" >
                                @foreach($makes as $l)
                                    <option value="{{$l->name}}" >{{$l->name}} </option>
                                @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group col-md-3">
                            <label class="font-weight-semibold @error('model') text-danger @enderror">Model</label>
                            <div class="form-group-feedback form-group-feedback-right">
                                <input type="text" name="model" value="{{ old('model') }}"
                                    class="form-control @error('model') border-danger @enderror" >
                            </div>
                        </div>
                        <div class="form-group col-md-3">
                            <label class="font-weight-semibold @error('mileage') text-danger @enderror">Mileage</label>
                            <div class="form-group-feedback form-group-feedback-right">
                                <input type="text" name="mileage" value="{{ old('mileage') }}"
                                    class="form-control @error('mileage') border-danger @enderror" >
                            </div>
                        </div>
                        <div class="form-group col-md-3">
                            <label class="font-weight-semibold @error('registration_number') text-danger @enderror">Registration Number</label>
                            <div class="form-group-feedback form-group-feedback-right">
                                <input type="text" name="registration_number" value="{{ old('registration_number') }}"
                                    class="form-control @error('registration_number') border-danger @enderror" >
                            </div>
                        </div>
                    </div><hr>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label class="font-weight-semibold @error('engine_id') text-danger @enderror">Engine Type<span
                                    class="text-red">*</span></label>
                            <div class="form-group-feedback form-group-feedback-right">
                                <select class="form-control" name="engine_id" >
                                @foreach($labours as $l)
                                    <option value="{{$l->id}}" >{{$l->labours}} </option>
                                @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="font-weight-semibold @error('address') text-danger @enderror">Workshop<span
                                    class="text-red">*</span></label>
                            <div class="form-group-feedback form-group-feedback-right">
                            <select class="form-control" name="workshop" >
                                <option value=''>--select--</option>
                                @foreach($workshops as $l)
                                    <option value="{{$l->id}}" >{{$l->name}} </option>
                                @endforeach
                                </select>
                            </div>
                        </div>
                    </div>

                    <div id="servicesform">
                    <label class="font-weight-semibold" >Select services <span
                                    class="text-red">*</span></label>
                    <input type="button" value="+" id="add" style="margin:1%" class="btn btn-xs btn-info"/>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                <span class="input-group-text">Service</span>
                                </div>

                                <select class="form-control" name="services[]" >
                                <option value=''>--select--</option>
                                @foreach($services as $l)
                                    <option value="{{$l->id}}" >{{$l->name}} </option>
                                @endforeach
                                </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label class="font-weight-semibold @error('date') text-danger @enderror">Scheduled Date<span
                                    class="text-red">*</span></label>
                            <div class="form-group-feedback form-group-feedback-right">
                                <input type="date" name="date" value="{{ old('date') }}"
                                    class="form-control @error('date') border-danger @enderror" required >
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="font-weight-semibold @error('time') text-danger @enderror">Scheduled Time<span
                                    class="text-red">*</span></label>
                            <div class="form-group-feedback form-group-feedback-right">
                                <input type="time" name="time" value="{{ old('time') }}"
                                    class="form-control @error('time') border-danger @enderror" required>
                            </div>
                        </div>
                    </div>
<hr>
                    <div class="row">
                        <div class="form-group col-md-2">
                            <label class="font-weight-semibold @error('date') text-danger @enderror">Car Pickup</label>
                            <div class="form-group-feedback form-group-feedback-right">
                                <input type="radio" name="pickup" value="{{ old('pickup') }}">
                            </div>
                        </div>

                        <div class="form-group col-md-5">
                            <label class="font-weight-semibold @error('pickup_time') text-danger @enderror">Pickup Time</label>
                            <div class="form-group-feedback form-group-feedback-right">
                                <input type="time" name="pickup_time" value="{{ old('pickup_time') }}"
                                    class="form-control @error('pickup_time') border-danger @enderror" >
                            </div>
                        </div>
                        <div class="form-group col-md-5">
                            <label class="font-weight-semibold @error('pickup_address') text-danger @enderror">Address</label>
                            <div class="form-group-feedback form-group-feedback-right">
                                <input type="text" name="pickup_address" value="{{ old('pickup_address') }}"
                                    class="form-control @error('pickup_address') border-danger @enderror" >
                            </div>
                        </div>
                    </div>
<hr>
                    <div id="partform">
                        <label class="font-weight-semibold" >Add Parts </label>
                        <input type="button" value="+" id="part-add" style="margin:1%" class="btn btn-xs btn-info"/>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                <span class="input-group-text">Item</span>
                                </div>

                                <select class="form-control" name="parts[]" >
                                <option value="">--select--</option>
                                @foreach($parts as $l)
                                    <option value="{{$l->id}}" >{{$l->name}} [{{$l->quantity}} items]</option>
                                @endforeach
                                </select>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label class="font-weight-semibold @error('parts_deliver') text-danger @enderror">Where do you want parts to get delivered?</label>
                            <div class="form-group-feedback form-group-feedback-right">
                            <select class="form-control" name="parts_deliver" >
                                <option value=''>--select--</option>
                                <option value='At Home'>At Home</option>
                                <option value='Workshop'>Workshop</option>
                            </select>
                            </div>
                        </div>
                    </div>

                    <div class="text-right">
                        <button type="submit" class="btn btn-primary">Submit <i class="icon-paperplane ml-2"></i></button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script src="//cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>

<script type="text/javascript">
$(document).ready(function () {

    $("#add").click(function() {

        HTML = [];

        HTML = "<div class=\"row\"><div class=\"col-sm-6\"><div class=\"input-group mb-2\"> <div class=\"input-group-prepend\"><span class=\"input-group-text\">Service</span></div><select class=\"form-control\" id=\"language\" name='services[]' >";

        <?php foreach ($services as $l) {?>
            HTML += "<option value=<?php echo $l->id; ?> > <?php echo $l->name; ?>  </option>";
        <?php }?>
        HTML += "</select></div></div><div class=\"col-sm-2\"><input type=\"button\" value=\"-\" id=\"remove\" style=\"margin:2%\" class=\"btn btn-xs btn-danger\" /></div></div>";
        $('#servicesform').append(HTML);

    });


    $("#part-add").click(function() {

        HTML = [];

        HTML = "<div class=\"row\"><div class=\"col-sm-6\"><div class=\"input-group mb-2\"> <div class=\"input-group-prepend\"><span class=\"input-group-text\">Item</span></div><select class=\"form-control\" name='parts[]' >";

        <?php foreach ($parts as $l) {?>
            HTML += "<option value=<?php echo $l->id; ?> > <?php echo $l->name; ?> [<?php echo $l->quantity; ?> items] </option>";
        <?php }?>
        HTML += "</select></div></div><div class=\"col-sm-4\"><input type=\"button\" value=\"-\" id=\"remove\" style=\"margin:2%\" class=\"btn btn-xs btn-danger\" /></div></div>";
        $('#partform').append(HTML);

    });

    $(document).on('click', '#remove', function(e){
        $(this).parent().parent().remove();
    });
});

</script>
@endsection
