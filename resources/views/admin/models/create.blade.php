@extends('admin.layouts.layout')

@section('heading')
    <h4><a href="{{ route('admin.pages.home') }}"><i class="icon-arrow-left52 mr-2"></i></a> <span class="font-weight-semibold">Home - Model</span> - Create</h4>
@endsection

@section('breadcrumbs')
    <div class="breadcrumb">
        <a href="{{ route('admin.pages.home') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
        <span class="breadcrumb-item">Model</span>
        <span class="breadcrumb-item active">Create</span>
    </div>
@endsection

@section('content')
    @include('admin.partials.header')
    <div class="content">
        @include('common.partials.flash')

        <div class="card">
            <div class="card-header header-elements-inline">
                <h5 class="card-title">Create Model </h5>
            </div>

            <div class="card-body">
                <form action="{{ route('models.store') }}" method="POST">
                    @csrf
                    <fieldset class="mb-3">
                        <legend class="text-uppercase font-size-sm font-weight-bold">Fill the form below to create a new model</legend>
                    </fieldset>
                    <div class="form-group">
                        <label class="font-weight-semibold @error('name') text-danger @enderror">Name</label>
                        <div class="form-group-feedback form-group-feedback-right">
                            <input type="text" name="name" value="{{ old('name') }}" class="form-control @error('name') border-danger @enderror" required>
                            @error('name')
                            <div class="form-control-feedback text-danger">
                                <i class="icon-cancel-circle2"></i>
                            </div>
                            @enderror
                        </div>
                        @error('name')
                        <span class="form-text text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label class="font-weight-semibold @error('car_id') text-danger @enderror">Car</label>
                        <div class="form-group-feedback form-group-feedback-right">
                            <select name="car_id" class="form-control form-control-select2 @error('car_id') border-danger @enderror" required>
                                <option value="">Select Car</option>
                                @forelse ($cars as $car)
                                    <option value="{{ $car->id }}">{{ $car->name }}</option>
                                @empty
                                    <option value="">No Car Found</option>
                                @endforelse
                            </select>
                            @error('car_id')
                            <div class="form-control-feedback text-danger">
                                <i class="icon-cancel-circle2"></i>
                            </div>
                            @enderror
                        </div>
                        @error('car_id')
                        <span class="form-text text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="text-right">
                        <button type="submit" class="btn btn-primary">Submit <i class="icon-paperplane ml-2"></i></button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
