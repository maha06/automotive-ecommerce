<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns:v="urn:schemas-microsoft-com:vml" style="width: 100%;">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;">
    <!--[if !mso]--><!-- -->
    <link href="https://fonts.googleapis.com/css?family=Work+Sans:300,400,500,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Quicksand:300,400,700" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('frontend/font/demo-files/demo.css') }}">

  <!-- CSS theme files
  ============================================ -->
  <link rel="stylesheet" href="{{ asset('frontend/css/bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ asset('frontend/css/fontello.css') }}">
  <link rel="stylesheet" href="{{ asset('frontend/css/owl.carousel.css') }}">
  <link rel="stylesheet" href="{{ asset('frontend/css/style.css') }}">
  <link rel="stylesheet" href="{{ asset('frontend/css/responsive.css') }}">
    <!-- <![endif]-->

    <title>Welcome to Mechon</title>

    <style type="text/css">
        body {
            width: 100%;
            background-color: #ffffff;
            margin: 0;
            padding: 0;
            -webkit-font-smoothing: antialiased;
            mso-margin-top-alt: 0px;
            mso-margin-bottom-alt: 0px;
            mso-padding-alt: 0px 0px 0px 0px;
        }

        p,
        h1,
        h2,
        h3,
        h4 {
            margin-top: 0;
            margin-bottom: 0;
            padding-top: 0;
            padding-bottom: 0;
        }

        span.preheader {
            display: none;
            font-size: 1px;
        }

        html {
            width: 100%;
        }

        table {
            font-size: 14px;
            border: 0;
        }

        /* ----------- responsivity ----------- */

        @media  only screen and (max-width: 640px) {
            /*------ top header ------ */
            .main-header {
                font-size: 20px !important;
            }

            .main-section-header {
                font-size: 28px !important;
            }

            .show {
                display: block !important;
            }

            .hide {
                display: none !important;
            }

            .align-center {
                text-align: center !important;
            }

            .no-bg {
                background: none !important;
            }

            /*----- main image -------*/
            .main-image img {
                width: 440px !important;
                height: auto !important;
            }

            /* ====== divider ====== */
            .divider img {
                width: 440px !important;
            }

            /*-------- container --------*/
            .container590 {
                width: 440px !important;
            }

            .container580 {
                width: 400px !important;
            }

            .main-button {
                width: 220px !important;
            }

            /*-------- secions ----------*/
            .section-img img {
                width: 320px !important;
                height: auto !important;
            }

            .team-img img {
                width: 100% !important;
                height: auto !important;
            }
        }

        @media  only screen and (max-width: 479px) {
            /*------ top header ------ */
            .main-header {
                font-size: 18px !important;
            }

            .main-section-header {
                font-size: 26px !important;
            }

            /* ====== divider ====== */
            .divider img {
                width: 280px !important;
            }

            /*-------- container --------*/
            .container590 {
                width: 280px !important;
            }

            .container590 {
                width: 280px !important;
            }

            .container580 {
                width: 260px !important;
            }

            /*-------- secions ----------*/
            .section-img img {
                width: 280px !important;
                height: auto !important;
            }
        }




        .form-container {
            border: 2px solid #d51920;
            padding: 20px;
        }
        @media (min-width: 1200px)
        {
            .container {
                width: 1170px;
            }
        }
        @media (min-width: 992px)
        {
            .container {
                width: 970px;
            }
        }
        @media (min-width: 769px)
        {
            .container {
                width: 750px;
            }
        }
        .container {
            margin-right: auto;
            margin-left: auto;
            padding-left: 15px;
            padding-right: 15px;
        }
        div, span, object, iframe, h1, h2, h3, h4, h5, h6, p, blockquote, pre, abbr, address, cite, code, del, dfn, em, img, ins, kbd, q, samp, small, strong, sub, sup, var, b, i, dl, dt, dd, ol, ul, li, fieldset, form, label, legend, table, caption, tbody, tfoot, thead, tr, th, td, article, aside, canvas, details, figcaption, figure, footer, header, hgroup, menu, nav, section, summary, time, mark, audio, video {
            margin: 0;
            padding: 0;
            border: 0;
            outline: 0;
            font-size: 100%;
            vertical-align: baseline;
            background: transparent;
            -webkit-font-smoothing: antialiased;
            -moz-osx-font-smoothing: grayscale;
        }
        .row {
            margin-left: -15px;
            margin-right: -15px;
        }
        @media (min-width: 769px)
        {
            .col-sm-12 {
                width: 100%;
            }
        }
        @media (min-width: 769px)
        {
            .col-sm-1, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9, .col-sm-10, .col-sm-11, .col-sm-12 {
                float: left;
            }

        }
        .col-xs-1, .col-sm-1, .col-md-1, .col-lg-1, .col-xs-2, .col-sm-2, .col-md-2, .col-lg-2, .col-xs-3, .col-sm-3, .col-md-3, .col-lg-3, .col-xs-4, .col-sm-4, .col-md-4, .col-lg-4, .col-xs-5, .col-sm-5, .col-md-5, .col-lg-5, .col-xs-6, .col-sm-6, .col-md-6, .col-lg-6, .col-xs-7, .col-sm-7, .col-md-7, .col-lg-7, .col-xs-8, .col-sm-8, .col-md-8, .col-lg-8, .col-xs-9, .col-sm-9, .col-md-9, .col-lg-9, .col-xs-10, .col-sm-10, .col-md-10, .col-lg-10, .col-xs-11, .col-sm-11, .col-md-11, .col-lg-11, .col-xs-12, .col-sm-12, .col-md-12, .col-lg-12 {
            position: relative;
            min-height: 1px;
            padding-left: 15px;
            padding-right: 15px;
        }
        .content-element4:not(:last-child) {
            margin-bottom: 90px;
        }
        .table-type-2 {
            border-radius: 5px 5px 3px 3px;
        }

        [class*="table-type"] {
            border-radius: 3px;
            overflow: hidden;
        }
        table {
            width: 100%;
            table-layout: fixed;
            border-collapse: collapse;
        }

        table, table td {
            padding: 0;
            border: none;
            border-collapse: collapse;
            border-spacing: 0;
        }
        [class*="table-type"] table tr > th, [class*="table-type"] table tr.bg-cell > td {
            color: white;
            font-size: 16px;
            font-weight: normal;
        }

        .table-type-2 table tr > td, .table-type-2 table tr > th {
            padding: 12px 20px;
            text-align: left;
        }
        /* [class*="table-type"] table tr > th, [class*="table-type"] table tr.bg-cell > td {
            color: #31353c;
            font-size: 16px;
            font-weight: normal;
        } */
        /* [class*="table-type"] table tr > td, [class*="table-type"] table tr > th {
            padding: 13px 25px;
            text-align: left;
        } */
        .table-type-1 tr:first-child > th, .table-type-2 tr > th {
            background: #d51920;
            /* color: white; */
        }
        .table-type-2 tr > th {
            border: 2px solid #e2e5e5;
        }
        .table-type-1 tr:first-child > th, .table-type-2 tr > th {
            /* background: #e2e5e5; */
        }
        [class*="table-type"] table tr > td:last-child {
            border-right: 2px solid #d51920;
        }
        [class*="table-type"] table tr > td:last-child {
            border-right: 4px solid #e2e5e5;
        }
        [class*="table-type-2"] table tr > td {
            color: black;
        }
        [class*="table-type"] table tr > td {
            border-top: 2px solid #d51920;
        }
        .table-type-2 table tr > td, .table-type-2 table tr > th {
            padding: 12px 20px;
            text-align: left;
        }
        [class*="table-type"] table tr > td {
            border-top: 2px solid #e2e5e5;
        }
        [class*="table-type"] table tr > td, [class*="table-type"] table tr > th {
            padding: 13px 25px;
            text-align: left;
        }
        .table-type-2 tr > td {
            border-width: 2px!important;
        }
        </style>
</head>


<body class="respond" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="-webkit-font-smoothing: antialiased; mso-margin-top-alt: 0px; mso-margin-bottom-alt: 0px; mso-padding-alt: 0px 0px 0px 0px; box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; -webkit-text-size-adjust: none; background-color: #ffffff; color: #718096; height: 100%; line-height: 1.4; margin: 0; padding: 0; width: 100% !important;">
<!-- pre-header -->
<table style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; font-size: 14px; border: 0; display: none!important;">
    <tr>
        <td style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative;">
            <div style="box-sizing: border-box; position: relative; overflow: hidden; display: none; font-size: 1px; color: #ffffff; line-height: 1px; font-family: Arial; maxheight: 0px; max-width: 0px; opacity: 0;">
                Pre-header for the newsletter template
            </div>
        </td>
    </tr>
</table>
<!-- pre-header end -->
<!-- header -->
<table border="0" width="100%" cellpadding="0" cellspacing="0" bgcolor="ffffff" style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; font-size: 14px; border: 0;">

    <tr>
        <td align="center" style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative;">
            <table border="0" align="center" width="590" cellpadding="0" cellspacing="0" class="container590" style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; font-size: 14px; border: 0;">

                <tr>
                    <td height="25" style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; font-size: 25px; line-height: 25px;"> </td>
                </tr>

                <tr>
                    <td align="center" style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative;">

                        <table border="0" align="center" width="590" cellpadding="0" cellspacing="0" class="container590" style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; font-size: 14px; border: 0;">

                            <tr>
                                <td align="center" height="70" style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; height: 70px;">
                                    <a href="" style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; color: #3869d4; display: block; border-style: none !important; border: 0 !important;"><img width="100" border="0" style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; max-width: 100%; border: none; display: block; width: 200px;" src="{{ asset('/frontend/images/logo.png') }}" alt=""></a>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<!-- end header -->

<!-- big image section -->
<table border="0" width="100%" cellpadding="0" cellspacing="0" bgcolor="ffffff" class="bg_color" style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; font-size: 14px; border: 0;">

    <tr>
        <td align="center" style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative;">
            <table border="0" align="center" width="590" cellpadding="0" cellspacing="0" class="container590" style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; font-size: 14px; border: 0;">
                <tr>
                    <td height="20" style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; font-size: 20px; line-height: 20px;"> </td>
                </tr>
                <tr>
                    <td align="center" style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative;">
                        <table border="0" width="500" align="center" cellpadding="0" cellspacing="0" class="container590" style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; font-size: 14px; border: 0;">
                            <tr>
                                <td style="box-sizing: border-box; position: relative; color: #888888; font-size: 16px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;">


                                    <div style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; line-height: 24px;">
                                        <h1 style="text-align:center !important;box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; padding-top: 0; padding-bottom: 0; font-size: 18px; font-weight: bold; margin-top: 0; color: black; margin-bottom: 20px;font-size:20px;">Hello! Your maintenance request has been Completed</h1>
                                    </div>

                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="center" style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative;">
                        <div class="container">
                            <div class="content-element3">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="content-element4">
                                            <div class="row">
                                                <h3 style="    font-size: 30px !important;
                                                color: black;
                                                margin-bottom: 20px !important; text-align: center;margin-top:20px;"><b>Customer Details</b></h3>
                                                <div class="col-sm-12">
                                                    <div class="table-type-2">
                                                      <table>
                                                        <tbody>
                                                          <tr>
                                                            <th>Name</th>
                                                            <td>{{ $booking->customer_name }}</td>
                                                          </tr>
                                                          <tr>
                                                            <th>Email</th>
                                                            <td>{{ $booking->customer_email }}</td>
                                                          </tr>
                                                          <tr>
                                                            <th>City</th>
                                                            <td>{{ $booking->city }}</td>
                                                          </tr>
                                                          <tr>
                                                            <th>Selected Workshop</th>
                                                            <td>{{ $booking->workshop->name }}</td>
                                                          </tr>
                                                          <tr>
                                                            <th>Mobile</th>
                                                            <td><b>{{ $booking->mobile }}</b></td>
                                                          </tr>
                                                        </tbody>
                                                      </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td height="20" style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; font-size: 20px; line-height: 20px;"> </td>
                </tr>
                <tr>
                    <td align="center" style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative;">
                        <div class="container">
                            <div class="content-element3">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="content-element4">
                                            <div class="row">
                                                <h3 style="    font-size: 30px !important;
                                                color: black;
                                                margin-bottom: 20px !important; text-align: center;margin-top:20px;"><b>Car Details</b></h3>
                                                <div class="col-sm-12">
                                                    <div class="table-type-2">
                                                      <table>
                                                        <tbody>
                                                          <tr>
                                                            <th>Make</th>
                                                            <td>{{ $booking->make }}</td>
                                                          </tr>
                                                          <tr>
                                                            <th>Model</th>
                                                            <td>{{ $booking->model }}</td>
                                                          </tr>
                                                          <tr>
                                                            <th>Mileage</th>
                                                            <td>{{ $booking->mileage }}</td>
                                                          </tr>
                                                          <tr>
                                                            <th>Registration Number</th>
                                                            <td><b>{{ $booking->registration_number }}</b></td>
                                                          </tr>
                                                        </tbody>
                                                      </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td height="20" style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; font-size: 20px; line-height: 20px;"> </td>
                </tr>
                <tr>
                    <td align="center" style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative;">
                        <div class="container">
                            <div class="content-element3">
                                <div class="row">
                                    <h3 style="    font-size: 30px !important;
                                    color: black;
                                    margin-bottom: 20px !important; text-align: center;margin-top:20px;"><b>Appoinment Details</b></h3>
                                    <div class="col-sm-12">
                                        <div class="table-type-2">
                                          <table>
                                            <tbody>
                                              <tr id="available-date">
                                                <th>Available Date</th>
                                                <td>{{ $booking->available_date }}</td>
                                              </tr>
                                              <tr id="available-time">
                                                <th>Available Time</th>
                                                <td>{{ $booking->available_time }}</td>
                                              </tr>
                                              <tr id="pickup-time">
                                                <th>Pickup Time</th>
                                                <td>{{ $booking->pickup_time }}</td>
                                              </tr>
                                              <tr id="pickup-address">
                                                <th>Pickup Address</th>
                                                <td><b>{{ $booking->pickup_address }}</b></td>
                                              </tr>
                                              <tr id="pickup-contact">
                                                <th>Pickup Contact</th>
                                                <td><b>{{ $booking->pickup_contact }}</b></td>
                                              </tr>
                                            </tbody>
                                          </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td height="20" style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; font-size: 20px; line-height: 20px;"> </td>
                </tr>
                <tr>
                    <td align="center" style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative;">
                        <div class="container">
                            <div class="content-element3">
                                <div class="row">
                                    <h3 style="    font-size: 30px !important;
                                    color: black;
                                    margin-bottom: 20px !important; text-align: center;margin-top:20px;"><b>Selected Services</b></h3>
                                    <div class="col-sm-12">
                                        <div class="table-type-1">
                                          <table id="confirm_services">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Service name</th>
                                                    <th>Service price</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($booking->services as $key => $service)
                                                    <tr>
                                                        <td style="color:black">{{ ++$key }}</td>
                                                        <td style="color:black">{{ $service->name }}</td>
                                                        <td style="color:black">{{ $service->price }}</td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                          </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td height="20" style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; font-size: 20px; line-height: 20px;"> </td>
                </tr>
                <tr>
                    <td align="center" style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative;">
                        <div class="container">
                            <div class="content-element3">
                                <div class="row">
                                    <h3 style="    font-size: 30px !important;
                                    color: black;
                                    margin-bottom: 20px !important; text-align: center;margin-top:20px;"><b>Selected Parts</b></h3>
                                    <div class="col-sm-12">
                                        <div class="table-type-1">
                                          <table id="confirm_parts">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Part name</th>
                                                    <th>Part price</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($booking->posts as $key => $post)
                                                    <tr>
                                                        <td style="color:black">{{ ++$key }}</td>
                                                        <td style="color:black">{{ $post->name }}</td>
                                                        <td style="color:black">{{ $post->price }}</td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                          </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td height="20" style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; font-size: 20px; line-height: 20px;"> </td>
                </tr>
                <tr>
                    <td align="center" style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative;">
                        <div class="container">
                            <div class="content-element3">
                                <div class="row">
                                    <h3 style="    font-size: 30px !important;
                                    color: black;
                                    margin-bottom: 20px !important; text-align: center;margin-top:20px;"><b>Total Cost</b></h3>
                                    <div class="col-sm-12">
                                        <div class="table-type-1">
                                          <table>
                                            <tbody>
                                              <tr>
                                                <th>Total Service Cost</th>
                                                <th>Total Parts Cost</th>
                                                <th>Total Cost</th>
                                              </tr>
                                              <tr class="total-cell">
                                                <td style="color:black">
                                                  <div>PKR {{ $booking->services->sum('price') }}</div>
                                                </td>
                                                <td style="color:black">PKR {{ $booking->posts->sum('price') }}</td>
                                                <td style="color:black">PKR {{ $booking->services->sum('price') +  $booking->posts->sum('price')  }}</td>
                                              </tr>
                                            </tbody>
                                          </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td height="20" style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; font-size: 20px; line-height: 20px;"> </td>
                </tr>
                <tr>
                    <td align="center" style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative;">
                                            <table border="0" width="400" align="center" cellpadding="0" cellspacing="0" class="container590" style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; font-size: 14px; border: 0;"><tbody style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative;"><tr>
                    <td align="center" style="box-sizing: border-box; position: relative; color: #888888; font-size: 16px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;">


                                                        <div style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; line-height: 24px; font-size: 26px; font-weight: bold; color: black; margin-bottom: 40px; margin-top: 30px;">

                                                            Thank you for contacting us !
                                                        </div>
                                                    </td>
                                                </tr></tbody></table>
                    </td>
                </tr>
                <tr>
                    <td align="center" style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative;">
                                <table border="0" align="center" width="590" cellpadding="0" cellspacing="0" class="container590 bg_color" style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; font-size: 14px; border: 0;"><tbody style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative;"><tr>
                    <td style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative;">
                                            <table border="0" width="300" align="left" cellpadding="0" cellspacing="0" style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; font-size: 14px; border: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;" class="container590">
                    <tbody style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative;">
<tr>
                    <!-- logo --><td align="left" style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative;">
                                                        <a target="_blank" rel="noopener noreferrer" href="" style="text-align: -webkit-center;box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; color: #3869d4; display: block; border-style: none !important; border: 0 !important;"><img width="80" border="0" style="width:20% !important;box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; max-width: 100%; border: none; display: block;" src="{{ asset('/frontend/images/logo.png') }}" alt=""></a>
                                                    </td>
                                                </tr>
                    <tr>
                    <td height="25" style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; font-size: 25px; line-height: 25px;"> </td>
                                                </tr>
                    <tr>
                    <td align="center" style="box-sizing: border-box; position: relative; color: #888888; font-size: 14px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 23px;" class="text_color">
                                                        <div style="box-sizing: border-box; position: relative; color: #333333; font-size: 14px; font-family: 'Work Sans', Calibri, sans-serif; font-weight: 600; mso-line-height-rule: exactly; line-height: 23px;">

                                                            Email us: <br><a target="_blank" rel="noopener noreferrer" href="mailto:contact@mechon.pk" style="box-sizing: border-box; position: relative; color: #888888; font-size: 14px; font-family: 'Hind Siliguri', Calibri, Sans-serif; font-weight: 400;">contact@mechon.pk</a>

                                                        </div>
                                                    </td>
                                                </tr>
                    </tbody>
</table>
                    <table border="0" width="2" align="left" cellpadding="0" cellspacing="0" style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; font-size: 14px; border: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;" class="container590"><tbody style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative;"><tr>
                    <td width="2" height="10" style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; font-size: 10px; line-height: 10px;"></td>
                                                </tr></tbody></table>
                    <table border="0" width="200" align="right" cellpadding="0" cellspacing="0" style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; font-size: 14px; border: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;" class="container590">
                    <tbody style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative;">
<tr>
                    <td class="hide" height="45" style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; font-size: 45px; line-height: 45px;"> </td>
                                                </tr>
                    <tr>
                    <td height="15" style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; font-size: 15px; line-height: 15px;"> </td>
                                                </tr>
                    <tr>
                    <td style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative;">
                                                        <table border="0" align="right" cellpadding="0" cellspacing="0" style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; font-size: 14px; border: 0;"><tbody style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative;"><tr>
                    <td style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative;">
                                                                    <a target="_blank" rel="noopener noreferrer" href="#" style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; color: #3869d4; display: block; border-style: none !important; border: 0 !important;"><img width="24" border="0" style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; max-width: 100%; border: none; display: block;" src="http://i.imgur.com/Qc3zTxn.png" alt=""></a>
                                                                </td>
                                                                <td style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative;">    </td>
                                                                <td style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative;">
                                                                    <a target="_blank" rel="noopener noreferrer" href="#" style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; color: #3869d4; display: block; border-style: none !important; border: 0 !important;"><img width="24" border="0" style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; max-width: 100%; border: none; display: block;" src="http://i.imgur.com/RBRORq1.png" alt=""></a>
                                                                </td>
                                                                <td style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative;">    </td>
                                                                <td style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative;">
                                                                    <a target="_blank" rel="noopener noreferrer" href="#" style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; color: #3869d4; display: block; border-style: none !important; border: 0 !important;"><img width="24" border="0" style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; max-width: 100%; border: none; display: block;" src="http://i.imgur.com/Wji3af6.png" alt=""></a>
                                                                </td>
                                                            </tr></tbody></table>
                    </td>
                                                </tr>
                    </tbody>
</table>
                    </td>
                                    </tr></tbody></table>
                    </td>
                </tr>
            </table>

        </td>
    </tr>

</table>
<!-- end section -->
<script src="{{ asset('frontend/js/libs/jquery.modernizr.js') }}"></script>
  <script src="{{ asset('frontend/js/libs/jquery-2.2.4.min.js') }}"></script>
  <script src="{{ asset('frontend/js/libs/jquery-ui.min.js') }}"></script>
  <script src="{{ asset('frontend/js/libs/retina.min.js') }}"></script>
  <script src="{{ asset('frontend/plugins/mad.customselect.js') }}"></script>
  <script src="{{ asset('frontend/plugins/sticky-sidebar.js') }}"></script>
  <script src="https://maps.googleapis.com/maps/api/js?libraries=places&amp;key=AIzaSyBN4XjYeIQbUspEkxCV2dhVPSoScBkIoic"></script>
  <script src="{{ asset('frontend/plugins/owl.carousel.min.js') }}"></script>
  <script src="{{ asset('frontend/plugins/fancybox/jquery.fancybox.min.js') }}"></script>
  <script src="{{ asset('frontend/plugins/instafeed.min.js') }}"></script>
  <script src="{{ asset('frontend/plugins/twitter/jquery.tweet.js') }}"></script>

  <!-- JS theme files
  ============================================ -->
  <script src="{{ asset('frontend/js/plugins.js') }}"></script>
  <script src="{{ asset('frontend/js/script.js') }}"></script>
  @stack('scripts')
</body>

</html>
