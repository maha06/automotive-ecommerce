<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <title>Report</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        {{-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"> --}}

        <!-- jQuery and JS bundle w/ Popper.js -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

        <!-- Styles -->
        <style>

            .border-container{
                border: 2px solid #000;
                padding:5px;
            }
            /* .border-title{
                height:15vh;
            }
            .border-title .title{
                text-align: center;
                position: relative;
                top: 25%;
            } */
            .fa-check{
                color:green;
            }
            .fa-times{
                color:red;
            }

            .border-top-section{
                border-top: 2px solid #000;
                margin-top:10px;
            }
            .border-under-title{
                border: 2px solid #000;
                margin-top:10px;
            }
            .clearfix {
                overflow: auto;
                }
            .clearfix::after {
                content: "";
                clear: both;
                display: table;
            }
            table, th, td {
                border: 1px solid black;
                border-collapse: collapse;
            }
            th { white-space:pre-line }
            td.check{
                padding-left:25px;
            }
        </style>
    </head>
    <body style="padding:10px 20px;">
        <h1>Hello, Nabeel Amjad</h1>
    </body>
</html>
