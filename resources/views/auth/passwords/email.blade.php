@extends('layouts.hero-section')

@section('content')
<div id="content">

    <!-- - - - - - - - - - - - - - Breadcrumbs - - - - - - - - - - - - - - - - -->

    <div class="breadcrumbs-wrap"  style="background:#616161">

      <div class="container">

        <h2 class="page-title">Reset Password</h2>

        <ul class="breadcrumbs">

          <li><a href="{{ route('pages.home') }}">Home</a></li>
          <li>Reset Password</li>

        </ul>

      </div>

    </div>
<div class="media-holder type-2" data-bg="{{ asset('frontend/images/1920x500_bg1.jpg') }}">

    <div class="container">
        <div style="margin:auto;" class="tabs type-2 style-3 tabs-section clearfix">
            <!--tabs navigation-->
        <ul class="tabs-nav clearfix">
          <li class="">
            <a href="#tab-1">Reset Password Link</a>
          </li>
        </ul>
        <!--tabs content-->
        <div class="tabs-content">
            @include('common.partials.flash')
            <div id="tab-1">
                <form method="POST" action="{{ route('password.email') }}">
                    @csrf
                    <div class="select-area tab-col-2">
                        <div class="select-col">
                            <div class="auto-custom-select">
                            <input type="email" name="email" placeholder="Email">
                            </div>
                        </div>
                    </div>
                    <div class="select-area tab-col-3">
                        <div class="select-col">
                            <input type="submit" value="Send Password Reset Link" class="btn btn-style-3" />
                        </div>
                    </div>
            </form>
        </div>
      </div>

    </div>

  </div>

</div>
{{-- <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Reset Password') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form method="POST" action="{{ route('password.email') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Send Password Reset Link') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div> --}}
@endsection
