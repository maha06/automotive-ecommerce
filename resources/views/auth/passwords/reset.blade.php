@extends('layouts.hero-section')

@section('content')
<div id="content">

    <!-- - - - - - - - - - - - - - Breadcrumbs - - - - - - - - - - - - - - - - -->

    <div class="breadcrumbs-wrap"  style="background:#616161">

      <div class="container">

        <h2 class="page-title">New Password</h2>

        <ul class="breadcrumbs">

          <li><a href="{{ route('pages.home') }}">Home</a></li>
          <li>New Password</li>

        </ul>

      </div>

    </div>
<div class="media-holder type-2" data-bg="{{ asset('frontend/images/1920x500_bg1.jpg') }}">

    <div class="container">
        <div style="margin:auto;" class="tabs type-2 style-3 tabs-section clearfix">
            <!--tabs navigation-->
        <ul class="tabs-nav clearfix">
          <li class="">
            <a href="#tab-1">New Password</a>
          </li>
        </ul>
        <!--tabs content-->
        <div class="tabs-content">
            @include('common.partials.flash')
            <div id="tab-1">
                <form method="POST" action="{{ route('password.update') }}">
                    @csrf
                    <div class="select-area tab-col-2">
                        <div class="select-col">
                            <div class="auto-custom-select">
                            <input type="email" name="email" placeholder="Email">
                            </div>
                        </div>
                        <div class="select-col">
                            <div class="auto-custom-select">
                            <input type="password" name="password" placeholder="Password">
                            </div>
                        </div>
                        <div class="select-col">
                            <div class="auto-custom-select">
                            <input type="password" name="password_confirmation" placeholder="Password Confirm">
                            </div>
                        </div>
                    </div>
                    <div class="select-area tab-col-3">
                        <div class="select-col">
                            <input type="submit" value="Reset" class="btn btn-style-3" />
                        </div>
                    </div>
            </form>
        </div>
      </div>

    </div>

  </div>

</div>
{{-- <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Reset Password') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('password.update') }}">
                        @csrf

                        <input type="hidden" name="token" value="{{ $token }}">

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Reset Password') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div> --}}
@endsection
