@extends('layouts.hero-section')

@section('content')
<div id="content">

    <!-- - - - - - - - - - - - - - Breadcrumbs - - - - - - - - - - - - - - - - -->

    <div class="breadcrumbs-wrap" style="background:#616161">

      <div class="container">

        <h2 class="page-title">Login</h2>

        <ul class="breadcrumbs">

          <li><a href="{{ route('pages.home') }}">Home</a></li>
          <li>Login</li>

        </ul>

      </div>

    </div>
<div class="media-holder type-2" data-bg="{{ asset('frontend/images/1920x500_bg1.jpg') }}">

    <div class="container">
        <div style="margin:auto;" class="tabs type-2 style-3 tabs-section clearfix">
            <!--tabs navigation-->
        <ul class="tabs-nav clearfix">
          <li class="">
            <a href="#tab-1">Login</a>
          </li>
          <li class="">
            <a href="#tab-2">Register</a>
          </li>
          <li class="">
            <a href="#tab-3">Register Workshop</a>
          </li>
        </ul>
        <!--tabs content-->
        <div class="tabs-content">
            @include('common.partials.flash')
            <div id="tab-1">
                <form method="POST" action="{{ route('login') }}">
                    @csrf
                    <div class="select-area tab-col-2">
                        <div class="select-col">
                            <div class="auto-custom-select">
                            <input type="email" name="email" placeholder="Email">
                            </div>
                        </div>
                        <div class="select-col">
                            <div class="auto-custom-select">
                            <input type="password" name="password" placeholder="Password">
                            </div>
                        </div>
                    </div>
                    <div class="select-area tab-col-3">
                        <div class="search-col forgot">
                            @if (Route::has('password.request'))
                            <a class="forgot-password" href="{{ route('password.request') }}">
                                {{ __('Forgot Your Password?') }}
                            </a>
                            @endif
                        </div>
                        <div class="select-col">
                            <input type="submit" value="Login" class="btn btn-style-3" />
                        </div>
                    </div>
            </form>
        </div>
        <div id="tab-2">
            <form method="POST" action="{{ route('register') }}">
            @csrf
                <div class="select-area tab-col-2">
                    <div class="select-col">
                        <div class="auto-custom-select">
                        <input type="text" name="name" placeholder="Name" required>
                        </div>
                    </div>
                    <div class="select-col">
                        <div class="auto-custom-select">
                        <input type="text" name="username" class="@error('username') border-danger @enderror" placeholder="Username" required>
                        </div>
                    </div>
                    <div class="select-col">
                        <div class="auto-custom-select">
                            <input type="email" class="@error('email') border-danger @enderror"  name="email" placeholder="Email" required>
                        </div>
                    </div>
                    <div class="select-col">
                        <div class="auto-custom-select">
                        <input type="tel" name="phone" placeholder="Phone" required>
                        </div>
                    </div>
                    <div class="select-col">
                        <div class="auto-custom-select">
                            <input type="password" class="@error('password') border-danger @enderror"  name="password" placeholder="Password" required>
                        </div>
                    </div>
                    <div class="select-col">
                        <div class="auto-custom-select">
                            <input type="password" class="@error('password_confirmation') border-danger @enderror"  name="password_confirmation" placeholder="Confirm Password" required>
                        </div>
                    </div>
                </div>
                <div class="select-area tab-col-3">
                    <div class="select-col">
                        <input type="submit" value="Register" class="btn btn-style-3" />
                    </div>
                </div>
            </form>
        </div>
        <div id="tab-3">
            <form method="POST" action="{{ route('workshop.register') }}"  enctype="multipart/form-data">
                @csrf
                <div class="select-area tab-col-2">
                    <div class="select-col">
                        <div class="auto-custom-select">
                        <input type="text" name="name" placeholder="Workshop Name" required>
                        </div>
                    </div>
                    <div class="select-col">
                        <div class="auto-custom-select">
                            <input type="text" name="address" placeholder="Area of Workshop" required>
                        </div>
                    </div>
                    <div class="select-col">
                        <div class="auto-custom-select">
                            <input type="text" name="location" placeholder="Location" required>
                        </div>
                    </div>
                    <div class="select-col">
                    <div class="auto-custom-select">
                        <input type="email" name="email" placeholder="Email" required>
                    </div>
                    </div>
                    <div class="select-col">
                        <div class="auto-custom-select">
                        <input type="tel" name="phone" placeholder="Phone" required>
                        </div>
                    </div>
                    <div class="select-col">
                        <div class="auto-custom-select">
                            <label><strong>Workshop Image :</strong></label><br>
                            <input type="file" name="image[]" required  multiple>
                        </div>
                    </div>
                    <div class="select-col">
                        <div class="auto-custom-select">
                        <input type="int" min="1" name="no_of_mechanics" placeholder="Number of Mechanics" required>
                        </div>
                    </div>
                    <div class="select-col">
                        <div class="auto-custom-select">
                        <input type="int" min="1" name="no_of_electrician" placeholder="Number of Electrician" required>
                        </div>
                    </div>
                    <div class="select-col">
                        <div class="auto-custom-select">
                        <input type="int" min="1" name="no_of_denter" placeholder="Number of Denters" required>
                        </div>
                    </div>
                    <div class="select-col">
                        <div class="auto-custom-select">
                        <input type="int" min="1" name="no_of_painter" placeholder="Number of Painter" required>
                        </div>
                    </div>
                    <!-- <div class="select-col">
                        <div class="auto-custom-select">
                            <label><strong>Services :</strong></label>
                            <div class="a-content">
                                <div class="input-wrapper">
                                    @foreach ($services as $service)
                                        <input type="checkbox" name="service_id[]" id="checkbox-{{ $service->id }}" value="{{ $service->id }}">
                                        <label for="checkbox-{{ $service->id }}">{{ $service->name }}</label>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div> -->
                    <div class="select-col">
                        <div class="auto-custom-select">
                            <textarea class="ckeditor" name="description" ></textarea>
                        </div>
                    </div>
                    <div class="select-col">
                        <div class="auto-custom-select">
                        <input type="int" min="1" name="no_of_bays" placeholder="Number of Bays" required>
                        </div>
                    </div>
                    <div class="select-col">
                        <div class="auto-custom-select">
                        <textarea name="major_tools_and_equipments" placeholder="Major Tools And Equipments" required></textarea>
                        </div>
                    </div>
                    <div class="select-col">
                    <div class="auto-custom-select">
                        <input type="password" name="password" placeholder="Password">
                    </div>
                    </div>
                    <div class="select-col">
                        <div class="auto-custom-select">
                            <input type="password" name="password_confirmation" placeholder="Confirm Password" required>
                        </div>
                    </div>
                </div>
                <div class="select-area tab-col-3">
                    <div class="select-col">
                        <input type="submit" value="Register" class="btn btn-style-3" />
                    </div>
                </div>
            </form>
          </div>
        </div>
      </div>

    </div>

  </div>

</div>
  {{-- <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Login') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Login') }}
                                </button>

                                @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div> --}}

<script src="//cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('.ckeditor').ckeditor();
    });
</script>
@endsection
