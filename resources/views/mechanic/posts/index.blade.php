@extends('mechanic.layouts.layout')

@section('heading')
    <h4><a href="{{ route('posts.index') }}"><i class="icon-arrow-left52 mr-2"></i></a> <span
            class="font-weight-semibold">Home - Posts</span> - View All</h4>
@endsection

@section('breadcrumbs')
    <div class="breadcrumb">
        <a href="{{ route('posts.index') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
        <span class="breadcrumb-item">Posts</span>
        <span class="breadcrumb-item active">View All</span>
    </div>
@endsection

@section('content')
   @include('admin.partials.header')
    <div class="content">
        @include('common.partials.flash')
        <div class="card has-table">
            <div class="card-header header-elements-inline">
                <h5 class="card-title">All Posts</h5>
                <div class="header-elements">
                    <a href="{{ route('posts.create') }}" class="mt-2 btn btn-primary">
                        Create New Post
                    </a>
                </div>
            </div>
            <table class="table datatable-basic">
                <thead>
                <tr>
                    <th></th>
                    <th>Name</th>
                    <th>Company</th>
                    <th>Price</th>
                    <th class="text-center">Actions</th>
                </tr>
                </thead>
                <tbody>
                @forelse ($posts as $post)
                    <tr>
                        <td>
                            <img style="width:150px;" alt="{{ $post->name }}" src={{ asset('/storage/' . explode(',', $post->image)[0])}} />
                        </td>
                        <td>{{ $post->name }}</td>
                        <td>{{ $post->company }}</td>
                        <td>{{ $post->price }}</td>
                        <td class="text-center">
                            <div class="list-icons">
                                <div class="dropdown">
                                    <a href="#" class="list-icons-item" data-toggle="dropdown">
                                        <i class="icon-menu9"></i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        <a href="{{ route('posts.edit', $post->id) }}"
                                           class="dropdown-item"><i class="icon-pencil5"></i> Edit
                                        </a>
                                        <a href="#" class="dropdown-item"
                                           onclick="event.preventDefault(); if(confirm('Are you sure you want to perform this action?')){document.getElementById('delete-post-{{ $post->id }}-form').submit();}">
                                            <i class="icon-trash"></i>
                                            Delete
                                        </a>
                                        <form action="{{ route('posts.destroy', $post->id) }}"
                                              id="delete-post-{{ $post->id }}-form" method="POST"
                                              style="display: none;">
                                            @csrf
                                            @method('DELETE')
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="7">
                            <div class="alert alert-info text-center">
                                No Posts Added So Far
                                <br>
                                <a href="{{ route('posts.create') }}" class="mt-2 btn btn-primary">
                                    Create New Post
                                </a>
                            </div>
                        </td>
                    </tr>
                @endforelse
                </tbody>
            </table>
            {!! $posts->links() !!}
        </div>
    </div>
@endsection
