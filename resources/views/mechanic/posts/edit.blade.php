@extends('mechanic.layouts.layout')

@section('heading')
    <h4>
        <a href="{{ route('admin.pages.home') }}">
            <i class="icon-arrow-left52 mr-2"></i>
        </a>
        <span class="font-weight-semibold">Home - Post</span>
    </h4>
@endsection

@section('breadcrumbs')
    <div class="breadcrumb">
        <a href="{{ route('posts.index') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
        <span class="breadcrumb-item">Post: {{ $post->name }}</span>
        <span class="breadcrumb-item active">Edit</span>
    </div>
@endsection
@section('content')
    @include('admin.partials.header')
    <div class="content">
        @include('common.partials.flash')
        <div class="card">
            <div class="card-header header-elements-inline">
                <h5 class="card-title">Post</h5>
            </div>
            <div class="card-body">
                <form action="{{ route('posts.update', $post->id) }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <div class="form-group">
                        <label class="font-weight-semibold @error('name') text-danger @enderror">Name <span
                                class="text-red">*</span></label>
                        <div class="form-group-feedback form-group-feedback-right">
                            <input type="text" name="name" value="{{ $post->name }}"
                                class="form-control @error('name') border-danger @enderror" required>
                            @error('name')
                            <div class="form-control-feedback text-danger">
                                <i class="icon-cancel-circle2"></i>
                            </div>
                            @enderror
                        </div>
                        @error('name')
                        <span class="form-text text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label class="font-weight-semibold @error('company') text-danger @enderror">Company <span
                                class="text-red">*</span></label>
                        <div class="form-group-feedback form-group-feedback-right">
                            <input type="text" name="company" value="{{ $post->company }}"
                                class="form-control @error('company') border-danger @enderror" required>
                            @error('company')
                            <div class="form-control-feedback text-danger">
                                <i class="icon-cancel-circle2"></i>
                            </div>
                            @enderror
                        </div>
                        @error('company')
                        <span class="form-text text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label class="font-weight-semibold @error('price') text-danger @enderror">Price <span
                                class="text-red">*</span></label>
                        <div class="form-group-feedback form-group-feedback-right">
                            <input type="number" name="price" value="{{ $post->price }}"
                                class="form-control @error('price') border-danger @enderror" required>
                            @error('price')
                            <div class="form-control-feedback text-danger">
                                <i class="icon-cancel-circle2"></i>
                            </div>
                            @enderror
                        </div>
                        @error('price')
                        <span class="form-text text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <input type="hidden" name="mechanic_id" value="{{ auth()->user()->id }}"/>
                    <div class="form-group">
                        <label class="font-weight-semibold @error('description') text-danger @enderror">Description <span
                                class="text-red">*</span></label>
                        <div class="form-group-feedback form-group-feedback-right">
                            <textarea name="description" class="form-control tinymce @error('description') border-danger @enderror">{{ $post->description }}</textarea>
                            @error('description')
                            <div class="form-control-feedback text-danger">
                                <i class="icon-cancel-circle2"></i>
                            </div>
                            @enderror
                        </div>
                        @error('description')
                        <span class="form-text text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label style="display: block;"
                            class="font-weight-semibold @error('image') text-danger @enderror">Image <span
                                class="text-red">*</span></label>
                        <div class="d-inline-block" style="position: relative;margin-bottom: inherit;">
                        @if($post->image)
                            @foreach(explode(',' , $post->image) as $image)
                                <div class="d-inline-block" style="position:relative;margin-bottom: inherit; ">
                                    <img src="{{ asset('/storage/'.$image) }}" height="100px" width="300px"/>
                                    <a href="{{route('post.delete',[$post->id,$image])}}" class="btn btn-danger text-white"
                                        onclick="return confirm('Are you sure you want to delete this image?')"
                                        style="position: absolute; top: 0; right: 0;"> &times; </a>
                                </div>
                            @endforeach
                        @else
                            <p>No Logo Found</p>
                        @endif
                        <input type="file" accept=".jpeg,.png,.bmp,.gif,.svg,.webp,.jpg" name="image[]" multiple>
                        <span class="form-text text-muted">Accepted formats: jpeg, png, bmp, gif, svg, jpg, or webp</span>
                        @error('image')
                        <span class="form-text text-danger">{{ $message }}</span>
                        @enderror
                        </div>
                    </div>
                    <div class="text-right">
                        <button type="submit" class="btn btn-primary">Submit <i class="icon-paperplane ml-2"></i></button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
