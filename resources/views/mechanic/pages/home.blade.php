@extends('mechanic.layouts.layout')

@section('heading')
    <h4><span class="font-weight-semibold">Home</span> - Dashboard</h4>
@endsection

@section('breadcrumbs')
    <div class="breadcrumb">
        <a href="{{ route('mechanic.index') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
        <span class="breadcrumb-item active">Dashboard</span>
    </div>
@endsection

@section('content')
@include('admin.partials.header')
<!-- Content area -->
<div class="content">
    @include('common.partials.flash')
    <!-- Dashboard content -->
    <div class="row">
        <div class="col-xl-12">
            <!-- Stats boxes -->
            <div class="row">
                @if(Auth::guard('mechanic-user')->check())
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
                    <div class="card bg-teal-400">
                        <a href="{{ route('mechanic.users') }}" class="text-white">
                            <div class="text-center card-body">
                                <div>
                                    <h1 class="font-weight-semibold mb-0"><i class="icon-3x mr-1 icon-users"></i>
                                    </h1>
                                </div>
                                <div class="pt-2">
                                    <h1 class="font-weight-semibold mb-0">Users</h1>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
                    <div class="card bg-teal-400">
                        <a href="{{ route('posts.index') }}" class="text-white">
                            <div class="text-center card-body">
                                <div>
                                    <h1 class="font-weight-semibold mb-0"><i class="icon-3x mr-1 icon-stack"></i>
                                    </h1>
                                </div>
                                <div class="pt-2">
                                    <h1 class="font-weight-semibold mb-0">Posts</h1>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                @endif
        </div>
    </div>
</div>
@endsection
