@extends('mechanic.layouts.layout')

@section('heading')
    <h4><a href="{{ route('users.index') }}"><i class="icon-arrow-left52 mr-2"></i></a> <span
            class="font-weight-semibold">Home - Users</span> - View All</h4>
@endsection

@section('breadcrumbs')
    <div class="breadcrumb">
        <a href="{{ route('users.index') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
        <span class="breadcrumb-item">Users</span>
        <span class="breadcrumb-item active">View All</span>
    </div>
@endsection

@section('content')
   @include('admin.partials.header')
    <div class="content">
        @include('common.partials.flash')
        <div class="card has-table">
            <div class="card-header header-elements-inline">
                <h5 class="card-title">All Users</h5>
            </div>

            <table class="table datatable-basic">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>User Name</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>Status</th>
                </tr>
                </thead>
                <tbody>
                @forelse ($users as $user)
                    <tr>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->username }}</td>
                        <td>{{ $user->email }}</td>
                        <td>{{ $user->phone }}</td>
                        <td>
                           {{ $user->status == 1 ? 'Active' : 'In Active'}}
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="7">
                            <div class="alert alert-info text-center">
                                No Users Added So Far
                            </div>
                        </td>
                    </tr>
                @endforelse
                </tbody>
            </table>
            {!! $users->links() !!}
        </div>
    </div>
@endsection
